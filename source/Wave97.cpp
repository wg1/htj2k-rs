/*
Copyright (c) 2000-2021, ICT-Link
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the HTJ2K project.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ImageUtil.h"
#include "j2k2.h"
#include "wavelet_codec.h"
#include "annexE.h"

void Dec_H97_2(struct Image_s *image) {
	byte4 width, height, xee;
	byte4 LoXs, HiXs;
	byte4 j;
	byte4 col1stepm, row1stepm;
	float kappa, delta, ganmma, beta, alpha;
	float *d0_TS, *d0_;

	kappa     = (float) KAPPA;
	delta     = (float) DELTA;
	ganmma    = (float) GANMMA;
	beta      = (float) BETA;
	alpha     = (float) ALPHA;
	width     = image->tbx1 - image->tbx0;
	height    = image->tby1 - image->tby0;
	col1stepm = image->col1step;
	row1stepm = image->row1step;

	if (image->tbx0 & 1) {
		LoXs = 1; // odd
		HiXs = 2;
	} else {
		LoXs = 2; // even
		HiXs = 1;
	}
	xee   = 1;
	d0_TS = (float *) image->data;
	for (j = 0; j < height; j++, d0_TS += col1stepm) {
		// step1		LoPass
		d0_ = d0_TS;
		if (!(image->tbx0 & 1))
			d0_[0] *= kappa;
		else
			d0_[1] *= kappa;

		// step2		HiPass
		d0_ = d0_TS;
		if (!(image->tbx0 & 1))
			d0_[1] /= kappa;
		else
			d0_[0] /= kappa;

		// step3		LoPass
		d0_ = d0_TS;
		if (!(image->tbx0 & 1)) // even start
			d0_[0] -= (d0_[1] * delta * 2);
		else
			d0_[1] -= (d0_[0] * delta * 2);

		// step4		HiPass
		d0_ = d0_TS;
		if (!(image->tbx0 & 1))
			d0_[1] -= (d0_[0] * ganmma * 2);
		else
			d0_[0] -= (d0_[1] * ganmma * 2);

		// step5		LoPass
		d0_ = d0_TS;
		if (!(image->tbx0 & 1))
			d0_[0] += (d0_[1] * beta * 2);
		else
			d0_[1] += (d0_[0] * beta * 2);

		// step6		HiPass
		d0_ = d0_TS;
		if (image->tbx0 & 1)
			d0_[0] += (d0_[1] * alpha * 2);
		else
			d0_[1] += (d0_[0] * alpha * 2);
	}
}

#if Cplus
void Enc_H97_3(struct Image_s *image) {
	byte4 width, height;
	byte4 LoXs, HiXs;
	byte4 j, i;
	byte4 col1step, row1step;
	float kappa, delta, ganmma, beta, alpha;
	float *d0_TS, *d0;

	kappa    = (float) KAPPA;
	delta    = (float) DELTA;
	ganmma   = (float) GANMMA;
	beta     = (float) BETA;
	alpha    = (float) ALPHA;
	width    = image->tbx1 - image->tbx0;
	height   = image->tby1 - image->tby0;
	col1step = image->col1step;
	row1step = image->row1step;
	if (image->tbx1 & 1) {
		LoXs = 1; // odd
		HiXs = 0;
	} else {
		LoXs = 0; // even
		HiXs = 1;
	}

	d0_TS = (float *) image->data;
	for (j = 0; j < height; j++, d0_TS = &d0_TS[col1step]) {
		// step1	HiPass
		d0 = d0_TS;
		i  = !(image->tbx0 & 1);
		d0 = &d0[i];
		if (!i) { // Left-extention only in tbx0 is odd.
			d0[0] -= (d0[1] * (float) 2.0 * alpha);
			d0 = &d0[2];
			i += 2;
		}
		for (; i < width - 1 - HiXs; i += 2, d0 = &d0[2]) // i is always even. (refer xs)
			d0[0] -= ((d0[-1] + d0[1]) * alpha);
		if (!(image->tbx1 & 1)) d0[0] -= (d0[-1] * 2 * alpha);

		// step2	LoPass
		d0 = d0_TS;
		i  = (image->tbx0 & 1);
		d0 = &d0[i];
		if (!(image->tbx0 & 1)) {
			d0[0] -= (d0[1] * 2 * beta);
			d0 = &d0[2];
			i += 2;
		}
		for (; i < width - LoXs; i += 2, d0 = &d0[2])
			d0[0] -= ((d0[-1] + d0[1]) * beta);
		if (image->tbx1 & 1) d0[0] -= (d0[-1] * 2 * beta);

		// step3	HiPass
		d0 = d0_TS;
		i  = !(image->tbx0 & 1);
		d0 = &d0[i];
		if (!i) {
			d0[0] += (d0[1] * 2 * ganmma);
			d0 = &d0[2];
			i += 2;
		}
		for (; i < width - HiXs; i += 2, d0 = &d0[2]) // i is always even. (refer xs)
			d0[0] += ((d0[-1] + d0[1]) * ganmma);
		if (!(image->tbx1 & 1)) d0[0] += (d0[-1] * 2 * ganmma);

		// step4	LoPass
		d0 = d0_TS;
		i  = (image->tbx0 & 1);
		d0 = &d0[i];
		if (!i) {
			d0[0] += (d0[1] * 2 * delta);
			d0 = &d0[2];
			i += 2;
		}
		for (; i < width - LoXs; i += 2, d0 = &d0[2])
			d0[0] += ((d0[-1] + d0[1]) * delta);
		if (image->tbx1 & 1) d0[0] += (d0[-1] * 2 * delta);

		// step5	HiPass
		d0 = d0_TS;
		i  = !(image->tbx0 & 1);
		d0 = &d0[i];
		for (; i < width; i += 2, d0 = &d0[2])
			d0[0] *= kappa;

		// step6	LoPass
		d0 = d0_TS;
		i  = (image->tbx0 & 1);
		d0 = &d0[i];
		for (; i < width; i += 2, d0 = &d0[2])
			d0[0] /= kappa;
	}
}

void Dec_H97_3(struct Image_s *image) {
	byte4 width, height;
	byte4 LoXs, HiXs;
	byte4 j, i;
	byte4 col1stepm, row1stepm;
	float kappa, delta, ganmma, beta, alpha;
	float *d0_TS, *d0_;

	kappa     = (float) KAPPA;
	delta     = (float) DELTA;
	ganmma    = (float) GANMMA;
	beta      = (float) BETA;
	alpha     = (float) ALPHA;
	width     = image->tbx1 - image->tbx0;
	height    = image->tby1 - image->tby0;
	col1stepm = image->col1step;
	row1stepm = image->row1step;

	if (image->tbx1 & 1) {
		LoXs = 1; // odd
		HiXs = 0;
	} else {
		LoXs = 0; // even
		HiXs = 1;
	}

	d0_TS = (float *) image->data;
	for (j = 0; j < height; j++, d0_TS += col1stepm) {
		// step1		LoPass
		d0_ = d0_TS;
		i   = (image->tbx0 & 1);
		d0_ = &d0_[i];
		for (; i < width; i += 2, d0_ = &d0_[2])
			d0_[0] *= kappa;

		// step2		HiPass
		d0_ = d0_TS;
		i   = !(image->tbx0 & 1);
		d0_ = &d0_[i];
		for (; i < width; i += 2, d0_ = &d0_[2])
			d0_[0] /= kappa;

		// step3		LoPass
		d0_ = d0_TS;
		i   = (image->tbx0 & 1);
		d0_ = &d0_[i];
		if (!(image->tbx0 & 1)) { // even start
			d0_[0] -= (d0_[1] * delta * 2);
			d0_ = &d0_[2];
			i += 2;
		}
		for (; i < (width - LoXs); i += 2, d0_ = &d0_[2])
			d0_[0] -= ((d0_[-1] + d0_[1]) * delta);
		if (image->tbx1 & 1) d0_[0] -= (d0_[-1] * delta * 2);

		// step4		HiPass
		d0_ = d0_TS;
		i   = !(image->tbx0 & 1);
		d0_ = &d0_[i];
		if (!i) {
			d0_[0] -= (d0_[1] * ganmma * 2);
			d0_ = &d0_[2];
			i += 2;
		}
		for (; i < width - HiXs; i += 2, d0_ = &d0_[2])
			d0_[0] -= ((d0_[-1] + d0_[1]) * ganmma);
		if (!(image->tbx1 & 1)) d0_[0] -= (d0_[-1] * ganmma * 2);

		// step5		LoPass
		d0_ = d0_TS;
		i   = (image->tbx0 & 1);
		d0_ = &d0_[i];
		if (!(image->tbx0 & 1)) {
			d0_[0] += (d0_[1] * beta * 2);
			d0_ = &d0_[2];
			i += 2;
		}
		for (; i < width - LoXs; i += 2, d0_ = &d0_[2])
			d0_[0] += ((d0_[-1] + d0_[1]) * beta);
		if (image->tbx1 & 1) d0_[0] += (d0_[-1] * beta * 2);

		// step6		HiPass
		d0_ = d0_TS;
		i   = !(image->tbx0 & 1);
		d0_ = &d0_[i];
		if (!i) {
			d0_[0] += (d0_[1] * alpha * 2);
			d0_ = &d0_[2];
			i += 2;
		}
		for (; i < width - HiXs; i += 2, d0_ = &d0_[2])
			d0_[0] += ((d0_[-1] + d0_[1]) * alpha);
		if (!(image->tbx1 & 1)) d0_[0] += (d0_[-1] * alpha * 2);
	}
}
#endif

#if Cplus
void Enc_H97(struct Image_s *image) {
	byte4 height, width;
	byte4 j;
	byte4 col1step, row1step;
	float *d0;

	width    = image->tbx1 - image->tbx0;
	height   = image->tby1 - image->tby0;
	col1step = image->col1step;
	row1step = image->row1step;

	if (width == 0) {        // nothing to do when width is 0
	} else if (width == 1) { // When width is 1, bypass in lowpass and multipul2 in hypass.
		if (image->tbx0 % 2 != 0) {
			d0 = (float *) &image->data[height];
			d0 = (float *) &image->data[0];
			for (j = 0; j < height; j++, d0 += col1step)
				*d0 *= 2.0;
		}
	} else if (width == 2)
		Enc_H97_3(image);
	else
		Enc_H97_3(image);
}
#endif

void Dec_H97(struct Image_s *image) {
	byte4 width, height;
	byte4 col1step;
	float *d_, *d_e;

	width    = image->tbx1 - image->tbx0;
	height   = image->tby1 - image->tby0;
	col1step = image->col1step;

	if (width == 0) {
	} else if (width == 1) {
		if (image->tbx0 % 2) {
			d_e = (float *) &image->data[height * col1step];
			d_  = (float *) &image->data[0];
			for (; d_ != d_e; d_ += col1step)
				*d_ = (float) (*d_ / 2);
		}
	} else if (width == 2)
		Dec_H97_2(image);
	else
		Dec_H97_3(image);
}

void Enc_V97_2(struct Image_s *image) {
	byte4 width, height;
	byte4 j, i;
	byte4 col1step, col2step, row1step;
	float kappa, delta, ganmma, beta, alpha;
	float *d0_TS, *d0;

	kappa    = (float) KAPPA;
	delta    = (float) DELTA;
	ganmma   = (float) GANMMA;
	beta     = (float) BETA;
	alpha    = (float) ALPHA;
	width    = image->tbx1 - image->tbx0;
	height   = image->tby1 - image->tby0;
	col1step = image->col1step;
	col2step = col1step << 1;
	row1step = image->row1step;

	d0_TS = (float *) image->data;
	for (i = 0; i < width; i++, d0_TS = &d0_TS[row1step]) {
		// step1	HiPass
		d0 = d0_TS;
		j  = !(image->tby0 & 1);
		d0 = &d0[j * col1step];
		if (!j) { // Left-extention only in tbx0 is odd.
			d0[0] -= (d0[col1step] * (float) 2.0 * alpha);
			d0 = &d0[col2step];
			j += 2;
		}
		for (; j < height - 1 /*-HiYs*/; j += 2, d0 = &d0[col2step]) // i is always even. (refer xs)
			d0[0] -= ((d0[-col1step] + d0[col1step]) * alpha);
		if (!(image->tby1 & 1)) d0[0] -= (d0[-col1step] * 2 * alpha);

		// step2	LoPass
		d0 = d0_TS;
		j  = (image->tby0 & 1);
		d0 = &d0[j * col1step];
		if (!j) {
			d0[0] -= (d0[col1step] * 2 * beta);
			d0 = &d0[col2step];
			j += 2;
		}
		for (; j < height /*-LoYs*/; j += 2, d0 = &d0[col2step])
			d0[0] -= ((d0[-col1step] + d0[col1step]) * beta);
		if (image->tby1 & 1) d0[0] -= (d0[-col1step] * 2 * beta);

		// step3	HiPass
		d0 = d0_TS;
		j  = !(image->tby0 & 1);
		d0 = &d0[j * col1step];
		if (!j) {
			d0[0] += (d0[col1step] * 2 * ganmma);
			d0 = &d0[col2step];
			j += 2;
		}
		for (; j < height - 1 /*-HiYs*/; j += 2, d0 = &d0[col2step]) // i is always even. (refer xs)
			d0[0] += ((d0[-col1step] + d0[col1step]) * ganmma);
		if (!(image->tby1 & 1)) d0[0] += (d0[-col1step] * 2 * ganmma);

		// step4	LoPass
		d0 = d0_TS;
		j  = (image->tby0 & 1);
		d0 = &d0[j * col1step];
		if (!j) {
			d0[0] += (d0[col1step] * 2 * delta);
			d0 = &d0[col2step];
			j += 2;
		}
		for (; j < height /*-LoYs*/; j += 2, d0 = &d0[col2step])
			d0[0] += ((d0[-col1step] + d0[col1step]) * delta);
		if (image->tby1 & 1) d0[0] += (d0[-col1step] * 2 * delta);

		// step5	HiPass
		d0 = d0_TS;
		j  = !(image->tby0 & 1);
		d0 = &d0[j * col1step];
		for (; j < height; j += 2, d0 = &d0[col2step])
			d0[0] *= kappa;

		// step6	LoPass
		d0 = d0_TS;
		j  = (image->tby0 & 1);
		d0 = &d0[j * col1step];
		for (; j < height; j += 2, d0 = &d0[col2step])
			d0[0] /= kappa;
	}
}

void Dec_V97_2(struct Image_s *image) {
	byte4 width, height;
	byte4 LoXs, HiXs;
	byte4 i;
	byte4 col1stepm, row1stepm;
	float kappa, delta, ganmma, beta, alpha;
	float *d0_TS, *d0_;

	kappa     = (float) KAPPA;
	delta     = (float) DELTA;
	ganmma    = (float) GANMMA;
	beta      = (float) BETA;
	alpha     = (float) ALPHA;
	width     = image->tbx1 - image->tbx0;
	height    = image->tby1 - image->tby0;
	col1stepm = image->col1step;
	row1stepm = image->row1step;

	if (image->tby0 & 1) {
		LoXs = 1; // odd
		HiXs = 2;
	} else {
		LoXs = 2; // even
		HiXs = 1;
	}
	d0_TS = (float *) image->data;
	for (i = 0; i < width; i++, d0_TS += row1stepm) {
		// step1		LoPass
		d0_ = d0_TS;
		if (!(image->tby0 & 1))
			d0_[0] *= kappa;
		else
			d0_[col1stepm] *= kappa;

		// step2		HiPass
		d0_ = d0_TS;
		if (!(image->tby0 & 1))
			d0_[col1stepm] /= kappa;
		else
			d0_[0] /= kappa;

		// step3		LoPass
		d0_ = d0_TS;
		if (!(image->tby0 & 1)) // even start
			d0_[0] -= (d0_[col1stepm] * delta * 2);
		else
			d0_[col1stepm] -= (d0_[0] * delta * 2);

		// step4		HiPass
		d0_ = d0_TS;
		if (!(image->tby0 & 1))
			d0_[col1stepm] -= (d0_[0] * ganmma * 2);
		else
			d0_[0] -= (d0_[col1stepm] * ganmma * 2);

		// step5		LoPass
		d0_ = d0_TS;
		if (!(image->tby0 & 1))
			d0_[0] += (d0_[col1stepm] * beta * 2);
		else
			d0_[col1stepm] += (d0_[0] * beta * 2);

		// step6		HiPass
		d0_ = d0_TS;
		if (image->tby0 & 1)
			d0_[0] += (d0_[col1stepm] * alpha * 2);
		else
			d0_[col1stepm] += (d0_[0] * alpha * 2);
	}
}

void Enc_V97_3(struct Image_s *image) {
	byte4 width, height;
	byte4 LoXs, HiXs;
	byte4 j, i;
	byte4 col1step, col2step, row1step;
	float kappa, delta, ganmma, beta, alpha;
	float *d0_TS, *d0;

	kappa    = (float) KAPPA;
	delta    = (float) DELTA;
	ganmma   = (float) GANMMA;
	beta     = (float) BETA;
	alpha    = (float) ALPHA;
	width    = image->tbx1 - image->tbx0;
	height   = image->tby1 - image->tby0;
	col1step = image->col1step;
	col2step = col1step << 1;
	row1step = image->row1step;

	if (image->tby1 & 1) {
		LoXs = 1; // odd
		HiXs = 0;
	} else {
		LoXs = 0; // even
		HiXs = 1;
	}

	d0_TS = (float *) image->data;
	for (i = 0; i < width; i++, d0_TS = &d0_TS[row1step]) {
		// step1	HiPass
		d0 = d0_TS;
		j  = !(image->tby0 & 1);
		d0 = &d0[j * col1step];
		if (!j) {
			d0[0] -= (d0[col1step] * (float) 2.0 * alpha);
			d0 = &d0[col2step];
			j += 2;
		}
		for (; j < height - HiXs; j += 2, d0 = &d0[col2step])
			d0[0] -= ((d0[-col1step] + d0[col1step]) * alpha);
		if (!(image->tby1 & 1)) d0[0] -= (d0[-col1step] * 2 * alpha);

		// step2	LoPass
		d0 = d0_TS;
		j  = (image->tby0 & 1);
		d0 = &d0[j * col1step];
		if (!j) {
			d0[0] -= (d0[col1step] * 2 * beta);
			d0 = &d0[col2step];
			j += 2;
		}
		for (; j < height - LoXs; j += 2, d0 = &d0[col2step])
			d0[0] -= ((d0[-col1step] + d0[col1step]) * beta);
		if (image->tby1 & 1) d0[0] -= (d0[-col1step] * 2 * beta);

		// step3	HiPass
		d0 = d0_TS;
		j  = !(image->tby0 & 1);
		d0 = &d0[j * col1step];
		if (!j) {
			d0[0] += (d0[col1step] * 2 * ganmma);
			d0 = &d0[col2step];
			j += 2;
		}
		for (; j < height - HiXs; j += 2, d0 = &d0[col2step]) // i is always even. (refer xs)
			d0[0] += ((d0[-col1step] + d0[col1step]) * ganmma);
		if (!(image->tby1 & 1)) d0[0] += (d0[-col1step] * 2 * ganmma);

		// step4	LoPass
		d0 = d0_TS;
		j  = (image->tby0 & 1);
		d0 = &d0[j * col1step];
		if (!j) {
			d0[0] += (d0[col1step] * 2 * delta);
			d0 = &d0[col2step];
			j += 2;
		}
		for (; j < height - LoXs; j += 2, d0 = &d0[col2step])
			d0[0] += ((d0[-col1step] + d0[col1step]) * delta);
		if (image->tby1 & 1) d0[0] += (d0[-col1step] * 2 * delta);

		// step5	HiPass
		d0 = d0_TS;
		j  = !(image->tby0 & 1);
		d0 = &d0[j * col1step];
		for (; j < height; j += 2, d0 = &d0[col2step])
			d0[0] *= kappa;

		// step6	LoPass
		d0 = d0_TS;
		j  = (image->tby0 & 1);
		d0 = &d0[j * col1step];
		for (; j < height; j += 2, d0 = &d0[col2step])
			d0[0] /= kappa;
	}
}

#if Cplus
void Dec_V97_3(struct Image_s *image) {
	byte4 width, height;
	byte4 LoXs, HiXs;
	byte4 j, i;
	byte4 col1stepm, col2stepm, row1stepm;
	float kappa, delta, ganmma, beta, alpha;
	float *d0_TS, *d0_;

	kappa     = (float) KAPPA;
	delta     = (float) DELTA;
	ganmma    = (float) GANMMA;
	beta      = (float) BETA;
	alpha     = (float) ALPHA;
	width     = image->tbx1 - image->tbx0;
	height    = image->tby1 - image->tby0;
	col1stepm = image->col1step;
	col2stepm = col1stepm * 2;
	row1stepm = image->row1step;

	if (image->tby1 & 1) {
		LoXs = 1; // odd
		HiXs = 0;
	} else {
		LoXs = 0; // even
		HiXs = 1;
	}

	d0_TS = (float *) image->data;
	for (i = 0; i < width; i++, d0_TS += row1stepm) {
		// step1		LoPass
		d0_ = d0_TS;
		j   = (image->tby0 & 1);
		d0_ = &d0_[j * col1stepm];
		for (; j < height; j += 2, d0_ = &d0_[col2stepm])
			d0_[0] *= kappa;

		// step2		HiPass
		d0_ = d0_TS;
		j   = !(image->tby0 & 1);
		d0_ = &d0_[j * col1stepm];
		for (; j < height; j += 2, d0_ = &d0_[col2stepm])
			d0_[0] /= kappa;

		// step3		LoPass
		d0_ = d0_TS;
		j   = (image->tby0 & 1);
		d0_ = &d0_[j * col1stepm];
		if (!j) { // even start
			d0_[0] -= (d0_[col1stepm] * delta * 2);
			d0_ = &d0_[col2stepm];
			j += 2;
		}
		for (; j < (height - LoXs); j += 2, d0_ = &d0_[col2stepm])
			d0_[0] -= ((d0_[-col1stepm] + d0_[col1stepm]) * delta);
		if (image->tby1 & 1) d0_[0] -= (d0_[-col1stepm] * delta * 2);

		// step4		HiPass
		d0_ = d0_TS;
		j   = !(image->tby0 & 1);
		d0_ = &d0_[j * col1stepm];
		if (!j) {
			d0_[0] -= (d0_[col1stepm] * ganmma * 2);
			d0_ = &d0_[col2stepm];
			j += 2;
		}
		for (; j < (height - HiXs); j += 2, d0_ = &d0_[col2stepm])
			d0_[0] -= ((d0_[-col1stepm] + d0_[col1stepm]) * ganmma);
		if (!(image->tby1 & 1)) d0_[0] -= (d0_[-col1stepm] * ganmma * 2);

		// step5		LoPass
		d0_ = d0_TS;
		j   = (image->tby0 & 1);
		d0_ = &d0_[j * col1stepm];
		if (!j) {
			d0_[0] += (d0_[col1stepm] * beta * 2);
			d0_ = &d0_[col2stepm];
			j += 2;
		}
		for (; j < (height - LoXs); j += 2, d0_ = &d0_[col2stepm])
			d0_[0] += ((d0_[-col1stepm] + d0_[col1stepm]) * beta);
		if (image->tby1 & 1) d0_[0] += (d0_[-col1stepm] * beta * 2);

		// step6		HiPass
		d0_ = d0_TS;
		j   = !(image->tby0 & 1);
		d0_ = &d0_[j * col1stepm];
		if (!j) {
			d0_[0] += (d0_[col1stepm] * alpha * 2);
			d0_ = &d0_[col2stepm];
			j += 2;
		}
		for (; j < height - HiXs; j += 2, d0_ = &d0_[col2stepm])
			d0_[0] += ((d0_[-col1stepm] + d0_[col1stepm]) * alpha);
		if (!(image->tby1 & 1)) d0_[0] += (d0_[-col1stepm] * alpha * 2);
	}
}
#endif

#if Cplus
void Enc_V97(struct Image_s *image) {
	byte4 height, width;
	float *d0, *d1;

	height = image->tby1 - image->tby0;

	if (height == 0) {
	} else if (height == 1) {
		width = image->tbx1 - image->tbx0;
		if (image->tby0 % 2 != 0) {
			d1 = (float *) &image->data[width];
			for (d0 = (float *) image->data; d0 != d1; ++d0)
				*d0 *= 2.0;
		}
	} else if (height == 2)
		Enc_V97_3(image);
	else
		Enc_V97_3(image);
}
#endif

void Dec_V97(struct Image_s *image) {
	byte4 height, width;
	float *d_, *d_e;

	height = image->tby1 - image->tby0;
	if (height == 0) {
	} else if (height == 1) {
		width = image->tbx1 - image->tbx0;
		if (image->tby0 % 2) {
			d_e = (float *) &image->data[width];
			for (d_ = (float *) image->data; d_ != d_e; ++d_)
				*d_ = (float) (*d_ / 2);
		}
	} else if (height == 2)
		Dec_V97_2(image);
	else
		Dec_V97_3(image);
}

void Enc_Quantize(struct Image_s *image, char e, ubyte2 myu, char Rb) {
	byte4 height, width;
	byte4 i, j;
	byte4 col1step, row1step;
	float *d, *d_TS;
	byte4 *D, *D_TS;
	float delta;
	float tempD0;
	byte4 tempD1;
	ubyte8 tempD2;

	width    = image->tbx1 - image->tbx0;
	height   = image->tby1 - image->tby0;
	col1step = image->col1step;
	row1step = image->row1step;
	// ll
	if ((11 + e - Rb) > 0) {
		switch ((11 + e - Rb)) {
			case 31:
				tempD2 = 0x80000000;
				delta  = (float) (tempD2 / (2048 + myu));
				break; // 20
			case 32:
				tempD2 = 0x100000000;
				delta  = (float) (tempD2 / (2048 + myu));
				break; // 21
			case 33:
				tempD2 = 0x200000000;
				delta  = (float) (tempD2 / (2048 + myu));
				break; // 22
			case 34:
				tempD2 = 0x400000000;
				delta  = (float) (tempD2 / (2048 + myu));
				break; // 23
			case 35:
				tempD2 = 0x800000000;
				delta  = (float) (tempD2 / (2048 + myu));
				break; // 24
			case 36:
				tempD2 = 0x1000000000;
				delta  = (float) (tempD2 / (2048 + myu));
				break; // 25
			case 37:
				tempD2 = 0x2000000000;
				delta  = (float) (tempD2 / (2048 + myu));
				break; // 26
			case 38:
				tempD2 = 0x4000000000;
				delta  = (float) (tempD2 / (2048 + myu));
				break; // 27
			case 39:
				tempD2 = 0x8000000000;
				delta  = (float) (tempD2 / (2048 + myu));
				break; // 28
			case 40:
				tempD2 = 0x10000000000;
				delta  = (float) (tempD2 / (2048 + myu));
				break; // 29
			case 41:
				tempD2 = 0x20000000000;
				delta  = (float) (tempD2 / (2048 + myu));
				break; // 30
			case 42:
				tempD2 = 0x40000000000;
				delta  = (float) (tempD2 / (2048 + myu));
				break; // 31
			case 43:
				tempD2 = 0x80000000000;
				delta  = (float) (tempD2 / (2048 + myu));
				break; // 32
			default:
				delta = (float) (1 << (11 + e - Rb)) / (float) (2048 + myu);
				break;
		}
	} else if ((11 + e - Rb) == 0)
		delta = (float) (1.0 / (float) (2048 + myu));
	else
		delta = (float) (1.0 / ((float) (2048 + myu) * (float) (1 << (Rb - e - 11))));

	d_TS = (float *) image->data;
	D_TS = (byte4 *) image->data;
	for (j = 0; j < height; j++, d_TS += col1step, D_TS += col1step) {
		d = d_TS;
		D = D_TS;
		for (i = 0; i < width; i++) {
			tempD0 = d[i];
			tempD0 *= delta;
			tempD1 = (byte4) tempD0;
			D[i]   = tempD1;
		}
	}
}

void Dec_Wave97_Q(struct Image_s *image, /*struct Image_s *ll_d0,*/ byte4 lll,
                  struct EEQcd_s *EEQcd /*, byte4 T, ubyte2 ccc*/) {
	byte4 height, width;
	byte4 col1step, row1step, row2step;
	float deltaLL, deltaHL, deltaLH, deltaHH, delta00;
	float deltaLL99, deltaHL99, deltaLH99, deltaHH99;
	float tempf;
	ubyte4 temp;
	byte2 e, myu;
	float myu_f;
	char Rb, G;
	byte4 i, j;
	float delta0, delta1;
	float *d0, *d_TS;
	byte4 width2;
	byte4 xs, ys;
	byte4 temp99;
	float tempff;

	width    = image->tbx1 - image->tbx0;
	height   = image->tby1 - image->tby0;
	col1step = image->col1step;
	row1step = image->row1step;
	row2step = row1step * 2;
	G        = EEQcd->G;

	temp    = 1 << 16;
	delta00 = (float) temp;

	// ll
	if (lll == 0) {
		Rb      = EEQcd->Rb[0];
		myu     = EEQcd->myu[0];
		myu_f   = (float) myu;
		myu_f   = myu_f + (float) 2048.0;
		e       = EEQcd->e[0];
		temp    = 1 << (27 - G - Rb);
		tempf   = (float) temp;
		deltaLL = myu_f / tempf;

		temp99    = 32 - G - e;
		temp99    = 1 << temp99;
		tempff    = (float) temp99;
		temp      = 1 << (Rb - e);
		tempf     = (float) temp;
		deltaLL99 = tempf * myu_f / (float) 2048; // /tempff *delta00;
	} else {
		deltaLL   = delta00;
		deltaLL99 = 1;
	}
	// hl
	Rb      = EEQcd->Rb[lll * 3 + 1];
	myu     = EEQcd->myu[lll * 3 + 1];
	myu_f   = (float) myu;
	myu_f   = myu_f + (float) 2048.0;
	e       = EEQcd->e[lll * 3 + 1];
	temp    = 1 << (27 - G - Rb);
	deltaHL = (float) ((float) (2048 + myu) / (float) temp);

	temp99    = 32 - G - e;
	temp99    = 1 << temp99;
	tempff    = (float) temp99;
	temp      = 1 << (Rb - e);
	tempf     = (float) temp;
	deltaHL99 = tempf * myu_f / (float) 2048; // /tempff *delta00;

	// lh
	Rb      = EEQcd->Rb[lll * 3 + 2];
	myu     = EEQcd->myu[lll * 3 + 2];
	myu_f   = (float) myu;
	myu_f   = myu_f + (float) 2048.0;
	e       = EEQcd->e[lll * 3 + 2];
	temp    = 1 << (27 - G - Rb);
	deltaLH = (float) ((float) (2048 + myu) / (float) temp);

	temp99    = 32 - G - e;
	temp99    = 1 << temp99;
	tempff    = (float) temp99;
	temp      = 1 << (Rb - e);
	tempf     = (float) temp;
	deltaLH99 = tempf * myu_f / (float) 2048; // /tempff *delta00;
	// hh
	Rb      = EEQcd->Rb[lll * 3 + 3];
	myu     = EEQcd->myu[lll * 3 + 3];
	myu_f   = (float) myu;
	myu_f   = myu_f + (float) 2048.0;
	e       = EEQcd->e[lll * 3 + 3];
	temp    = 1 << (27 - G - Rb);
	deltaHH = (float) ((float) (2048 + myu) / (float) temp);

	temp99    = 32 - G - e;
	temp99    = 1 << temp99;
	tempff    = (float) temp99;
	temp      = 1 << (Rb - e);
	tempf     = (float) temp;
	deltaHH99 = tempf * myu_f / (float) 2048; // /tempff *delta00;

	if (image->tbx0 & 1)
		xs = 1; // odd
	else
		xs = 0; // even
	if (image->tby0 & 1)
		ys = 1; // odd
	else
		ys = 0; // even

	d_TS = (float *) image->data;
	if (width % 2)
		width2 = (width / 2 + 1) * 2;
	else
		width2 = width;
	for (j = 0; j < height; j++, d_TS += col1step) {
		if ((!((j - ys) & 1)) && (!xs)) {
			delta0 = deltaLL;
			delta1 = deltaHL;
		} else if ((!((j - ys) & 1)) && xs) {
			delta0 = deltaHL;
			delta1 = deltaLL;
		} else if (((j - ys) & 1) && (!xs)) {
			delta0 = deltaLH;
			delta1 = deltaHH;
		} else {
			delta0 = deltaHH;
			delta1 = deltaLH;
		}
		d0 = d_TS;
		for (i = 0; i < width2; i += 2, d0 = &d0[row2step]) {
			if (d0[0]) d0[0] = (d0[0] * delta0) / delta00;
			if (d0[1]) d0[1] = (d0[1] * delta1) / delta00;
		}
	}
}

void DecWaveIL97(struct Image_s *image, byte4 lll, struct Image_s *ll_d, struct Image_s *ll,
                 struct Image_s *hl, struct Image_s *lh, struct Image_s *hh /*, struct EEQcd_s *EEQcd*/) {
	byte4 row1step, col1step;
	byte4 row2step, col2step;
	byte4 width, height;
	byte4 j, i;
	byte4 *D_;
	byte4 tempD;
	float tempf;
	float *d0, *d_TS;
	float *Dl_;

	width    = image->tbx1 - image->tbx0;
	height   = image->tby1 - image->tby0;
	col1step = image->col1step;
	col2step = col1step * 2;
	row1step = image->row1step;
	row2step = row1step * 2;

	if (width == 0) {
	} else {
		if (lll == 0) {
			// LL
			D_   = (byte4 *) ll->data;
			d_TS = (float *) &image->data[0];
			if (image->tby0 & 1) d_TS = &d_TS[col1step];
			for (j = 0; j < ll->height; j++, d_TS = &d_TS[col2step]) {
				d0 = d_TS;
				if (image->tbx0 & 1) d0 = &d0[row1step];
				for (i = 0; i < ll->width; i++, d0 += row2step) {
					tempD = (D_[j * ll->col1step + i] & 0x7fffffff);
					tempf = (float) tempD;
					if (D_[j * ll->col1step + i] & NEGATIVE_BIT)
						*d0 = -1 * tempf;
					else
						*d0 = tempf;
				}
			}
			// HL
			D_   = (byte4 *) hl->data;
			d_TS = (float *) &image->data[0];
			if (image->tby0 & 1) d_TS = &d_TS[col1step];
			for (j = 0; j < hl->height; j++, d_TS = &d_TS[col2step]) {
				d0 = d_TS;
				if (!(image->tbx0 & 1)) d0 = &d_TS[row1step];
				for (i = 0; i < hl->width; i++, d0 = &d0[row2step]) {
					tempD = D_[j * hl->col1step + i] & 0x7fffffff;
					tempf = (float) tempD;
					if (D_[j * hl->col1step + i] & NEGATIVE_BIT) {
						*d0 = -1 * tempf;
					} else
						*d0 = tempf;
				}
			}
			// LH
			D_   = (byte4 *) lh->data;
			d_TS = (float *) &image->data[0];
			if (!(image->tby0 & 1)) d_TS = &d_TS[col1step];
			for (j = 0; j < lh->height; j++, d_TS = &d_TS[col2step]) {
				d0 = d_TS;
				if (image->tbx0 & 1) d0 = &d0[row1step];
				for (i = 0; i < lh->width; i++, d0 = &d0[row2step]) {
					tempD = D_[j * lh->col1step + i] & 0x7fffffff;
					tempf = (float) tempD;
					if (D_[j * lh->col1step + i] & NEGATIVE_BIT)
						*d0 = -1 * tempf;
					else
						*d0 = tempf;
				}
			}
			// HH
			D_   = (byte4 *) hh->data;
			d_TS = (float *) &image->data[0];
			if (!(image->tby0 & 1)) d_TS = &d_TS[col1step];
			for (j = 0; j < hh->height; j++, d_TS = &d_TS[col2step]) {
				d0 = d_TS;
				if (!(image->tbx0 & 1)) d0 = &d0[row1step];
				for (i = 0; i < hh->width; i++, d0 = &d0[row2step]) {
					tempD = D_[j * hh->col1step + i] & 0x7fffffff;
					tempf = (float) tempD;
					if (D_[j * hh->col1step + i] & NEGATIVE_BIT)
						*d0 = -1 * tempf;
					else
						*d0 = tempf;
				}
			}
		} else {
#if Cplus
			// LL
			Dl_  = (float *) ll_d->data;
			d_TS = (float *) &image->data[0];
			if (image->tby0 & 1) d_TS = &d_TS[col1step];
			for (j = 0; j < ll_d->height; j++, d_TS = &d_TS[col2step]) {
				d0 = d_TS;
				if (image->tbx0 & 1) d0 = &d0[row1step];
				for (i = 0; i < ll_d->width; i++, d0 = &d0[row2step])
					*d0 = Dl_[j * ll_d->col1step + i];
			}
			// HL
			D_   = (byte4 *) hl->data;
			d_TS = (float *) &image->data[0];
			if (image->tby0 & 1) d_TS = &d_TS[col1step];
			for (j = 0; j < hl->height; j++, d_TS += col2step) {
				d0 = d_TS;
				if (!(image->tbx0 & 1)) d0 = &d0[row1step];
				for (i = 0; i < hl->width; i++, d0 += row2step) {
					tempD = D_[j * hl->col1step + i] & 0x7fffffff;
					tempf = (float) tempD;
					if (D_[j * hl->col1step + i] & NEGATIVE_BIT)
						*d0 = -1 * tempf;
					else
						*d0 = tempf;
				}
			}
			// LH
			D_ = (byte4 *) lh->data;
			if (!(image->tby0 & 1))
				d_TS = (float *) &image->data[col1step];
			else
				d_TS = (float *) &image->data[0];
			for (j = 0; j < lh->height; j++, d_TS += col2step) {
				if (image->tbx0 & 1)
					d0 = d_TS + row1step;
				else
					d0 = d_TS;
				for (i = 0; i < lh->width; i++, d0 += row2step) {
					tempD = D_[j * lh->col1step + i] & 0x7fffffff;
					tempf = (float) tempD;
					if (D_[j * lh->col1step + i] & NEGATIVE_BIT) {
						*d0 = -1 * tempf; //(float)tempD;
					} else
						*d0 = tempf;
				}
			}
			// HH
			D_ = (byte4 *) hh->data;
			if (!(image->tby0 & 1))
				d_TS = (float *) &image->data[col1step];
			else
				d_TS = (float *) &image->data[0];
			for (j = 0; j < hh->height; j++, d_TS += col2step) {
				if (!(image->tbx0 & 1))
					d0 = d_TS + row1step;
				else
					d0 = d_TS;
				for (i = 0; i < hh->width; i++, d0 += row2step) {
					tempD = D_[j * hh->col1step + i] & 0x7fffffff;
					tempf = (float) tempD;
					if (D_[j * hh->col1step + i] & NEGATIVE_BIT)
						*d0 = -1 * tempf;
					else
						*d0 = tempf;
				}
			}
#else
			// HL
			DecWaveIL97_1(hl);
			// LH
			DecWaveIL97_1(lh);
			// HH
			DecWaveIL97_1(hh);
			DecWaveIL97_2(image, ll_d, hl, lh, hh);
#endif
		}
	}
}
