/*
Copyright (c) 2000-2021, ICT-Link
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the HTJ2K project.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ImageUtil.h"
#include "j2k2.h"
#include "t2codec.h"

byte4 TagtreeCreate0(struct Tile_s *t, struct ECod_s *ECod) {
	byte2 ccc;
	char rrr;
	byte4 ppp;
	char bbb;
	struct Prc_s *Prc;

	for (ccc = 0; ccc < t->numCompts; ccc++) {
		for (rrr = 0; rrr <= ECod->EECod[ccc * ECod->numTiles + t->T].DecompLev; rrr++) {
			for (ppp = 0; ppp < t->Cmpt[ccc].EEPrc[rrr].numPrc; ppp++) {
				for (bbb = 0; bbb < t->Cmpt[ccc].EEPrc[rrr].numBand; bbb++) {
					Prc = &(t->Cmpt[ccc].EEPrc[rrr].EPrc[ppp].Prc[bbb]);
					if (nullptr == (Prc->incltree = TagtreeCreate1(Prc->numCblkx, Prc->numCblky))) {
						return EXIT_FAILURE;
					}
					if (nullptr == (Prc->zerotree = TagtreeCreate1(Prc->numCblkx, Prc->numCblky))) {
						return EXIT_FAILURE;
					}
				}
			}
		}
	}
	return EXIT_SUCCESS;
}
//[Func 32]
struct Tagtree_s *TagtreeCreate1(byte4 numCblkX, byte4 numCblkY) {
	struct TagtreeNode_s *node, *parent0, *parent1, *node_e;
	struct Tagtree_s *tag;
	byte4 n, x, y, num, i, j, num0;

	tag           = new struct Tagtree_s;
	tag->numCblkX = numCblkX;
	tag->numCblkY = numCblkY;

	y    = numCblkY;
	x    = numCblkX;
	num0 = 0;
	do {
		n = x * y;
		x = ceil2(x, 2);
		y = ceil2(y, 2);
		num0 += n;
	} while (n > 1);

	if (num0 <= 0) {
		printf("[TagtreeCreate1]:: number of node error. num0=%d\n", num0);
		return nullptr;
	}
	tag->numNode = num0;
	if (nullptr == (tag->node = new struct TagtreeNode_s[num0])) {
		printf("[TagtreeCreate1]:: tag->node malloc error. \n");
		return nullptr;
	}

	for (node = &tag->node[0], node_e = &tag->node[num0]; node != node_e; ++node) {
		node->state  = UNHANDLE;
		node->known  = ZERO;
		node->value  = CHARMAX;
		node->parent = NULL;
		node->child  = NULL;
	}

	node = &tag->node[0];
	y    = numCblkY;
	x    = numCblkX;
	num  = 0;
	do {
		n = x * y;
		num += n;
		parent0 = &tag->node[num];
		parent1 = &tag->node[num];
		for (j = 0; j < y; j++) {
			for (i = 0; i < x; i++) {
				node->parent = parent0;
				++node;
				if ((i & 1) || (i == x - 1)) ++parent0;
			}
			if ((j & 1) || (j == y - 1)) {
				parent1 = parent0;
			} else
				parent0 = parent1;
		}
		y = ceil2(y, 2);
		x = ceil2(x, 2);
	} while (n > 1);

	tag->node[num0 - 1].parent = nullptr;

	return tag;
}

void TagtreeDestory(struct Tagtree_s *tag) {
	delete[] tag->node;
	delete tag;
}

/////////////////////////////////////////////////////////////////////////

struct StreamChain_s *NumberOfPassesEnc(byte2 n, struct StreamChain_s *s) {
	if (n <= 0) {
		return nullptr;
	} else if (n == 1) {
		if (nullptr == (s = StreamBitWriteJ2K(0, 1, s, PACKETINFOLENGTH))) {
			printf("[NumberOfPassesEnc]:: StreamBitWriteJ2K error.\n");
			return nullptr;
		}
	} else if (n == 2) {
		if (nullptr == (s = StreamBitWriteJ2K(2, 2, s, PACKETINFOLENGTH))) {
			printf("[NumberOfPassesEnc]:: StreamBitWriteJ2K error.\n");
			return nullptr;
		}
	} else if (n <= 5) {
		if (nullptr == (s = StreamBitWriteJ2K((0xc | (n - 3)), 4, s, PACKETINFOLENGTH))) {
			printf("[NumberOfPassesEnc]:: StreamBitWriteJ2K error.\n");
			return nullptr;
		}
	} else if (n <= 36) {
		if (nullptr == (s = StreamBitWriteJ2K((0x1e0 | (n - 6)), 9, s, PACKETINFOLENGTH))) {
			printf("[NumberOfPassesEnc]:: StreamBitWriteJ2K error.\n");
			return nullptr;
		}
	} else if (n <= 164) {
		if (nullptr == (s = StreamBitWriteJ2K((0xff80 | (n - 37)), 16, s, PACKETINFOLENGTH))) {
			printf("[NumberOfPassesEnc]:: StreamBitWriteJ2K error.\n");
			return nullptr;
		}
	} else {
		return nullptr;
	}
	return s;
}

struct StreamChain_s *LblockChange(byte4 n, struct StreamChain_s *s) {
	if (n < 0) return nullptr;

	while (--n >= 0) {
		if (nullptr == (s = StreamBitWriteJ2K(1, 1, s, PACKETINFOLENGTH))) {
			printf("[LblockChange]:: StreamBitWriteJ2K error.\n");
			return nullptr;
		}
	}
	if (nullptr == (s = StreamBitWriteJ2K(0, 1, s, PACKETINFOLENGTH))) {
		printf("[LblockChange]:: StreamBitWriteJ2K error.\n");
		return nullptr;
	}

	return s;
}

byte4 TagTreeNodeSetValue(struct TagtreeNode_s *leaf, char value) {
	struct TagtreeNode_s *node;
	if (value < 0) return EXIT_FAILURE;

	node = leaf;
	while ((node != nullptr) && (node->value > value)) {
		node->value = value;
		node        = node->parent;
	}
	return EXIT_SUCCESS;
}

byte4 TagTreeNodeInit(struct Tagtree_s *leaf) {
	struct TagtreeNode_s *nodeS, *nodeE;

	nodeS = &leaf->node[0];
	nodeE = &leaf->node[leaf->numNode];
	for (; nodeS != nodeE; ++nodeS) {
		nodeS->state = UNHANDLE;
		nodeS->known = ZERO;
		nodeS->value = CHARMAX;
	}
	return EXIT_SUCCESS;
}

struct StreamChain_s *TagTreeEnc(struct TagtreeNode_s *leaf, char threshold, struct StreamChain_s *s) {
	struct TagtreeNode_s *nodeP, *node;
	char value;

	node = leaf;
	while ((node->parent != nullptr) && (node->parent->state != FIXED)) {
		nodeP        = node->parent;
		nodeP->child = node;
		node         = nodeP;
	}

	if (node->state == YET) {
		value = node->known;
	} else if (node->state == UNHANDLE) {
		if (node->parent != nullptr)
			value = node->parent->value;
		else
			value = 0;
	} else {
		return nullptr;
	}

	for (;; node = node->child) {
		while (node->value > value) {
			if (value > threshold) {
				node->state = YET;
				node->known = value;
				goto OUT;
			}
			s = StreamBitWriteJ2K(0, 1, s, PACKETINFOLENGTH);
			value++;
		}
		if (value > threshold) {
			node->state = YET;
			node->known = value;
			goto OUT;
		}
		s           = StreamBitWriteJ2K(1, 1, s, PACKETINFOLENGTH);
		node->state = FIXED;
		if (node->child == nullptr) goto OUT;
	}
OUT:
	return s;
}

byte4 PacketInfoEnc(struct Packet_s *P /*, struct EECod_s *EECod*/) {
	struct Cblk_s *Cbs, *Cbe;
	struct Tagtree_s *inclTree, *zeroTree;
	struct TagtreeNode_s *node_Zero, *node_incl;
	uchar numBands, bbb;
	ubyte2 LayerNo;
	ubyte4 t1, t2, t3, t4;
	uchar adjust;
	byte4 i;
	char flag;

	flag = 0;
	for (bbb = 0; bbb < P->numband; bbb++) {
		if (P->EPrc->Prc[bbb].numCblk != 0) {
			Cbs = &P->EPrc->Prc[bbb].Cblk[0];
			Cbe = &P->EPrc->Prc[bbb].Cblk[P->EPrc->Prc[bbb].numCblk];
			for (; Cbs != Cbe; ++Cbs) {
				if (!Cbs->NoStream) {
					flag = 1;
					break;
				}
			}
		}
	}

	if (flag)
		P->s = StreamBitWriteJ2K(1, 1, P->s, PACKETINFOLENGTH);
	else {
		P->s = Stream1ByteWrite(P->s, 0x00, PACKETINFOLENGTH);
		return EXIT_SUCCESS;
	}

	numBands = P->numband;
	LayerNo  = P->LayerNo;
	for (bbb = 0; bbb < numBands; bbb++) {
		inclTree  = P->EPrc->Prc[bbb].incltree;
		zeroTree  = P->EPrc->Prc[bbb].zerotree;
		Cbs       = &P->EPrc->Prc[bbb].Cblk[0];
		Cbe       = &P->EPrc->Prc[bbb].Cblk[P->EPrc->Prc[bbb].numCblk];
		node_Zero = &zeroTree->node[0];
		node_incl = &inclTree->node[0];
		for (; Cbs != Cbe; ++Cbs, ++node_Zero, ++node_incl) {
			TagTreeNodeSetValue(node_Zero, Cbs->zerobits);
			TagTreeNodeSetValue(node_incl, Cbs->firstLayer);
		}

		P->EPrc->Prc[bbb].f = EXIT_SUCCESS;
		Cbs                 = &P->EPrc->Prc[bbb].Cblk[0];
		Cbe                 = &P->EPrc->Prc[bbb].Cblk[P->EPrc->Prc[bbb].numCblk];
		inclTree            = P->EPrc->Prc[bbb].incltree;
		zeroTree            = P->EPrc->Prc[bbb].zerotree;
		node_Zero           = &zeroTree->node[0];
		node_incl           = &inclTree->node[0];
		if (((Cbs->tcbx1 - Cbs->tcbx0) * (Cbs->tcby1 - Cbs->tcby0))) {
			for (i = 0; Cbs != Cbe; i++, ++Cbs, ++node_Zero, ++node_incl) {
				if (Cbs->included) {
					if (Cbs->length)
						P->s = StreamBitWriteJ2K(1, 1, P->s, PACKETINFOLENGTH);
					else {
						P->s = StreamBitWriteJ2K(0, 1, P->s, PACKETINFOLENGTH);
						goto NEXT_Cb2;
					}
				} else {
					P->s = TagTreeEnc(node_incl, (char) LayerNo, P->s);
					if ((Cbs->firstLayer == LayerNo) && (Cbs->length))
						Cbs->included = true;
					else
						goto NEXT_Cb2;

					P->s = TagTreeEnc(node_Zero, CHARMAX, P->s);
				}
				P->s   = NumberOfPassesEnc(Cbs->numPasses, P->s);
				adjust = Cbs->Lblock;
				t1     = floorlog2(Cbs->numPasses);
				t2     = ceil2log2(Cbs->length);
				t3     = t1 + adjust;
				if (t2 > t3) adjust = (uchar) (adjust + (t2 - t3));
				Cbs->Lblock = adjust;
				adjust      = (uchar) (adjust - 3);
				P->s        = LblockChange(adjust, P->s);
				t4          = Cbs->Lblock + t1;
				P->s        = StreamBitWriteJ2K(Cbs->length, t4, P->s, PACKETINFOLENGTH);
			NEXT_Cb2:;
			}
		}
	}
	P->s = ByteStuffOutJ2K(P->s);

	return EXIT_SUCCESS;
}

struct StreamChain_s *PacketInfoBind2FileStream(struct StreamChain_s *s, struct StreamChain_s *t) {
	struct StreamChain_s *b = nullptr, *a = nullptr, *c = nullptr;
	byte4 length;
	byte4 i;
	byte4 chain_counter = 0;
	char flag0xFF;

	///	printf("[PacketInfoBind2FileStream] start s=%x t=%x\n", s, t);

	if (s->child != nullptr) {
		b = s->child;
		while (b->child != nullptr) {
			b = b->child;
		}
	} else {
		b = s;
	}
	//	printf("[PacketInfoBind2FileStream] 1 b=%x\n", b);

	length = 0;
	if (t->parent != nullptr) {
		length += t->cur_p;
		a = t->parent;
		chain_counter++;
		while (a->parent != nullptr) {
			length += a->cur_p;
			a = a->parent;
			chain_counter++;
		}
		length += a->cur_p;
	} else {
		length += t->cur_p;
		a = t;
	}
	//	printf("[PacketInfoBind2FileStream] 2 length=%d a=%x a->buf_length=%d  a->cur_p=%d
	//a->buf[a->cur_p-2]=%x a->buf[a->cur_p-1]=%x chain_counter=%d\n", length, a, a->buf_length, a->cur_p,
	//a->buf[a->cur_p-2], a->buf[a->cur_p-1], chain_counter);

	if (a->buf[a->cur_p - 1] == 0xff) {
		length++;
		flag0xFF = 1;
	} else
		flag0xFF = 0;

	if (length) {
		//		printf("[PacketInfoBind2FileStream] 2-1 length=%d chain_counter=%d\n", length,
		//chain_counter);

		if (nullptr == (c = StreamChainMake(nullptr, length, JPEG2000))) {
			printf("[PacketInfoBind2FileStream]:: StreamChainMake error\n");
			return nullptr;
		}
		b->child      = c;
		b->buf_length = b->cur_p;
		c->parent     = b;

		//		printf("[PacketInfoBind2FileStream] 3 length=%d chain_counter=%d\n", length, chain_counter);

		for (i = 0; i < chain_counter; i++) {
			memcpy(&c->buf[c->cur_p], &a->buf[0], a->cur_p * sizeof(uchar));
			c->cur_p += a->buf_length;
			c->total_p += a->buf_length;
			a = a->child;
		}
		//		printf("[PacketInfoBind2FileStream] 4\n");
		memcpy(&c->buf[c->cur_p], &a->buf[0], a->cur_p * sizeof(uchar));
		//		printf("[PacketInfoBind2FileStream] 5\n");
		c->cur_p += a->cur_p;
		c->total_p += a->cur_p;
		if (flag0xFF == 1) {
			c->buf[c->cur_p] = 0x00;
			c->cur_p++;
			c->total_p++;
		}

		if (c->cur_p != c->buf_length) {
			printf("[PacketInfoBind2FileStream]:: cur_p error cur_p=%d buf_length=%d\n", c->cur_p,
			       c->buf_length);
			return nullptr;
		}
	}

	//	printf("[PacketInfoBind2FileStream] complete!\n");
	return c;
}
