/*
Copyright (c) 2000-2021, ICT-Link
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the HTJ2K project.
*/

#include <stdio.h>
#include <stdlib.h>
#include "j2k2.h"
#include "ImageUtil.h"
#include "wavelet_codec.h"
#include "Icc.h"
#include "t2codec.h"
#include "annexA.h"
#include "t1codec.h"
#include "ctx_enc.h"
#include "MqCodec.h"
#include "annexB.h"
#include "annexE.h"

//[Func 6]
struct EQcd_s *EQCreate(struct ECod_s *ECod, struct Siz_s *Siz, char ENCDEC, struct J2kParam_s *J2kParam) {
	byte4 i;
	byte4 numCmpts_and_numTiles;
	byte4 ttt;
	struct EQcd_s *EQcd;
	uchar RI;
	byte4 ccc;
	uchar numBand, rrr, bbb, bbb2;

	if (nullptr == (EQcd = new struct EQcd_s)) {
		printf("[EQCreate]::struct EQcd_s malloc error\n");
		return nullptr;
	}
	EQcd->numCompts       = ECod->numCompts;
	EQcd->numTiles        = ECod->numTiles;
	numCmpts_and_numTiles = EQcd->numCompts * EQcd->numTiles;

	if (nullptr == (EQcd->EEQcd = new struct EEQcd_s[numCmpts_and_numTiles])) {
		printf("[EQCreate]::EQcd->EEQcd[%d] malloc error\n", numCmpts_and_numTiles);
		return nullptr;
	}

	for (i = 0; i < numCmpts_and_numTiles; i++) {
		numBand = (uchar) (ECod->EECod[i].DecompLev * 3 + 1);
		if (nullptr == (EQcd->EEQcd[i].e = new char[numBand])) {
			printf("[EQCreate]::e byte2[%d] malloc error\n", numBand);
			return nullptr;
		}
		if (nullptr == (EQcd->EEQcd[i].myu = new ubyte2[numBand])) {
			printf("[EQCreate]::myu byte4[%d] malloc error\n", numBand);
			return nullptr;
		}
		if (nullptr == (EQcd->EEQcd[i].Rb = new char[numBand])) {
			printf("[EQCreate]::Rb uchar[%d] malloc error\n", numBand);
			return nullptr;
		}
		EQcd->EEQcd[i].r = 0.5;
	}

	for (ccc = 0; ccc < ECod->numCompts; ccc++) {
		RI = (uchar) ((Siz->Ssiz[ccc] & 0x3f) + 1);
		for (ttt = 0; ttt < ECod->numTiles; ttt++) {
			numBand = (uchar) (ECod->EECod[ccc * ECod->numTiles + ttt].DecompLev * 3 + 1);
			for (bbb = 0; bbb < numBand; bbb++) {
				if (bbb == 0)
					EQcd->EEQcd[ccc * ECod->numTiles + ttt].Rb[bbb] = RI;
				else {
					if (bbb % 3 == 1)
						EQcd->EEQcd[ccc * ECod->numTiles + ttt].Rb[bbb] = (uchar) (RI + 1);
					else if (bbb % 3 == 2)
						EQcd->EEQcd[ccc * ECod->numTiles + ttt].Rb[bbb] = (uchar) (RI + 1);
					else
						EQcd->EEQcd[ccc * ECod->numTiles + ttt].Rb[bbb] = (uchar) (RI + 2);
				}
			}
		}
	}

	if (ENCDEC == ENC) {
		if (J2kParam->Sqcd == 2) { // Sqcd=2:Table
			for (ccc = 0; ccc < ECod->numCompts; ccc++) {
				for (ttt = 0; ttt < ECod->numTiles; ttt++) {
					bbb2 = 0;
					switch (J2kParam->Q_select) {
						case 0:
							EQcd->EEQcd[ccc * ECod->numTiles + ttt].e[bbb2] =
							    (char) (EQcd->EEQcd[ccc * ECod->numTiles + ttt].Rb[bbb2]
							            - EEE0_LL[ECod->EECod[ccc * ECod->numTiles + ttt].DecompLev]
							            - J2kParam->Q_value0);
							EQcd->EEQcd[ccc * ECod->numTiles + ttt].myu[bbb2] =
							    MYU0_LL[ECod->EECod[ccc * ECod->numTiles + ttt].DecompLev];
							break;
						case 1:
							EQcd->EEQcd[ccc * ECod->numTiles + ttt].e[bbb2] =
							    (char) (EQcd->EEQcd[ccc * ECod->numTiles + ttt].Rb[bbb2]
							            - EEE1_LL[ECod->EECod[ccc * ECod->numTiles + ttt].DecompLev]
							            - J2kParam->Q_value0);
							EQcd->EEQcd[ccc * ECod->numTiles + ttt].myu[bbb2] =
							    MYU1_LL[ECod->EECod[ccc * ECod->numTiles + ttt].DecompLev];
							break;
						case 2:
							EQcd->EEQcd[ccc * ECod->numTiles + ttt].e[bbb2] =
							    (char) (EQcd->EEQcd[ccc * ECod->numTiles + ttt].Rb[bbb2]
							            - EEE2_LL[ECod->EECod[ccc * ECod->numTiles + ttt].DecompLev]
							            - J2kParam->Q_value0);
							EQcd->EEQcd[ccc * ECod->numTiles + ttt].myu[bbb2] =
							    MYU2_LL[ECod->EECod[ccc * ECod->numTiles + ttt].DecompLev];
							break;
						case 3:
							EQcd->EEQcd[ccc * ECod->numTiles + ttt].e[bbb2] =
							    (char) (EQcd->EEQcd[ccc * ECod->numTiles + ttt].Rb[bbb2]
							            - EEE3_LL[ECod->EECod[ccc * ECod->numTiles + ttt].DecompLev]
							            - J2kParam->Q_value0);
							EQcd->EEQcd[ccc * ECod->numTiles + ttt].myu[bbb2] =
							    MYU3_LL[ECod->EECod[ccc * ECod->numTiles + ttt].DecompLev];
							break;
						case 4:
							EQcd->EEQcd[ccc * ECod->numTiles + ttt].e[bbb2] =
							    (char) (EQcd->EEQcd[ccc * ECod->numTiles + ttt].Rb[bbb2]
							            - EEE4_LL[ECod->EECod[ccc * ECod->numTiles + ttt].DecompLev]
							            - J2kParam->Q_value0);
							EQcd->EEQcd[ccc * ECod->numTiles + ttt].myu[bbb2] =
							    MYU4_LL[ECod->EECod[ccc * ECod->numTiles + ttt].DecompLev];
							break;
						case 5:
							EQcd->EEQcd[ccc * ECod->numTiles + ttt].e[bbb2] =
							    (char) (EQcd->EEQcd[ccc * ECod->numTiles + ttt].Rb[bbb2]
							            - EEE5_LL[ECod->EECod[ccc * ECod->numTiles + ttt].DecompLev]
							            - J2kParam->Q_value0);
							EQcd->EEQcd[ccc * ECod->numTiles + ttt].myu[bbb2] =
							    MYU5_LL[ECod->EECod[ccc * ECod->numTiles + ttt].DecompLev];
							break;
						case 6:
							EQcd->EEQcd[ccc * ECod->numTiles + ttt].e[bbb2] =
							    (char) (EQcd->EEQcd[ccc * ECod->numTiles + ttt].Rb[bbb2]
							            - EEE6_LL[ECod->EECod[ccc * ECod->numTiles + ttt].DecompLev]
							            - J2kParam->Q_value0);
							EQcd->EEQcd[ccc * ECod->numTiles + ttt].myu[bbb2] =
							    MYU6_LL[ECod->EECod[ccc * ECod->numTiles + ttt].DecompLev];
							break;
						case 7:
							EQcd->EEQcd[ccc * ECod->numTiles + ttt].e[bbb2] =
							    (char) (EQcd->EEQcd[ccc * ECod->numTiles + ttt].Rb[bbb2]
							            - EEE7_LL[ECod->EECod[ccc * ECod->numTiles + ttt].DecompLev]
							            - J2kParam->Q_value0);
							EQcd->EEQcd[ccc * ECod->numTiles + ttt].myu[bbb2] =
							    MYU7_LL[ECod->EECod[ccc * ECod->numTiles + ttt].DecompLev];
							break;
						default:
							printf(
							    "[EQCreate] :: Q_select number have to be 0 between to 7. Q_select is %d\n",
							    J2kParam->Q_select);
							return nullptr;
							break;
					}
					bbb2++;
					for (rrr = ECod->EECod[ccc * ECod->numTiles + ttt].DecompLev; rrr > 0; rrr--) {
						for (bbb = 0; bbb < 3; bbb++) {
							switch (J2kParam->Q_select) {
								case 0:
									EQcd->EEQcd[ccc * ECod->numTiles + ttt].e[bbb2] =
									    (char) (EQcd->EEQcd[ccc * ECod->numTiles + ttt].Rb[bbb2]
									            - EEE0_HLLHHH[rrr][bbb] - J2kParam->Q_value1);
									EQcd->EEQcd[ccc * ECod->numTiles + ttt].myu[bbb2] =
									    MYU0_HLLHHH[rrr][bbb];
									break;
								case 1:
									EQcd->EEQcd[ccc * ECod->numTiles + ttt].e[bbb2] =
									    (char) (EQcd->EEQcd[ccc * ECod->numTiles + ttt].Rb[bbb2]
									            - EEE1_HLLHHH[rrr][bbb] - J2kParam->Q_value1);
									EQcd->EEQcd[ccc * ECod->numTiles + ttt].myu[bbb2] =
									    MYU1_HLLHHH[rrr][bbb];
									break;
								case 2:
									EQcd->EEQcd[ccc * ECod->numTiles + ttt].e[bbb2] =
									    (char) (EQcd->EEQcd[ccc * ECod->numTiles + ttt].Rb[bbb2]
									            - EEE2_HLLHHH[rrr][bbb] - J2kParam->Q_value1);
									EQcd->EEQcd[ccc * ECod->numTiles + ttt].myu[bbb2] =
									    MYU2_HLLHHH[rrr][bbb];
									break;
								case 3:
									EQcd->EEQcd[ccc * ECod->numTiles + ttt].e[bbb2] =
									    (char) (EQcd->EEQcd[ccc * ECod->numTiles + ttt].Rb[bbb2]
									            - EEE3_HLLHHH[rrr][bbb] - J2kParam->Q_value1);
									EQcd->EEQcd[ccc * ECod->numTiles + ttt].myu[bbb2] =
									    MYU3_HLLHHH[rrr][bbb];
									break;
								case 4:
									EQcd->EEQcd[ccc * ECod->numTiles + ttt].e[bbb2] =
									    (char) (EQcd->EEQcd[ccc * ECod->numTiles + ttt].Rb[bbb2]
									            - EEE4_HLLHHH[rrr][bbb] - J2kParam->Q_value1);
									EQcd->EEQcd[ccc * ECod->numTiles + ttt].myu[bbb2] =
									    MYU4_HLLHHH[rrr][bbb];
									break;
								case 5:
									EQcd->EEQcd[ccc * ECod->numTiles + ttt].e[bbb2] =
									    (char) (EQcd->EEQcd[ccc * ECod->numTiles + ttt].Rb[bbb2]
									            - EEE5_HLLHHH[rrr][bbb] - J2kParam->Q_value1);
									EQcd->EEQcd[ccc * ECod->numTiles + ttt].myu[bbb2] =
									    MYU5_HLLHHH[rrr][bbb];
									break;
								case 6:
									EQcd->EEQcd[ccc * ECod->numTiles + ttt].e[bbb2] =
									    (char) (EQcd->EEQcd[ccc * ECod->numTiles + ttt].Rb[bbb2]
									            - EEE6_HLLHHH[rrr][bbb] - J2kParam->Q_value1);
									EQcd->EEQcd[ccc * ECod->numTiles + ttt].myu[bbb2] =
									    MYU6_HLLHHH[rrr][bbb];
									break;
								case 7:
									EQcd->EEQcd[ccc * ECod->numTiles + ttt].e[bbb2] =
									    (char) (EQcd->EEQcd[ccc * ECod->numTiles + ttt].Rb[bbb2]
									            - EEE7_HLLHHH[rrr][bbb] - J2kParam->Q_value1);
									EQcd->EEQcd[ccc * ECod->numTiles + ttt].myu[bbb2] =
									    MYU7_HLLHHH[rrr][bbb];
									break;
								default:
									printf("[EQCreate] :: Q_select number have to be 0 between to 7. "
									       "Q_select is %d\n",
									       J2kParam->Q_select);
									return nullptr;
									break;
							}
							bbb2++;
						}
					}
					EQcd->EEQcd[ccc * ECod->numTiles + ttt].G    = 0;
					EQcd->EEQcd[ccc * ECod->numTiles + ttt].Sqcd = J2kParam->Sqcd;
				}
			}
		} else if (J2kParam->Sqcd == 1) { // should be repair!
			for (ccc = 0; ccc < ECod->numCompts; ccc++) {
				for (ttt = 0; ttt < ECod->numTiles; ttt++) {
					bbb2 = 0;
					if (J2kParam->Q_select == 0) {
						EQcd->EEQcd[ccc * ECod->numTiles + ttt].e[bbb2] =
						    (char) (EQcd->EEQcd[ccc * ECod->numTiles + ttt].Rb[bbb2]
						            - EEE0_LL[ECod->EECod[ccc * ECod->numTiles + ttt].DecompLev]);
						EQcd->EEQcd[ccc * ECod->numTiles + ttt].myu[bbb2] =
						    MYU0_LL[ECod->EECod[ccc * ECod->numTiles + ttt].DecompLev];
					} else if (J2kParam->Q_select == 1) {
						EQcd->EEQcd[ccc * ECod->numTiles + ttt].e[bbb2] =
						    (char) (EQcd->EEQcd[ccc * ECod->numTiles + ttt].Rb[bbb2]
						            - EEE1_LL[ECod->EECod[ccc * ECod->numTiles + ttt].DecompLev]);
						EQcd->EEQcd[ccc * ECod->numTiles + ttt].myu[bbb2] =
						    MYU1_LL[ECod->EECod[ccc * ECod->numTiles + ttt].DecompLev];
					}
					bbb2++;
					for (rrr = ECod->EECod[ccc * ECod->numTiles + ttt].DecompLev; rrr > 0; rrr--) {
						for (bbb = 0; bbb < 3; bbb++) {
							if (J2kParam->Q_select == 0) {
								EQcd->EEQcd[ccc * ECod->numTiles + ttt].e[bbb2] =
								    (char) (EQcd->EEQcd[ccc * ECod->numTiles + ttt].Rb[bbb2]
								            - EEE0_HLLHHH[rrr][bbb]);
								EQcd->EEQcd[ccc * ECod->numTiles + ttt].myu[bbb2] = MYU0_HLLHHH[rrr][bbb];
							} else if (J2kParam->Q_select == 1) {
								EQcd->EEQcd[ccc * ECod->numTiles + ttt].e[bbb2] =
								    (char) (EQcd->EEQcd[ccc * ECod->numTiles + ttt].Rb[bbb2]
								            - EEE1_HLLHHH[rrr][bbb]);
								EQcd->EEQcd[ccc * ECod->numTiles + ttt].myu[bbb2] = MYU1_HLLHHH[rrr][bbb];
							}
							bbb2++;
						}
					}
					EQcd->EEQcd[ccc * ECod->numTiles + ttt].G    = 0;
					EQcd->EEQcd[ccc * ECod->numTiles + ttt].Sqcd = J2kParam->Sqcd;
				}
			}
		} else if (J2kParam->Sqcd == 0) { // rossless
			for (i = 0; i < numCmpts_and_numTiles; i++) {
				numBand = (uchar) (ECod->EECod[i].DecompLev * 3 + 1);
				for (bbb = 0; bbb < numBand; bbb++) {
					EQcd->EEQcd[i].e[bbb]   = (char) (EQcd->EEQcd[i].Rb[bbb]);
					EQcd->EEQcd[i].myu[bbb] = 0;
				}
				EQcd->EEQcd[i].G    = 0;
				EQcd->EEQcd[i].Sqcd = J2kParam->Sqcd;
			}
		}
	}
	return EQcd;
}

void EQcdDestory(struct EQcd_s *EQcd) {
	byte4 i;
	byte4 temp2;

	temp2 = EQcd->numCompts * EQcd->numTiles;
	for (i = 0; i < temp2; i++) {
#if MEM_DEBUG2
		mem0debug_fp = fopen("mem0debug_fp.txt", "a+");
		fprintf(mem0debug_fp, "[EQcdDestory] D EQcd->EEQcd[i].myu []=%08x\n", EQcd->EEQcd[i].myu);
		fprintf(mem0debug_fp, "[EQcdDestory] D EQcd->EEQcd[i].e []=%08x\n", EQcd->EEQcd[i].e);
		fprintf(mem0debug_fp, "[EQcdDestory] D EQcd->EEQcd[i].Rb []=%08x\n", EQcd->EEQcd[i].Rb);
		fclose(mem0debug_fp);
#endif
		delete[] EQcd->EEQcd[i].myu;
		delete[] EQcd->EEQcd[i].e;
		delete[] EQcd->EEQcd[i].Rb;
	}
#if MEM_DEBUG2
	mem0debug_fp = fopen("mem0debug_fp.txt", "a+");
	fprintf(mem0debug_fp, "[EQcdDestory] D EQcd->EEQcd =%08x\n", EQcd->EEQcd);
	fprintf(mem0debug_fp, "[EQcdDestory] D EQcd =%08x\n", EQcd);
	fclose(mem0debug_fp);
#endif
	delete EQcd->EEQcd;
	delete EQcd;
}
