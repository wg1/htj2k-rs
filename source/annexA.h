/*
Copyright (c) 2000-2021, ICT-Link
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the HTJ2K project.
*/

#ifndef ANNEX_A_H
#define ANNEX_A_H

struct ECod_s *ECodDec(struct StreamChain_s *s, struct Codec_s *Codec, struct J2kParam_s *J2kParam);
struct Poc_s *PocDec(struct Poc_s *Poc, struct StreamChain_s *s, struct Codec_s *Codec);
byte4 ECodDecTilePart(struct StreamChain_s *s, struct Siz_s *siz, struct ECod_s *ECod, byte4 TileNo);
byte4 ECocDec(struct ECod_s *ECod, struct StreamChain_s *s, byte4 TileNo, struct Codec_s *Codec);
struct EQcd_s *EQcdDec(struct ECod_s *ECod, struct StreamChain_s *s, struct Siz_s *Siz);
byte4 MarkerSkip(struct StreamChain_s *s);
byte4 EQccMarDec(struct EQcd_s *EQcd, struct ECod_s *ECod, struct StreamChain_s *s, byte4 TileNo);
struct Com_s *ComDec(struct StreamChain_s *s);
byte4 EQcdDecTilePart(struct EQcd_s *EQcd, struct ECod_s *ECod, struct StreamChain_s *s, byte4 TileNo,
                      struct Siz_s *Siz);
struct Sot_s *SotCreate(struct Siz_s *Siz);
byte4 SotDec(struct Codec_s *Codec, struct StreamChain_s *str);
byte4 PpmDec(struct Codec_s *Codec, struct StreamChain_s *str);
struct Cap_s *CapDec(struct StreamChain_s *str /*, struct Codec_s *Codec*/);

struct StreamChain_s *CodMarWrite(struct StreamChain_s *s, struct ECod_s *ECod, byte4 t);
struct StreamChain_s *CocMarWrite(struct StreamChain_s *s, struct ECod_s *ECod, byte4 c, byte4 t);
struct StreamChain_s *SocMarWrite(struct StreamChain_s *s);
struct StreamChain_s *EocMarWrite(struct StreamChain_s *s);
struct StreamChain_s *SotMarWrite(struct StreamChain_s *s, byte4 Isot, ubyte4 Psot, uchar TPsot,
                                  uchar TNsot);
struct StreamChain_s *SizMarWrite(struct StreamChain_s *s, struct Siz_s *Siz);
struct StreamChain_s *SopMarWrite(struct StreamChain_s *s, byte4 index);
struct StreamChain_s *RgnMarWrite(struct StreamChain_s *s, struct ERgn_s *ERgn, ubyte2 ccc);
struct StreamChain_s *QcdMarWrite(struct StreamChain_s *s, struct ECod_s *ECod, struct EQcd_s *EQcd,
                                  byte4 t);
struct StreamChain_s *QccMarWrite(struct StreamChain_s *s, struct ECod_s *ECod, struct EQcd_s *EQcd,
                                  byte4 t, ubyte2 ccc);
byte4 PsotCal2(struct Tile_s *t, struct EECod_s *EECod, byte4 TileMarkerByte);
byte4 CodCompare(struct EECod_s *EECod1, struct EECod_s *EECod2);
byte4 QcdCompare(struct EEQcd_s *EEQcd1, struct EEQcd_s *EEQcd2, struct EECod_s *EECod);
byte4 QcdMarByteCalculate(struct ECod_s *ECod, struct EQcd_s *EQcd, byte4 t);
byte4 QccMarByteCalculate(struct ECod_s *ECod, struct EQcd_s *EQcd, byte4 t, ubyte2 c);
byte4 CocMarByteCalculate(struct ECod_s *ECod, struct Siz_s *Siz, byte4 t, byte2 ccc);
byte4 CodMarByteCalculate(struct ECod_s *ECod, byte4 t);
byte4 ERgnMarDec(struct ERgn_s *ERgn, struct StreamChain_s *s);
struct Codec_s *CreateCodec(struct J2kParam_s *J2kParam);
byte4 DestroyCodec(struct Codec_s *Codec);
struct Tile_s *TileCreate(struct Codec_s *Codec, byte4 numTilePart);

#endif