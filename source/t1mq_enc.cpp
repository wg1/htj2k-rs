/*
Copyright (c) 2000-2021, ICT-Link
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the HTJ2K project.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "ImageUtil.h"
#include "j2k2.h"
#include "ctx_enc.h"
#include "t1codec.h"
#include "MqCodec.h"
#include "RateControl.h"
#include "vlc_codec.h"
#include "MelCodec.h"
#include "HT_cleanUp.h"

#if Cplus
byte4 BitTopSearch(struct Cblk_s *Cblk) {
	byte4 x0, x1, y0, y1;
	byte4 col1step, row1step;
	byte4 CbData_OR;

	x0        = Cblk->tcbx0 - Cblk->Band->tbx0;
	x1        = Cblk->tcbx1 - Cblk->Band->tbx0;
	y0        = Cblk->tcby0 - Cblk->Band->tby0;
	y1        = Cblk->tcby1 - Cblk->Band->tby0;
	col1step  = Cblk->Band->col1step;
	row1step  = Cblk->Band->row1step;
	CbData_OR = 0;

	byte4 j;
	byte4 *D;
	byte4 *s, *e;

	D = Cblk->Band->data;
	for (j = y0; j < y1; j++) {
		s = (byte4 *) (&D[col1step * j + x0 * row1step]);
		e = (byte4 *) (&D[col1step * j + x1 * row1step]);
		for (; s != e; ++s)
			CbData_OR |= *s;
	}

	return CbData_OR;
}
#endif

struct mqenc_s *Mqenc_Create(byte4 max_contexts) {
	struct mqenc_s *enc;
	//	byte4	cx_length=0x40000;

	if (nullptr == (enc = new struct mqenc_s)) {
		printf("[Mqenc_Create]:: enc mqenc_s malloc error\n");
		return nullptr;
	}
	enc->num_CX    = 0x40000;
	enc->num_index = max_contexts;
	if (nullptr == (enc->cx = new uchar[enc->num_CX])) {
		printf("[Mqenc_Create]:: enc->cx uchar[%d]malloc error\n", enc->num_CX);
		return nullptr;
	}

	if (nullptr == (enc->ind = new uchar[enc->num_index])) {
		printf("[Mqenc_Create]:: enc->ind uchar[%d]malloc error\n", enc->num_index);
		return nullptr;
	}

	return enc;
}

struct StreamChain_s *mqenc_flush(struct StreamChain_s *str, struct mqenc_s *enc, byte4 termmode) {
	char k;

	switch (termmode) {
		case MQENC_PTERM:
			k = (char) (11 - enc->ctreg + 1);
			while (k > 0) {
				enc->creg <<= enc->ctreg;
				enc->ctreg = 0;
				str        = mqenc_byteout(str, enc);
				k          = (char) (k - enc->ctreg);
			}
			if (enc->B_buf != 0xff) {
				str = mqenc_byteout(str, enc);
			} else {
				enc->B_buf = enc->B_buf;
			}
			break;
		case MQENC_DEFTERM2:
			k = (char) (11 - enc->ctreg + 1);
			while (k > 0) {
				enc->creg <<= enc->ctreg;
				enc->ctreg = 0;
				str        = mqenc_byteout(str, enc);
				k          = (char) (k - enc->ctreg);
			}
			if (enc->B_buf != 0xff) {
				str = mqenc_byteout(str, enc);
			} else {
				enc->B_buf = enc->B_buf;
			}
			break;
		case MQENC_DEFTERM1:
			mq_setbits(enc);
			enc->creg <<= enc->ctreg;
			str = mqenc_byteout(str, enc);
			enc->creg <<= enc->ctreg;
			str = mqenc_byteout(str, enc);
			if (enc->B_buf != 0xff)
				str = mqenc_byteout(str, enc);
			else
				enc->B_buf = enc->B_buf;

			break;
		case MQENC_JBIG2_PROCEDURE:
			mq_setbits(enc);
			enc->creg <<= enc->ctreg;
			str = mqenc_byteout(str, enc);
			enc->creg <<= enc->ctreg;
			str = mqenc_byteout(str, enc);
			if (enc->B_buf != 0xff) {
				if (str->cur_p & CB_BUF_LENGTH) {
					if (NULL == (str = StreamChainMake(str, CB_BUF_LENGTH, str->stream_type))) {
						printf("[mqenc_flush]::StreamChainMake error\n");
						return NULL;
					}
				}
				str->buf[str->cur_p] = enc->B_buf;
				str->cur_p++;
				str->total_p++;
				enc->B_buf = 0xff;
			}
			str        = mqenc_byteout(str, enc);
			enc->B_buf = 0xac;
			str        = mqenc_byteout(str, enc);
			break;
		default:
			break;
	}
	return str;
}

void mq_setbits(struct mqenc_s *enc) {
	ubyte4 tmp;
	tmp = enc->creg + enc->areg;
	enc->creg |= 0xffff;
	if (enc->creg >= tmp) {
		enc->creg -= 0x8000;
	}
}

void mqenc_init(struct mqenc_s *enc) {
	enc->areg       = 0x8000;
	enc->B_buf      = 0x00;
	enc->creg       = 0;
	enc->ctreg      = 12;
	enc->first_flag = 0;
	enc->total_CT   = 0;
}

void mqindex_init(struct mqenc_s *enc) {
	memset(enc->ind, 0, sizeof(char) * MAX_CONTEXTS);
	enc->ind[CTXUNI] = 46;
	enc->ind[CTXRUN] = 3;
	enc->ind[0]      = 4;
}

#if Cplus
struct StreamChain_s *mqenc_byteout(struct StreamChain_s *str, struct mqenc_s *enc) {
	char FF_flag;
	if (!enc->first_flag) {
		enc->B_buf = (uchar) ((enc->creg) >> 19);
		enc->creg &= 0x7ffff;
		enc->ctreg      = 8;
		enc->first_flag = 1;
		return str;
	}

	if (str->cur_p == str->buf_length) {
		if (NULL == (str = StreamChainMake(str, str->buf_length, str->stream_type))) {
			printf("[mqenc_byteou]::StreamChainMake error\n");
			return NULL;
		}
	}

	if (enc->B_buf == 0xff)
		FF_flag = 1;
	else {
		enc->B_buf = (uchar) (enc->B_buf + ((enc->creg & 0x8000000) ? 1 : 0));
		enc->creg &= 0x7ffffff;
		if (enc->B_buf == 0xff)
			FF_flag = 1;
		else
			FF_flag = 0;
	}

	str->buf[str->cur_p] = enc->B_buf;
	str->cur_p++;
	str->total_p++;
	if (FF_flag) {
		enc->B_buf = (uchar) (enc->creg >> 20);
		enc->creg &= 0xfffff;
		enc->ctreg = 7;
	} else {
		enc->B_buf = (uchar) ((enc->creg) >> 19);
		enc->creg &= 0x7ffff;
		enc->ctreg = 8;
	}

	return str;
}

struct StreamChain_s *MQ_Enc(struct StreamChain_s *str, struct mqenc_s *enc, ubyte4 *QeIndexTable_,
                             uchar *CX_, byte4 Length) {
	char index;
	uchar *CX_s, *CX_e;
	uchar CX, D;
	uchar MPS_D;
	ubyte2 Qe;

	CX_s = &CX_[0];
	CX_e = &CX_[Length];
	for (; CX_s != CX_e; ++CX_s) {
		CX    = (uchar) ((*CX_s) & 0x1f);
		D     = (uchar) ((*CX_s) & 0x80);
		index = (char) (enc->ind[CX] & 0x7f);
		if ((index < 0) || (47 <= index)) {
			printf("[MQ_Enc]:: index error. index=%d\n", index);
			return nullptr;
		}
		MPS_D     = (uchar) (enc->ind[CX] & 0x80);
		Qe        = (ubyte2) (QeIndexTable_[index] & 0xffff);
		enc->areg = (ubyte2) (enc->areg - Qe);
		if (D ^ MPS_D) { // code LPS
			if (enc->areg < Qe)
				enc->creg += Qe; // C2
			else
				enc->areg = Qe;                                              // C1
			enc->ind[CX] = (uchar) (((QeIndexTable_[index] >> 24) ^ MPS_D)); // C1,C2
		}

		else { // code MPS
			if (enc->areg < Qe) {
				enc->areg    = Qe;
				enc->ind[CX] = (uchar) ((QeIndexTable_[index] >> 16) | MPS_D); // C5
			} else {
				enc->creg += Qe; // C3
				if (!(enc->areg & 0x8000))
					enc->ind[CX] = (uchar) ((QeIndexTable_[index] >> 16) | MPS_D); // C4
			}
		}
		for (; !(enc->areg & 0x8000); enc->areg <<= 1) {
			enc->creg <<= 1;
			enc->ctreg--;
			enc->total_CT++;
			if (!enc->ctreg) str = mqenc_byteout(str, enc);
		}
	}
	return str;
}
#endif

struct StreamChain_s *MQ_Enc_Lazy(struct StreamChain_s *str,
                                  /*struct mqenc_s *enc,*/ /*struct Cblk_s *Cblk,*/ uchar *CX_,
                                  byte4 Length) {
	uchar *CX_s, *CX_e;
	uchar D;
	CX_s = &CX_[0];
	CX_e = &CX_[Length];
	for (; CX_s != CX_e; ++CX_s) {
		D = (uchar) ((*CX_s) >> 7);
		if (NULL == (str = StreamBitWriteJ2K(D, 1, str, str->buf_length))) {
			printf("[MQ_Enc_Lazy]::StreamBitWriteJ2K error\n");
			return NULL;
		}
	}
	return str;
}

byte4 T1Enc(struct Codec_s *Codec, struct Cblk_s *Cblk, struct EECod_s *EECod, byte4 eee, byte4 Rb,
            byte4 myu, char G, struct mqenc_s *enc, uchar *ctx_table, char shift) {
	float Delta = 0.0;
	byte4 width, height;
	byte4 height1, height2;
	byte4 x0, x1, y0, y1;
	byte4 row1stepf, col1stepf;
	byte4 one;
	byte4 i, ii, j, jj, col1stepCb, row1stepCb;
	byte4 bitmask;
	byte4 P = 0;
	byte4 Length;
	double Dis;
	byte4 *CbD_;
	byte4 *BaD_;
	ubyte4 *QeIndexTable_ = QeIndexTable;
	ubyte2 *flag_;
	uchar *CX_;
	ubyte2 numPasses;
	struct StreamChain_s *str;
	char Lazy, Lazy_Start;
	char bittop, bitend;
	char pass, endpass;
	byte4 CbData_OR;

	CbData_OR = BitTopSearch(Cblk);
	one       = 0x40000000;
	for (bittop = 30; bittop >= 0; bittop--, one >>= 1) {
		if (CbData_OR & one) break;
	}

	bitend = 0;
	if (bittop == -1) {
		Cblk->Nb         = Cblk->zerobits;
		endpass          = 0;
		Cblk->length     = 0;
		Cblk->NoStream   = 1;  // no stream
		Cblk->firstLayer = -1; // 0xff;
		Cblk->Distortion = nullptr;
		Cblk->RdSlope    = nullptr;
		Cblk->s          = nullptr;
		return EXIT_SUCCESS;
	}

	Cblk->Mb         = (ubyte2) (G + eee - 1);
	Cblk->zerobits   = (char) (Cblk->Mb + shift - bittop - 1);
	endpass          = (char) ((bittop - bitend) * 3 + 1);
	Cblk->NoStream   = 0; // exist stream
	Cblk->firstLayer = 0x00;
	Cblk->Nb         = (ubyte2) (Cblk->zerobits + bittop + 1);
	Cblk->numPasses  = endpass;
	Cblk->Pass_Addr  = new byte4[endpass + 1];
	memset(Cblk->Pass_Addr, 0, sizeof(byte4) * (endpass + 1));

	if ((EECod->RateMode == RATE_CONTROL_ByteLimit) || (EECod->RateMode == RATE_CONTROL_RateValue)) {
		if (nullptr == (Cblk->Distortion = new float[endpass + 1])) {
			printf("[T1Enc]:: Cblk.Distortion[%d] malloc error\n", (endpass + 1));
			return EXIT_FAILURE;
		}
		if (nullptr == (Cblk->RdSlope = new float[endpass + 1])) {
			printf("[T1Enc]:: Cblk.RdSlope[%d] malloc error\n", (endpass + 1));
			return EXIT_FAILURE;
		}
		memset(&(Cblk->RdSlope[0]), 0, sizeof(float) * (endpass + 1));
		memset(Cblk->Distortion, 0, sizeof(float) * (endpass + 1));
	} else {
		Cblk->Distortion = nullptr;
		Cblk->RdSlope    = nullptr;
	}
	if (nullptr == (str = StreamChainMake(nullptr, CB_BUF_LENGTH, JPEG2000))) {
		printf("[T1Enc]:: StreamChainMake(CB_BUF_LENGTH) malloc error\n");
		return EXIT_FAILURE;
	}

	width  = Cblk->tcbx1 - Cblk->tcbx0;
	height = Cblk->tcby1 - Cblk->tcby0;
	if (height % 4) {
		height1 = height / 4 + 1;
		height2 = height1 * 4 - height;
	} else {
		height1 = height / 4;
		height2 = 0;
	}

	// flag
	x0                = Cblk->tcbx0 - Cblk->Band->tbx0;
	x1                = Cblk->tcbx1 - Cblk->Band->tbx0;
	y0                = Cblk->tcby0 - Cblk->Band->tby0;
	y1                = Cblk->tcby1 - Cblk->Band->tby0;
	Codec->flag->tbx0 = 1;
	Codec->flag->tbx1 = 1 + (x1 - x0);
	Codec->flag->tby0 = 4;
	Codec->flag->tby1 = 4 + (y1 - y0);
	col1stepf         = Codec->flag->col1step;
	row1stepf         = Codec->flag->row1step;
	memset(Codec->flag->Pdata, 0, sizeof(ubyte2) * Codec->flag->numData);
	flag_ = (ubyte2 *) &Codec->flag->data[0];

	// CbData
	row1stepCb          = Codec->CbData->row1step;
	col1stepCb          = Codec->CbData->col1step;
	Codec->CbData->tbx1 = Cblk->tcbx1 - Cblk->tcbx0;
	Codec->CbData->tby1 = Cblk->tcby1 - Cblk->tcby0;
	CbD_                = &Codec->CbData->data[0];
	BaD_                = &Cblk->Band->data[0];
	for (j = 0, jj = Cblk->tcby0 - Cblk->Band->tby0; j < Codec->CbData->tby1; j++, jj++) {
		for (i = 0, ii = Cblk->tcbx0 - Cblk->Band->tbx0; i < Codec->CbData->tbx1; i++, ii++) {
			CbD_[j * col1stepCb + i * row1stepCb] =
			    BaD_[jj * Cblk->Band->col1step + ii * Cblk->Band->row1step];
		}
	}

	if ((EECod->RateMode == RATE_CONTROL_ByteLimit) || (EECod->RateMode == RATE_CONTROL_RateValue)) {
		// Delta
		if ((11 + eee - Rb) > 0)
			Delta = (float) (2048 + myu) / (float) (1 << (11 + eee - Rb));
		else if ((11 + eee - Rb) == 0)
			Delta = (float) (2048 + myu);
		else
			Delta = (float) (2048 + myu) * (float) (1 << (Rb - eee - 11));
		DistortionCal(&Dis, CbD_, width, height1, col1stepCb, row1stepCb);
		Cblk->Distortion[0] = (float) (Delta * Dis);
	}

	row1stepf <<= 1;
	row1stepCb <<= 2;
	enc->p = 0;
	for (pass = 0; pass < endpass; pass++) {
		one     = 1 << bittop;
		bitmask = mask2[bittop];
		switch (pass % 3) {
			case 1:
				CX_ = &enc->cx[enc->p];
				P = enc_sigpass(CX_, flag_, Codec->CbData, ctx_table, width, height, row1stepf, row1stepCb,
				                bittop, bitmask);
				enc->p += P;
				break;
			case 2:
				CX_ = &enc->cx[enc->p];
				P   = enc_magpass(CX_, flag_, Codec->CbData, width, height, row1stepf, row1stepCb, bittop,
                                bitmask);
				enc->p += P;
				break;
			case 0:
				CX_ = &enc->cx[enc->p];
				P   = enc_clnpass(CX_, flag_, Codec->CbData, ctx_table, SIGN, width, height, row1stepf,
                                row1stepCb, bittop, bitmask);
				enc->p += P;
				bittop--;
				break;
		}
		Cblk->Pass_Addr[pass] = enc->p;
		if ((EECod->RateMode == RATE_CONTROL_ByteLimit) || (EECod->RateMode == RATE_CONTROL_RateValue)) {
			DistortionCal((&Dis), CbD_, width, height1, col1stepCb, row1stepCb);
			Cblk->Distortion[pass + 1] = (float) (Delta * (Dis));
		}
	}

	if ((EECod->RateMode == RATE_CONTROL_ByteLimit) || (EECod->RateMode == RATE_CONTROL_RateValue))
		Cblk->Distortion[endpass] = 0.0;

	// MQ
	Lazy = 0;
	if (EECod->CbBypass == 4) {
		Lazy_Start = 10;
		numPasses  = 10;
	} else if (EECod->CbBypass == 3) {
		Lazy_Start = 7;
		numPasses  = 7;
	} else if (EECod->CbBypass == 2) {
		Lazy_Start = 4;
		numPasses  = 4;
	} else if (EECod->CbBypass == 1) {
		Lazy_Start = 1;
		numPasses  = 1;
	} else {
		Lazy_Start = endpass;
		numPasses  = Cblk->numPasses;
	}
	if (numPasses > Cblk->numPasses) numPasses = Cblk->numPasses;

	if (EECod->CbTerm) {
		CX_ = &enc->cx[0];
		for (pass = 0; pass < numPasses; pass++, CX_ += Length) {
			mqenc_init(enc);
			mqindex_init(enc);
			Length        = Cblk->Layer_Length[pass + 1];
			enc->total_CT = 0;
			if (Length) {
				str = MQ_Enc(str, enc, QeIndexTable_, CX_, Length);
			}
			str = mqenc_flush(str, enc, MQENC_DEFTERM1);
		}
	} else {
		CX_ = &enc->cx[0];
		mqenc_init(enc);
		mqindex_init(enc);
		Length = enc->p;
		str    = MQ_Enc(str, enc, QeIndexTable_, CX_, Length);
		str    = mqenc_flush(str, enc, MQENC_DEFTERM1);
	}

	Cblk->length     = str->total_p;
	Cblk->numSegment = 1;
	Cblk->s          = str;
	return EXIT_SUCCESS;
}

byte4 T1Enc_HTJ2K(struct J2kParam_s *J2kParam, struct Codec_s *Codec, struct Cblk_s *Cblk,
                  struct EECod_s *EECod, byte4 eee, byte4 Rb, byte4 myu, char G,
                  /*struct mqenc_s *enc,*/ char shift) {
	float Delta;
	byte4 width, height;
	byte4 height1, height2;
	byte4 x0, x1, y0, y1;
	byte4 one;
	double Dis;
	char bittop, bitend = 0;
	char endpass;
	byte4 CbData_OR;
	byte4 P;
	byte4 melc_rraw_Length;
	struct RRawCodec_s *codecR;
	struct MelCodec_s *codecM;
	struct fRawCodec_s *codecf;
	struct StreamChain_s *str_rraw;
	struct StreamChain_s *str_melc;
	struct StreamChain_s *str_fraw;

	CbData_OR = BitTopSearch(Cblk) & 0x7fffffff;
	one       = 0x40000000;
	for (bittop = 30; bittop >= 0; bittop--, one >>= 1) {
		if (CbData_OR & one) break;
	}
	one = 1;

	bitend = 0;
	if (bittop == -1) {
		Cblk->Nb         = Cblk->zerobits;
		endpass          = 0;
		Cblk->length     = 0;
		Cblk->NoStream   = 1;
		Cblk->firstLayer = -1;
		Cblk->Distortion = NULL;
		Cblk->RdSlope    = NULL;
		Cblk->s          = NULL;
		return EXIT_SUCCESS;
	}

	Cblk->Mb         = (ubyte2) (G + eee - 1);
	Cblk->zerobits   = (char) (Cblk->Mb + shift - 1);
	endpass          = (uchar) ((bittop - bitend) * 3 + 1);
	Cblk->NoStream   = 0;
	Cblk->firstLayer = 0x00;
	Cblk->Nb         = (ubyte2) (Cblk->zerobits + bittop + 1);
	Cblk->numPasses  = 1;

	if ((EECod->RateMode == RATE_CONTROL_ByteLimit) || (EECod->RateMode == RATE_CONTROL_RateValue)) {
		if (NULL == (Cblk->Distortion = new float[endpass + 1])) {
			printf("[T1Enc]::Cblk.Distortion[%d]\n", (endpass + 1));
			return NULL;
		}
		if (NULL == (Cblk->RdSlope = new float[endpass + 1])) {
			printf("[T1Enc]::Cblk.RdSlope[%d]\n", (endpass + 1));
			return NULL;
		}
		memset(&(Cblk->RdSlope[0]), 0, sizeof(float) * (endpass + 1));
		memset(Cblk->Distortion, 0, sizeof(float) * (endpass + 1));
	} else {
		Cblk->Distortion = NULL;
		Cblk->RdSlope    = NULL;
	}
	if (nullptr == (str_rraw = StreamChainMake(NULL, 4096, JPEG2000))) {
		printf("[T1Enc]::StreamChainMake str_rraw[CB_BUF_LENGTH] malloc error\n");
		return NULL;
	}
	str_rraw->cur_p = str_rraw->buf_length - 1;

	if (nullptr == (str_fraw = StreamChainMake(NULL, 16384, JPEG2000))) {
		printf("[T1Enc]::StreamChainMake str_rraw[CB_BUF_LENGTH] malloc error\n");
		return NULL;
	}
	if (nullptr == (str_melc = StreamChainMake(NULL, 4096, JPEG2000))) {
		printf("[T1Enc]::StreamChainMake str_rraw[CB_BUF_LENGTH] malloc error\n");
		return NULL;
	}

	width  = Cblk->tcbx1 - Cblk->tcbx0;
	height = Cblk->tcby1 - Cblk->tcby0;
	if (height % 4) {
		height1 = height / 4 + 1;
		height2 = height1 * 4 - height;
	} else {
		height1 = height / 4;
		height2 = 0;
	}

	// flag
	x0 = Cblk->tcbx0 - Cblk->Band->tbx0;
	x1 = Cblk->tcbx1 - Cblk->Band->tbx0;
	y0 = Cblk->tcby0 - Cblk->Band->tby0;
	y1 = Cblk->tcby1 - Cblk->Band->tby0;

	memset(Codec->Image_mex->Pdata, 0, sizeof(uchar) * Codec->Image_mex->numData);
	if ((EECod->RateMode == RATE_CONTROL_ByteLimit) || (EECod->RateMode == RATE_CONTROL_RateValue)) {
		if ((11 + eee - Rb) > 0)
			Delta = (float) (2048 + myu) / (float) (1 << (11 + eee - Rb));
		else if ((11 + eee - Rb) == 0)
			Delta = (float) (2048 + myu);
		else
			Delta = (float) (2048 + myu) * (float) (1 << (Rb - eee - 11));
		DistortionCal(&Dis, (byte4 *) Codec->CbData->data, width, height1, Codec->CbData->col1step,
		              Codec->CbData->row1step);
		Cblk->Distortion[0] = (float) (Delta * Dis);
	}

	Cblk->p = 31 - Cblk->Mb;
	codecR  = Codec->codecR;
	codecf  = Codec->codecf;
	codecM  = Codec->codecM;
	EncfRawStart(codecf);
	EncRRawStart(codecR);
	EncMelStart(codecM);
	//	enc->p=0;
	if (EXIT_FAILURE
	    == encode_cln(J2kParam, Cblk, Codec->Image_mex, width, height, codecR, codecM, codecf, str_rraw,
	                  str_melc, str_fraw)) {
		printf("[T1Enc_HTJ2K]:: encode_cln error\n");
		return EXIT_FAILURE;
	}
	str_fraw = terminate(str_fraw, codecf);
	str_rraw = terminate(str_rraw, codecR);
	str_melc = terminate(str_melc, codecM);

	// rraw & melc OverLap Control
	if ((codecR->ctreg) && (codecM->ctreg)) {
		if ((codecR->ctreg <= 4) && (codecM->ctreg <= 4)) {
			str_melc->buf[str_melc->cur_p - 1] |= str_rraw->buf[str_rraw->cur_p + 1];
			str_rraw->buf[str_rraw->cur_p + 1] = 0x00;
			str_rraw->cur_p++;
		}
	}
	Cblk->fraw_Length = str_fraw->cur_p;
	Cblk->melc_Length = str_melc->cur_p;
	Cblk->rraw_Length = str_rraw->buf_length - str_rraw->cur_p - 1;
	melc_rraw_Length  = (str_rraw->buf_length - str_rraw->cur_p - 1) + str_melc->cur_p;
	str_rraw->buf[str_rraw->buf_length - 2] &= 0xf0;
	str_rraw->buf[str_rraw->buf_length - 2] |= (melc_rraw_Length & 0xf);
	str_rraw->buf[str_rraw->buf_length - 1] = (uchar) ((melc_rraw_Length >> 4) & 0xff);

	P                = Cblk->fraw_Length + Cblk->melc_Length + Cblk->rraw_Length;
	Cblk->length     = P;
	Cblk->numSegment = 1;

	Cblk->str_fraw = str_fraw;
	Cblk->str_rraw = str_rraw;
	Cblk->str_melc = str_melc;
	if (Cblk->length) {
		Cblk->s = StreamChainMake(NULL, Cblk->length, JPEG2000);
		memcpy(&Cblk->s->buf[0], str_fraw->buf, sizeof(uchar) * Cblk->fraw_Length);
		memcpy(&Cblk->s->buf[Cblk->fraw_Length], str_melc->buf, sizeof(uchar) * Cblk->melc_Length);
		memcpy(&Cblk->s->buf[Cblk->fraw_Length + Cblk->melc_Length], &str_rraw->buf[str_rraw->cur_p + 1],
		       sizeof(uchar) * Cblk->rraw_Length);
		Cblk->s->cur_p   = P;
		Cblk->s->total_p = P;
	} else {
		Cblk->s = nullptr;
	}
	return EXIT_SUCCESS;
}

//#if Cplus
void DistortionCal(double *Dis, byte4 *d2_, byte4 width, byte4 height1, byte4 col1stepCb,
                   byte4 row1stepCb) {
	byte4 D;
	byte4 i, j;
	byte8 Dis1;

	row1stepCb /= 4;
	Dis1 = 0;
	for (j = 0; j < height1; j += 4) {
		for (i = 0; i < width; i++) {
			D = d2_[(4 * j + 0) * col1stepCb + i * row1stepCb] & 0x7fffffff;
			Dis1 += (byte8) (D * D);
			D = d2_[(4 * j + 1) * col1stepCb + i * row1stepCb] & 0x7fffffff;
			Dis1 += (byte8) (D * D);
			D = d2_[(4 * j + 2) * col1stepCb + i * row1stepCb] & 0x7fffffff;
			Dis1 += (byte8) (D * D);
			D = d2_[(4 * j + 3) * col1stepCb + i * row1stepCb] & 0x7fffffff;
			Dis1 += (byte8) (D * D);
		}
	}
	*Dis = (double) sqrt((double) Dis1);
}
//#endif
