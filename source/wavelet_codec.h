/*
Copyright (c) 2000-2021, ICT-Link
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the HTJ2K project.
*/

#ifndef wavelet_codec_h
#define wavelet_codec_h

#include "j2k2.h"
#include "ImageUtil.h"
#include "asmasm.h"

#define WAVE_DEBUG01 0 // V
#define WAVE_DEBUG02 0 // H
#define WAVE_DEBUG03 0 // ENC Q-I
#define WAVE_DEBUG14 0 // ICC/YUV - WAVE
#define WAVE_DEBUG13 0 // 97WAVE - 97Q

#define WAVE_DEBUG4 0 // Q
#define WAVE_DEBUG9 0 //
#define WAVE_DEBUG5 0 // H97
#define WAVE_DEBUG6 0 // H97
#define WAVE_DEBUG7 0 // V97
#define WAVE_DEBUG8 0 // V97

/********************************************************************************************************/
/********************************************************************************************************/
/************************constant************************************************************************/
/********************************************************************************************************/
/********************************************************************************************************/
#define Filter53 1
#define Filter97 0

//#if Cplus
#define ALPHA 1.586134342059924
#define BETA 0.052980118572961
#define GANMMA 0.882911075530934
#define DELTA 0.443506852043971
#define KAPPA 1.230174104914001
//#endif

/********************************************************************************************************/
/********************************************************************************************************/
/************************structure**********************************************************************/
/********************************************************************************************************/
/********************************************************************************************************/

typedef struct wavelet_s {
	struct Image_s *wave;
	struct Image_s *ll_d;
	struct Image_s *MaskForROI;
	char numBands; // lev=0 lowest level
	char NL;       //
} wavelet_t;

void Wavelet_Create(struct wavelet_s *wave1, byte4 width, byte4 height, char NL);
byte4 Wavelet_Destory(struct wavelet_s *wave1);
void WaveAdrCal(struct wavelet_s *Wave, /*struct Tile_s *t,*/ struct Image_s *Image, struct EECod_s *EECod,
                uchar FlagFlag);
void Wavelet_Init(struct wavelet_s *wave);

byte4 WaveEnc(struct J2kParam_s *J2kParam, /*ubyte2 ccc,*/ struct wavelet_s *wavelet_enc,
              struct EECod_s *EECod, struct EEQcd_s *EEQcd);
void Enc_V53(struct Image_s *image);
void Enc_H53(struct Image_s *image);
void Enc_Quantize(struct Image_s *image, char e, ubyte2 myu, char Rb);
// void	Enc_Quantize_HW( struct Image_s *image, char e, ubyte2 myu, char Rb, char DecimalPoint, uchar bbb );

void WaveDec(struct EECod_s *EECod, struct EEQcd_s *EEQcd,
             struct wavelet_s *Wave /*, byte4 T, ubyte2 ccc*/);
void DecWaveIL_NL0(struct Image_s *image, struct Image_s *ll, struct EEQcd_s *EEQcd);
void DecWaveIL53(struct Image_s *image, byte4 lll, struct Image_s *ll_d, struct Image_s *ll,
                 struct Image_s *hl, struct Image_s *lh, struct Image_s *hh, struct EEQcd_s *EEQcd);
void Dec_H53(struct Image_s *image);
void Dec_V53(struct Image_s *image);
void DecWaveIL97(struct Image_s *image, byte4 lll, struct Image_s *ll_d, struct Image_s *ll,
                 struct Image_s *hl, struct Image_s *lh, struct Image_s *hh /*, struct EEQcd_s *EEQcd*/);
void Dec_Wave97_Q(struct Image_s *image, /*struct Image_s *ll_d0,*/ byte4 lll,
                  struct EEQcd_s *EEQcd /*, byte4 T, ubyte2 ccc*/);
void Dec_H97(struct Image_s *image);
void Dec_V97(struct Image_s *image);

int Tbxy(int tcxy, int lll, int xo);

void Dec_H53_2(struct Image_s *image);
void Dec_V53_2(struct Image_s *image);
#if Cplus
void Enc_V53_2(struct Image_s *image);
void Enc_H53_2(struct Image_s *image);
void Enc_WaveIL(struct Image_s *image, byte4 lll, struct Image_s *ll_d, struct Image_s *ll,
                struct Image_s *hl, struct Image_s *lh, struct Image_s *hh);
char Enc_WaveIL_G(struct Image_s *Band, byte2 e, char G);
// void Dec_V53_2(struct Image_s *image);
// void Dec_H53_2(struct Image_s *image);
void Enc_V97(struct Image_s *image);
void Enc_H97(struct Image_s *image);
void Enc_V97_HW(struct J2kParam_s *J2kParam, struct Image_s *image, char rrr, char NL);
// void	Enc_H97_HW( struct J2kParam_s *J2kParam, struct Image_s *image, char rrr );

void Dec_V97_2(struct Image_s *image);
void Dec_H97_2(struct Image_s *image); // for width is 2
void Enc_H97_3(struct Image_s *image);
void Dec_H97_3(struct Image_s *image); // for width is 3,4,***
#else
#	ifdef __cplusplus
extern "C" {
extern void Enc_V53_2(struct Image_s *image);
extern void Enc_H53_2(struct Image_s *image);
extern void Enc_WaveIL(struct Image_s *image, byte4 lll, struct Image_s *ll_d, struct Image_s *ll,
                       struct Image_s *hl, struct Image_s *lh, struct Image_s *hh);
char Enc_WaveIL_G(struct Image_s *Band, byte2 e, char G);
// extern void Dec_V53_2(struct Image_s *image);
// extern void Dec_H53_2(struct Image_s *image);
extern void DecWaveIL53_1(struct Image_s *image, char Delta);
extern void DecWaveIL53_30(struct Image_s *image, struct Image_s *ll_d, struct Image_s *hl,
                           struct Image_s *lh, struct Image_s *hh);
extern void Enc_V97(struct Image_s *image);
extern void Enc_H97(struct Image_s *image);
extern void Dec_V97_2(struct Image_s *image);
extern void Dec_H97_2(struct Image_s *image);
extern void DecWaveIL97_1(struct Image_s *image);
extern void DecWaveIL97_2(struct Image_s *image, struct Image_s *ll_d, struct Image_s *hl,
                          struct Image_s *lh, struct Image_s *hh);
extern void Dec_Q(struct Image_s *image, float deltaLL, float deltaHL, float deltaLH, float deltaHH);
}
#	endif
#endif

#if ICT_Link09
void WaveDebug(struct wavelet_s *Wave, char lll);
#endif

#endif
