/*
Copyright (c) 2000-2021, ICT-Link
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the HTJ2K project.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "j2k2.h"
#include "ImageUtil.h"
#include "wavelet_codec.h"
#include "t2codec.h"
#include "annexB.h"

//[Func 29]
byte4 PrcAdrCal(struct Tile_s *t, struct wavelet_s *wave1, ubyte2 ccc,
                struct EECod_s *EECod /*, uchar FlagFlag*/) {
	byte4 width, height;
	byte4 trx0, trx1, try0, try1;
	byte4 kkk, x, y;
	byte4 xstart, ystart, tpy0, tpy1;
	byte4 temp;
	struct EEPrc_s *EEPrc;
	struct Prc_s *Prc;
	struct Image_s *wave, *ll_d;
	byte2 numPrcwidth, numPrcheight, numPrc, ppp;
	char rrr, bbb;

	if (nullptr == (t->Cmpt[ccc].EEPrc = new struct EEPrc_s[EECod->DecompLev + 1])) {
		printf("[PrcAdrCal:: EEPrc_s[%d] malloc error]\n", (EECod->DecompLev + 1));
		return EXIT_FAILURE;
	}

	EEPrc = t->Cmpt[ccc].EEPrc;
	for (rrr = 0; rrr <= EECod->DecompLev; rrr++) {
		width  = (1 << EECod->PrcW[rrr]);
		height = (1 << EECod->PrcH[rrr]);
		temp   = EECod->DecompLev - rrr; // denominator of fomula B-14
		if (temp < 0) {
			printf("[PrcAdrCal:: EECod->Decomplev-rrr=%d should be positive.]\n", temp);
			return EXIT_FAILURE;
		}

		if (temp <= 0x20) { // B.14
			temp = 1 << temp;
			trx0 = ceil2(t->Cmpt[ccc].tcx0, temp);
			trx1 = ceil2(t->Cmpt[ccc].tcx1, temp);
			try0 = ceil2(t->Cmpt[ccc].tcy0, temp);
			try1 = ceil2(t->Cmpt[ccc].tcy1, temp);
		} else {
			trx0 = 0;
			trx1 = 0;
			try0 = 0;
			try1 = 0;
		}

		// B.16
		if (trx1 > trx0)
			numPrcwidth = (byte2) (ceil2(trx1, width) - floor2(trx0, width));
		else
			numPrcwidth = 0;
		if (try1 > try0)
			numPrcheight = (byte2) (ceil2(try1, height) - floor2(try0, height));
		else
			numPrcheight = 0;
		numPrc = (byte2) (numPrcwidth * numPrcheight);
		if (numPrc) {
			if (nullptr == (EEPrc[rrr].EPrc = new struct EPrc_s[numPrc])) {
				printf("[PrcAdrCal]:: EPrc_s[%d] malloc error\n", numPrc);
				return EXIT_FAILURE;
			}
		}
		EEPrc[rrr].numPrcx = numPrcwidth;
		EEPrc[rrr].numPrcy = numPrcheight;
		EEPrc[rrr].numPrc  = numPrc;
		if (rrr == 0)
			EEPrc[rrr].numBand = 1;
		else
			EEPrc[rrr].numBand = 3;
		for (ppp = 0; ppp < numPrc; ppp++) {
			if (nullptr == (EEPrc[rrr].EPrc[ppp].Prc = new struct Prc_s[EEPrc[rrr].numBand])) {
				printf("[PrcAdrCal]:: Prc_s[%d] malloc error\n", EEPrc[rrr].numBand);
				return EXIT_FAILURE;
			}
		}
	}

	EEPrc = t->Cmpt[ccc].EEPrc;
	for (rrr = 0; rrr <= EECod->DecompLev; rrr++) {
		if (rrr == 0) {
			width  = (1 << EECod->PrcW[rrr]);
			height = (1 << EECod->PrcH[rrr]);
			ll_d   = &(wave1->ll_d[rrr]);
			xstart = ll_d->tbx0 / width;
			ystart = ll_d->tby0 / height;
		} else {
			width  = (1 << (EECod->PrcW[rrr] - 1));
			height = (1 << (EECod->PrcH[rrr] - 1));
			ll_d   = &(wave1->ll_d[rrr]);
			xstart = ll_d->tbx0 / (width * 2);
			ystart = ll_d->tby0 / (height * 2);
		}
		for (bbb = 0; bbb < EEPrc[rrr].numBand; bbb++) {
			if (rrr == 0)
				wave = &(wave1->wave[0]);
			else
				wave = &(wave1->wave[(rrr - 1) * 3 + bbb + 1]);
			for (y = 0; y < EEPrc[rrr].numPrcy; y++) {
				if (y == 0)
					tpy0 = (byte4) (wave->tby0);
				else
					tpy0 = (ystart + y) * height;
				if (y == (byte4) (EEPrc[rrr].numPrcx - 1))
					tpy1 = wave->tby1 < (ystart + y + 1) * height ? wave->tby1 : (ystart + y + 1) * height;
				else
					tpy1 = wave->tby1 < (ystart + y + 1) * height ? wave->tby1 : (ystart + y + 1) * height;
				for (x = 0; x < EEPrc[rrr].numPrcx; x++) {
					kkk       = y * EEPrc[rrr].numPrcx + x;
					Prc       = &(EEPrc[rrr].EPrc[kkk].Prc[bbb]);
					Prc->Band = (struct Image_s *) wave;
					Prc->tpy0 = tpy0;
					Prc->tpy1 = tpy1;
					Prc->f    = ZERO;
					if (x == 0)
						Prc->tpx0 = (byte4) (wave->tbx0);
					else
						Prc->tpx0 =
						    ((xstart + x) * width) > wave->tbx1 ? wave->tbx1 : ((xstart + x) * width);
					if (x == (byte4) (EEPrc[rrr].numPrcx - 1))
						Prc->tpx1 =
						    wave->tbx1 < (xstart + x + 1) * width ? wave->tbx1 : (xstart + x + 1) * width;
					else
						Prc->tpx1 =
						    wave->tbx1 < (xstart + x + 1) * width ? wave->tbx1 : (xstart + x + 1) * width;
				}
			}
		}
	}

	return EXIT_SUCCESS;
}

void PrcDestory(struct Tile_s *t, struct ECod_s *ECod, char ENC_DEC) {
	byte4 ccc, rrr, kkk;
	char bbb;
	for (ccc = 0; ccc < t->numCompts; ccc++) {
		for (rrr = 0; rrr < ECod->EECod[t->T].DecompLev + 1; rrr++) {
			for (kkk = 0; kkk < t->Cmpt[ccc].EEPrc->numPrc; kkk++) {
				for (bbb = 0; bbb < t->Cmpt[ccc].EEPrc[rrr].numBand; bbb++) {
					CblkDestory(&t->Cmpt[ccc].EEPrc[rrr].EPrc[kkk].Prc[bbb], ENC_DEC);
				}
				delete[] t->Cmpt[ccc].EEPrc[rrr].EPrc[kkk].Prc;
			}
			delete[] t->Cmpt[ccc].EEPrc[rrr].EPrc;
		}
		delete[] t->Cmpt[ccc].EEPrc;
	}
}

//[Func 30]
byte4 CblkAdrCal(/*struct J2kParam_s *J2kParam,*/ struct Codec_s *Codec, struct EEPrc_s *EEPrc,
                 struct EECod_s *EECod, struct EEQcd_s *EEQcd /*, ubyte2 ccc, uchar FlagFlag*/) {
	byte4 ppp, numPrc, numCblk, numCblkx, numCblky, kkk, x, y;
	byte4 xstart, ystart, cby0, cby1;
	struct EPrc_s *EPrc;
	struct Cblk_s *Cblk;
	ubyte2 height, width;
	char b2, bbb;
	char rrr;

	height = (ubyte2) (1 << (EECod->CbH + 2));
	width  = (ubyte2) (1 << (EECod->CbW + 2));
	for (rrr = 0; rrr <= EECod->DecompLev; rrr++) {
		numPrc = EEPrc[rrr].numPrc;
		for (ppp = 0; ppp < numPrc; ppp++) {
			EPrc = &(EEPrc[rrr].EPrc[ppp]);
			for (bbb = 0; bbb < EEPrc[rrr].numBand; bbb++) {
				if (rrr == 0)
					b2 = 0;
				else
					b2 = (char) ((rrr - 1) * 3 + 1 + bbb);
				if (EPrc->Prc[bbb].tpx1 > EPrc->Prc[bbb].tpx0)
					numCblkx = ceil2(EPrc->Prc[bbb].tpx1, width) - EPrc->Prc[bbb].tpx0 / width;
				else
					numCblkx = 1;
				if (EPrc->Prc[bbb].tpy1 > EPrc->Prc[bbb].tpy0)
					numCblky = ceil2(EPrc->Prc[bbb].tpy1, height) - EPrc->Prc[bbb].tpy0 / height;
				else
					numCblky = 1;
				numCblk                 = numCblkx * numCblky;
				EPrc->Prc[bbb].numCblk  = numCblk;
				EPrc->Prc[bbb].numCblkx = numCblkx;
				EPrc->Prc[bbb].numCblky = numCblky;
				if (nullptr == (EPrc->Prc[bbb].Cblk = CreateCblk(numCblk, rrr, bbb, Codec, EECod))) {
					printf("[CblkAdrCal] CreateCblk error\n");
					return EXIT_FAILURE;
				}
				xstart = EPrc->Prc[bbb].tpx0 >> (EECod->CbW + 2);
				ystart = EPrc->Prc[bbb].tpy0 >> (EECod->CbH + 2);
				for (y = 0; y < numCblky; y++) {
					cby0 = EPrc->Prc[bbb].tpy0 > (ystart + y) * height ? EPrc->Prc[bbb].tpy0
					                                                   : (ystart + y) * height;
					cby1 = EPrc->Prc[bbb].tpy1 < (ystart + y + 1 /*numCblky*/) * height
					           ? EPrc->Prc[bbb].tpy1
					           : (ystart + y + 1 /*numCblky*/) * height;
					for (x = 0; x < numCblkx; x++) {
						kkk            = y * EPrc->Prc[bbb].numCblkx + x;
						Cblk           = &(EPrc->Prc[bbb].Cblk[kkk]);
						Cblk->Band     = EPrc->Prc[bbb].Band;
						Cblk->tcby0    = cby0;
						Cblk->tcby1    = cby1;
						Cblk->included = 0;
						if (rrr == 0)
							Cblk->Mb = (byte2) (EEQcd->G + EEQcd->e[0] - 1);
						else
							Cblk->Mb = (byte2) (EEQcd->G + EEQcd->e[(rrr - 1) * 3 + bbb + 1] - 1);
						Cblk->tcbx0 = EPrc->Prc[bbb].tpx0 > (xstart + x) * width ? EPrc->Prc[bbb].tpx0
						                                                         : (xstart + x) * width;
						Cblk->tcbx1 = EPrc->Prc[bbb].tpx1 < (xstart + x + 1) * width
						                  ? EPrc->Prc[bbb].tpx1
						                  : (xstart + x + 1) * width;
					}
				}
			}
		}
	}
	return EXIT_SUCCESS;
}

struct Cblk_s *CreateCblk(byte4 numCblk, char rrr, char bbb, struct Codec_s *Codec, struct EECod_s *EECod) {
	struct Cblk_s *Cb, *Cbs, *Cbe;
	char bbb2;
	byte4 kkk;

	if (rrr == 0)
		bbb2 = 0;
	else {
		if (bbb == 0)
			bbb2 = (char) (rrr * 3 - 2);
		else if (bbb == 1)
			bbb2 = (char) (rrr * 3 - 1);
		else
			bbb2 = (char) (rrr * 3);
	}

	if (numCblk <= 0) {
		printf("[CreateCblk:: numCblk=%d should be positive.]\n", numCblk);
		return nullptr;
	}
	if (nullptr == (Cb = new struct Cblk_s[numCblk])) {
		printf("[CreateCblk]:: Cblk_s malloc error]\n");
		return nullptr;
	}

	Cbs = &Cb[0];
	Cbe = &Cb[numCblk];
	for (kkk = 0; kkk < numCblk; ++Cbs, kkk++) {
		Cbs->Segments_max    = 64;
		Cbs->flags           = nullptr;
		Cbs->length          = 0;
		Cbs->s_adr           = 0;
		Cbs->Lblock          = 3;
		Cbs->numPasses       = 0;
		Cbs->numSegment      = 0;
		Cbs->Last_numSegment = 0;
		Cbs->firstLayer      = 0;
		Cbs->Nb              = 0;
		Cbs->Mb              = 0;
		Cbs->zerobits        = 0;
		Cbs->decOn           = 0;
		Cbs->NoStream        = 2;
		Cbs->HT_firstCleanUP = 0;
		Cbs->s_blk           = 0;
		Cbs->total_numPasses = 0;
		Cbs->s_blk_flag      = 0;
		Cbs->HT_mode         = 0x00;
		if (Codec->Siz->Rsiz & 0x4000 && Codec->Cap != nullptr)
			Cbs->HT_mode = (char) (0x90 | Codec->Cap->HTmode);
		else if (Codec->Siz->Rsiz & 0x4000 && Codec->Cap == nullptr)
			Cbs->HT_mode |= HT_Mode;
		else
			Cbs->HT_mode = (char) 0;

		if (EECod->numLayer <= 0) {
			printf("[CreateCblk]:: EECod->numLayer should be positive %d\n", EECod->numLayer);
			return nullptr;
		}
		if (nullptr == (Cbs->Layer_Saddr = new byte4[EECod->numLayer])) {
			printf("[CreateCblk]:: Cbs->Layer_Saddr malloc error. EECod->numLayer=%d\n", EECod->numLayer);
			return nullptr;
		}
		if (nullptr == (Cbs->Layer_Length = new byte4[EECod->numLayer])) {
			printf("[CreateCblk]:: Cbs->Layer_Length malloc error. EECod->numLayer=%d\n", EECod->numLayer);
			return nullptr;
		}
		if (nullptr == (Cbs->Layer_passes = new ubyte2[EECod->numLayer])) {
			printf("[CreateCblk]:: Cbs->Layer_passes malloc error. EECod->numLayer=%d\n", EECod->numLayer);
			return nullptr;
		}
		memset(Cbs->Layer_Saddr, 0, sizeof(byte4) * EECod->numLayer);
		memset(Cbs->Layer_Length, 0, sizeof(byte4) * EECod->numLayer);
		memset(Cbs->Layer_passes, 0, sizeof(ubyte2) * EECod->numLayer);
		if (Cbs->Segments_max <= 0) {
			printf("[CreateCblk]:: Cbs->Segments_max should be positive %d\n", Cbs->Segments_max);
			return nullptr;
		}
		if (nullptr == (Cbs->Seg_Saddr = new byte4[Cbs->Segments_max])) {
			printf("[CreateCblk]:: Cbs->Seg_Saddr malloc error. EECod->numLayer=%d\n", EECod->numLayer);
			return nullptr;
		}
		if (nullptr == (Cbs->Seg_Length = new byte4[Cbs->Segments_max])) {
			printf("[CreateCblk]:: Cbs->Seg_Length malloc error. EECod->numLayer=%d\n", EECod->numLayer);
			return nullptr;
		}
		if (nullptr == (Cbs->Seg_passes = new ubyte2[Cbs->Segments_max])) {
			printf("[CreateCblk]:: Cbs->Seg_passes malloc error. EECod->numLayer=%d\n", EECod->numLayer);
			return nullptr;
		}
		memset(Cbs->Seg_Saddr, 0, sizeof(byte4) * Cbs->Segments_max);
		memset(Cbs->Seg_Length, 0, sizeof(byte4) * Cbs->Segments_max);
		memset(Cbs->Seg_passes, 0, sizeof(ubyte2) * Cbs->Segments_max);
	}
	return Cb;
}

void CblkDestory(struct Prc_s *Prc, char ENC_DEC) {
	struct Cblk_s *Cbs;
	byte4 numCblk;

	numCblk = Prc->numCblk;
	for (Cbs = &Prc->Cblk[0]; Cbs != &Prc->Cblk[numCblk]; ++Cbs) {
		if (ENC_DEC == ENC) StreamChainDestory(Cbs->s);

		if (Cbs->Layer_Saddr != nullptr) {
			delete[] Cbs->Layer_Saddr;
		}
		if (Cbs->Layer_Length != nullptr) {
			delete[] Cbs->Layer_Length;
		}

		if (Cbs->flags) {
		}
	}
	delete[] Prc->Cblk;
}

byte4 PacketCreate(struct Tile_s *Tile, struct ECod_s *ECod, char ENCDEC) {
	byte4 numPackets;
	byte4 numPricincts;
	byte2 ccc;
	byte4 ppp;
	uchar rrr;

	numPackets = 0;
	for (ccc = 0; ccc < Tile->numCompts; ccc++) {
		for (rrr = 0; rrr <= ECod->EECod[ccc * ECod->numTiles + Tile->T].DecompLev; rrr++) {
			numPricincts = (Tile->Cmpt[ccc].EEPrc[rrr].numPrcx * Tile->Cmpt[ccc].EEPrc[rrr].numPrcy);
			numPricincts *= ECod->EECod[ccc * ECod->numTiles + Tile->T].numLayer;
			numPackets += numPricincts;
		}
	}
	if (numPackets <= 0) {
		printf("[PacketCreate]:: numPackets should be positive. numPackets=%d\n", numPackets);
		return EXIT_FAILURE;
	}

	//	printf("[PacketCreate] numPackets=%d\n", numPackets );
	if (nullptr == (Tile->Packet = new struct Packet_s[numPackets])) {
		printf("[PacketCreate]:: Tile->Packet malloc error. \n");
		return EXIT_FAILURE;
	}
	Tile->numPackets = numPackets;
	for (ppp = 0; ppp < numPackets; ppp++) {
		if (ENCDEC) {
			if (nullptr == (Tile->Packet[ppp].s = StreamChainMake(nullptr, PACKETINFOLENGTH, JPEG2000))) {
				printf("[PacketCreate]:: Tile->Packet[%d].s malloc error. \n", ppp);
				return EXIT_FAILURE;
			}
		} else {
			Tile->Packet[ppp].s = nullptr;
		}
		Tile->Packet[ppp].packetNo        = 0;
		Tile->Packet[ppp].SadrPI          = 0;
		Tile->Packet[ppp].EadrPI          = 0;
		Tile->Packet[ppp].Sadr_PacketCode = 0;
		Tile->Packet[ppp].Eadr_PacketCode = 0;
	}
	//	printf("[PacketCreate] complete. numPackets=%d\n", numPackets );
	return numPackets;
}

void PacketDestory(struct Tile_s *t) {
	byte4 i;
	for (i = 0; i < t->numPackets; i++)
		StreamChainDestory(t->Packet[i].s);
	delete[] t->Packet;
}

//[Func 33]
byte4 ProgCnt(struct Codec_s *Codec, struct Tile_s *Tile, uchar tilePartNo, struct wavelet_s *Wave,
              struct ECod_s *ECod, byte2 *Cppp, uchar *flagD, uchar Control) {
	struct Siz_s *Siz;
	struct Poc_s *Poc;
	byte4 PacketsCount;
	byte4 maxnumPrc;
	byte4 colpstep, colrstep, colcstep, colkstep;
	ubyte4 PaPa, numberOfPackets;
	byte2 maxnumPasses;
	byte2 ccc;
	ubyte2 CSpoc, CEpoc, LYSpoc, LYEpoc;
	char rrr, maxDecompLev;
	char flag0, flag1;
	uchar RSpoc, REpoc, Ppoc;

	Siz = Codec->Siz;
	Poc = Codec->Poc;

	PacketsCount = 0;
	if (tilePartNo) PacketsCount = Tile->PacketsCount[tilePartNo - 1];

	maxDecompLev = 0;
	maxnumPasses = 0;
	for (ccc = 0; ccc < Codec->Siz->Csiz; ccc++) {
		maxDecompLev = (maxDecompLev > ECod->EECod[ccc * ECod->numTiles + Tile->T].DecompLev)
		                   ? maxDecompLev
		                   : ECod->EECod[ccc * ECod->numTiles + Tile->T].DecompLev;
		maxnumPasses = (maxnumPasses > ECod->EECod[ccc * ECod->numTiles + Tile->T].numLayer)
		                   ? maxnumPasses
		                   : ECod->EECod[ccc * ECod->numTiles + Tile->T].numLayer;
	}
	maxDecompLev++;
	maxnumPrc = 0;
	for (ccc = 0; ccc < Codec->Siz->Csiz; ccc++) {
		for (rrr = 0; rrr <= ECod->EECod[ccc * ECod->numTiles + Tile->T].DecompLev; rrr++) {
			maxnumPrc = (maxnumPrc > Tile->Cmpt[ccc].EEPrc[rrr].numPrc) ? maxnumPrc
			                                                            : Tile->Cmpt[ccc].EEPrc[rrr].numPrc;
		}
	}

	numberOfPackets = Codec->Siz->Csiz * maxDecompLev * maxnumPasses * maxnumPrc;
	colcstep        = 1;                                       // maxDecompLev*maxnumPasses*maxnumPrc;
	colrstep        = Siz->Csiz;                               // maxnumPasses*maxnumPrc;
	colpstep        = Siz->Csiz * maxDecompLev;                // maxnumPrc;
	colkstep        = Siz->Csiz * maxDecompLev * maxnumPasses; // 1;

	if (Control) {
		if ((Codec->Poc != nullptr) && (Codec->tilePoc == nullptr)) {
			flag0 = 1;
		} else {
			flag0 = 0;
		}
		if (flag0) { // no TilePart header POC and Tile header POC is exist.
			for (PaPa = 0; PaPa < Codec->Poc->numProgOrderChange; PaPa++) {
				RSpoc  = Codec->Poc->RSpoc[PaPa];
				REpoc  = Codec->Poc->REpoc[PaPa];
				CSpoc  = Codec->Poc->CSpoc[PaPa];
				CEpoc  = Codec->Poc->CEpoc[PaPa];
				LYSpoc = 0;
				LYEpoc = Codec->Poc->LYEpoc[PaPa];
				Ppoc   = Codec->Poc->Ppoc[PaPa];
				switch (Ppoc) {
					case LRCP: // 0
						PacketsCount =
						    LRCP_cnt(PacketsCount, RSpoc, REpoc, LYSpoc, LYEpoc, CSpoc, CEpoc, Codec, Tile,
						             Wave, flagD, colcstep, colrstep, colpstep, colkstep);
						break;
					case RLCP: // 1
						PacketsCount =
						    RLCP_cnt(PacketsCount, RSpoc, REpoc, LYSpoc, LYEpoc, CSpoc, CEpoc, Codec, Tile,
						             Wave, flagD, colcstep, colrstep, colpstep, colkstep);
						break;
					case RPCL: // 2
						PacketsCount =
						    RPCL_cnt(PacketsCount, RSpoc, REpoc, LYSpoc, LYEpoc, CSpoc, CEpoc, Cppp, Codec,
						             Tile, Wave, flagD, colcstep, colrstep, colpstep, colkstep);
						break;
					case PCRL: // 3
						PacketsCount =
						    PCRL_cnt(PacketsCount, RSpoc, REpoc, LYSpoc, LYEpoc, CSpoc, CEpoc, Cppp, Codec,
						             Tile, Wave, flagD, colcstep, colrstep, colpstep, colkstep);
						break;
					case CPRL: // 4
						PacketsCount =
						    CPRL_cnt(PacketsCount, RSpoc, REpoc, LYSpoc, LYEpoc, CSpoc, CEpoc, Cppp, Codec,
						             Tile, Wave, flagD, colcstep, colrstep, colpstep, colkstep);
						break;
					default:
						printf("[ProgCnt] :: Progresion control change index error. Ppoc=%d \n", Ppoc);
						return EXIT_FAILURE;
						break;
				}
			}
		}
		// This is for "POC" marker in "Tile-header". Tile-header "POC" should be overwritten on main header
		// "POC".
		if (Codec->tilePoc != nullptr) {
			flag1 = 1;
			for (PaPa = 0; PaPa < Codec->tilePoc->numProgOrderChange; PaPa++) {
				RSpoc  = Codec->tilePoc->RSpoc[PaPa];
				REpoc  = Codec->tilePoc->REpoc[PaPa];
				CSpoc  = Codec->tilePoc->CSpoc[PaPa];
				CEpoc  = Codec->tilePoc->CEpoc[PaPa];
				LYSpoc = 0;
				LYEpoc = Codec->tilePoc->LYEpoc[PaPa];
				Ppoc   = Codec->tilePoc->Ppoc[PaPa];
				switch (Ppoc) {
					case LRCP: // 0
						PacketsCount =
						    LRCP_cnt(PacketsCount, RSpoc, REpoc, LYSpoc, LYEpoc, CSpoc, CEpoc, Codec, Tile,
						             Wave, flagD, colcstep, colrstep, colpstep, colkstep);
						break;
					case RLCP: // 1
						PacketsCount =
						    RLCP_cnt(PacketsCount, RSpoc, REpoc, LYSpoc, LYEpoc, CSpoc, CEpoc, Codec, Tile,
						             Wave, flagD, colcstep, colrstep, colpstep, colkstep);
						break;
					case RPCL: // 2
						PacketsCount =
						    RPCL_cnt(PacketsCount, RSpoc, REpoc, LYSpoc, LYEpoc, CSpoc, CEpoc, Cppp, Codec,
						             Tile, Wave, flagD, colcstep, colrstep, colpstep, colkstep);
						break;
					case PCRL: // 3
						PacketsCount =
						    PCRL_cnt(PacketsCount, RSpoc, REpoc, LYSpoc, LYEpoc, CSpoc, CEpoc, Cppp, Codec,
						             Tile, Wave, flagD, colcstep, colrstep, colpstep, colkstep);
						break;
					case CPRL: // 4
						PacketsCount =
						    CPRL_cnt(PacketsCount, RSpoc, REpoc, LYSpoc, LYEpoc, CSpoc, CEpoc, Cppp, Codec,
						             Tile, Wave, flagD, colcstep, colrstep, colpstep, colkstep);
						break;
					default:
						printf("[ProgCnt] :: Progresion control change index error. Ppoc=%d \n", Ppoc);
						return EXIT_FAILURE;
						break;
				}
			}
		} else {
			flag1 = 0;
		}
	} else {
		RSpoc  = 0;
		REpoc  = maxDecompLev;
		CSpoc  = 0;
		CEpoc  = Codec->Siz->Csiz;
		LYSpoc = 0;
		LYEpoc = maxnumPasses;
		Ppoc   = ECod->EECod[Tile->T].ProgOrder;

		switch (ECod->EECod[Tile->T].ProgOrder) {
			case LRCP: // 0
				PacketsCount = LRCP_cnt(PacketsCount, RSpoc, REpoc, LYSpoc, LYEpoc, CSpoc, CEpoc, Codec,
				                        Tile, Wave, flagD, colcstep, colrstep, colpstep, colkstep);
				break;
			case RLCP: // 1
				PacketsCount = RLCP_cnt(PacketsCount, RSpoc, REpoc, LYSpoc, LYEpoc, CSpoc, CEpoc, Codec,
				                        Tile, Wave, flagD, colcstep, colrstep, colpstep, colkstep);
				break;
			case RPCL: // 2
				PacketsCount = RPCL_cnt(PacketsCount, RSpoc, REpoc, LYSpoc, LYEpoc, CSpoc, CEpoc, Cppp,
				                        Codec, Tile, Wave, flagD, colcstep, colrstep, colpstep, colkstep);
				break;
			case PCRL: // 3
				PacketsCount = PCRL_cnt(PacketsCount, RSpoc, REpoc, LYSpoc, LYEpoc, CSpoc, CEpoc, Cppp,
				                        Codec, Tile, Wave, flagD, colcstep, colrstep, colpstep, colkstep);
				break;
			case CPRL: // 4
				PacketsCount = CPRL_cnt(PacketsCount, RSpoc, REpoc, LYSpoc, LYEpoc, CSpoc, CEpoc, Cppp,
				                        Codec, Tile, Wave, flagD, colcstep, colrstep, colpstep, colkstep);
				break;
			default:
				printf("[ProgCnt] :: Progresion control index error. ProgOrder==%d\n",
				       ECod->EECod[Tile->T].ProgOrder);
				return EXIT_FAILURE;
				break;
		}
	}
	if (Control)
		Tile->PacketsCount[tilePartNo] = PacketsCount;
	else {
		if (PacketsCount != Tile->numPackets) {
			printf("[ProgCnt] :: total Packets number error Count=%d Total=%d\n", PacketsCount,
			       Tile->numPackets);
			return EXIT_FAILURE;
		}
	}

	return EXIT_SUCCESS;
}

byte4 LRCP_cnt(byte4 PacketNo, uchar RSpoc, uchar REpoc, byte2 LYSpoc, byte2 LYEpoc, byte2 CSpoc,
               byte2 CEpoc, struct Codec_s *Codec, struct Tile_s *Tile, struct wavelet_s *Wave,
               uchar *flagD, byte4 colcstep, byte4 colrstep, byte4 colpstep, byte4 colkstep) {
	struct ECod_s *ECod;
	byte2 lll, numLayer;
	uchar rrr, numDecompLev;
	byte2 ccc;
	ubyte2 ppp, numPrcs;
	byte4 width_llD, height_llD;
	byte4 i;
	uchar *Ps_ON;

	Ps_ON = new uchar[Codec->Siz->Csiz];
	memset(Ps_ON, 0, sizeof(uchar) * Codec->Siz->Csiz);

	ECod = Codec->ECod;
	i    = PacketNo;
	if (CEpoc > Codec->Siz->Csiz) CEpoc = Codec->Siz->Csiz;
	for (lll = LYSpoc; lll < LYEpoc; lll++) {
		for (rrr = RSpoc; rrr < REpoc; rrr++) {
			for (ccc = CSpoc; ccc < CEpoc; ccc++) {
				numLayer     = ECod->EECod[ccc * ECod->numTiles + Tile->T].numLayer;
				numDecompLev = ECod->EECod[ccc * ECod->numTiles + Tile->T].DecompLev;
				if ((lll < numLayer) && (rrr <= numDecompLev)) {
					width_llD  = Wave[ccc].ll_d[rrr].tbx1 - Wave[ccc].ll_d[rrr].tbx0;
					height_llD = Wave[ccc].ll_d[rrr].tby1 - Wave[ccc].ll_d[rrr].tby0;
					if (!Ps_ON[ccc]) Ps_ON[ccc] = (uchar) ((width_llD * height_llD) ? 1 : 0);
					numPrcs = Tile->Cmpt[ccc].EEPrc[rrr].numPrc;
					for (ppp = 0; ppp < numPrcs; ppp++) {
						if (!flagD[ccc * colcstep + rrr * colrstep + lll * colpstep + ppp * colkstep]) {
							if (Ps_ON[ccc]) {
								Tile->Packet[i].LayerNo  = lll;
								Tile->Packet[i].resoNo   = rrr;
								Tile->Packet[i].cmptsNo  = ccc;
								Tile->Packet[i].prcNo    = ppp;
								Tile->Packet[i].packetNo = i;
								Tile->Packet[i].EPrc     = &(Tile->Cmpt[ccc].EEPrc[rrr].EPrc[ppp]);
								Tile->Packet[i].numband  = Tile->Cmpt[ccc].EEPrc[rrr].numBand;
								i++;
								flagD[ccc * colcstep + rrr * colrstep + lll * colpstep + ppp * colkstep] =
								    1;
							}
						}
					}
				}
			}
		}
	}
	return i;
}

byte4 RLCP_cnt(byte4 PacketNo, uchar RSpoc, uchar REpoc, byte2 LYSpoc, byte2 LYEpoc, byte2 CSpoc,
               byte2 CEpoc, struct Codec_s *Codec, struct Tile_s *t, struct wavelet_s *Wave, uchar *flagD,
               byte4 colcstep, byte4 colrstep, byte4 colpstep, byte4 colkstep) {
	struct ECod_s *ECod;
	byte2 lll, numLayer;
	uchar rrr, numDecompLev;
	byte2 ccc;
	ubyte2 ppp, numPrcs;
	byte4 width_llD, height_llD;
	byte4 i, i2;
	uchar *Ps_ON;

	Ps_ON = new uchar[Codec->Siz->Csiz];
	memset(Ps_ON, 0, sizeof(uchar) * Codec->Siz->Csiz);

	ECod = Codec->ECod;
	i    = PacketNo;
	i2   = 0;
	for (rrr = RSpoc; rrr < REpoc; rrr++) {
		for (lll = LYSpoc; lll < LYEpoc; lll++) {
			for (ccc = CSpoc; ccc < CEpoc; ccc++) {
				numLayer     = ECod->EECod[ccc * ECod->numTiles + t->T].numLayer;
				numDecompLev = ECod->EECod[ccc * ECod->numTiles + t->T].DecompLev;
				if ((lll < numLayer) && (rrr <= numDecompLev)) {
					width_llD  = Wave[ccc].ll_d[rrr].tbx1 - Wave[ccc].ll_d[rrr].tbx0;
					height_llD = Wave[ccc].ll_d[rrr].tby1 - Wave[ccc].ll_d[rrr].tby0;
					if (!Ps_ON[ccc]) Ps_ON[ccc] = (uchar) ((width_llD * height_llD) ? 1 : 0);
					numPrcs = t->Cmpt[ccc].EEPrc[rrr].numPrc;
					for (ppp = 0; ppp < numPrcs; ppp++) {
						if (!flagD[ccc * colcstep + rrr * colrstep + lll * colpstep + ppp * colkstep]) {
							if (Ps_ON[ccc]) {
								t->Packet[i].LayerNo  = lll;
								t->Packet[i].resoNo   = rrr;
								t->Packet[i].cmptsNo  = ccc;
								t->Packet[i].prcNo    = ppp;
								t->Packet[i].packetNo = i;
								t->Packet[i].EPrc     = &(t->Cmpt[ccc].EEPrc[rrr].EPrc[ppp]);
								t->Packet[i].numband  = t->Cmpt[ccc].EEPrc[rrr].numBand;
								i++;
								flagD[ccc * colcstep + rrr * colrstep + lll * colpstep + ppp * colkstep] =
								    1;
							}
							//							else
							//								i2++;
						}
					}
				}
			}
		}
	}
	return i; //(i+i2);
}

byte4 RPCL_cnt(byte4 PacketNo, uchar RSpoc, uchar REpoc, byte2 LYSpoc, byte2 LYEpoc, byte2 CSpoc,
               byte2 CEpoc, byte2 *Cppp, struct Codec_s *Codec, struct Tile_s *t, struct wavelet_s *Wave,
               uchar *flagD, byte4 colcstep, byte4 colrstep, byte4 colpstep, byte4 colkstep) {
	struct Siz_s *Siz;
	struct ECod_s *ECod;
	byte2 lll, numLayer;
	uchar rrr, DecompLev;
	byte2 ccc;
	byte4 numPrc;
	byte4 x, y, prcy0, prcx0, prcy1, prcx1, try0, trx0;
	bool xflag0, yflag0;
	bool flagP = 0;
	byte4 width_llD, height_llD;
	byte4 i, i2;
	uchar *Ps_ON;

	Ps_ON = new uchar[Codec->Siz->Csiz];
	memset(Ps_ON, 0, sizeof(uchar) * Codec->Siz->Csiz);

	ECod = Codec->ECod;
	Siz  = Codec->Siz;
	i    = PacketNo;
	i2   = 0;
	for (rrr = RSpoc; rrr < REpoc; rrr++) {
		for (y = t->ty0; y < t->ty1; y++) {
			for (x = t->tx0; x < t->tx1; x++) {
				for (ccc = CSpoc; ccc < CEpoc; ccc++) {
					numLayer  = ECod->EECod[ccc * ECod->numTiles + t->T].numLayer;
					DecompLev = ECod->EECod[ccc * ECod->numTiles + t->T].DecompLev;
					if (rrr <= DecompLev) {
						width_llD  = Wave[ccc].ll_d[rrr].tbx1 - Wave[ccc].ll_d[rrr].tbx0;
						height_llD = Wave[ccc].ll_d[rrr].tby1 - Wave[ccc].ll_d[rrr].tby0;
						if (!Ps_ON[ccc]) Ps_ON[ccc] = (uchar) ((width_llD * height_llD) ? 1 : 0);
						numPrc = t->Cmpt[ccc].EEPrc[rrr].numPrc;
						prcx0  = ECod->EECod[ccc * ECod->numTiles + t->T].PrcW[rrr] + (DecompLev - rrr);
						if (prcx0 >= 0x20) {
							if (x == 0)
								xflag0 = 1;
							else
								xflag0 = 0;
						} else {
							prcx1 = (1 << prcx0) * Siz->XRsiz[ccc];
							if (x % prcx1 == 0)
								xflag0 = 1;
							else
								xflag0 = 0;
						}
						prcy0 = ECod->EECod[ccc * ECod->numTiles + t->T].PrcH[rrr] + (DecompLev - rrr);
						if (prcy0 >= 0x20) {
							if (y == 0)
								yflag0 = 1;
							else
								yflag0 = 0;
						} else {
							prcy1 = (1 << prcy0) * Siz->YRsiz[ccc];
							if (y % prcy1 == 0)
								yflag0 = 1;
							else
								yflag0 = 0;
						}
						if (DecompLev - rrr >= 0x20) {
							if (t->Cmpt[ccc].tcy0 != 0)
								try0 = 1;
							else
								try0 = 0;
							if (t->Cmpt[ccc].tcx0 != 0)
								trx0 = 1;
							else
								trx0 = 0;
						} else {
							try0 = ceil2(t->Cmpt[ccc].tcy0, (1 << (DecompLev - rrr)));
							trx0 = ceil2(t->Cmpt[ccc].tcx0, (1 << (DecompLev - rrr)));
						}
						if (yflag0
						    || ((y == t->ty0)
						        && (try0 % (1 << ECod->EECod[ccc * ECod->numTiles + t->T].PrcH[rrr])
						            != 0))) {
							if (xflag0
							    || ((x == t->tx0)
							        && (trx0 % (1 << ECod->EECod[ccc * ECod->numTiles + t->T].PrcW[rrr])
							            != 0))) {
								for (lll = LYSpoc; lll < LYEpoc; lll++) {
									if ((lll < numLayer) && (rrr <= DecompLev)) {
										if (!flagD[ccc * colcstep + rrr * colrstep + lll * colpstep
										           + Cppp[ccc * colcstep + rrr * colrstep] * colkstep]) {
											if (Ps_ON[ccc]) {
												t->Packet[i].LayerNo = lll;
												t->Packet[i].resoNo  = rrr;
												t->Packet[i].cmptsNo = ccc;
												t->Packet[i].prcNo = Cppp[ccc * colcstep + rrr * colrstep];
												t->Packet[i].packetNo = i;
												t->Packet[i].EPrc =
												    &(t->Cmpt[ccc]
												          .EEPrc[rrr]
												          .EPrc[Cppp[ccc * colcstep + rrr * colrstep]]);
												t->Packet[i].numband = t->Cmpt[ccc].EEPrc[rrr].numBand;
												i++;
												flagD[ccc * colcstep + rrr * colrstep + lll * colpstep
												      + Cppp[ccc * colcstep + rrr * colrstep] * colkstep] =
												    1;
												flagP = 1;
											}
											//											else
											//												i2++;
										}
									}
								}
								if (flagP) {
									Cppp[ccc * colcstep + rrr * colrstep]++;
									flagP = 0;
								}
							}
						}
					}
				}
			}
		}
	}
	return i; //(i+i2);
}

byte4 PCRL_cnt(byte4 PacketNo, uchar RSpoc, uchar REpoc, byte2 LYSpoc, byte2 LYEpoc, byte2 CSpoc,
               byte2 CEpoc, byte2 *Cppp, struct Codec_s *Codec, struct Tile_s *t, struct wavelet_s *Wave,
               uchar *flagD, byte4 colcstep, byte4 colrstep, byte4 colpstep, byte4 colkstep) {
	byte2 lll, numLayer;
	uchar rrr, DecompLev;
	byte2 ccc;
	byte4 i, i2;
	byte4 x, y, prcy0, prcx0, prcy1, prcx1, try0, trx0;
	byte4 width_llD, height_llD;
	bool xflag0, yflag0, flagP = 0;
	uchar Ps_ON;
	struct ECod_s *ECod;
	struct Siz_s *Siz;

	ECod = Codec->ECod;
	Siz  = Codec->Siz;

	i  = PacketNo;
	i2 = 0;
	for (y = t->ty0; y < t->ty1; y++) {
		for (x = t->tx0; x < t->tx1; x++) {
			for (ccc = CSpoc; ccc < CEpoc; ccc++) {
				Ps_ON     = 0;
				numLayer  = ECod->EECod[ccc * ECod->numTiles + t->T].numLayer;
				DecompLev = ECod->EECod[ccc * ECod->numTiles + t->T].DecompLev;
				for (rrr = RSpoc; rrr < REpoc; rrr++) {
					if (rrr <= DecompLev) {
						width_llD  = Wave[ccc].ll_d[rrr].tbx1 - Wave[ccc].ll_d[rrr].tbx0;
						height_llD = Wave[ccc].ll_d[rrr].tby1 - Wave[ccc].ll_d[rrr].tby0;
						if (Ps_ON == 0) Ps_ON = (uchar) ((width_llD * height_llD) ? 1 : 0);
						prcx0 = ECod->EECod[ccc * ECod->numTiles + t->T].PrcW[rrr] + (DecompLev - rrr);
						if (prcx0 >= 0x20) {
							if (x == 0)
								xflag0 = 1;
							else
								xflag0 = 0;
						} else {
							prcx1 = (1 << prcx0) * Siz->XRsiz[ccc];
							if (x % prcx1 == 0)
								xflag0 = 1;
							else
								xflag0 = 0;
						}
						prcy0 = ECod->EECod[ccc * ECod->numTiles + t->T].PrcH[rrr] + (DecompLev - rrr);
						if (prcy0 >= 0x20) {
							if (y == 0)
								yflag0 = 1;
							else
								yflag0 = 0;
						} else {
							prcy1 = (1 << prcy0) * Siz->YRsiz[ccc];
							if (y % prcy1 == 0)
								yflag0 = 1;
							else
								yflag0 = 0;
						}
						if (DecompLev - rrr >= 0x20) {
							if (t->Cmpt[ccc].tcy0 != 0)
								try0 = 1;
							else
								try0 = 0;
							if (t->Cmpt[ccc].tcx0 != 0)
								trx0 = 1;
							else
								trx0 = 0;
						} else {
							try0 = ceil2(t->Cmpt[ccc].tcy0, (1 << (DecompLev - rrr)));
							trx0 = ceil2(t->Cmpt[ccc].tcx0, (1 << (DecompLev - rrr)));
						}
						if (yflag0
						    || ((y == t->ty0)
						        && (try0 % (1 << ECod->EECod[ccc * ECod->numTiles + t->T].PrcH[rrr])
						            != 0))) {
							if (xflag0
							    || ((x == t->tx0)
							        && (trx0 % (1 << ECod->EECod[ccc * ECod->numTiles + t->T].PrcW[rrr])
							            != 0))) {
								for (lll = LYSpoc; lll < LYEpoc; lll++) {
									if ((lll < numLayer) && (rrr <= DecompLev)) {
										if (!flagD[ccc * colcstep + rrr * colrstep + lll * colpstep
										           + Cppp[ccc * colcstep + rrr * colrstep] * colkstep]) {
											if (Ps_ON) {
												t->Packet[i].LayerNo = lll;
												t->Packet[i].resoNo  = rrr;
												t->Packet[i].cmptsNo = ccc;
												t->Packet[i].prcNo = Cppp[ccc * colcstep + rrr * colrstep];
												t->Packet[i].packetNo = i;
												t->Packet[i].EPrc =
												    &(t->Cmpt[ccc]
												          .EEPrc[rrr]
												          .EPrc[Cppp[ccc * colcstep + rrr * colrstep]]);
												t->Packet[i].numband = t->Cmpt[ccc].EEPrc[rrr].numBand;
												i++;
												flagD[ccc * colcstep + rrr * colrstep + lll * colpstep
												      + Cppp[ccc * colcstep + rrr * colrstep] * colkstep] =
												    1;
												flagP = 1;
											}
										}
									}
								}
								if (flagP) {
									Cppp[ccc * colcstep + rrr * colrstep]++;
									flagP = 0;
								}
							}
						}
					}
				}
			}
		}
	}
	return i; //(i+i2);
}

byte4 CPRL_cnt(byte4 PacketNo, uchar RSpoc, uchar REpoc, byte2 LYSpoc, byte2 LYEpoc, byte2 CSpoc,
               byte2 CEpoc, byte2 *Cppp, struct Codec_s *Codec, struct Tile_s *t, struct wavelet_s *Wave,
               uchar *flagD, byte4 colcstep, byte4 colrstep, byte4 colpstep, byte4 colkstep) {
	byte2 lll, numLayer;
	uchar rrr, DecompLev;
	byte2 ccc;
	byte4 x, y, prcy0, prcx0, prcy1, prcx1, try0, trx0;
	bool xflag0, yflag0, flagP = 0;
	struct ECod_s *ECod;
	struct Siz_s *Siz;
	byte4 width_llD, height_llD;
	byte4 i, i2;
	uchar Ps_ON;

	ECod = Codec->ECod;
	Siz  = Codec->Siz;

	i  = PacketNo;
	i2 = 0;
	for (ccc = CSpoc; ccc < CEpoc; ccc++) {
		for (y = t->ty0; y < t->ty1; y++) {
			for (x = t->tx0; x < t->tx1; x++) {
				Ps_ON = 0;
				for (rrr = RSpoc; rrr < REpoc; rrr++) {
					numLayer  = ECod->EECod[ccc * ECod->numTiles + t->T].numLayer;
					DecompLev = ECod->EECod[ccc * ECod->numTiles + t->T].DecompLev;
					if (rrr <= DecompLev) {
						width_llD  = Wave[ccc].ll_d[rrr].tbx1 - Wave[ccc].ll_d[rrr].tbx0;
						height_llD = Wave[ccc].ll_d[rrr].tby1 - Wave[ccc].ll_d[rrr].tby0;
						if (!Ps_ON) Ps_ON = (uchar) ((width_llD * height_llD) ? 1 : 0);
						prcx0 = ECod->EECod[ccc * ECod->numTiles + t->T].PrcW[rrr] + (DecompLev - rrr);
						if (prcx0 >= 0x20) {
							if (x == 0)
								xflag0 = 1;
							else
								xflag0 = 0;
						} else {
							prcx1 = (1 << prcx0) * Siz->XRsiz[ccc];
							if (x % prcx1 == 0)
								xflag0 = 1;
							else
								xflag0 = 0;
						}
						prcy0 = ECod->EECod[ccc * ECod->numTiles + t->T].PrcH[rrr] + (DecompLev - rrr);
						if (prcy0 >= 0x20) {
							if (y == 0)
								yflag0 = 1;
							else
								yflag0 = 0;
						} else {
							prcy1 = (1 << prcy0) * Siz->YRsiz[ccc];
							if (y % prcy1 == 0)
								yflag0 = 1;
							else
								yflag0 = 0;
						}
						if (DecompLev - rrr >= 0x20) {
							if (t->Cmpt[ccc].tcy0 != 0)
								try0 = 1;
							else
								try0 = 0;
							if (t->Cmpt[ccc].tcx0 != 0)
								trx0 = 1;
							else
								trx0 = 0;
						} else {
							try0 = ceil2(t->Cmpt[ccc].tcy0, (1 << (DecompLev - rrr)));
							trx0 = ceil2(t->Cmpt[ccc].tcx0, (1 << (DecompLev - rrr)));
						}
						if (yflag0
						    || ((y == t->ty0)
						        && (try0 % (1 << ECod->EECod[ccc * ECod->numTiles + t->T].PrcH[rrr])
						            != 0))) {
							if (xflag0
							    || ((x == t->tx0)
							        && (trx0 % (1 << ECod->EECod[ccc * ECod->numTiles + t->T].PrcW[rrr])
							            != 0))) {
								for (lll = LYSpoc; lll < LYEpoc; lll++) {
									if ((lll < numLayer) && (rrr <= DecompLev)) {
										if (!flagD[ccc * colcstep + rrr * colrstep + lll * colpstep
										           + Cppp[ccc * colcstep + rrr * colrstep] * colkstep]) {
											if (Ps_ON) {
												t->Packet[i].LayerNo = lll;
												t->Packet[i].resoNo  = rrr;
												t->Packet[i].cmptsNo = ccc;
												t->Packet[i].prcNo = Cppp[ccc * colcstep + rrr * colrstep];
												t->Packet[i].packetNo = i;
												t->Packet[i].EPrc =
												    &(t->Cmpt[ccc]
												          .EEPrc[rrr]
												          .EPrc[Cppp[ccc * colcstep + rrr * colrstep]]);
												t->Packet[i].numband = t->Cmpt[ccc].EEPrc[rrr].numBand;
												i++;
												flagD[ccc * colcstep + rrr * colrstep + lll * colpstep
												      + Cppp[ccc * colcstep + rrr * colrstep] * colkstep] =
												    1;
												flagP = 1;
											}
											//											else
											//											i2++;
										}
									}
								}
								if (flagP) {
									Cppp[ccc * colcstep + rrr * colrstep]++;
									flagP = 0;
								}
							}
						}
					}
				}
			}
		}
	}
	return i; //(i+i2);
}

byte4 TagtreeCreate2(struct Tile_s *t, byte4 Ps, byte4 Pe) {
	byte4 i;
	char bbb, numBands;

	for (i = Ps; i < Pe; i++) {
		numBands = t->Packet[i].numband;
		for (bbb = 0; bbb < numBands; bbb++) {
			t->Packet[i].EPrc->Prc[bbb].incltree =
			    TagtreeCreate1(t->Packet[i].EPrc->Prc[bbb].numCblkx, t->Packet[i].EPrc->Prc[bbb].numCblky);
			t->Packet[i].EPrc->Prc[bbb].zerotree =
			    TagtreeCreate1(t->Packet[i].EPrc->Prc[bbb].numCblkx, t->Packet[i].EPrc->Prc[bbb].numCblky);
		}
	}
	return EXIT_SUCCESS;
}
