/*
Copyright (c) 2000-2021, ICT-Link
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the HTJ2K project.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "j2k2.h"
#include "ImageUtil.h"
#include "vlc_codec.h"
#include "HT_cleanUp.h"

struct StreamChain_s *Enc_U_Prefix(struct StreamChain_s *str_rraw, struct RRawCodec_s *codecR,
                                   uchar Prefix) {
	uchar CwL;
	byte4 CwD;

	switch (Prefix) {
		case 0:
		case 4:
			CwD = 0;
			CwL = 0;
			break;
		case 1:
			CwD = 1;
			CwL = 1;
			break;
		case 2:
			CwD = 2; // 01b =>10b
			CwL = 2;
			break;
		case 3:
			CwD = 4; // 001b=>100b
			CwL = 3;
			break;
		default:
			CwD = 0; // 000b
			CwL = 3;
			break;
	}
	codecR->CwD = CwD;
	codecR->CwL = CwL;
	if (nullptr == (str_rraw = Write_nBits(str_rraw, codecR, CwD, CwL))) {
		printf("[Enc_U_Prefix]:: Write_nits error.\n");
		return nullptr;
	}
	return str_rraw;
}

uchar Dec_U_Prefix(struct StreamChain_s *str_rraw, struct RRawCodec_s *codecR)
// 6.3.6 Table3
{
	uchar CdW;
	CdW = (uchar) (codecR->Creg & 0x1);
	Load_nBits(str_rraw, codecR, 1);
	if (CdW == 1)
		return 1;
	else {
		CdW = (uchar) (codecR->Creg & 0x1);
		Load_nBits(str_rraw, codecR, 1);
		if (CdW == 1)
			return 2;
		else {
			CdW = (uchar) (codecR->Creg & 0x1);
			Load_nBits(str_rraw, codecR, 1);
			if (CdW == 1)
				return 3;
			else
				return 5;
		}
	}
}

struct StreamChain_s *Enc_U_Suffix(struct StreamChain_s *str_rraw, struct RRawCodec_s *codecR, uchar Prefix,
                                   uchar Suffix) {
	uchar CwL;

	if (Prefix < 3)
		CwL = 0;
	else if (Prefix < 5)
		CwL = 1;
	else
		CwL = 5;

	codecR->CwD = (byte4) Suffix;
	codecR->CwL = CwL;
	if (nullptr == (str_rraw = Write_nBits(str_rraw, codecR, (ubyte4) Suffix, CwL))) {
		printf("[Enc_U_Suffix]:: Write_nBits error. Suffix=%d CwL=%d\n", Suffix, CwL);
		return nullptr;
	}
	return str_rraw;
}

uchar Dec_U_Suffix(struct StreamChain_s *str_rraw, struct RRawCodec_s *codecR, uchar U_Prefix) {
	uchar i, U_Suffix, val;

	if (U_Prefix < 3) {
		U_Suffix = 0;
		return U_Suffix;
	}

	U_Suffix = (uchar) (codecR->Creg & 0x1);
	Load_nBits(str_rraw, codecR, 1);
	if (U_Prefix == 3) {
		return U_Suffix;
	} else {
		for (i = 1; i < 5; i++) {
			val = (uchar) (codecR->Creg & 0x1);
			Load_nBits(str_rraw, codecR, 1);
			U_Suffix = (uchar) (U_Suffix + (val << i));
		}
		return U_Suffix;
	}
}

struct StreamChain_s *Enc_U_Extension(struct StreamChain_s *str_rraw, struct RRawCodec_s *codecR,
                                      uchar Extension, uchar CwL) {
	uchar Extension_inv;
	if (CwL) {
		Extension_inv = Ext_Inv[Extension];
		str_rraw      = Write_nBits(str_rraw, codecR, (ubyte4) Extension, CwL);
	}
	return str_rraw;
}

uchar Dec_U_Extension(struct StreamChain_s *str_rraw, struct RRawCodec_s *codecR, uchar U_Suffix) {
	uchar i, U_Ext, val;

	if (U_Suffix < 28) {
		U_Ext = 0;
		return U_Ext;
	}

	U_Ext = (uchar) (codecR->Creg & 0x1);
	Load_nBits(str_rraw, codecR, 1);
	for (i = 1; i < 4; i++) {
		val = (uchar) (codecR->Creg & 0x1);
		Load_nBits(str_rraw, codecR, 1);
		U_Ext = (uchar) (U_Ext + (val << i));
	}
	return U_Ext;
}

struct StreamChain_s *Enc_U(struct StreamChain_s *str_rraw, struct RRawCodec_s *codecR, uchar u_off,
                            uchar qmex0, uchar qmex1, byte4 jjj) {
	uchar Suffix0 = 0, Suffix1 = 0, Prefix0 = 0, Prefix1 = 0, Ext0 = 0, Ext1 = 0, Ext0L = 0, Ext1L = 0;
	//	byte4	tmpL, tmpD;

	if ((!jjj) && (u_off == 4)) {
		qmex0 -= 2;
		qmex1 -= 2;
	}

	if (qmex0 < 3) {
		Prefix0 = qmex0;
		Suffix0 = 0;
		Ext0L   = 0;
		Ext0    = 0;
	} else if (qmex0 < 5) {
		Prefix0 = 3;
		Suffix0 = (uchar) (qmex0 - Prefix0);
		Ext0L   = 0;
		Ext0    = 0;
	} else if (qmex0 < 33) {
		Prefix0 = 5;
		Suffix0 = (uchar) (qmex0 - Prefix0);
		Ext0L   = 0;
		Ext0    = 0;
	} else {
		Prefix0 = 5;
		Suffix0 = (uchar) (28 + ((qmex0 - 33) % 4));
		Ext0L   = 4;
		Ext0    = (uchar) floor2((qmex0 - 33), 4);
	}

	if (qmex1 < 3) {
		Prefix1 = qmex1;
		Suffix1 = 0;
		Ext1L   = 0;
		Ext1    = 0;
	} else if (qmex1 < 5) {
		Prefix1 = 3;
		Suffix1 = (uchar) (qmex1 - Prefix1);
		Ext1L   = 0;
		Ext1    = 0;
	} else if (qmex1 < 33) {
		Prefix1 = 5;
		Suffix1 = (uchar) (qmex1 - Prefix1);
		Ext1L   = 0;
		Ext1    = 0;
	} else {
		Prefix1 = 5;
		Suffix1 = (uchar) (28 + ((qmex1 - 33) % 4));
		Ext1L   = 4;
		Ext1    = (uchar) floor2((qmex1 - 33), 4);
	}

	if (u_off == 0) {
		return str_rraw;
		//		tmpL=0;
		//		tmpD=0;
	} else if (u_off == 1) {
		if (nullptr == (str_rraw = Enc_U_Prefix(str_rraw, codecR, Prefix0))) {
			printf("[Enc_U]:: u_off=1. Enc_U_Prefix error.\n");
			return nullptr;
		}
		//		tmpD = codecR->CwD;
		//		tmpL = codecR->CwL;
		str_rraw = Enc_U_Suffix(str_rraw, codecR, Prefix0, Suffix0);
		//		tmpD = (codecR->CwD<<tmpL) + tmpD;
		//		tmpL += codecR->CwL;
		str_rraw = Enc_U_Extension(str_rraw, codecR, Ext0, Ext0L);
	} else if (u_off == 2) {
		if (nullptr == (str_rraw = Enc_U_Prefix(str_rraw, codecR, Prefix1))) {
			printf("[Enc_U]:: u_off=2. Enc_U_Prefix error.\n");
			return nullptr;
		}
		//		tmpD = codecR->CwD;
		//		tmpL = codecR->CwL;
		str_rraw = Enc_U_Suffix(str_rraw, codecR, Prefix1, Suffix1);
		//		tmpD = (codecR->CwD<<tmpL) + tmpD;
		//		tmpL += codecR->CwL;
		str_rraw = Enc_U_Extension(str_rraw, codecR, Ext1, Ext1L);
	} else if ((jjj == 0) && (u_off == 3)) {
		if (nullptr == (str_rraw = Enc_U_Prefix(str_rraw, codecR, Prefix0))) {
			printf("[Enc_U]:: jjj=0,u_off=3. Enc_U_Prefix error. 0\n");
			return nullptr;
		}
		//		tmpD = codecR->CwD;
		//		tmpL = codecR->CwL;
		if (Prefix0 > 2) {
			str_rraw    = Write_nBits(str_rraw, codecR, (qmex1 - 1), 1);
			codecR->CwD = (qmex1 - 1);
			codecR->CwL = 1;
			//			tmpD = (codecR->CwD<<tmpL) + tmpD;
			//			tmpL += codecR->CwL;
			str_rraw = Enc_U_Suffix(str_rraw, codecR, Prefix0, Suffix0);
			//			tmpD = (codecR->CwD<<tmpL) + tmpD;
			//			tmpL += codecR->CwL;
			str_rraw = Enc_U_Extension(str_rraw, codecR, Ext0, Ext0L);
		} else {
			if (nullptr == (str_rraw = Enc_U_Prefix(str_rraw, codecR, Prefix1))) {
				printf("[Enc_U]:: jjj=0,u_off=3. Enc_U_Prefix error. 1\n");
				return nullptr;
			}
			//			tmpD = (codecR->CwD<<tmpL) + tmpD;
			//			tmpL += codecR->CwL;
			str_rraw = Enc_U_Suffix(str_rraw, codecR, Prefix0, Suffix0);
			//			tmpD = (codecR->CwD<<tmpL) + tmpD;
			//			tmpL += codecR->CwL;
			str_rraw = Enc_U_Suffix(str_rraw, codecR, Prefix1, Suffix1);
			//			tmpD = (codecR->CwD<<tmpL) + tmpD;
			//			tmpL += codecR->CwL;
			str_rraw = Enc_U_Extension(str_rraw, codecR, Ext0, Ext0L);
			str_rraw = Enc_U_Extension(str_rraw, codecR, Ext1, Ext1L);
		}
	} else { //(Y_Line==1 and u_off==3) or (Y_line==0 and u_off==4) both.
		if (nullptr == (str_rraw = Enc_U_Prefix(str_rraw, codecR, Prefix0))) {
			printf("[Enc_U]:: jjj!=0,u_off=3. Enc_U_Prefix error. Prefix0=%d\n", Prefix0);
			return nullptr;
		}
		//		tmpD = codecR->CwD;
		//		tmpL = codecR->CwL;
		if (nullptr == (str_rraw = Enc_U_Prefix(str_rraw, codecR, Prefix1))) {
			printf("[Enc_U]:: jjj!=0,u_off=3. Enc_U_Prefix error. Prefix1=%d\n", Prefix1);
			return nullptr;
		}
		//		tmpD = (codecR->CwD<<tmpL) + tmpD;
		//		tmpL += codecR->CwL;
		str_rraw = Enc_U_Suffix(str_rraw, codecR, Prefix0, Suffix0);
		//		tmpD = (codecR->CwD<<tmpL) + tmpD;
		//		tmpL += codecR->CwL;
		str_rraw = Enc_U_Suffix(str_rraw, codecR, Prefix1, Suffix1);
		//		tmpD = (codecR->CwD<<tmpL) + tmpD;
		//		tmpL += codecR->CwL;
		str_rraw = Enc_U_Extension(str_rraw, codecR, Ext0, Ext0L);
		str_rraw = Enc_U_Extension(str_rraw, codecR, Ext1, Ext1L);
	}

	return str_rraw;
}

void Dec_U(struct StreamChain_s *str_rraw, struct RRawCodec_s *codecR, uchar *qmex_p,
           /*uchar Add,*/ byte4 jjj) {
	uchar Suffix0 = 0, Suffix1 = 0, Prefix0 = 0, Prefix1 = 0, Ext0 = 0, Ext1 = 0; // osamu:ubyte4 -> uchar

	if (!jjj) {
		if (codecR->u_off == 4 /*0x100*/) {
			Prefix0   = Dec_U_Prefix(str_rraw, codecR);
			Prefix1   = Dec_U_Prefix(str_rraw, codecR);
			Suffix0   = Dec_U_Suffix(str_rraw, codecR, Prefix0);
			Suffix1   = Dec_U_Suffix(str_rraw, codecR, Prefix1);
			Ext0      = Dec_U_Extension(str_rraw, codecR, Suffix0);
			Ext1      = Dec_U_Extension(str_rraw, codecR, Suffix1);
			qmex_p[0] = (uchar) (2 + Prefix0 + Suffix0 + 4 * Ext0);
			qmex_p[1] = (uchar) (2 + Prefix1 + Suffix1 + 4 * Ext1);
		} else if (codecR->u_off == 3 /*0xc0*/) {
			Prefix0 = Dec_U_Prefix(str_rraw, codecR);
			if (Prefix0 > 2) {
				Prefix1 = (uchar) (codecR->Creg & 0x1);
				Load_nBits(str_rraw, codecR, 1);
				qmex_p[1] = (uchar) (Prefix1 + 1); // % u_pfx = ubit+1;
				Suffix0   = Dec_U_Suffix(str_rraw, codecR, Prefix0);
				Ext0      = Dec_U_Extension(str_rraw, codecR, Suffix0);
			} else {
				Prefix1   = Dec_U_Prefix(str_rraw, codecR);
				Suffix0   = Dec_U_Suffix(str_rraw, codecR, Prefix0);
				Suffix1   = Dec_U_Suffix(str_rraw, codecR, Prefix1);
				Ext0      = Dec_U_Extension(str_rraw, codecR, Suffix0);
				Ext1      = Dec_U_Extension(str_rraw, codecR, Suffix1);
				qmex_p[1] = (uchar) (Prefix1 + Suffix1 + 4 * Ext1);
			}
			qmex_p[0] = (uchar) (Prefix0 + Suffix0 + 4 * Ext0);
		} else if (codecR->u_off == 1 /*0x40*/) {
			Prefix0   = Dec_U_Prefix(str_rraw, codecR);
			Suffix0   = Dec_U_Suffix(str_rraw, codecR, Prefix0);
			Ext0      = Dec_U_Extension(str_rraw, codecR, Suffix0);
			qmex_p[0] = (uchar) (Prefix0 + Suffix0 + 4 * Ext0);
			qmex_p[1] = 0;
		} else if (codecR->u_off == 2 /*0x80*/) {
			Prefix1   = Dec_U_Prefix(str_rraw, codecR);
			Suffix1   = Dec_U_Suffix(str_rraw, codecR, Prefix1);
			Ext1      = Dec_U_Extension(str_rraw, codecR, Suffix1);
			qmex_p[0] = 0;
			qmex_p[1] = (uchar) (Prefix1 + Suffix1 + 4 * Ext1);
		} else {
			qmex_p[0] = 0;
			qmex_p[1] = 0;
		}
	} else {
		if (codecR->u_off == 0) {
			qmex_p[0] = 0;
			qmex_p[1] = 0;
		} else if (codecR->u_off == 1 /*0x40*/) {
			Prefix0   = Dec_U_Prefix(str_rraw, codecR);
			Suffix0   = Dec_U_Suffix(str_rraw, codecR, Prefix0);
			Ext0      = Dec_U_Extension(str_rraw, codecR, Suffix0);
			qmex_p[0] = (uchar) (Prefix0 + Suffix0 + 4 * Ext0);
			qmex_p[1] = 0;
		} else if (codecR->u_off == 2 /*0x80*/) {
			Prefix1   = Dec_U_Prefix(str_rraw, codecR);
			Suffix1   = Dec_U_Suffix(str_rraw, codecR, Prefix1);
			Ext1      = Dec_U_Extension(str_rraw, codecR, Suffix1);
			qmex_p[0] = 0;
			qmex_p[1] = (uchar) (Prefix1 + Suffix1 + 4 * Ext1);
		} else {
			Prefix0   = Dec_U_Prefix(str_rraw, codecR);
			Prefix1   = Dec_U_Prefix(str_rraw, codecR);
			Suffix0   = Dec_U_Suffix(str_rraw, codecR, Prefix0);
			Suffix1   = Dec_U_Suffix(str_rraw, codecR, Prefix1);
			Ext0      = Dec_U_Extension(str_rraw, codecR, Suffix0);
			Ext1      = Dec_U_Extension(str_rraw, codecR, Suffix1);
			qmex_p[0] = (uchar) (Prefix0 + Suffix0 + 4 * Ext0);
			qmex_p[1] = (uchar) (Prefix1 + Suffix1 + 4 * Ext1);
		}
	}
}

/*********************************************************************************************************/
/******************** RRAW *******************************************************************************/
/*********************************************************************************************************/
struct RRawCodec_s *RRawCodecCreate() {
	struct RRawCodec_s *codecR;

	if (nullptr == (codecR = new struct RRawCodec_s)) {
		printf("[RRawCodecCreate]:: codecR malloc error\n");
		return nullptr;
	}
	return codecR;
}

bool RRawCodecDestroy(struct RRawCodec_s *codec) {
	delete codec;
	return true;
}

void EncRRawStart(struct RRawCodec_s *codecR /*, bool want_ilw*/) {
	codecR->total_bits = 0;
	codecR->last_byte  = 0x00;
	codecR->Creg       = 0x0FFF;
	codecR->ctreg      = 12;
}

byte4 DecRRawStart(struct StreamChain_s *str_rraw, struct RRawCodec_s *codecR /*, uchar have_ilw*/) {
	str_rraw->buf[str_rraw->buf_length - 1] = 0xff;
	str_rraw->buf[str_rraw->buf_length - 2] |= 0x0f;
	codecR->Creg      = 0;
	codecR->ctreg     = 0;
	str_rraw->cur_p   = str_rraw->buf_length - 2;
	codecR->last_byte = 0xff;
	Load_Byte(str_rraw, codecR);
	codecR->ctreg -= 4;
	codecR->Creg >>= 4;
	while (codecR->ctreg < 16)
		Load_Byte(str_rraw, codecR);
	codecR->u_off = 0;
	return EXIT_SUCCESS;
}

struct StreamChain_s *terminate(struct StreamChain_s *str_rraw, struct RRawCodec_s *codecR) {
	uchar temp1, temp2;
	while (codecR->ctreg >= 8) {
		temp1 = (uchar) (codecR->Creg & 0xff);
		temp2 = (uchar) (codecR->Creg & 0x7f);
		if ((temp2 == 0x7f) && (codecR->last_byte > 0x8F)) {
			codecR->last_byte = temp2;
			codecR->Creg >>= 7;
			codecR->ctreg -= 7;
		} else {
			codecR->last_byte = temp1;
			codecR->Creg >>= 8;
			codecR->ctreg -= 8;
		}
		if (nullptr == (str_rraw = Stream1ByteWriteHT(str_rraw, codecR->last_byte))) {
			printf("[terminate]:: Stream1ByteWriteHT error. codecR->last_byte=%d\n", codecR->last_byte);
			return nullptr;
		}
	}
	if (codecR->ctreg) {
		codecR->last_byte = (uchar) (codecR->Creg & 0xFF);
		if (nullptr == (str_rraw = Stream1ByteWriteHT(str_rraw, codecR->last_byte))) {
			printf("[terminate]:: Stream1ByteWriteHT error. codecR->last_byte=%d, codecR->ctreg=%d\n",
			       codecR->last_byte, codecR->ctreg);
			return nullptr;
		}
	}
	return str_rraw;
}

struct StreamChain_s *Write_nBits(struct StreamChain_s *str_rraw, struct RRawCodec_s *codecR, ubyte4 CwD,
                                  uchar CwL) {
	if (!CwL) return str_rraw;

	codecR->Creg |= (CwD << codecR->ctreg);
	codecR->ctreg += CwL;
	while (codecR->ctreg >= 16) {
		if (nullptr == (str_rraw = Write_Byte(str_rraw, codecR))) {
			printf("[Write_nBits]:: Write_Byte error.\n");
			return nullptr;
		}
	}
	return str_rraw;
}

void Load_nBits(struct StreamChain_s *str_rraw, struct RRawCodec_s *codecR, uchar num_bits) {
	codecR->total_bits += num_bits;
	codecR->Creg >>= num_bits;
	codecR->ctreg -= num_bits;
	while (codecR->ctreg < 16)
		Load_Byte(str_rraw, codecR);
}

struct StreamChain_s *Write_Byte(struct StreamChain_s *str_rraw, struct RRawCodec_s *codecR) {
	uchar temp1, temp2;

	temp1 = (uchar) (codecR->Creg & 0xff);
	temp2 = (uchar) (codecR->Creg & 0x7f);
	if ((temp2 == 0x07f) && (codecR->last_byte > 0x8F)) {
		codecR->last_byte = temp2;
		codecR->Creg >>= 7;
		codecR->ctreg -= 7;
	} else {
		codecR->last_byte = temp1;
		codecR->Creg >>= 8;
		codecR->ctreg -= 8;
	}
	if (nullptr == (str_rraw = Stream1ByteWriteHT(str_rraw, codecR->last_byte))) return nullptr;

	return str_rraw;
}

void Load_Byte(struct StreamChain_s *str_rraw, struct RRawCodec_s *codecR) {
	uchar bits;
	if (str_rraw->buf[str_rraw->cur_p] == 0x7f && codecR->last_byte > 0x8f)
		bits = 7;
	else
		bits = 8;
	codecR->last_byte = str_rraw->buf[str_rraw->cur_p];
	str_rraw->cur_p--;
	codecR->Creg |= (codecR->last_byte << codecR->ctreg);
	codecR->ctreg += bits;
}

/************************************************************************************/
/**************  fRAW  **************************************************************/
/************************************************************************************/
void EncfRawStart(struct fRawCodec_s *codecf) {
	codecf->ctreg     = 0;
	codecf->Creg      = 0;
	codecf->last_byte = 0;
}

void DecfRawStart(struct StreamChain_s *str_fraw, struct fRawCodec_s *codecf) {
	codecf->Creg      = 0;
	codecf->ctreg     = 0;
	codecf->last_byte = 0x00;
	while (codecf->ctreg < 16)
		Load_Byte(str_fraw, codecf);
}

ubyte4 consume_word(struct StreamChain_s *str, struct fRawCodec_s *codecf, uchar num_bits) {
	ubyte4 val;
	val = codecf->Creg;
	Load_Stream(str, codecf, num_bits);
	return val;
}

struct StreamChain_s *Write_Byte(struct StreamChain_s *str_fraw, struct fRawCodec_s *codecf) {
	if (codecf->ctreg < 8) return NULL;
	if (codecf->last_byte == 0xFF) {
		codecf->last_byte = (uchar) (codecf->Creg & 0x7F);
		codecf->Creg >>= 7;
		codecf->ctreg -= 7;
	} else {
		codecf->last_byte = (uchar) codecf->Creg;
		codecf->Creg >>= 8;
		codecf->ctreg -= 8;
	}
	str_fraw = Stream1ByteWrite(str_fraw, codecf->last_byte, str_fraw->buf_length);
	return str_fraw;
}

void Load_Byte(struct StreamChain_s *str_fraw, struct fRawCodec_s *codecf) {
	ubyte4 ld_tmp;
	uchar new_bits;

	new_bits = (uchar) ((str_fraw->lastbyte == 0xff) ? 7 : 8);
	ld_tmp   = 0xFF;
	if (str_fraw->cur_p < str_fraw->buf_length) {
		str_fraw->lastbyte = str_fraw->buf[str_fraw->cur_p];
		ld_tmp             = str_fraw->lastbyte;
		str_fraw->cur_p++;
	}
	codecf->Creg |= (ld_tmp << codecf->ctreg);
	codecf->ctreg = (uchar) (codecf->ctreg + new_bits);
}

struct StreamChain_s *terminate(struct StreamChain_s *str_fraw, struct fRawCodec_s *codecf) {
	uchar fill_mask;
	uchar tempD;

	str_fraw = flush_completed_bytes(str_fraw, codecf);

	bool last_was_FF = (codecf->last_byte == 0xFF);
	if (codecf->ctreg > 0) {
		fill_mask = (uchar) (0xFF << codecf->ctreg);
		if (last_was_FF) {
			fill_mask &= 0x7F;
		}
		tempD = (uchar) (codecf->Creg |= fill_mask);
		if (tempD != 0xff) Stream1ByteWrite(str_fraw, tempD, str_fraw->buf_length);
	} else if (last_was_FF) {
		str_fraw->cur_p--;
		str_fraw->buf[str_fraw->cur_p] = 0x00;
	}

	return str_fraw;
}

struct StreamChain_s *flush_completed_bytes(struct StreamChain_s *str_fraw, struct fRawCodec_s *codecf) {
	while (true) {
		if (codecf->last_byte == 0xFF) {
			if (codecf->ctreg < 7) break; // Do not flush partial byte here
			codecf->last_byte = (uchar) (codecf->Creg & 0x7F);
			codecf->Creg >>= 7;
			codecf->ctreg -= 7;
		} else {
			if (codecf->ctreg < 8) break; // Do not flush partial byte here
			codecf->last_byte = (uchar) codecf->Creg;
			codecf->Creg >>= 8;
			codecf->ctreg -= 8;
		}
		str_fraw = Stream1ByteWrite(str_fraw, codecf->last_byte, str_fraw->buf_length);
	}
	return str_fraw;
}

struct StreamChain_s *Write_Stream(struct StreamChain_s *str_fraw, struct fRawCodec_s *codecf, ubyte4 outD,
                                   uchar num_bits) {
	codecf->Creg |= (outD << codecf->ctreg);
	codecf->ctreg = (uchar) (codecf->ctreg + num_bits);

	while (codecf->ctreg >= 16)
		str_fraw = Write_Byte(str_fraw, codecf);

	return str_fraw;
}

void Load_Stream(struct StreamChain_s *str_fraw, struct fRawCodec_s *codecf, uchar num_bits) {
	codecf->Creg >>= num_bits;
	codecf->ctreg = (uchar) (codecf->ctreg - num_bits);
	while (codecf->ctreg < 16)
		Load_Byte(str_fraw, codecf);
}

struct StreamChain_s *Enc_MagSgn(struct J2kParam_s *J2kParam, struct StreamChain_s *str_fraw,
                                 struct fRawCodec_s *codecf, uchar num_bits, byte4 val) {
	byte4 temp0, outD;

	temp0 = val & 0x7fffffff;
	temp0 -= 1;
	temp0 <<= 1;
	temp0 &= mask6[num_bits];
	outD = temp0;
	if (val & 0x80000000) outD++;
	str_fraw = Write_Stream(str_fraw, codecf, outD, num_bits);

	J2kParam->frawD = outD;
	J2kParam->frawL = num_bits;

	return str_fraw;
}

byte4 dec_MagSgn(struct StreamChain_s *str_fraw, struct fRawCodec_s *codecf, uchar num_bits,
                 uchar epciron1) {
	byte4 outD, val;
	outD = (byte4) (mask6[num_bits] & codecf->Creg);
	val  = outD + (epciron1 << num_bits);
	Load_Stream(str_fraw, codecf, num_bits);
	return val;
}
