/*
Copyright (c) 2000-2021, ICT-Link
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the HTJ2K project.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "j2k2.h"
#include "ImageUtil.h"
#include "wavelet_codec.h"
#include "Icc.h"
#include "annexE.h"

struct Tile_s *TileCreate(struct Codec_s *Codec, byte4 numTilePart) {
	struct Tile_s *Tile;
	byte4 ttt, j;

	if (Codec->numTiles <= 0) {
		printf("[TileCreate]:: Codec->numTiles should be positive val.  Codec->numTiles=%d \n",
		       Codec->numTiles);
		return nullptr;
	}
	if (Codec->Siz->Csiz <= 0) {
		printf("[TileCreate]:: Codec->numTiles should be positive val.  Codec->Siz->Csiz=%d \n",
		       Codec->Siz->Csiz);
		return nullptr;
	}

	if (nullptr == (Tile = new struct Tile_s[Codec->numTiles])) {
		printf("[TileCreate]:: Tile malloc error. \n");
		return nullptr;
	}
	for (ttt = 0; ttt < Codec->numTiles; ttt++) {
		Tile[ttt].str_ppt  = nullptr;
		Tile[ttt].ppt_flag = 0;

		if (nullptr == (Tile[ttt].Cmpt = new struct Compts_s[Codec->Siz->Csiz])) {
			printf("[TileCreate]:: Tile[%d].Cmpt malloc error. \n", ttt);
			return nullptr;
		}
		if (Codec->Sot != nullptr) {
			if (Codec->Sot->numTilePart[ttt] <= 0) {
				printf("[TileCreate]:: Codec->Sot->numTilePart[%d] should be positive val.  "
				       "Codec->Sot->numTilePart[%d]=%d \n",
				       ttt, ttt, Codec->Sot->numTilePart[ttt]);
				return nullptr;
			}
			Tile[ttt].TNsot = Codec->Sot->numTilePart[ttt];
			if (nullptr == (Tile[ttt].Saddr = new byte4[Codec->Sot->numTilePart[ttt]])) {
				printf("[TileCreate]:: Tile[%d].Saddr malloc error1. \n", ttt);
				return nullptr;
			}
			if (nullptr == (Tile[ttt].Psot = new byte4[Codec->Sot->numTilePart[ttt]])) {
				printf("[TileCreate]:: Tile[%d].Psot malloc error1. \n", ttt);
				return nullptr;
			}
			if (nullptr == (Tile[ttt].TPsot = new uchar[Codec->Sot->numTilePart[ttt]])) {
				printf("[TileCreate]:: Tile[%d].TPsot malloc error1. \n", ttt);
				return nullptr;
			}
			if (nullptr == (Tile[ttt].PacketsCount = new byte4[Codec->Sot->numTilePart[ttt]])) {
				printf("[TileCreate]:: Tile[%d].PacketsCount malloc error1. \n", ttt);
				return nullptr;
			}
			for (j = 0; j < Codec->Sot->numTilePart[ttt]; j++) {
				Tile[ttt].Saddr[j]        = Codec->Sot->Saddr[ttt][j];
				Tile[ttt].Psot[j]         = Codec->Sot->Psot[ttt][j];
				Tile[ttt].TPsot[j]        = Codec->Sot->TPsot[ttt][j];
				Tile[ttt].PacketsCount[j] = 0;
			}
		} else {
			Tile[ttt].TNsot = 0;
			if (nullptr == (Tile[ttt].Saddr = new byte4[numTilePart])) {
				printf("[TileCreate]:: Tile[%d].Saddr malloc error2. \n", ttt);
				return nullptr;
			}
			if (nullptr == (Tile[ttt].Psot = new byte4[numTilePart])) {
				printf("[TileCreate]:: Tile[%d].Psot malloc error2. \n", ttt);
				return nullptr;
			}
			if (nullptr == (Tile[ttt].TPsot = new uchar[numTilePart])) {
				printf("[TileCreate]:: Tile[%d].TPsot malloc error2. \n", ttt);
				return nullptr;
			}
			if (nullptr == (Tile[ttt].PacketsCount = new byte4[numTilePart])) {
				printf("[TileCreate]:: Tile[%d].PacketsCount malloc error2. \n", ttt);
				return nullptr;
			}
			for (j = 0; j < numTilePart; j++) {
				Tile[ttt].Saddr[j]        = 0;
				Tile[ttt].Psot[j]         = 0;
				Tile[ttt].TPsot[j]        = 0;
				Tile[ttt].PacketsCount[j] = 0;
			}
		}
		Tile[ttt].ERgn = nullptr;
	}

	return Tile;
}

struct Codec_s *CreateCodec(struct J2kParam_s *J2kParam) {
	struct Codec_s *Codec;

	Codec              = new struct Codec_s;
	Codec->ECod        = nullptr;
	Codec->EQcd        = nullptr;
	Codec->ERgn        = nullptr;
	Codec->Siz         = nullptr;
	Codec->Poc         = nullptr;
	Codec->tilePoc     = nullptr;
	Codec->Sot         = nullptr;
	Codec->Cap         = nullptr;
	Codec->ppm_flag    = 0;
	Codec->ppm_Counter = 0;
	Codec->ppm_Nppm    = 0;
	Codec->ppm_Saddr   = NULL;
	Codec->ppm_Saddr2  = NULL;
	Codec->ppm_Length  = NULL;
	Codec->ppm_Length2 = NULL;
	Codec->debugFlag   = J2kParam->debugFlag;
	if (J2kParam->debugFlag) strcpy(Codec->debugFname, J2kParam->debugFname);
	return Codec;
}

byte4 DestroyCodec(struct Codec_s *Codec) {
	delete Codec;
	return EXIT_SUCCESS;
}

//[Func 1]
struct Siz_s *SizEnc(byte4 width, byte4 height, ubyte2 numCmpts, struct J2kParam_s *J2kParam) {
	struct Siz_s *Siz;
	ubyte2 ccc;

	if (nullptr == (Siz = SizCreate(numCmpts))) {
		printf("[SizEnc::SizCreate error]\n");
		return nullptr;
	}
	Siz->Xsiz = width + J2kParam->Tile_OffsetX;  // X_TOFFSET_;
	Siz->Ysiz = height + J2kParam->Tile_OffsetY; // Y_TOFFSET_;
	if (J2kParam->Tile_x > Siz->Xsiz)
		Siz->XTsiz = width;
	else
		Siz->XTsiz = J2kParam->Tile_x;
	if (J2kParam->Tile_y > Siz->Ysiz)
		Siz->YTsiz = height;
	else
		Siz->YTsiz = J2kParam->Tile_y;
	if (J2kParam->HTJ2K)
		Siz->Rsiz = 0x4000;
	else
		Siz->Rsiz = 0x0000;
	if (numCmpts == 3) {
		Siz->XRsiz[0] = XSUBSAMPLING0_;
		Siz->XRsiz[1] = XSUBSAMPLING1_;
		Siz->XRsiz[2] = XSUBSAMPLING2_;
		Siz->YRsiz[0] = YSUBSAMPLING0_;
		Siz->YRsiz[1] = YSUBSAMPLING1_;
		Siz->YRsiz[2] = YSUBSAMPLING2_;
	} else {
		for (ccc = 0; ccc < numCmpts; ccc++) {
			Siz->XRsiz[ccc] = 1;
			Siz->YRsiz[ccc] = 1;
		}
	}
	Siz->XOsiz  = J2kParam->OffsetX;
	Siz->YOsiz  = J2kParam->OffsetY;
	Siz->XTOsiz = J2kParam->Tile_OffsetX;
	Siz->YTOsiz = J2kParam->Tile_OffsetY;

	Siz->Xdim      = Siz->Xsiz - Siz->XOsiz;
	Siz->Ydim      = Siz->Ysiz - Siz->YOsiz;
	Siz->numXTiles = ceil2((Siz->Xsiz - Siz->XTOsiz), Siz->XTsiz);
	Siz->numYTiles = ceil2((Siz->Ysiz - Siz->YTOsiz), Siz->YTsiz);
	Siz->numTiles  = Siz->numXTiles * Siz->numYTiles;
	return Siz;
}

//[Func 2]
struct Siz_s *SizCreate(byte2 numCmpts) {
	struct Siz_s *Siz;

	if (nullptr == (Siz = new struct Siz_s)) {
		printf("[SizCreate:: Siz_s malloc error]\n");
		return nullptr;
	}

	Siz->Csiz = numCmpts;
	if (nullptr == (Siz->Ssiz = new char[numCmpts])) {
		printf("[SizCreate:: Siz->Ssiz[%d] malloc error]\n", numCmpts);
		return nullptr;
	}

	if (nullptr == (Siz->XRsiz = new char[numCmpts])) {
		printf("[SizCreate:: Siz->XRsiz[%d] malloc error]\n", numCmpts);
		return nullptr;
	}

	if (nullptr == (Siz->YRsiz = new char[numCmpts])) {
		printf("[SizCreate:: Siz->YRsiz[%d] malloc error]\n", numCmpts);
		return nullptr;
	}
	return Siz;
}

byte4 SizSsiz(struct Siz_s *siz, ubyte2 numCmpts, struct Image_s *Stream) {
	ubyte2 ccc;
	for (ccc = 0; ccc < numCmpts; ccc++) {
		if (Stream->MaxValue < 2)
			siz->Ssiz[ccc] = 0;
		else if (Stream->MaxValue < 4)
			siz->Ssiz[ccc] = 1;
		else if (Stream->MaxValue < 8)
			siz->Ssiz[ccc] = 2;
		else if (Stream->MaxValue < 0x10)
			siz->Ssiz[ccc] = 3;
		else if (Stream->MaxValue < 0x20)
			siz->Ssiz[ccc] = 4;
		else if (Stream->MaxValue < 0x40)
			siz->Ssiz[ccc] = 5;
		else if (Stream->MaxValue < 0x80)
			siz->Ssiz[ccc] = 6;
		else if (Stream->MaxValue < 0x100)
			siz->Ssiz[ccc] = 7;
		else if (Stream->MaxValue < 0x200)
			siz->Ssiz[ccc] = 0;
		else if (Stream->MaxValue < 0x400)
			siz->Ssiz[ccc] = 9;
		else if (Stream->MaxValue < 0x800)
			siz->Ssiz[ccc] = 10;
		else if (Stream->MaxValue < 0x1000)
			siz->Ssiz[ccc] = 11;
		else if (Stream->MaxValue < 0x2000)
			siz->Ssiz[ccc] = 12;
		else if (Stream->MaxValue < 0x4000)
			siz->Ssiz[ccc] = 13;
		else if (Stream->MaxValue < 0x8000)
			siz->Ssiz[ccc] = 14;
		else if (Stream->MaxValue < 0x10000)
			siz->Ssiz[ccc] = 15;
		else {
			siz->Ssiz[ccc] = -1;
			return EXIT_FAILURE;
		}
	}
	return EXIT_SUCCESS;
}

//[Func 4]
struct Siz_s *SizDec(struct StreamChain_s *codstream) {
	struct Siz_s *Siz;
	ubyte2 ccc;

	if (nullptr == (Siz = new struct Siz_s)) {
		printf("[SizDec] :: Siz malloc error\n");
		return nullptr;
	}

	Siz->Lsiz   = Ref_2Byte(codstream);
	Siz->Rsiz   = Ref_2Byte(codstream);
	Siz->Xsiz   = Ref_4Byte(codstream);
	Siz->Ysiz   = Ref_4Byte(codstream);
	Siz->XOsiz  = Ref_4Byte(codstream);
	Siz->YOsiz  = Ref_4Byte(codstream);
	Siz->XTsiz  = Ref_4Byte(codstream);
	Siz->YTsiz  = Ref_4Byte(codstream);
	Siz->XTOsiz = Ref_4Byte(codstream);
	Siz->YTOsiz = Ref_4Byte(codstream);

	Siz->Csiz = Ref_2Byte(codstream);
	if (nullptr == (Siz->Ssiz = new char[Siz->Csiz])) {
		printf("[SizDec] :: Siz->Ssiz[%d] malloc error\n", Siz->Csiz);
		return nullptr;
	}
	if (nullptr == (Siz->XRsiz = new char[Siz->Csiz])) {
		printf("[SizDec] :: Siz->XRsiz[%d] malloc error\n", Siz->Csiz);
		return nullptr;
	}
	if (nullptr == (Siz->YRsiz = new char[Siz->Csiz])) {
		printf("[SizDec] :: Siz->YRsiz[%d] malloc error\n", Siz->Csiz);
		return nullptr;
	}
	for (ccc = 0; ccc < Siz->Csiz; ccc++) {
		Siz->Ssiz[ccc]  = Ref_1Byte(codstream);
		Siz->XRsiz[ccc] = Ref_1Byte(codstream);
		Siz->YRsiz[ccc] = Ref_1Byte(codstream);
	}
	Siz->Xdim      = Siz->Xsiz - Siz->XOsiz;
	Siz->Ydim      = Siz->Ysiz - Siz->YOsiz;
	Siz->numXTiles = ceil2((Siz->Xsiz - Siz->XTOsiz), Siz->XTsiz);
	Siz->numYTiles = ceil2((Siz->Ysiz - Siz->YTOsiz), Siz->YTsiz);
	Siz->numTiles  = Siz->numXTiles * Siz->numYTiles;
	return Siz;
}

struct Sot_s *SotCreate(struct Siz_s *Siz) {
	struct Sot_s *Sot;
	byte4 t;

	Sot                = new struct Sot_s;
	Sot->Psot          = new byte4 *[Siz->numTiles];
	Sot->Saddr         = new byte4 *[Siz->numTiles];
	Sot->TPsot         = new uchar *[Siz->numTiles];
	Sot->Packet_Saddr  = new byte4 *[Siz->numTiles];
	Sot->Packet_Length = new byte4 *[Siz->numTiles];
	Sot->PI_Saddr      = new byte4 *[Siz->numTiles];
	Sot->PI_Length     = new byte4 *[Siz->numTiles];
	for (t = 0; t < Siz->numTiles; t++) {
		Sot->Psot[t]          = new byte4[256];
		Sot->Saddr[t]         = new byte4[256];
		Sot->TPsot[t]         = new uchar[256];
		Sot->Packet_Saddr[t]  = new byte4[256];
		Sot->Packet_Length[t] = new byte4[256];
		Sot->PI_Saddr[t]      = new byte4[256];
		Sot->PI_Length[t]     = new byte4[256];
		memset(Sot->Psot[t], 0, sizeof(byte4) * 256);
		memset(Sot->Saddr[t], 0, sizeof(byte4) * 256);
		memset(Sot->TPsot[t], 0, sizeof(uchar) * 256);
		memset(Sot->Packet_Saddr[t], 0, sizeof(byte4) * 256);
		memset(Sot->Packet_Length[t], 0, sizeof(byte4) * 256);
		memset(Sot->PI_Saddr[t], 0, sizeof(byte4) * 256);
		memset(Sot->PI_Length[t], 0, sizeof(byte4) * 256);
	}
	Sot->numTilePart = new uchar[Siz->numTiles];
	memset(Sot->numTilePart, 0, sizeof(uchar) * Siz->numTiles);

	return Sot;
}

byte4 SotDec(struct Codec_s *Codec, struct StreamChain_s *str) {
	ubyte2 Lsot;
	ubyte2 Isot;
	byte4 Psot;
	uchar TPsot, TNsot;
	byte4 Saddr /*, tempAddr*/;

	Saddr = str->cur_p - 2;
	Lsot  = Ref_2Byte(str);
	if (Lsot != 0xa) {
		printf("[SotDec]::Lsot error Lsot=%x cur_p=%x\n", Lsot, str->cur_p);
		return EXIT_FAILURE;
	}
	Isot  = Ref_2Byte(str);
	Psot  = Ref_4Byte(str);
	TPsot = Ref_1Byte(str);
	TNsot = Ref_1Byte(str);
	Codec->Sot->numTilePart[Isot]++;
	Codec->Sot->Psot[Isot][TPsot]  = Psot;
	Codec->Sot->Saddr[Isot][TPsot] = Saddr;
	if (Codec->ppm_flag) {
		Codec->Sot->PI_Saddr[Isot][TPsot]  = Codec->ppm_Saddr[Codec->ppm_Counter] + 4;
		Codec->Sot->PI_Length[Isot][TPsot] = Codec->ppm_Length[Codec->ppm_Counter];
		Codec->ppm_Counter++;
	}
	str->cur_p = Saddr + Psot;
	return EXIT_SUCCESS;
}

byte4 PpmDec(struct Codec_s *Codec, struct StreamChain_s *str) {
	ubyte2 Lppm;
	uchar Zppm;
	byte4 Nppm;
	byte4 PPM_Eaddr;

	Lppm      = Ref_2Byte(str);
	PPM_Eaddr = str->cur_p + (Lppm - 2);

	Zppm = Ref_1Byte(str);
	while (str->cur_p < PPM_Eaddr) {
		if (Codec->ppm_flag == 0x95) Codec->ppm_flag = Codec->ppm_flag;
		if (Codec->ppm_Nppm == 0) {
			Codec->ppm_Saddr[Codec->ppm_flag]  = str->cur_p;
			Codec->ppm_Length[Codec->ppm_flag] = Nppm = Ref_4Byte(str);
			str->cur_p += Nppm;
		} else {
			Codec->ppm_Saddr2[Codec->ppm_flag]  = str->cur_p;
			Codec->ppm_Length2[Codec->ppm_flag] = Codec->ppm_Nppm;
			str->cur_p += Codec->ppm_Nppm;
			Codec->ppm_Nppm = 0;
		}
		Codec->ppm_flag++;
	}
	if (str->cur_p == PPM_Eaddr) {
		Codec->ppm_Nppm = 0;
	} else {
		Codec->ppm_flag--;
		Codec->ppm_Length[Codec->ppm_flag] = PPM_Eaddr - Codec->ppm_Saddr[Codec->ppm_flag];
		Codec->ppm_Nppm                    = str->cur_p - PPM_Eaddr;
		str->cur_p                         = PPM_Eaddr;
	}
	return EXIT_SUCCESS;
}

struct ECod_s *ECodCreate(struct Siz_s *Siz, struct J2kParam_s *J2kParam) {
	struct ECod_s *ECod;
	struct EECod_s *EECod;
	byte4 i, j, numTilesAndnumCmpts;
	uchar Scod;

	if (nullptr == (ECod = new struct ECod_s)) {
		printf("[ECodCreate:: ECod_s malloc error]\n");
		return nullptr;
	}

	ECod->numCompts     = Siz->Csiz;
	ECod->numTiles      = Siz->numTiles;
	numTilesAndnumCmpts = Siz->Csiz * Siz->numTiles;
	if (nullptr == (ECod->EECod = new struct EECod_s[numTilesAndnumCmpts])) {
		printf("[ECodCreate:: ECod->EECod_s[%d] malloc error]\n", numTilesAndnumCmpts);
		return nullptr;
	}

	for (i = 0; i < numTilesAndnumCmpts; i++) {
		EECod                  = &ECod->EECod[i];
		EECod->CbBypass        = J2kParam->CbBypass;
		EECod->CbH             = J2kParam->CbH;
		EECod->CbPreterm       = J2kParam->CbPreterm;
		EECod->CbReset         = J2kParam->CbReset;
		EECod->CbSegmentSymbol = J2kParam->CbSegmentSymbol;
		EECod->CbTerm          = J2kParam->CbTerm;
		EECod->CbVcausal       = J2kParam->CbVcausal;
		EECod->CbW             = J2kParam->CbW;
		EECod->DecompLev       = J2kParam->DecompLev;
		EECod->EphOn           = J2kParam->EphOn;
		EECod->Filter          = J2kParam->Filter;
		if (Siz->Csiz != 3)
			EECod->MC = 0;
		else
			EECod->MC = J2kParam->MC;
		EECod->numLayer  = J2kParam->numLayer;
		EECod->ProgOrder = J2kParam->ProgOrder;
		EECod->SopOn     = J2kParam->SopOn;
		EECod->RateMode  = J2kParam->RateControl;
	}

	for (i = 0; i < numTilesAndnumCmpts; i++) {
		EECod = &ECod->EECod[i];
		if (nullptr == (EECod->PrcH = new uchar[EECod->DecompLev + 1])) {
			printf("[ECodCreate:: EECod->PrcH[%d] malloc error]\n", (EECod->DecompLev + 1));
			return nullptr;
		}
		if (nullptr == (EECod->PrcW = new uchar[EECod->DecompLev + 1])) {
			printf("[ECodCreate:: EECod->PrcW[%d] malloc error]\n", (EECod->DecompLev + 1));
			return nullptr;
		}
	}
	for (i = 0; i < numTilesAndnumCmpts; i++) {
		EECod = &ECod->EECod[i];
		for (j = 0; j < EECod->DecompLev + 1; j++) {
			EECod->PrcH[j] = J2kParam->PrcH[j];
			EECod->PrcW[j] = J2kParam->PrcW[j];
		}
	}

	for (i = 0; i < numTilesAndnumCmpts; i++) {
		EECod = &ECod->EECod[i];
		Scod  = 0;
		for (j = 0; j <= EECod->DecompLev; j++) {
			if (EECod->PrcH[j] != 15) {
				Scod = 1;
				break;
			}
			if (EECod->PrcW[j] != 15) {
				Scod = 1;
				break;
			}
		}
		if (!Scod)
			EECod->Scod = 0;
		else
			EECod->Scod = 1;
	}
	return ECod;
}

void ECodDestory(struct ECod_s *ECod) {
	byte4 temp, i;
	temp = ECod->numCompts * ECod->numTiles;
	for (i = 0; i < temp; i++) {
		delete[] ECod->EECod[i].PrcH;
		delete[] ECod->EECod[i].PrcW;
	}
	delete[] ECod->EECod;
	delete[] ECod;
}

//[Func 8]
struct ECod_s *ECodDec(struct StreamChain_s *s, struct Codec_s *Codec, struct J2kParam_s *J2kParam) {
	struct Siz_s *Siz;
	struct ECod_s *ECod;
	ubyte4 ttt, tiles_cmpts;
	byte4 i, j;
	ubyte2 rrr, Lcod;
	uchar temp2, Scod, SPcod_G;

	Siz = Codec->Siz;
	j   = s->cur_p;

	Lcod                = Ref_2Byte(s);
	Scod                = Ref_1Byte(s);
	J2kParam->SopOn     = (uchar) (Scod & 0x02);
	J2kParam->EphOn     = (uchar) (Scod & 0x04);
	J2kParam->ProgOrder = Ref_1Byte(s);
	J2kParam->numLayer  = Ref_2Byte(s);
	J2kParam->MC        = Ref_1Byte(s);
	J2kParam->DecompLev = Ref_1Byte(s);
	J2kParam->CbW       = Ref_1Byte(s);
	J2kParam->CbH       = Ref_1Byte(s);
	SPcod_G             = Ref_1Byte(s);

	if (SPcod_G & 0x01) {
		if ((SPcod_G & 0xc0) == 0x00)
			J2kParam->CbBypass = 4;
		else if ((SPcod_G & 0xc0) == 0xc0)
			J2kParam->CbBypass = 3;
		else if ((SPcod_G & 0xc0) == 0x80)
			J2kParam->CbBypass = 2;
		else if ((SPcod_G & 0xc0) == 0x40)
			J2kParam->CbBypass = 1;
		else
			J2kParam->CbBypass = 0;
	} else
		J2kParam->CbBypass = 0;
	J2kParam->CbReset         = (uchar) (SPcod_G & 0x02);
	J2kParam->CbTerm          = (uchar) (SPcod_G & 0x04);
	J2kParam->CbVcausal       = (uchar) (SPcod_G & 0x08);
	J2kParam->CbPreterm       = (uchar) (SPcod_G & 0x10);
	J2kParam->CbSegmentSymbol = (uchar) (SPcod_G & 0x20);
	J2kParam->Filter          = Ref_1Byte(s);

	ECod = ECodCreate(Siz, J2kParam);

	for (i = 0; i < (Siz->Csiz * Siz->numTiles); i++) {
		ECod->EECod[i].DecompLev       = J2kParam->DecompLev;
		ECod->EECod[i].CbW             = J2kParam->CbW;
		ECod->EECod[i].CbH             = J2kParam->CbH;
		ECod->EECod[i].CbBypass        = J2kParam->CbBypass;
		ECod->EECod[i].CbReset         = J2kParam->CbReset;
		ECod->EECod[i].CbTerm          = J2kParam->CbTerm;
		ECod->EECod[i].CbVcausal       = J2kParam->CbVcausal;
		ECod->EECod[i].CbPreterm       = J2kParam->CbPreterm;
		ECod->EECod[i].CbSegmentSymbol = J2kParam->CbSegmentSymbol;
		ECod->EECod[i].EphOn           = J2kParam->EphOn;
		ECod->EECod[i].SopOn           = J2kParam->SopOn;
		ECod->EECod[i].ProgOrder       = J2kParam->ProgOrder;
		ECod->EECod[i].numLayer        = J2kParam->numLayer;
		//		if( J2kParam->MC_OFF )
		//			ECod->EECod[i].MC = 0;
		//		else
		ECod->EECod[i].MC     = J2kParam->MC;
		ECod->EECod[i].Scod   = Scod;
		ECod->EECod[i].Filter = J2kParam->Filter;
	}

	if (Scod & 0x01) {
		tiles_cmpts = ECod->numCompts * ECod->numTiles;
		for (rrr = 0; rrr <= J2kParam->DecompLev; rrr++) {
			temp2 = Ref_1Byte(s);
			for (ttt = 0; ttt < tiles_cmpts; ttt++) {
				ECod->EECod[ttt].PrcW[rrr] = (uchar) (temp2 & 0x0f);
				ECod->EECod[ttt].PrcH[rrr] = (uchar) (((temp2 & 0xf0) >> 4));
			}
		}
	}

	if ((ubyte2) (s->cur_p - j) != (Lcod)) {
		return nullptr;
	}
	return ECod;
}

byte4 ECodDecTilePart(struct StreamChain_s *s, struct Siz_s *siz, struct ECod_s *ECod, byte4 TileNo) {
	byte2 Lcod, ccc;
	byte4 ttt;
	char rrr, numDecompLev;
	uchar temp2;
	uchar Scod, SPcod_G;
	byte2 numLayer;
	uchar ProgOrder, MC, DecompLev, CbW, CbH, CbBypass, CbReset, CbTerm, CbVcausal;
	uchar CbPreterm, CbSegmentSymbol, Filter, PrcW, PrcH, SopOn, EphOn;
	byte4 j;

	if (ECod == nullptr) {
		return EXIT_FAILURE;
	}

	j    = s->cur_p;
	Lcod = Ref_2Byte(s);

	Scod = Ref_1Byte(s);
	PrcW = PrcH     = 15;
	SopOn           = (uchar) (Scod & 0x02);
	EphOn           = (uchar) (Scod & 0x04);
	ProgOrder       = Ref_1Byte(s);
	numLayer        = Ref_2Byte(s);
	MC              = Ref_1Byte(s);
	DecompLev       = Ref_1Byte(s);
	CbW             = Ref_1Byte(s);
	CbH             = Ref_1Byte(s);
	SPcod_G         = Ref_1Byte(s);
	CbBypass        = (uchar) (SPcod_G & 0x01);
	CbReset         = (uchar) (SPcod_G & 0x02);
	CbTerm          = (uchar) (SPcod_G & 0x04);
	CbVcausal       = (uchar) (SPcod_G & 0x08);
	CbPreterm       = (uchar) (SPcod_G & 0x10);
	CbSegmentSymbol = (uchar) (SPcod_G & 0x20);
	Filter          = Ref_1Byte(s);

	numDecompLev = (char) (DecompLev + 1);
	for (ccc = 0; ccc < siz->Csiz; ccc++) {
		ttt                              = TileNo + ccc * siz->numTiles;
		ECod->EECod[ttt].CbBypass        = CbBypass;
		ECod->EECod[ttt].CbH             = CbH;
		ECod->EECod[ttt].CbPreterm       = CbPreterm;
		ECod->EECod[ttt].CbReset         = CbReset;
		ECod->EECod[ttt].CbSegmentSymbol = CbSegmentSymbol;
		ECod->EECod[ttt].CbTerm          = CbTerm;
		ECod->EECod[ttt].CbVcausal       = CbVcausal;
		ECod->EECod[ttt].CbW             = CbW;
		ECod->EECod[ttt].EphOn           = EphOn;
		ECod->EECod[ttt].Filter          = Filter;
		ECod->EECod[ttt].MC              = MC;
		ECod->EECod[ttt].numLayer        = numLayer;
		ECod->EECod[ttt].ProgOrder       = ProgOrder;
		ECod->EECod[ttt].SopOn           = SopOn;
		ECod->EECod[ttt].DecompLev       = DecompLev;

		delete[] ECod->EECod[ttt].PrcH;
		ECod->EECod[ttt].PrcH = new uchar[numDecompLev];

		delete[] ECod->EECod[ttt].PrcW;
		ECod->EECod[ttt].PrcW = new uchar[numDecompLev];
	}

	if (Scod & 0x01) {
		for (rrr = 0; rrr < numDecompLev; rrr++) {
			temp2 = Ref_1Byte(s);
			for (ccc = 0; ccc < siz->Csiz; ccc++) {
				ttt                        = TileNo + ccc * siz->numTiles;
				ECod->EECod[ttt].PrcW[rrr] = (uchar) (temp2 & 0x0f);
				ECod->EECod[ttt].PrcH[rrr] = (uchar) (((temp2 & 0xf0) >> 4));
			}
		}
	} else {
		for (rrr = 0; rrr < numDecompLev; rrr++) {
			for (ccc = 0; ccc < siz->Csiz; ccc++) {
				ttt                        = TileNo + ccc * siz->numTiles;
				ECod->EECod[ttt].PrcW[rrr] = 0xf;
				ECod->EECod[ttt].PrcH[rrr] = 0xf;
			}
		}
	}

	if ((ubyte2) (s->cur_p - j) != (Lcod)) {
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}

//[Func 9]
byte4 ECocDec(struct ECod_s *ECod, struct StreamChain_s *s, byte4 TileNo, struct Codec_s *Codec) {
	byte4 i;
	byte4 kk, rrr;
	byte4 startTile, endTile, T;
	byte2 Ccoc;
	byte2 Lcoc;
	uchar Scoc, SPcoc_D, temp2;
	uchar DecompLev, CbW, CbH, CbBypass = '\0', CbReset, CbTerm, CbVcausal;
	uchar CbPreterm, CbSegmentSymbol;

	i = s->cur_p;
	if (TileNo == -1) {
		startTile = 0;
		endTile   = ECod->numTiles;
	} else {
		startTile = TileNo;
		endTile   = TileNo + 1;
	}

	Lcoc = Ref_2Byte(s);

	if (ECod->numCompts < 257)
		Ccoc = (ubyte2) Ref_1Byte(s);
	else
		Ccoc = Ref_2Byte(s);

	Scoc      = Ref_1Byte(s);
	DecompLev = Ref_1Byte(s); // SPcod_D SPcoc_A
	CbW       = Ref_1Byte(s); // SPcod_E SPcoc_B
	CbH       = Ref_1Byte(s); // SPcod_F SPcoc_C
	SPcoc_D   = Ref_1Byte(s); // SPcod_G SPcoc_D
	if (SPcoc_D & 0x01) {
		if ((SPcoc_D & 0xc0) == 0)
			CbBypass = 4;
		else if ((SPcoc_D & 0xc0) == 0xc0)
			CbBypass = 3;
		else if ((SPcoc_D & 0xc0) == 0x80)
			CbBypass = 2;
		else if ((SPcoc_D & 0xc0) == 0x40)
			CbBypass = 1;
	} else {
		CbBypass = 0;
	}
	CbReset                  = (uchar) (SPcoc_D & 0x02);
	CbTerm                   = (uchar) (SPcoc_D & 0x04);
	CbVcausal                = (uchar) (SPcoc_D & 0x08);
	CbPreterm                = (uchar) (SPcoc_D & 0x10);
	CbSegmentSymbol          = (uchar) (SPcoc_D & 0x20);
	ECod->EECod[Ccoc].Filter = Ref_1Byte(s);

	for (kk = startTile; kk < endTile; kk++) {
		T                              = Ccoc * ECod->numTiles + kk;
		ECod->EECod[T].DecompLev       = DecompLev;
		ECod->EECod[T].CbW             = CbW;
		ECod->EECod[T].CbH             = CbH;
		ECod->EECod[T].CbBypass        = CbBypass;
		ECod->EECod[T].CbReset         = CbReset;
		ECod->EECod[T].CbTerm          = CbTerm;
		ECod->EECod[T].CbVcausal       = CbVcausal;
		ECod->EECod[T].CbPreterm       = CbPreterm;
		ECod->EECod[T].CbSegmentSymbol = CbSegmentSymbol;
		ECod->EECod[T].Scod            = Scoc;
		delete[] ECod->EECod[T].PrcH;
		ECod->EECod[T].PrcH = new uchar[DecompLev + 1];
		delete[] ECod->EECod[T].PrcW;
		ECod->EECod[T].PrcW = new uchar[DecompLev + 1];
	}

	if (Scoc & 0x01) {
		for (rrr = 0; rrr <= DecompLev; rrr++) {
			temp2 = Ref_1Byte(s);
			for (kk = startTile; kk < endTile; kk++) {
				T                        = Ccoc * ECod->numTiles + kk;
				ECod->EECod[T].PrcW[rrr] = (uchar) (temp2 & 0x0f);
				ECod->EECod[T].PrcH[rrr] = (uchar) (((temp2 & 0xf0) >> 4));
			}
		}
	} else {
		for (rrr = 0; rrr <= DecompLev; rrr++) {
			for (kk = startTile; kk < endTile; kk++) {
				T                        = Ccoc * ECod->numTiles + kk;
				ECod->EECod[T].PrcH[rrr] = 15;
				ECod->EECod[T].PrcW[rrr] = 15;
			}
		}
	}
	if ((byte2) (s->cur_p - i) != (Lcoc)) {
		return EXIT_FAILURE;
	}

	if (Codec->debugFlag) {
		FILE *fp;
		fp = fopen(Codec->debugFname, "a+");
		fprintf(fp, "[ECocMarkerDec(%d)]\n", Ccoc);
		for (kk = startTile; kk < endTile; kk++) {
			T = Ccoc * ECod->numTiles + kk;
			switch (ECod->EECod[T].ProgOrder) {
				case 0:
					fprintf(fp, "Progression Order[%d] is LRCP\n", T);
					break;
				case 1:
					fprintf(fp, "Progression Order[%d] is RLCP\n", T);
					break;
				case 2:
					fprintf(fp, "Progression Order[%d] is RPCL\n", T);
					break;
				case 3:
					fprintf(fp, "Progression Order[%d] is PCRL\n", T);
					break;
				case 4:
					fprintf(fp, "Progression Order[%d] is CPRL\n", T);
					break;
				default:
					fprintf(fp, "Progression Order[%d] is ERROR\n", T);
					break;
			}
			fprintf(fp, "Number of layers[%d] = %d\n", T, ECod->EECod[T].numLayer);
			fprintf(fp, "Number of decomposition levels[%d] = %d\n", T, ECod->EECod[T].DecompLev);
			fprintf(fp, "Code-block width[%d]  = %d\n", T, ECod->EECod[T].CbW);
			fprintf(fp, "Code-block height[%d] = %d\n", T, ECod->EECod[T].CbH);
			if (ECod->EECod[T].MC)
				fprintf(fp, "Multi component transformation[%d] is on\n", T);
			else
				fprintf(fp, "Multi component transformation[%d] is off\n", T);
			if (SPcoc_D & 2) fprintf(fp, "RESET &");
			if (SPcoc_D & 4) fprintf(fp, "TERM &");
			if (SPcoc_D & 8) fprintf(fp, "Vcausal &");
			if (SPcoc_D & 0x10) fprintf(fp, "PredicTERM &");
			if (SPcoc_D & 0x20) fprintf(fp, "SegmentSYMBOL ");
			if (SPcoc_D) fprintf(fp, "\n");
			if (ECod->EECod[T].Filter)
				fprintf(fp, "Wavelet transformation[%d] is 53-filter\n", T);
			else
				fprintf(fp, "Wavelet transformation[%d] is 97-filter\n", T);
			if (Scoc & 0x01) {
				fprintf(fp, "Pricincts ON\n");
				for (rrr = 0; rrr <= DecompLev; rrr++) {
					for (kk = startTile; kk < endTile; kk++) {
						T = Ccoc * ECod->numTiles + kk;
						fprintf(fp, "\tECod->EECod[%d].PrcW_H[%d]=, %d, %d\n", T, rrr,
						        ECod->EECod[T].PrcW[rrr], ECod->EECod[T].PrcH[rrr]);
					}
				}
			}
		}
		fclose(fp);
	}
	return EXIT_SUCCESS;
}

//[Func 10]
struct EQcd_s *EQcdDec(struct ECod_s *ECod, struct StreamChain_s *s, struct Siz_s *Siz) {
	struct EQcd_s *EQcd;
	byte4 i, band, maxBandnum, startTile, endTile, T = 0, ttt;
	byte2 maxDecompLev, myu, e, ccc, Lqcd, e_myu;
	uchar nb, Sqcd = 0;

	i = s->cur_p;

	if (nullptr == (EQcd = EQCreate(ECod, Siz, DEC, nullptr))) {
		printf("[EQcdDec] :: EQCreate error\n");
		return nullptr;
	}
	startTile = 0;
	endTile   = ECod->numTiles;

	EQcd->numCompts = ECod->numCompts;
	EQcd->numTiles  = ECod->numTiles;
	Lqcd            = Ref_2Byte(s);
	Sqcd            = Ref_1Byte(s);

	maxDecompLev = 0;
	for (ccc = 0; ccc < EQcd->numCompts; ccc++) {
		for (ttt = startTile; ttt < endTile; ttt++) {
			T = ccc * EQcd->numTiles + ttt;
			if (maxDecompLev <= (byte2) ECod->EECod[T].DecompLev)
				maxDecompLev = (byte2) ECod->EECod[T].DecompLev;
		}
	}
	//	maxBandnum=maxDecompLev*3+1;
	//	maxBandnum = ECod->EECod[0].DecompLev*3+1;
	if (Sqcd & 0x02) {
		//		if( Lqcd!=(5+6*ECod->EECod[0].DecompLev) )
		//			return NULL;
		maxDecompLev = (byte2) ((Lqcd - 5) / 6);
		maxBandnum   = maxDecompLev * 3 + 1;
		for (band = 0; band < maxBandnum; band++) {
			e_myu                    = Ref_2Byte(s);
			EQcd->EEQcd[0].e[band]   = (char) (((e_myu & 0xf800) >> 11));
			EQcd->EEQcd[0].myu[band] = (ubyte2) (e_myu & 0x07ff);
		}
		for (ccc = 0; ccc < EQcd->numCompts; ccc++) {
			for (ttt = startTile; ttt < endTile; ttt++) {
				T = ccc * EQcd->numTiles + ttt;
				for (band = 0; band < (unsigned) (ECod->EECod[T].DecompLev * 3 + 1); band++) {
					EQcd->EEQcd[T].e[band]   = EQcd->EEQcd[0].e[band];
					EQcd->EEQcd[T].myu[band] = EQcd->EEQcd[0].myu[band];
				}
			}
		}
	} else if (Sqcd & 0x01) {
		e_myu = Ref_2Byte(s);
		e     = (char) ((e_myu & 0xf800) >> 11);
		myu   = (ubyte2) (e_myu & 0x07ff);
		for (ccc = 0; ccc < EQcd->numCompts; ccc++) {
			for (ttt = startTile; ttt < endTile; ttt++) {
				T                     = ccc * EQcd->numTiles + ttt;
				nb                    = ECod->EECod[T].DecompLev;
				EQcd->EEQcd[T].e[0]   = (char) (e - ECod->EECod[T].DecompLev + nb);
				EQcd->EEQcd[T].myu[0] = myu;
				for (band = 1; band < (unsigned) (ECod->EECod[T].DecompLev * 3 + 1); band++) {
					if (nb <= 0) return nullptr;
					EQcd->EEQcd[T].e[band]   = (char) (e - ECod->EECod[T].DecompLev + nb);
					EQcd->EEQcd[T].myu[band] = myu;
					if (band % 3 == 0) nb--;
				}
			}
		}
	} else if (!(Sqcd & 0x03)) {
		maxDecompLev = (byte2) ((Lqcd - 4) / 3);
		maxBandnum   = maxDecompLev * 3 + 1;
		for (band = 0; band < maxBandnum; band++) {
			e_myu                    = (ubyte2) Ref_1Byte(s);
			EQcd->EEQcd[0].e[band]   = (char) (((e_myu & 0xf8) >> 3));
			EQcd->EEQcd[0].myu[band] = 0;
		}
		for (ccc = 0; ccc < EQcd->numCompts; ccc++) {
			for (ttt = startTile; ttt < endTile; ttt++) {
				T = ccc * EQcd->numTiles + ttt;
				for (band = 0; band < (unsigned) (ECod->EECod[T].DecompLev * 3 + 1); band++) {
					EQcd->EEQcd[T].e[band]   = EQcd->EEQcd[0].e[band];
					EQcd->EEQcd[T].myu[band] = 0;
				}
			}
		}
	} else
		return nullptr;

	for (ccc = 0; ccc < EQcd->numCompts; ccc++) {
		for (ttt = startTile; ttt < endTile; ttt++) {
			T                   = ccc * EQcd->numTiles + ttt;
			EQcd->EEQcd[T].Sqcd = (char) ((Sqcd & 0x03));
			EQcd->EEQcd[T].G    = (char) (((Sqcd & 0xe0) >> 5));
		}
	}

	if ((byte2) (s->cur_p - i) != (Lqcd)) {
		return nullptr;
	}

	return EQcd;
}
//[Func 6]
struct EPpm_s *EPpmCreate(byte4 numTile) {
	struct EPpm_s *EPpm;
	if (nullptr == (EPpm = new struct EPpm_s)) {
		printf("[EPpmCreate]:: EPpm malloc error\n");
		return nullptr;
	}

	if (nullptr == (EPpm->EEPpm = new struct EEPpm_s[numTile])) {
		printf("[EPpmCreate]:: EEPpm malloc error\n");
		return nullptr;
	}

	return EPpm;
}

void EPpmDestory(struct EPpm_s *EPpm) {
	delete[] EPpm->EEPpm;
	delete EPpm;
}

struct Poc_s *PocDec(struct Poc_s *Poc, struct StreamChain_s *s, struct Codec_s *Codec) {
	byte4 Lpoc;
	byte4 i, j;
	byte2 numProgOrderChange;
	ubyte2 *CSpoc = nullptr, *CEpoc = nullptr, *LYEpoc = nullptr;
	uchar *RSpoc = nullptr, *REpoc = nullptr, *Ppoc = nullptr;

	//	printf("[PocDec]\n");
	j = s->cur_p;
	if (Poc == nullptr) {
		numProgOrderChange = 0;
		if (nullptr == (Poc = new struct Poc_s)) {
			printf("[PocDec]:: Poc malloc error\n");
			return nullptr;
		}
		Poc->numProgOrderChange = 0;
	} else {
		//		printf("[PocDec] 2nd PocDec\n");
		numProgOrderChange = (byte2) Poc->numProgOrderChange;
		if (nullptr == (CSpoc = new ubyte2[numProgOrderChange])) {
			printf("[PocDec]:: CSpoc malloc error\n");
			return nullptr;
		}
		if (nullptr == (CEpoc = new ubyte2[numProgOrderChange])) {
			printf("[PocDec]:: CEpoc malloc error\n");
			return nullptr;
		}
		if (nullptr == (RSpoc = new uchar[numProgOrderChange])) {
			printf("[PocDec]:: RSpoc malloc error\n");
			return nullptr;
		}
		if (nullptr == (REpoc = new uchar[numProgOrderChange])) {
			printf("[PocDec]:: REpoc malloc error\n");
			return nullptr;
		}
		if (nullptr == (LYEpoc = new ubyte2[numProgOrderChange])) {
			printf("[PocDec]:: LYEpoc malloc error\n");
			return nullptr;
		}
		if (nullptr == (Ppoc = new uchar[numProgOrderChange])) {
			printf("[PocDec]:: Ppoc malloc error\n");
			return nullptr;
		}

		for (i = 0; i < numProgOrderChange; i++) {
			CSpoc[i]  = Poc->CSpoc[i];
			CEpoc[i]  = Poc->CEpoc[i];
			RSpoc[i]  = Poc->RSpoc[i];
			REpoc[i]  = Poc->REpoc[i];
			LYEpoc[i] = Poc->LYEpoc[i];
			Ppoc[i]   = Poc->Ppoc[i];
		}
		delete[] Poc->CSpoc;
		delete[] Poc->CEpoc;
		delete[] Poc->RSpoc;
		delete[] Poc->REpoc;
		delete[] Poc->LYEpoc;
		delete[] Poc->Ppoc;
	}
	Lpoc = Ref_2Byte(s);
	if (Codec->Siz->Csiz > 256)
		Poc->numProgOrderChange += (Lpoc - 2) / 9;
	else
		Poc->numProgOrderChange += (Lpoc - 2) / 7;

	//	printf("[PocDec]Poc->numProgOrderChange=%d\n", Poc->numProgOrderChange);
	if (nullptr == (Poc->CSpoc = new ubyte2[Poc->numProgOrderChange])) {
		printf("[PocDec]:: Poc->CSpoc malloc error\n");
		return nullptr;
	}
	if (nullptr == (Poc->CEpoc = new ubyte2[Poc->numProgOrderChange])) {
		printf("[PocDec]:: Poc->CEpoc malloc error\n");
		return nullptr;
	}
	if (nullptr == (Poc->RSpoc = new uchar[Poc->numProgOrderChange])) {
		printf("[PocDec]:: Poc->RSpoc malloc error\n");
		return nullptr;
	}
	if (nullptr == (Poc->REpoc = new uchar[Poc->numProgOrderChange])) {
		printf("[PocDec]:: Poc->REpoc malloc error\n");
		return nullptr;
	}
	if (nullptr == (Poc->LYEpoc = new ubyte2[Poc->numProgOrderChange])) {
		printf("[PocDec]:: Poc->LYEpoc malloc error\n");
		return nullptr;
	}
	if (nullptr == (Poc->Ppoc = new uchar[Poc->numProgOrderChange])) {
		printf("[PocDec]:: Poc->Ppoc malloc error\n");
		return nullptr;
	}
	for (i = 0; i < numProgOrderChange; i++) {
		Poc->CSpoc[i]  = CSpoc[i];
		Poc->CEpoc[i]  = CEpoc[i];
		Poc->RSpoc[i]  = RSpoc[i];
		Poc->REpoc[i]  = REpoc[i];
		Poc->LYEpoc[i] = LYEpoc[i];
		Poc->Ppoc[i]   = Ppoc[i];
	}
	for (; i < Poc->numProgOrderChange; i++) {
		Poc->RSpoc[i] = Ref_1Byte(s);
		if (Codec->Siz->Csiz > 256)
			Poc->CSpoc[i] = Ref_2Byte(s);
		else
			Poc->CSpoc[i] = Ref_1Byte(s);

		Poc->LYEpoc[i] = Ref_2Byte(s);
		Poc->REpoc[i]  = Ref_1Byte(s);
		if (Codec->Siz->Csiz > 256)
			Poc->CEpoc[i] = Ref_2Byte(s);
		else
			Poc->CEpoc[i] = Ref_1Byte(s);
		Poc->Ppoc[i] = Ref_1Byte(s);
	}

	if ((ubyte2) (s->cur_p - j) != (Lpoc)) {
		return nullptr;
	}
	return Poc;
}

byte4 MarkerSkip(struct StreamChain_s *s) {
	byte4 L;
	L = Ref_2Byte(s);
	s->cur_p += (L - 2);

	return L;
}

byte4 ERgnMarDec(struct ERgn_s *ERgn, struct StreamChain_s *str) {
	byte2 Lrgn;
	byte2 Crgn;
	uchar Srgn;
	uchar SPrgn;

	Lrgn = Ref_2Byte(str);
	if (Lrgn == 5)
		Crgn = Ref_1Byte(str);
	else if (Lrgn == 6)
		Crgn = Ref_2Byte(str);
	else {
		printf("[ERgnMarDec] RGN marker Lrgn value is incollected. Lrgn=%d\n", Lrgn);
		return EXIT_FAILURE;
	}
	Srgn  = Ref_1Byte(str);
	SPrgn = Ref_1Byte(str);

	ERgn->EERgn[Crgn].Crgn  = Crgn;
	ERgn->EERgn[Crgn].Lrgn  = Lrgn;
	ERgn->EERgn[Crgn].Srgn  = Srgn;
	ERgn->EERgn[Crgn].SPrgn = SPrgn;

	return EXIT_SUCCESS;
}

byte4 EQcdDecTilePart(struct EQcd_s *EQcd, struct ECod_s *ECod, struct StreamChain_s *s, byte4 TileNo,
                      struct Siz_s *Siz) {
	ubyte2 Lqcd;
	uchar Sqcd = 0;
	ubyte4 i;
	uchar nb;
	byte2 maxDecompLev, jj;
	ubyte4 band;
	ubyte2 myu;
	uchar e;
	byte4 startTile, endTile, T, tt;
	ubyte2 cc;
	i = s->cur_p;

	if (EQcd == nullptr) {
		if (TileNo == NULL)
			EQcd = EQCreate(ECod, Siz, DEC, nullptr);
		else
			return EXIT_FAILURE;
	}

	if (TileNo == NULL) {
		startTile = 0;
		endTile   = ECod->numTiles;
	} else {
		startTile = TileNo;
		endTile   = TileNo + 1;
	}

	EQcd->numCompts = ECod->numCompts;
	EQcd->numTiles  = ECod->numTiles;

	Lqcd = Ref_2Byte(s);
	Sqcd = Ref_1Byte(s);

	maxDecompLev = ECod->EECod[startTile].DecompLev;
	for (cc = 0; cc < EQcd->numCompts; cc++) {
		for (tt = startTile; tt < endTile; tt++) {
			T = cc * EQcd->numTiles + tt;
			if (maxDecompLev >= (byte2) ECod->EECod[T].DecompLev)
				maxDecompLev = (ubyte2) ECod->EECod[T].DecompLev;
		}
	}
	maxDecompLev = (byte2) (maxDecompLev * 3 + 1);

	if (Sqcd & 0x02) {
		if (Lqcd != 3 + 2 * maxDecompLev) return EXIT_FAILURE;
		ubyte2 *e_myu;
		ubyte2 jj;
		e_myu = new ubyte2[maxDecompLev];
		for (jj = 0; jj < maxDecompLev; jj++)
			e_myu[jj] = Ref_2Byte(s);
		for (cc = 0; cc < EQcd->numCompts; cc++) {
			for (tt = startTile; tt < endTile; tt++) {
				T = cc * EQcd->numTiles + tt;
				for (band = 0; band < (unsigned) (ECod->EECod[T].DecompLev * 3 + 1); band++) {
					EQcd->EEQcd[T].e[band]   = (char) (((e_myu[band] & 0xf800) >> 11));
					EQcd->EEQcd[T].myu[band] = (ubyte2) (e_myu[band] & 0x07ff);
				}
			}
		}
		delete[] e_myu;
	} else if (Sqcd & 0x01) {
		if (Lqcd != 5) return EXIT_FAILURE;
		ubyte2 e_myu;
		e_myu = Ref_2Byte(s);
		e     = ((e_myu & 0xf800) > 11);
		myu   = (ubyte2) (e_myu & 0x07ff);
		for (cc = 0; cc < EQcd->numCompts; cc++) {
			for (tt = startTile; tt < endTile; tt++) {
				T                     = cc * EQcd->numTiles + tt;
				nb                    = ECod->EECod[T].DecompLev;
				EQcd->EEQcd[T].e[0]   = (char) (e - ECod->EECod[T].DecompLev + nb);
				EQcd->EEQcd[T].myu[0] = myu;
				for (band = 1; band < (unsigned) (ECod->EECod[T].DecompLev * 3 + 1); band++) {
					if (nb <= 0) return EXIT_FAILURE;
					EQcd->EEQcd[T].e[band]   = (char) (e - ECod->EECod[T].DecompLev + nb);
					EQcd->EEQcd[T].myu[band] = myu;
					if (band % 3 == 0) nb--;
				}
			}
		}
	} else if (!(Sqcd & 0x03)) {
		if (Lqcd != 3 + maxDecompLev) return EXIT_FAILURE;
		ubyte2 *e_myu;
		e_myu = new ubyte2[maxDecompLev];
		for (jj = 0; jj < maxDecompLev; jj++)
			e_myu[jj] = (ubyte2) Ref_1Byte(s);
		for (cc = 0; cc < EQcd->numCompts; cc++) {
			for (tt = startTile; tt < endTile; tt++) {
				T = cc * EQcd->numTiles + tt;
				for (band = 0; band < (unsigned) (ECod->EECod[T].DecompLev * 3 + 1); band++) {
					EQcd->EEQcd[T].e[band]   = (char) ((e_myu[band] & 0xf8) >> 3);
					EQcd->EEQcd[T].myu[band] = 0;
				}
			}
		}
		delete[] e_myu;
	} else
		return EXIT_FAILURE;

	for (cc = 0; cc < EQcd->numCompts; cc++) {
		for (tt = startTile; tt < endTile; tt++) {
			T                   = cc * EQcd->numTiles + tt;
			EQcd->EEQcd[T].Sqcd = (char) (Sqcd & 0x03);
			EQcd->EEQcd[T].G    = (char) ((Sqcd & 0xe0) >> 5);
		}
	}

	if ((ubyte2) (s->cur_p - i) != (Lqcd + 2)) {
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}

//[Func 11]
byte4 EQccMarDec(struct EQcd_s *EQcd, struct ECod_s *ECod, struct StreamChain_s *s, byte4 TileNo) {
	ubyte2 Lqcc, Cqcc, Sqcc;
	ubyte4 i;
	byte4 startTile, endTile, T, tt;
	ubyte2 maxDecompLev, jj;
	ubyte4 band;
	ubyte2 myu;
	char e;
	char nb;
	uchar CqccByte;
	i = s->cur_p;

	if (TileNo == NULL) {
		startTile = 0;
		endTile   = EQcd->numTiles;
	} else {
		startTile = TileNo;
		endTile   = TileNo + 1;
	}

	Lqcc = Ref_2Byte(s);

	if (ECod->numCompts < 257) {
		Cqcc     = (ubyte2) Ref_1Byte(s);
		CqccByte = 1;
	} else {
		Cqcc     = Ref_2Byte(s);
		CqccByte = 2;
	}

	Sqcc         = Ref_1Byte(s);
	maxDecompLev = 0;
	for (tt = startTile; tt < endTile; tt++) {
		T = Cqcc * EQcd->numTiles + tt;
		if (maxDecompLev <= (ubyte2) ECod->EECod[T].DecompLev)
			maxDecompLev = (ubyte2) ECod->EECod[T].DecompLev;
	}
	maxDecompLev = (ubyte2) (maxDecompLev * 3 + 1);

	if (Sqcc & 0x02) {
		if (Lqcc != (3 + CqccByte + 2 * maxDecompLev)) {
			printf("[EQccMarDec]:: Lqcc is error. Lqcc=%d Ref_data %d\n", Lqcc,
			       (3 + CqccByte + 2 * maxDecompLev));
			return EXIT_FAILURE;
		}
		ubyte2 *e_myu;
		ubyte2 jj;
		e_myu = new ubyte2[maxDecompLev];
		for (jj = 0; jj < maxDecompLev; jj++)
			e_myu[jj] = Ref_2Byte(s);
		for (tt = startTile; tt < endTile; tt++) {
			T = Cqcc * EQcd->numTiles + tt;
			for (band = 0; band < (unsigned) (ECod->EECod[T].DecompLev * 3 + 1); band++) {
				EQcd->EEQcd[T].e[band]   = (char) ((e_myu[band] & 0xf800) >> 11);
				EQcd->EEQcd[T].myu[band] = (ubyte2) (e_myu[band] & 0x07ff);
			}
		}
		delete[] e_myu;
	} else if (Sqcc & 0x01) {
		if (Lqcc != (5 + CqccByte)) {
			printf("[EQccMarDec]:: Lqcc is error. Lqcc=%d Ref_data %d\n", Lqcc, (5 + CqccByte));
			return EXIT_FAILURE;
		}
		ubyte2 e_myu;
		e_myu = Ref_2Byte(s);
		e     = (char) ((e_myu & 0xf800) >> 11);
		myu   = (ubyte2) (e_myu & 0x07ff);
		for (tt = startTile; tt < endTile; tt++) {
			T                     = Cqcc * EQcd->numTiles + tt;
			nb                    = ECod->EECod[T].DecompLev;
			EQcd->EEQcd[T].e[0]   = (char) (e - ECod->EECod[T].DecompLev + nb);
			EQcd->EEQcd[T].myu[0] = myu;
			for (band = 1; band < (unsigned) (ECod->EECod[T].DecompLev * 3 + 1); band++) {
				if (nb <= 0) {
					printf("[EQccMarDec]:: nb is positive. nb=%d \n", nb);
					return EXIT_FAILURE;
				}
				EQcd->EEQcd[T].e[band]   = (char) (e - ECod->EECod[T].DecompLev + nb);
				EQcd->EEQcd[T].myu[band] = myu;
				if (band % 3 == 0) nb--;
			}
		}
	} else if (!(Sqcc & 0x03)) {
		if (Lqcc != (3 + CqccByte + maxDecompLev)) {
			printf("[EQccMarDec]:: Lqcc is error. Lqcc=%d Ref_data %d\n", Lqcc, (3 + CqccByte + maxDecompLev));
			return EXIT_FAILURE;
		}
		ubyte2 *e_myu;
		if (nullptr == (e_myu = new ubyte2[maxDecompLev])) {
			printf("[EQccMarDec]:: e_myu malloc error\n");
			return EXIT_FAILURE;
		}
		for (jj = 0; jj < maxDecompLev; jj++)
			e_myu[jj] = (ubyte2) Ref_1Byte(s);
		for (tt = startTile; tt < endTile; tt++) {
			T = Cqcc * EQcd->numTiles + tt;
			for (band = 0; band < (unsigned) (ECod->EECod[T].DecompLev * 3 + 1); band++) {
				EQcd->EEQcd[T].e[band]   = (char) (((e_myu[band] & 0xf8) >> 3));
				EQcd->EEQcd[T].myu[band] = 0;
			}
		}
		delete[] e_myu;
	} else {
		printf("[EQccMarDec]:: sqcc error. sqcc=%d\n", Sqcc);
		return EXIT_FAILURE;
	}
	for (tt = startTile; tt < endTile; tt++) {
		T                   = Cqcc * EQcd->numTiles + tt;
		EQcd->EEQcd[T].Sqcd = (char) (Sqcc & 0x03);
		EQcd->EEQcd[T].G    = (char) ((Sqcc & 0xe0) >> 5);
	}
	return EXIT_SUCCESS;
}

//[Func 27]
struct StreamChain_s *CodMarWrite(struct StreamChain_s *s, struct ECod_s *ECod, byte4 t) {
	byte4 index;
	struct EECod_s *EECod;
	ubyte2 Lcod;
	uchar DecompLev, i, flag, Scod, CbStyle, PrcWH;

	if (t == NULL)
		index = 0;
	else
		index = t;

	EECod     = &ECod->EECod[index];
	DecompLev = EECod->DecompLev;
	for (i = 0, flag = 0; i <= DecompLev; i++) {
		if (EECod->PrcH[i] != 15) flag++;
		if (EECod->PrcW[i] != 15) flag++;
	}
	if (flag == 0) {
		Lcod = 12;
	} else {
		Lcod = (ubyte2) (13 + DecompLev);
	}
	s    = Stream2ByteWrite(s, COD, BUF_LENGTH, TT_BIG_ENDIAN);
	s    = Stream2ByteWrite(s, Lcod, BUF_LENGTH, TT_BIG_ENDIAN);
	Scod = 0;
	Scod |= (uchar) ((flag) ? (uchar) 1 : (uchar) 0);
	Scod |= (uchar) ((EECod->SopOn) ? (uchar) 2 : (uchar) 0);
	Scod |= (uchar) ((EECod->EphOn) ? (uchar) 4 : (uchar) 0);
	s       = Stream1ByteWrite(s, Scod, BUF_LENGTH);
	s       = Stream1ByteWrite(s, EECod->ProgOrder, BUF_LENGTH);
	s       = Stream2ByteWrite(s, EECod->numLayer, BUF_LENGTH, TT_BIG_ENDIAN);
	s       = Stream1ByteWrite(s, EECod->MC, BUF_LENGTH);
	s       = Stream1ByteWrite(s, DecompLev, BUF_LENGTH);
	s       = Stream1ByteWrite(s, EECod->CbW, BUF_LENGTH);
	s       = Stream1ByteWrite(s, EECod->CbH, BUF_LENGTH);
	CbStyle = 0;
	if (EECod->CbBypass == 3)
		CbStyle = 0xc0;
	else if (EECod->CbBypass == 2)
		CbStyle = 0x80;
	else if (EECod->CbBypass == 1)
		CbStyle = 0x40;
	else
		CbStyle = 0x00;
	CbStyle |= (uchar) ((EECod->CbBypass) ? 1 : 0);
	CbStyle |= (uchar) ((EECod->CbReset) ? 2 : 0);
	CbStyle |= (uchar) ((EECod->CbTerm) ? 4 : 0);
	CbStyle |= (uchar) ((EECod->CbVcausal) ? 8 : 0);
	CbStyle |= (uchar) ((EECod->CbPreterm) ? 16 : 0);
	CbStyle |= (uchar) ((EECod->CbSegmentSymbol) ? 32 : 0);
	s = Stream1ByteWrite(s, CbStyle, BUF_LENGTH);
	s = Stream1ByteWrite(s, EECod->Filter, BUF_LENGTH);
	if (flag) {
		for (i = 0; i <= DecompLev; i++) {
			PrcWH = (uchar) ((EECod->PrcH[i] << 4) & 0xf0);
			PrcWH |= (uchar) (EECod->PrcW[i] & 0x0f);
			s = Stream1ByteWrite(s, PrcWH, BUF_LENGTH);
		}
	}
	return s;
}

byte4 CodMarByteCalculate(struct ECod_s *ECod, byte4 TileNumber) {
	uchar DecompLev;
	ubyte2 Lcod;
	byte2 ccc;
	uchar Scod;

	ccc       = 0;
	DecompLev = ECod->EECod[ECod->numTiles * ccc + TileNumber].DecompLev;
	Scod      = (uchar) ((ECod->EECod[ECod->numTiles * ccc + TileNumber].Scod) & 0x01);
	if (Scod)
		Lcod = (ubyte2) (13 + DecompLev);
	else
		Lcod = 12;
	return (Lcod + 2);
}

struct StreamChain_s *CocMarWrite(struct StreamChain_s *s, struct ECod_s *ECod, byte4 c, byte4 t) {
	uchar DecompLev, i, flag, Scoc, CbStyle, PrcWH;
	ubyte2 Lcoc;
	byte4 index;
	struct EECod_s *EECod;

	if (t == NULL)
		index = 0;
	else
		index = t;

	EECod     = &ECod->EECod[c * ECod->numTiles + index];
	DecompLev = EECod->DecompLev;
	flag      = EECod->Scod;
	if (ECod->numCompts < 257) {
		if (flag == 0)
			Lcoc = 9;
		else
			Lcoc = (ubyte2) (10 + DecompLev);
	} else {
		if (flag == 0)
			Lcoc = 10;
		else
			Lcoc = (ubyte2) (11 + DecompLev);
	}

	s    = Stream2ByteWrite(s, COC, BUF_LENGTH, TT_BIG_ENDIAN);
	s    = Stream2ByteWrite(s, Lcoc, BUF_LENGTH, TT_BIG_ENDIAN);
	Scoc = 0;
	Scoc |= (uchar) (flag ? 1 : 0);
	if (ECod->numCompts < 257)
		s = Stream1ByteWrite(s, (uchar) c, BUF_LENGTH);
	else
		s = Stream2ByteWrite(s, (ubyte2) c, BUF_LENGTH, TT_BIG_ENDIAN);
	s       = Stream1ByteWrite(s, Scoc, BUF_LENGTH);
	s       = Stream1ByteWrite(s, DecompLev, BUF_LENGTH);
	s       = Stream1ByteWrite(s, EECod->CbW, BUF_LENGTH);
	s       = Stream1ByteWrite(s, EECod->CbH, BUF_LENGTH);
	CbStyle = 0;
	if (EECod->CbBypass == 3)
		CbStyle = 0xc0;
	else if (EECod->CbBypass == 2)
		CbStyle = 0x80;
	else if (EECod->CbBypass == 1)
		CbStyle = 0x40;
	else
		CbStyle = 0x00;
	CbStyle |= (uchar) ((EECod->CbBypass) ? 1 : 0);
	CbStyle |= (uchar) ((EECod->CbReset) ? 2 : 0);
	CbStyle |= (uchar) ((EECod->CbTerm) ? 4 : 0);
	CbStyle |= (uchar) ((EECod->CbVcausal) ? 8 : 0);
	CbStyle |= (uchar) ((EECod->CbPreterm) ? 16 : 0);
	CbStyle |= (uchar) ((EECod->CbSegmentSymbol) ? 32 : 0);
	s = Stream1ByteWrite(s, CbStyle, BUF_LENGTH);
	s = Stream1ByteWrite(s, EECod->Filter, BUF_LENGTH);
	if (flag) {
		for (i = 0; i <= DecompLev; i++) {
			PrcWH = (uchar) ((EECod->PrcH[i] << 4) & 0xf0);
			PrcWH |= (uchar) (EECod->PrcW[i] & 0x0f);
			s = Stream1ByteWrite(s, PrcWH, BUF_LENGTH);
		}
	}

	return s;
}

byte4 CocMarByteCalculate(struct ECod_s *ECod, struct Siz_s *Siz, byte4 t, byte2 ccc) {
	uchar DecompLev;
	ubyte2 Lcoc;
	uchar Scoc;

	DecompLev = ECod->EECod[ECod->numTiles * ccc + t].DecompLev;
	Scoc      = (uchar) ((ECod->EECod[ECod->numTiles * ccc + t].Scod) & 0x01);
	if (Siz->Csiz < 257) {
		if (Scoc)
			Lcoc = (ubyte2) (10 + DecompLev);
		else
			Lcoc = 9;
	} else {
		if (Scoc)
			Lcoc = (ubyte2) (11 + DecompLev);
		else
			Lcoc = 10;
	}

	return (Lcoc + 2);
}

struct StreamChain_s *SopMarWrite(struct StreamChain_s *s, byte4 index) {
	s = Stream2ByteWrite(s, SOP, s->buf_length, TT_BIG_ENDIAN);
	s = Stream2ByteWrite(s, 4, s->buf_length, TT_BIG_ENDIAN);
	s = Stream2ByteWrite(s, (ubyte2) index, s->buf_length, TT_BIG_ENDIAN);
	return s;
}

struct StreamChain_s *SocMarWrite(struct StreamChain_s *s) {
	s = Stream2ByteWrite(s, SOC, 2, TT_BIG_ENDIAN);
	return s;
}

struct StreamChain_s *EocMarWrite(struct StreamChain_s *s) {
	s = Stream2ByteWrite(s, EOC, 2, TT_BIG_ENDIAN);
	return s;
}

struct StreamChain_s *SotMarWrite(struct StreamChain_s *s, byte4 Isot, ubyte4 Psot, uchar TPsot,
                                  uchar TNsot) {
	ubyte2 Lsot;

	Lsot = 10;
	s    = Stream2ByteWrite(s, SOT, s->buf_length /*12*/, TT_BIG_ENDIAN);
	s    = Stream2ByteWrite(s, Lsot, s->buf_length /*10*/, TT_BIG_ENDIAN);
	s    = Stream2ByteWrite(s, (ubyte2) Isot, s->buf_length /*8*/, TT_BIG_ENDIAN);
	s    = Stream4ByteWrite(s, Psot, s->buf_length /*6*/, TT_BIG_ENDIAN);
	s    = Stream1ByteWrite(s, TPsot, s->buf_length /*2*/);
	s    = Stream1ByteWrite(s, TNsot, s->buf_length /*1*/);
	return s;
}

struct StreamChain_s *SizMarWrite(struct StreamChain_s *s, struct Siz_s *Siz) {
	ubyte2 i, Lsiz;

	Lsiz = (ubyte2) (38 + 3 * Siz->Csiz);
	s    = Stream2ByteWrite(s, SIZ, BUF_LENGTH, TT_BIG_ENDIAN);
	s    = Stream2ByteWrite(s, Lsiz, BUF_LENGTH, TT_BIG_ENDIAN);
	s    = Stream2ByteWrite(s, Siz->Rsiz, BUF_LENGTH, TT_BIG_ENDIAN);
	s    = Stream4ByteWrite(s, Siz->Xsiz, BUF_LENGTH, TT_BIG_ENDIAN);
	s    = Stream4ByteWrite(s, Siz->Ysiz, BUF_LENGTH, TT_BIG_ENDIAN);
	s    = Stream4ByteWrite(s, Siz->XOsiz, BUF_LENGTH, TT_BIG_ENDIAN);
	s    = Stream4ByteWrite(s, Siz->YOsiz, BUF_LENGTH, TT_BIG_ENDIAN);
	s    = Stream4ByteWrite(s, Siz->XTsiz, BUF_LENGTH, TT_BIG_ENDIAN);
	s    = Stream4ByteWrite(s, Siz->YTsiz, BUF_LENGTH, TT_BIG_ENDIAN);
	s    = Stream4ByteWrite(s, Siz->XTOsiz, BUF_LENGTH, TT_BIG_ENDIAN);
	s    = Stream4ByteWrite(s, Siz->YTOsiz, BUF_LENGTH, TT_BIG_ENDIAN);
	s    = Stream2ByteWrite(s, Siz->Csiz, BUF_LENGTH, TT_BIG_ENDIAN);
	for (i = 0; i < Siz->Csiz; i++) {
		s = Stream1ByteWrite(s, Siz->Ssiz[i], BUF_LENGTH);
		s = Stream1ByteWrite(s, Siz->XRsiz[i], BUF_LENGTH);
		s = Stream1ByteWrite(s, Siz->YRsiz[i], BUF_LENGTH);
	}
	return s;
}

struct StreamChain_s *RgnMarWrite(struct StreamChain_s *str, struct ERgn_s *ERgn, ubyte2 ccc) {
	if (ERgn == nullptr) {
		printf("[RgmMarWrite]:: ERgn is not created!\n");
		return nullptr;
	}

	str = Stream2ByteWrite(str, RGN, str->buf_length, TT_BIG_ENDIAN);

	if ((&ERgn->EERgn[ccc]) == nullptr) {
		printf("[RgmMarWrite]:: ERgn->EERgn[%d]=%x\n", ccc, &ERgn->EERgn[ccc]);
		return nullptr;
	}
	str = Stream2ByteWrite(str, ERgn->EERgn[ccc].Lrgn, str->buf_length, TT_BIG_ENDIAN);
	if (ERgn->EERgn[ccc].Lrgn == 5)
		str = Stream1ByteWrite(str, (uchar) ERgn->EERgn[ccc].Crgn, str->buf_length);
	else if (ERgn->EERgn[ccc].Lrgn == 6)
		str = Stream2ByteWrite(str, ERgn->EERgn[ccc].Crgn, str->buf_length, TT_BIG_ENDIAN);

	str = Stream1ByteWrite(str, 0x00, str->buf_length);
	str = Stream1ByteWrite(str, ERgn->EERgn[ccc].SPrgn, str->buf_length);
	return str;
}

//[Func 28]
struct StreamChain_s *QcdMarWrite(struct StreamChain_s *s, struct ECod_s *ECod, struct EQcd_s *EQcd,
                                  byte4 t) {
	byte4 index;
	ubyte2 Lqcd, Q;
	uchar DecompLev, Sqcd0, Sqcd1, numQ, i;

	if (t == NULL)
		index = 0;
	else
		index = t;

	DecompLev = ECod->EECod[index].DecompLev;
	Sqcd1     = (uchar) ((EQcd->EEQcd[index].Sqcd) & 0x1f);
	switch (Sqcd1) {
		case 0:
			Lqcd = (ubyte2) (4 + 3 * DecompLev);
			break;
		case 1:
			Lqcd = 5;
			break;
		case 2:
			Lqcd = (ubyte2) (5 + 6 * DecompLev);
			break;
		default:
			printf("[QcdMarWrite]:: Sqcd error. Sqcd=%d\n", Sqcd1);
			return nullptr;
	}

	s     = Stream2ByteWrite(s, QCD, BUF_LENGTH, TT_BIG_ENDIAN);
	s     = Stream2ByteWrite(s, Lqcd, BUF_LENGTH, TT_BIG_ENDIAN);
	Sqcd0 = (uchar) (Sqcd1 | ((EQcd->EEQcd[index].G) << 5));
	s     = Stream1ByteWrite(s, Sqcd0, BUF_LENGTH);

	switch (Sqcd1) {
		case 0:
			numQ = (uchar) (1 + 3 * DecompLev);
			for (i = 0; i < numQ; i++) {
				s = Stream1ByteWrite(s, (uchar) ((0x1f & EQcd->EEQcd[index].e[i]) << 3), BUF_LENGTH);
			}
			break;
		case 1:
			s = Stream2ByteWrite(s, (uchar) ((0x1f & EQcd->EEQcd[index].e[0]) << 11), BUF_LENGTH,
			                     TT_BIG_ENDIAN);
			break;
		case 2:
			numQ = (uchar) (1 + 3 * DecompLev);
			for (i = 0; i < numQ; i++) {
				Q = (ubyte2) (((ubyte2) (0x1f & EQcd->EEQcd[index].e[i]) << 11)
				              | (0x07ff & EQcd->EEQcd[index].myu[i]));
				s = Stream2ByteWrite(s, Q, BUF_LENGTH, TT_BIG_ENDIAN);
			}
			break;
		default:
			printf("[QcdMarWrite]:: Sqcd error. Sqcd=%d\n", Sqcd1);
			return nullptr;
	}

	return s;
}

byte4 QcdMarByteCalculate(struct ECod_s *ECod, struct EQcd_s *EQcd, byte4 t) {
	byte4 index, Lqcd;
	uchar DecompLev, Sqcd1;

	if (t == NULL)
		index = 0;
	else
		index = t;

	DecompLev = ECod->EECod[index].DecompLev;
	Sqcd1     = (uchar) ((EQcd->EEQcd[index].Sqcd) & 0x1f);
	switch (Sqcd1) {
		case 0:
			Lqcd = (ubyte2) (4 + 3 * DecompLev);
			break;
		case 1:
			Lqcd = 5;
			break;
		case 2:
			Lqcd = (ubyte2) (5 + 6 * DecompLev);
			break;
		default:
			return EXIT_FAILURE;
	}
	return (Lqcd + 2);
}

struct StreamChain_s *QccMarWrite(struct StreamChain_s *s, struct ECod_s *ECod, struct EQcd_s *EQcd,
                                  byte4 t, ubyte2 ccc) {
	struct EEQcd_s *EEQcd;
	byte4 index;
	ubyte2 Lqcc, Q;
	uchar DecompLev, Sqcc0, Sqcc1, numQ, i;

	if (t == NULL)
		index = 0;
	else
		index = t;

	DecompLev = ECod->EECod[ECod->numTiles * ccc + index].DecompLev;
	EEQcd     = &(EQcd->EEQcd[ECod->numTiles * ccc + index]);
	Sqcc1     = (uchar) ((EEQcd->Sqcd) & 0x1f);
	if (ECod->numCompts < 257) {
		switch (Sqcc1) {
			case 0:
				Lqcc = (ubyte2) (5 + 3 * DecompLev);
				break;
			case 1:
				Lqcc = 6;
				break;
			case 2:
				Lqcc = (ubyte2) (6 + 6 * DecompLev);
				break;
			default:
				return nullptr;
		}
	} else {
		switch (Sqcc1) {
			case 0:
				Lqcc = (ubyte2) (6 + 3 * DecompLev);
				break;
			case 1:
				Lqcc = 7;
				break;
			case 2:
				Lqcc = (ubyte2) (7 + 6 * DecompLev);
				break;
			default:
				return nullptr;
		}
	}

	s = Stream2ByteWrite(s, QCC, BUF_LENGTH, TT_BIG_ENDIAN);
	s = Stream2ByteWrite(s, Lqcc, BUF_LENGTH, TT_BIG_ENDIAN);
	if (ECod->numCompts < 257)
		s = Stream1ByteWrite(s, (uchar) ccc, BUF_LENGTH);
	else
		s = Stream2ByteWrite(s, ccc, BUF_LENGTH, TT_BIG_ENDIAN);
	Sqcc0 = (uchar) (Sqcc1 | ((EEQcd->G) << 5));
	s     = Stream1ByteWrite(s, Sqcc0, BUF_LENGTH);

	switch (Sqcc1) {
		case 0:
			numQ = (uchar) (1 + 3 * DecompLev);
			for (i = 0; i < numQ; i++) {
				s = Stream1ByteWrite(s, (uchar) ((0x1f & EEQcd->e[i]) << 3), BUF_LENGTH);
			}
			break;
		case 1:
			s = Stream2ByteWrite(s, (uchar) ((0x1f & EEQcd->e[0]) << 11), BUF_LENGTH, TT_BIG_ENDIAN);
			break;
		case 2:
			numQ = (uchar) (1 + 3 * DecompLev);
			for (i = 0; i < numQ; i++) {
				Q = (ubyte2) (((0x1f & EEQcd->e[i]) << 11) | (0x07ff & EEQcd->myu[i]));
				s = Stream2ByteWrite(s, Q, BUF_LENGTH, TT_BIG_ENDIAN);
			}
			break;
		default:
			return nullptr;
	}

	return s;
}

byte4 QccMarByteCalculate(struct ECod_s *ECod, struct EQcd_s *EQcd, byte4 t, ubyte2 c) {
	struct EEQcd_s *EEQcd;
	byte4 index;
	uchar DecompLev, Sqcc1;
	ubyte2 Lqcc;

	if (t == NULL)
		index = 0;
	else
		index = t;

	DecompLev = ECod->EECod[ECod->numTiles * c + index].DecompLev;
	EEQcd     = &(EQcd->EEQcd[ECod->numTiles * c + index]);
	Sqcc1     = (uchar) ((EEQcd->Sqcd) & 0x1f);
	if (ECod->numCompts < 257) {
		switch (Sqcc1) {
			case 0:
				Lqcc = (ubyte2) (5 + 3 * DecompLev);
				break;
			case 1:
				Lqcc = 6;
				break;
			case 2:
				Lqcc = (ubyte2) (6 + 6 * DecompLev);
				break;
			default:
				return EXIT_FAILURE;
		}
	} else {
		switch (Sqcc1) {
			case 0:
				Lqcc = (ubyte2) (6 + 3 * DecompLev);
				break;
			case 1:
				Lqcc = 7;
				break;
			case 2:
				Lqcc = (ubyte2) (7 + 6 * DecompLev);
				break;
			default:
				return EXIT_FAILURE;
		}
	}
	return (Lqcc + 2);
}

struct Com_s *ComDec(struct StreamChain_s *s) {
	struct Com_s *Com;
	ubyte2 i;

	Com       = new struct Com_s;
	Com->Lcom = Ref_2Byte(s);
	Com->Rcom = Ref_2Byte(s);
	Com->Ccom = new char[(Com->Lcom - 4)];

#if MEM_DEBUG1
	mem0debug_fp = fopen("mem0debug_fp.txt", "a+");
	fprintf(mem0debug_fp, "[ComDec] Com =%08x size=%d\n", Com, sizeof(struct Com_s));
	fprintf(mem0debug_fp, "[ComDec] Com->Ccom[%d] =%08x size=%d\n", (Com->Lcom - 4), Com->Ccom,
	        sizeof(char) * (Com->Lcom - 4));
	fclose(mem0debug_fp);
#endif

	for (i = 0; i < Com->Lcom - 4; i++) {
		Com->Ccom[i] = Ref_1Byte(s);
	}

	return Com;
}

byte4 CodCompare(struct EECod_s *EECod1, struct EECod_s *EECod2) {
	char flag = 0;
	char rrr;

	if (EECod1->CbBypass != EECod2->CbBypass) flag++;
	if (EECod1->CbH != EECod2->CbH) flag++;
	if (EECod1->CbPreterm != EECod2->CbPreterm) flag++;
	if (EECod1->CbReset != EECod2->CbReset) flag++;
	if (EECod1->CbSegmentSymbol != EECod2->CbSegmentSymbol) flag++;
	if (EECod1->CbTerm != EECod2->CbTerm) flag++;
	if (EECod1->CbVcausal != EECod2->CbVcausal) flag++;
	if (EECod1->CbW != EECod2->CbW) flag++;
	if (EECod1->DecompLev != EECod2->DecompLev) flag++;
	if (EECod1->EphOn != EECod2->EphOn) flag++;
	if (EECod1->Filter != EECod2->Filter) flag++;
	if (EECod1->MC != EECod2->MC) flag++;
	if (EECod1->numLayer != EECod2->numLayer) flag++;
	if (EECod1->ProgOrder != EECod2->ProgOrder) flag++;
	if (EECod1->SopOn != EECod2->SopOn) flag++;
	if (!flag) {
		for (rrr = 0; rrr <= EECod1->DecompLev; rrr++) {
			if (EECod1->PrcH[rrr] != EECod2->PrcH[rrr]) flag++;
			if (EECod1->PrcW[rrr] != EECod2->PrcW[rrr]) flag++;
		}
	}
	if (!flag)
		return EXIT_SUCCESS;
	else
		return EXIT_FAILURE;
}

byte4 QcdCompare(struct EEQcd_s *EEQcd1, struct EEQcd_s *EEQcd2, struct EECod_s *EECod) {
	char flag = 0;
	char bbb, DecompLev, numBand;

	DecompLev = EECod->DecompLev;
	numBand   = (char) (DecompLev * 3 + 1);
	for (bbb = 0; bbb < numBand; bbb++) {
		if (EEQcd1->e[bbb] != EEQcd2->e[bbb]) flag++;
		if (EEQcd1->myu[bbb] != EEQcd2->myu[bbb]) flag++;
		if (EEQcd1->Rb[bbb] != EEQcd2->Rb[bbb]) flag++;
	}
	if (EEQcd1->G != EEQcd2->G) flag++;
	if (EEQcd1->Sqcd != EEQcd2->Sqcd) flag++;
	if (!flag)
		return EXIT_SUCCESS;
	else
		return EXIT_FAILURE;
}

struct Cap_s *CapDec(struct StreamChain_s *str) {
	struct Cap_s *Cap;
	byte4 i, NumberOfCapability;

	if (nullptr == (Cap = new struct Cap_s)) {
		printf("[CapDec]:: Cap malloc error\n");
		return nullptr;
	}
	Cap->Lcap          = Ref_2Byte(str);
	NumberOfCapability = (Cap->Lcap - 6) / 2;

	if (nullptr == (Cap->Ccap = new ubyte2[NumberOfCapability])) {
		printf("[CapDec]:: Cap->Ccap malloc error\n");
		return nullptr;
	}
	Cap->Pcap = Ref_4Byte(str);
	for (i = 0; i < NumberOfCapability; i++)
		Cap->Ccap[i] = Ref_2Byte(str);

	Cap->p = (uchar) (Cap->Ccap[0] & 0x1f);
	if (Cap->p == 0)
		Cap->MAGB = 8;
	else if (Cap->p < 20)
		Cap->MAGB = (uchar) (Cap->p + 8);
	else if (Cap->p < 31)
		Cap->MAGB = (uchar) (4 * (Cap->p - 19) + 27);
	else if (Cap->p == 31)
		Cap->MAGB = 74;
	else {
		printf("[CapDec]:: Cap->p Error(Cap->p=%d)\n", Cap->p);
		return nullptr;
	}

	Cap->HTmode = (uchar) ((Cap->Ccap[0] & 0xc000) >> 14);
	Cap->ROI    = (uchar) ((Cap->Ccap[0] & 0x1000) >> 12);

	//	printf("numberOfCap=%d, Pcap=%x\n", NumberOfCapability, Cap->Pcap );
	//	for( i=0 ; i<NumberOfCapability ; i++ )
	//		printf( "Cap->Ccap[%d]=%x\n", i, Cap->Ccap[i] );
	//	printf(" Cap->p=%d Cap->MAGB=%d Cap->HTmode=%d Cap->ROI=%d\n", Cap->p, Cap->MAGB, Cap->HTmode,
	//Cap->ROI);

	return Cap;
}
