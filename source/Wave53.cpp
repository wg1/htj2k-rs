/*
Copyright (c) 2000-2021, ICT-Link
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the HTJ2K project.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ImageUtil.h"
#include "j2k2.h"
#include "wavelet_codec.h"
#include "annexE.h"
#include "J2k_Debug.h"

#if Cplus
void Enc_H53_2(struct Image_s *image) {
	byte4 width, height, xee;
	byte4 LoXs;
	byte4 j, i;
	byte4 *d_TS, *d0;
	byte4 col1step, row1step;

	width    = image->width;
	height   = image->height;
	col1step = image->col1step;
	row1step = image->row1step;

	LoXs = (image->tbx0 & 1);
	xee  = 1;
	d_TS = image->data;
	for (j = 0; j < height; j++, d_TS = &d_TS[col1step]) {
		// step1	HiPass
		d0 = d_TS;
		i  = 1;
		if (LoXs) {
			d0[0] -= d0[1];
			d0 = &d0[2];
			i  = 2;
		}
		for (; i < width - xee; i += 2, d0 = &d0[2])
			d0[1] -= ((d0[0] + d0[2]) >> 1);
		if (!(image->tbx1 & 1)) d0[1] -= d0[0];

		// step2 Lopass
		d0 = d_TS;
		i  = 1;
		if (!LoXs) {
			d0[0] += ((d0[1] * 2 + 2) >> 2);
			d0 = &d0[2];
			i  = 2;
		}
		for (; i < width - xee; i += 2, d0 = &d0[2])
			d0[0] += ((d0[-1] + d0[1] + 2) >> 2);
		if (image->tbx1 & 1) {
			d0[0] += ((d0[-1] * 2 + 2) >> 2);
		}
	}
}
#endif

//#if Cplus
void Dec_H53_2(struct Image_s *image) {
	byte4 width, height, xee;
	byte4 LoXs;
	byte4 j, i;
	byte4 *d_TS, *d0;
	byte4 col1step, row1step;

	width    = image->width;
	height   = image->height;
	col1step = image->col1step;
	row1step = image->row1step;

	LoXs = (image->tbx0 & 1);
	xee  = 1;

	d_TS = image->data;
	for (j = 0; j < height; j++, d_TS = &d_TS[col1step]) {
		// step2	LoPass
		d0 = d_TS;
		i  = 1;
		if (!LoXs) {
			d0[0] -= ((d0[1] * 2 + 2) >> 2);
			d0 = &d0[2];
			i  = 2;
		} else
			d0 = &d0[1];
		for (; i < width - xee; i += 2, d0 = &d0[2])
			d0[0] = d0[0] - ((d0[-1] + d0[1] + 2) >> 2);
		if (image->tbx1 & 1) d0[0] -= ((d0[-1] * 2 + 2) >> 2);

		// step1 HiPass
		d0 = d_TS;
		i  = 1;
		if (LoXs) {
			d0[0] += d0[1];
			d0 = &d0[2];
			i  = 2;
		} else
			d0 = &d0[1];
		for (; i < width - xee; i += 2, d0 = &d0[2])
			d0[0] += ((d0[-1] + d0[1]) >> 1);
		if (!(image->tbx1 & 1)) d0[0] += d0[-1];
	}
}
//#endif

void Enc_H53(struct Image_s *image) {
	byte4 height, width;
	byte4 j;
	byte4 col1step;

	width    = image->tbx1 - image->tbx0;
	height   = image->tby1 - image->tby0;
	col1step = image->col1step;

	if (width == 0) {
	} else if (width == 1) {
		if (image->tbx0 & 1) {
			for (j = 0; j < height; j++) {
				image->data[j * col1step] = image->data[j * col1step] * 2;
			}
		}
	} else {
		Enc_H53_2(image);
	}
}

void Dec_H53(struct Image_s *image) {
	byte4 width, height, xee;
	byte4 LoXs, HiXs;
	byte4 j;
	byte4 col1step, row1step;

	width    = image->width;
	height   = image->height;
	col1step = image->col1step;
	row1step = image->row1step;
	if (image->tbx0 & 1) {
		LoXs = 1; // odd
		HiXs = 2;
	} else {
		LoXs = 2; // even
		HiXs = 1;
	}
	xee = 1;

	if (width == 0) {
	} else if (width == 1) {
		if (image->tbx0 & 1) {
			for (j = 0; j < height; j++)
				image->data[j * col1step] /= 2;
		}
	} else {
		Dec_H53_2(image);
	}
}

#if Cplus
void Enc_V53_2(struct Image_s *image) {
	byte4 width, height, yee;
	byte4 LoYs;
	byte4 j, i;
	byte4 *d_TS, *d0;
	byte4 col1step, col2step, row1step;

	width    = image->width;
	height   = image->height;
	col1step = image->col1step;
	col2step = col1step * 2;
	row1step = image->row1step;

	LoYs = (image->tby0 & 1);
	yee  = 1;
	d_TS = image->data;
	for (i = 0; i < width; i++, d_TS = &d_TS[row1step]) {
		// step1	HiPass
		d0 = d_TS;
		j  = 1;
		if (LoYs) {
			d0[0] -= d0[col1step];
			d0 = &d0[col2step];
			i  = 2;
		}
		for (; j < height - yee; j += 2, d0 = &d0[col2step])
			d0[col1step] -= ((d0[0] + d0[col2step]) >> 1);
		if (!(image->tby1 & 1)) d0[col1step] -= d0[0];

		// step2 Lopass
		d0 = d_TS;
		j  = 1;
		if (!LoYs) {
			d0[0] += ((d0[col1step] * 2 + 2) >> 2);
			d0 = &d0[col2step];
			j  = 2;
		}
		for (; j < height - yee; j += 2, d0 = &d0[col2step])
			d0[0] += ((d0[-col1step] + d0[col1step] + 2) >> 2);
		if (image->tby1 & 1) {
			d0[0] += ((d0[-col1step] * 2 + 2) >> 2);
		}
	}
}
#endif

void Dec_V53_2(struct Image_s *image) {
	byte4 width, height, yee;
	byte4 LoYs;
	byte4 j, i;
	byte4 *d_TS, *d0;
	byte4 col1step, col2step, row1step;

	width    = image->width;
	height   = image->height;
	col1step = image->col1step;
	col2step = col1step * 2;
	row1step = image->row1step;

	LoYs = (image->tby0 & 1);
	yee  = 1;

	d_TS = image->data;
	for (i = 0; i < width; i++, d_TS = &d_TS[row1step]) {
		// step2	LoPass
		d0 = d_TS;
		j  = 1;
		if (!LoYs) {
			d0[0] -= ((d0[col1step] * 2 + 2) >> 2);
			d0 = &d0[col2step];
			j  = 2;
		} else
			d0 = &d0[col1step];
		for (; j < height - yee; j += 2, d0 = &d0[col2step])
			d0[0] = d0[0] - ((d0[-col1step] + d0[col1step] + 2) >> 2);
		if (image->tby1 & 1) d0[0] -= ((d0[-col1step] * 2 + 2) >> 2);

		// step1 HiPass
		d0 = d_TS;
		j  = 1;
		if (LoYs) {
			d0[0] += d0[col1step];
			d0 = &d0[col2step];
			j  = 2;
		} else
			d0 = &d0[col1step];
		for (; j < height - yee; j += 2, d0 = &d0[col2step])
			d0[0] += ((d0[-col1step] + d0[col1step]) >> 1);
		if (!(image->tby1 & 1)) d0[0] += d0[-col1step];
	}
}

void Enc_V53(struct Image_s *image) {
	byte4 height, width;
	byte4 *d_, *d_e;
	byte4 col1step, row1step;

	width    = image->tbx1 - image->tbx0;
	height   = image->tby1 - image->tby0;
	col1step = image->col1step;
	row1step = image->row1step;

	if (height == 0) {
		return;
	} else if (height == 1) {
		if ((image->tby0 & 1)) {
			d_e = &image->data[width];
			for (d_ = image->data; d_ != d_e; ++d_)
				*d_ *= 2;
		}
	} else {
		Enc_V53_2(image);
	}
}

void Dec_V53(struct Image_s *image) {
	byte4 height, width;
	byte4 *d1, *d2;
	byte4 col1step;

	width    = image->width;
	height   = image->height;
	col1step = image->col1step;

	if (height == 0) {
		return;
	} else if (height == 1) {
		if ((image->tby0 & 1)) {
			d1 = &image->data[0];
			d2 = &d1[width];
			for (; d1 != d2; ++d1)
				*d1 /= 2;
		}
		return;
	} else {
		Dec_V53_2(image);
	}
}

void DecWaveIL53(struct Image_s *image, byte4 lll, struct Image_s *ll_d, struct Image_s *ll,
                 struct Image_s *hl, struct Image_s *lh, struct Image_s *hh, struct EEQcd_s *EEQcd) {
	byte4 width, height;
	byte4 j, i;
	byte4 *d0, *d_TS;
	byte4 row1step, col1step;
	byte4 row2step, col2step;
	byte4 *D_;
	char delta1;

	width    = image->tbx1 - image->tbx0;
	height   = image->tby1 - image->tby0;
	col1step = image->col1step;
	col2step = col1step * 2;
	row1step = image->row1step;
	row2step = row1step * 2;

	if (width == 0) {
	} else {
		if (lll == 0) {
			// LL
			D_     = ll->data;
			delta1 = (char) (30 - (EEQcd->e[0] + EEQcd->G - 2));
			if (image->tby0 & 1)
				d_TS = &image->data[col1step];
			else
				d_TS = &image->data[0];
			for (j = 0; j < ll->height; j++, d_TS += col2step) {
				if (image->tbx0 & 1)
					d0 = d_TS + row1step;
				else
					d0 = d_TS;
				for (i = 0; i < ll->width; i++, d0 += row2step) {
					if (D_[j * ll->col1step + i] & NEGATIVE_BIT)
						*d0 = ll_d->data[j * ll_d->col1step + i * ll_d->row1step] =
						    -1 * ((D_[j * ll->col1step + i] & 0x7fffffff) >> delta1);
					else
						*d0 = ll_d->data[j * ll_d->col1step + i * ll_d->row1step] =
						    D_[j * ll->col1step + i] >> delta1;
				}
			}
			// HL
			D_     = hl->data;
			delta1 = (char) (30 - (EEQcd->e[3 * lll + 1] + EEQcd->G - 2));
			if (image->tby0 & 1)
				d_TS = &image->data[col1step];
			else
				d_TS = &image->data[0];
			for (j = 0; j < hl->height; j++, d_TS += col2step) {
				if (!(image->tbx0 & 1))
					d0 = d_TS + row1step;
				else
					d0 = d_TS;
				for (i = 0; i < hl->width; i++, d0 += row2step) {
					if (D_[j * hl->col1step + i] & NEGATIVE_BIT)
						*d0 = -1 * ((D_[j * hl->col1step + i] & 0x7fffffff) >> delta1);
					else
						*d0 = D_[j * hl->col1step + i] >> delta1;
				}
			}
			// LH
			D_     = lh->data;
			delta1 = (char) (30 - (EEQcd->e[3 * lll + 2] + EEQcd->G - 2));
			if (!(image->tby0 & 1))
				d_TS = &image->data[col1step];
			else
				d_TS = &image->data[0];
			for (j = 0; j < lh->height; j++, d_TS += col2step) {
				if (image->tbx0 & 1)
					d0 = d_TS + row1step;
				else
					d0 = d_TS;
				for (i = 0; i < lh->width; i++, d0 += row2step) {
					if (D_[j * lh->col1step + i] & NEGATIVE_BIT)
						*d0 = -1 * ((D_[j * lh->col1step + i] & 0x7fffffff) >> delta1);
					else
						*d0 = D_[j * lh->col1step + i] >> delta1;
				}
			}
			// HH
			D_     = hh->data;
			delta1 = (char) (30 - (EEQcd->e[3 * lll + 3] + EEQcd->G - 2));
			if (!(image->tby0 & 1))
				d_TS = &image->data[col1step];
			else
				d_TS = &image->data[0];
			for (j = 0; j < hh->height; j++, d_TS += col2step) {
				if (!(image->tbx0 & 1))
					d0 = d_TS + row1step;
				else
					d0 = d_TS;
				for (i = 0; i < hh->width; i++, d0 += row2step) {
					if (D_[j * hh->col1step + i] & NEGATIVE_BIT)
						*d0 = -1 * ((D_[j * hh->col1step + i] & 0x7fffffff) >> delta1);
					else
						*d0 = D_[j * hh->col1step + i] >> delta1;
				}
			}
		} else {
			// LL
			if (image->tby0 & 1)
				d_TS = &image->data[col1step];
			else
				d_TS = &image->data[0];
			for (j = 0; j < ll_d->height; j++, d_TS += col2step) {
				if (image->tbx0 & 1)
					d0 = d_TS + row1step;
				else
					d0 = d_TS;
				for (i = 0; i < ll_d->width; i++, d0 += row2step) {
					*d0 = ll_d->data[j * ll_d->col1step + i];
				}
			}
			// HL
			D_     = hl->data;
			delta1 = (char) (30 - (EEQcd->e[3 * lll + 1] + EEQcd->G - 2));
			if (image->tby0 & 1)
				d_TS = &image->data[col1step];
			else
				d_TS = &image->data[0];
			for (j = 0; j < hl->height; j++, d_TS += col2step) {
				if (!(image->tbx0 & 1))
					d0 = d_TS + row1step;
				else
					d0 = d_TS;
				for (i = 0; i < hl->width; i++, d0 += row2step) {
					if (D_[j * hl->col1step + i] & NEGATIVE_BIT)
						*d0 = -1 * ((D_[j * hl->col1step + i] & 0x7fffffff) >> delta1);
					else
						*d0 = D_[j * hl->col1step + i] >> delta1;
				}
			}
			// LH
			D_     = lh->data;
			delta1 = (char) (30 - (EEQcd->e[3 * lll + 2] + EEQcd->G - 2));
			if (!(image->tby0 & 1))
				d_TS = &image->data[col1step];
			else
				d_TS = &image->data[0];
			for (j = 0; j < lh->height; j++, d_TS += col2step) {
				if (image->tbx0 & 1)
					d0 = d_TS + row1step;
				else
					d0 = d_TS;
				for (i = 0; i < lh->width; i++, d0 += row2step) {
					if (D_[j * lh->col1step + i] & NEGATIVE_BIT)
						*d0 = -1 * ((D_[j * lh->col1step + i] & 0x7fffffff) >> delta1);
					else
						*d0 = D_[j * lh->col1step + i] >> delta1;
				}
			}
			// HH
			D_     = hh->data;
			delta1 = (char) (30 - (EEQcd->e[3 * lll + 3] + EEQcd->G - 2));
			if (!(image->tby0 & 1))
				d_TS = &image->data[col1step];
			else
				d_TS = &image->data[0];
			for (j = 0; j < hh->height; j++, d_TS += col2step) {
				if (!(image->tbx0 & 1))
					d0 = d_TS + row1step;
				else
					d0 = d_TS;
				for (i = 0; i < hh->width; i++, d0 += row2step) {
					if (D_[j * hh->col1step + i] & NEGATIVE_BIT)
						*d0 = -1 * ((D_[j * hh->col1step + i] & 0x7fffffff) >> delta1);
					else
						*d0 = D_[j * hh->col1step + i] >> delta1;
				}
			}
		}
	}
}

void DecWaveIL_NL0(struct Image_s *image, struct Image_s *ll, struct EEQcd_s *EEQcd) {
	byte4 width, height;
	byte4 j, i;
	byte4 *d0, *d_TS;
	byte4 row1step, col1step;
	byte4 *D_;
	char delta1;

	width    = image->tbx1 - image->tbx0;
	height   = image->tby1 - image->tby0;
	col1step = image->col1step;
	row1step = image->row1step;

	D_     = ll->data;
	delta1 = (char) (30 - (EEQcd->e[0] + EEQcd->G - 2));
	if (image->tby0 & 1)
		d_TS = &image->data[col1step];
	else
		d_TS = &image->data[0];
	for (j = 0; j < ll->height; j++, d_TS += col1step) {
		if (image->tbx0 & 1)
			d0 = d_TS + row1step;
		else
			d0 = d_TS;
		for (i = 0; i < ll->width; i++, d0++) {
			if (D_[j * ll->col1step + i] & NEGATIVE_BIT)
				*d0 = -1 * ((D_[j * ll->col1step + i] & 0x7fffffff) >> delta1);
			else
				*d0 = D_[j * ll->col1step + i] >> delta1;
		}
	}
}
