/*
Copyright (c) 2000-2021, ICT-Link
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the HTJ2K project.
*/

#ifndef j2k2_h
#define j2k2_h

#include "ImageUtil.h"
#include "MemDebug.h"
#include "asmasm.h"

#define ICT_Link00 0
#define ICT_Link01 0
#define ICT_Link02 0
#define ICT_Link03 0 // qinf_p and qmex_p are compared in rraw
#define ICT_Link04 0 // Melc
#define ICT_Link05 0 // Melc
#define ICT_Link06 0 // CX
#define ICT_Link07 0 //
#define ICT_Link08 0 // Band data monitor. CodeBlock decode.
#define ICT_Link09 0 // Band data monitor2. Wavelet transform.
#define ICT_Link10 0 // Band data monitor2. Wavelet transform.
#define ICT_Link11 0 // Band data monitor2. Wavelet transform.
#define ICT_Link12 0 // Cblk dec information monitor.
#define ICT_Link13 0 // Cblk dec data monitor pass by pass.
#define ICT_Link14 0 // H/W Test data for quad process
#define ICT_Link15 0 // H/W Test data for Upair process

#define FILTER53 1
#define FILTER97 0
#define MEM_DEBUG1 0
#define MEM_DEBUG2 0
#define M_DEBUG 0

#define BITSHIFT_16ON 1
#define BITSHIFT_16 16
#define NUM_BYTE_TERM 3

#define LLcoef0 3.86479
#define HLcoef0 1.0227
#define LHcoef0 1.0227
#define HHcoef0 0.270999
#define LLcoef1 1.965907
#define HLcoef1 1.011286
#define LHcoef1 1.011286
#define HHcoef1 0.520576

#define SOC 0xff4f
#define CAP 0xff50
#define SIZ 0xff51
#define COD 0xff52
#define COC 0xff53
#define TLM 0xff55
#define PLM 0xff57
#define PLT 0xff58
#define CPF 0xff59
#define QCD 0xff5c
#define QCC 0xff5d
#define RGN 0xff5e
#define POC 0xff5f
#define PPM 0xff60
#define PPT 0xff61
#define CRG 0xff63
#define COM 0xff64
#define SOT 0xff90
#define SOP 0xff91
#define EPH 0xff92
#define SOD 0xff93
#define EOC 0xffd9

#define UBYTE2BITS 14
#define UBYTE4BITS 30
#define MaxBitDepth 38
#define BYTE_LIMIT 100
#define TOTAL 0
#define ONLYSTREAM 1
#define RATE_CONTROL_OFF 0       // RateControl off
#define RATE_CONTROL_RateValue 1 // RateControl with rate value
#define RATE_CONTROL_ByteLimit 2 // RateControl with ByteLimit
#define NEGATIVE_BIT 0x80000000
#define NEGATIVE_BIT_SHIFT 31
#define CBS 0x01
#define CB 0x02

#define LRCP 0
#define RLCP 1
#define RPCL 2
#define PCRL 3
#define CPRL 4

#define SIGPASS 0
#define REFPASS 1
#define CLNPASS 2

#define TILE_CNT_ 2
#define X_OFFSET_ 0
#define Y_OFFSET_ 0
#define X_TOFFSET_ 0
#define Y_TOFFSET_ 0
#define X_TSIZE_ 0
#define Y_TSIZE_ 0
#define XSUBSAMPLING0_ 1
#define XSUBSAMPLING1_ 1
#define XSUBSAMPLING2_ 1
#define YSUBSAMPLING0_ 1
#define YSUBSAMPLING1_ 1
#define YSUBSAMPLING2_ 1
#define PROGORDER_ 0
#define NUMLAYER_ 1
#define MC_ 1
#define DECOMPLEV_ 1
#define CB_W_ 3
#define CB_H_ 3
#define CB_BYPASS_ 0
#define CB_RESET_ 0
#define CB_TERM_ 0
#define CB_VCAUSAL_ 0
#define CB_PTERM_ 0
#define CB_SEGMENTSYMBOL_ 0
#define FILTER_ FILTER53 // 0:97 1:53
#define PRC_WH_ 0
#define SOP_ON_ 1
#define EPH_ON_ 1
#define S_QCD_ 0 // 0:NoQuantize 1:quantize with according style.  2:quantize with parameter.
#define G_ 2
#define numTileCmptsQcd_ 0
#define RATECONTROL_ \
	RATE_CONTROL_OFF // RATE_CONTROL_OFF=>off: RATE_CONTROL_RateValue=>rate-mode:
	                 // RATE_CONTROL_MODE2:ByteLimit-mode
#define RATE_ 1.0
#define BYTELIMIT_ 400000
#define ROI_ 0

#if (MEM_DEBUG1 || MEM_DEBUG2)
static FILE *mem0debug_fp;
#endif

typedef struct J2kParam_s {
	uchar *PrcW;           // 4	0
	uchar *PrcH;           // 4	4
	byte2 numLayer;        // 2	8
	uchar ProgOrder;       // 1	10
	uchar MC;              // 1	11
	char DecompLev;        // 1	12
	uchar CbW;             // 1	13
	uchar CbH;             // 1	14
	uchar CbBypass;        // 1	15
	uchar CbReset;         // 1	16
	uchar CbTerm;          // 1	17
	uchar CbVcausal;       // 1	18
	uchar CbPreterm;       // 1	19
	uchar CbSegmentSymbol; // 1	20
	uchar Filter;          // 1	21
	uchar SopOn;
	uchar EphOn;
	uchar Scod;
	char Sqcd;
	byte4 Tile_x;
	byte4 Tile_y;
	byte4 Tile_OffsetX;
	byte4 Tile_OffsetY;
	byte4 OffsetX;
	byte4 OffsetY;
	char RateControl;
	byte4 byteLimit;
	float rate;
	char ROI;
	char *Roi_fname;
	char metric;
	char Q_select;
	byte2 Q_value0;
	byte2 Q_value1;
	char *fname;
	char *fileForm;
	uchar HTJ2K;
	uchar MC_OFF;
	uchar Sign_OFF;
	char DEC_DecompLev;
	uchar debugFlag;
	char debugFname[256];
	ubyte2 HT_ccc;
	char HT_rrr;
	char HT_bbb;
	byte4 HT_kkk;
	byte4 frawD;
	uchar frawL;
	/*	char	HW;
	    char	HW_DecimalPoint;
	    char	HW_DisCard;
	    char	HT_ALPHA_TEST_ON;
	    char	HT_ALPHA_TEST;
	    char	HT_BETA_TEST_ON;
	    char	HT_BETA_TEST;
	    char	HT_GAMMA_TEST_ON;
	    char	HT_GAMMA_TEST;
	    char	HT_DELTA_TEST_ON;
	    char	HT_DELTA_TEST;
	    char	HT_KAPPA0_TEST_ON;
	    char	HT_KAPPA0_TEST;
	    char	HT_KAPPA1_TEST_ON;
	    char	HT_KAPPA1_TEST;
	    char	HT_WAVE_TEST_ON;
	    char	HT_WAVE_TEST;
	    byte4	HT_DelayAlpha;
	    byte4	HT_DelayBeta;
	    byte4	HT_DelayGamma;
	    byte4	HT_DelayDelta;
	    byte4	HT_DelayKappa0;
	    byte4	HT_DelayKappa1;
	    byte4	HT_TG;
	    byte4	HT_Width;
	    byte4	HT_Height;
	    byte4	HT_Width_Margin;
	    byte4	HT_Height_Margin;
	    char	HT_CleanUP_ALL_TEST_ON;
	    char	HT_CleanUP_ALL_TEST;
	    char	HT_CleanUP_CX0_TEST_ON;
	    char	HT_CleanUP_CX0_TEST;
	    char	HT_CleanUP_CX1_TEST_ON;
	    char	HT_CleanUP_CX1_TEST;*/

} J2kParam_t;

typedef struct Cap_s {
	ubyte2 Lcap;
	byte4 Pcap;
	ubyte2 *Ccap;
	uchar HTmode;
	uchar ROI;
	ubyte2 p;
	uchar MAGB;
} Cap_t;

typedef struct Com_s {
	ubyte2 Lcom;
	ubyte2 Rcom;
	char *Ccom;
} Com_t;

typedef struct Siz_s {
	byte2 Lsiz;
	ubyte2 Csiz; //
	byte2 Rsiz;  //
	char *Ssiz;  // bit depth
	char *XRsiz; //
	char *YRsiz; //
	byte4 Xsiz;  //
	byte4 Ysiz;  //
	byte4 XTsiz;
	byte4 YTsiz;
	byte4 XOsiz;  //
	byte4 YOsiz;  //
	byte4 XTOsiz; //
	byte4 YTOsiz; //
	byte4 Xdim;
	byte4 Ydim;
	byte4 numTiles;  //
	byte4 numXTiles; //
	byte4 numYTiles; //
} Siz_t;

typedef struct Sot_s {
	byte4 **Psot;
	byte4 **Saddr;
	uchar **TPsot;
	uchar *numTilePart;
	byte4 **Packet_Saddr;
	byte4 **Packet_Length;
	byte4 **PI_Saddr;
	byte4 **PI_Length;
} Sot_t;

typedef struct MainMarker_s {
	struct ECod_s *ECod;
	struct EQcd_s *EQcd;
	struct ERgn_s *ERgn;
} MainMarker_t;

typedef struct Poc_s {
	ubyte4 numProgOrderChange;
	uchar *RSpoc;
	uchar *REpoc;
	ubyte2 *CSpoc;
	ubyte2 *CEpoc;
	ubyte2 *LYEpoc;
	uchar *Ppoc;
} Poc_t;

typedef struct EECod_s {
	uchar *PrcW;           // 4	0
	uchar *PrcH;           // 4	4
	byte2 numLayer;        // 2	8
	uchar ProgOrder;       // 1	10
	uchar MC;              // 1	11
	char DecompLev;        // 1	12
	uchar CbW;             // 1	13
	uchar CbH;             // 1	14
	uchar CbBypass;        // 1	15
	uchar CbReset;         // 1	16
	uchar CbTerm;          // 1	17
	uchar CbVcausal;       // 1	18
	uchar CbPreterm;       // 1	19
	uchar CbSegmentSymbol; // 1	20
	uchar Filter;          // 1	21
	uchar SopOn;
	uchar EphOn;
	uchar Scod;
	char RateMode;
} EECod_t;

typedef struct ECod_s {
	ubyte2 numCompts;
	byte4 numTiles;
	struct EECod_s *EECod;
} ECod_t;

typedef struct EEQcd_s {
	char *e;
	ubyte2 *myu;
	char *Rb;
	float r;
	char Sqcd;
	char G;
} EEQcd_t; // 16 = (4+4+4+4+1+1)

typedef struct EQcd_s {
	ubyte2 numCompts;
	byte4 numTiles;
	struct EEQcd_s *EEQcd;
} EQcd_t;

typedef struct EERgn_s {
	byte2 Lrgn;
	ubyte2 Crgn;
	uchar Srgn;
	uchar SPrgn;
	struct Image_s *Image_Roi;
	struct wavelet_s *Wave_Roi;
} EERgn_t;

typedef struct ERgn_s {
	ubyte2 numCompts;
	struct EERgn_s *EERgn;
} ERgn_t;

typedef struct EEPpm_s {
	ubyte2 Lppm;
	uchar Zppm;
	byte4 Nppm;
	uchar *lppm;
} EEPpm_t;

typedef struct EPpm_s {
	byte4 numTile;
	ubyte2 Lppm;
	struct EEPpm_s *EEPpm;
} EPpm_t;

typedef struct Roi_s {
	struct Image_s *RoiMask;
	uchar *S_Value;
} Roi_t;

typedef struct Cblk_s {
	struct Image_s *Band;    // 0
	struct Image_s *flags;   // 4
	struct StreamChain_s *s; // 8
	byte4 tcbx0;             // c
	byte4 tcbx1;             // 10
	byte4 tcby0;             // 14
	byte4 tcby1;             // 18
	byte4 length;            // 1c
	byte4 s_adr;             // 20
	float *Distortion;       // 28
	float *RdSlope;          // 2c
	ubyte2 numPasses;        // 30
	/*ubyte2*/ char zerobits;
	/*ubyte2*/ char firstLayer;
	ubyte2 Nb;
	ubyte2 Mb;
	uchar Lblock;
	char included;
	char NoStream;
	char decOn;
	uchar numSegment;
	uchar Last_numSegment;
	struct StreamChain_s *str_rraw;
	struct StreamChain_s *str_fraw;
	struct StreamChain_s *str_melc;
	byte4 rraw_Length;
	byte4 fraw_Length;
	byte4 melc_Length; // = -1;// osamu: might be BUG in Linux or mscOS: this value shall be initialized as
	                   // a negative integer!!!
	byte4 *Layer_Saddr;
	byte4 *Layer_Length;
	ubyte2 *Layer_passes;
	byte4 *Seg_Saddr;
	byte4 *Seg_Length;
	ubyte2 *Seg_passes;
	uchar Segments_max;
	byte4 *Pass_Addr;
	byte4 Pass_Length[32 * 3];
	byte4 total_numPasses;
	byte4 placeholder_passes;
	byte4 p;
	uchar s_blk;
	uchar s_blk_flag;
	char HT_firstCleanUP;
	char HT_mode; // 7bit 0:Legacy 1:HT(when mixed mode). 4bit 0:Legacy 1:watch bit1 and bit0. bit1-0
	              // 00:HTfixed 01:each tile HT fixed 11:mixed.
} Cblk_t;

typedef struct Prc_s {
	struct Image_s *Band;
	struct Cblk_s *Cblk;
	struct Tagtree_s *incltree;
	struct Tagtree_s *zerotree;
	byte4 tpx0;
	byte4 tpx1;
	byte4 tpy0;
	byte4 tpy1;
	byte4 numCblk;
	byte4 numCblkx;
	byte4 numCblky;
	byte2 layerNo;
	uchar f;
} Prc_t; // 48=(4*11+2)

typedef struct EPrc_s {
	struct Prc_s *Prc;
	struct StreamChain_s *s;
	byte4 ValueOfCodeData;
} EPrc_t; // 12=(4+4+4)

typedef struct EEPrc_s {
	struct EPrc_s *EPrc;
	byte2 numPrcx;
	byte2 numPrcy;
	byte2 numPrc;
	char numBand;
} EEPrc_t; // 12=(4+2+2+2+1)

typedef struct Compts_s {
	struct EEPrc_s *EEPrc;
	struct wavelet_s *wave;
	struct Image_s *data;
	byte4 tcx0;
	byte4 tcx1;
	byte4 tcy0;
	byte4 tcy1;
	ubyte2 C;
	uchar firstTile;
} Compts_t; // 32=(4+4+4+4+4+4+4+2+1)

typedef struct Tile_s {
	struct Packet_s *Packet; // 4		0
	struct Compts_s *Cmpt;   // 4		4
	byte4 numPackets;        // 4		8
	byte4 ErrorDifuson;      // 4		12
	byte4 T;                 // 4		16
	byte4 tx0;               // 4		20
	byte4 tx1;               // 4		24
	byte4 ty0;               // 4		28
	byte4 ty1;               // 4		32
	ubyte2 numCompts;        // 2		36
	uchar TNsot;             // 1		38
	uchar *TPsot;            // 1		39
	byte4 *Saddr;
	byte4 *Psot;
	byte4 *PacketsCount;
	struct ERgn_s *ERgn;
	struct EPpm_s *EPpm;
	struct StreamChain_s *str_ppt;
	uchar ppt_flag;
	uchar MAGB;
} Tile_t;

typedef struct Codec_s {
	byte4 numTiles;
	struct Tile_s *Tile;
	struct Siz_s *Siz;
	struct Sot_s *Sot;
	struct ECod_s *ECod;
	struct Poc_s *Poc;
	struct Poc_s *tilePoc;
	struct EQcd_s *EQcd;
	struct ERgn_s *ERgn;
	struct EPpm_s *EPpm;
	struct Cap_s *Cap;
	byte4 XTSiz;
	byte4 YTSiz;
	char Fileform;
	struct Roi_s *pMASK;
	struct Image_s *flag;
	struct Image_s *CbData;
	struct RRawCodec_s *codecR;
	struct fRawCodec_s *codecf;
	struct MelCodec_s *codecM;
	struct Image_s *Image_qinf;
	struct Image_s *Image_qmex;
	struct Image_s *Image_msbits;
	struct Image_s *Image_mex;
	struct Image_s *Image_sigma;
	struct Image_s *Image_refine;
	struct Image_s *Image_epcironK;
	struct Image_s *Image_epciron1;
	struct Image_s *Image_msbits2;
	struct Image_s *Image_rho;
	byte4 ppm_flag;
	byte4 ppm_Nppm;
	byte4 ppm_Counter;
	byte4 *ppm_Saddr;
	byte4 *ppm_Length;
	byte4 *ppm_Saddr2;
	byte4 *ppm_Length2;
	uchar debugFlag;
	char debugFname[256];
} Codec_t;

typedef struct Packet_s {
	struct StreamChain_s *s;
	struct EPrc_s *EPrc;
	char numband;
	char curtilepart;
	byte4 packetNo;
	ubyte2 cmptsNo;
	ubyte2 LayerNo;
	uchar resoNo;
	ubyte2 prcNo;
	uchar NoPacket;
	byte4 SadrPI;
	byte4 EadrPI;
	byte4 Sadr_PacketCode;
	byte4 Eadr_PacketCode;
	uchar TPsot;
} Packet_t;

typedef struct Tagtree_s {
	struct TagtreeNode_s *node;
	byte4 numNode;
	byte4 numCblkX;
	byte4 numCblkY;
} Tagtree_t;

typedef struct TagtreeNode_s {
	char value;
	char state;
	char known;
	struct TagtreeNode_s *parent;
	struct TagtreeNode_s *child;
} TagtreeNode_t;

byte4 EncMainBody(struct J2kParam_s *J2kParam, struct Tile_s *t, struct EECod_s *EECod,
                  struct EEQcd_s *EEQcd, struct Codec_s *codec, ubyte2 ccc, uchar shift);
byte4 TileEnc(char HTJ2K, struct wavelet_s *Wave, struct Image_s *Image, struct Image_s *Stream,
              struct Codec_s *Codec, byte4 index, struct ERgn_s *ERgn);
byte4 J2kEnc(struct Image_s *Stream, struct Codec_s *Codec, struct StreamChain_s *s,
             struct J2kParam_s *J2kParam);
#ifdef _MSC_VER
__declspec(dllexport) struct J2kParam_s *J2kParamCreate(void);
__declspec(dllexport) byte4
    J2K_EncMain(struct Image_s *InputImage, struct StreamChain_s *s, struct J2kParam_s *J2kParam);
#else
struct J2kParam_s *J2kParamCreate(void);
byte4 J2K_EncMain(struct Image_s *InputImage, struct StreamChain_s *s, struct J2kParam_s *J2kParam);
#endif
struct StreamChain_s *MainHeaderWrite(struct ECod_s *ECod, struct EQcd_s *EQcd, struct ERgn_s *ERgn,
                                      char onROI, struct Siz_s *Siz, struct StreamChain_s *s);
byte4 MainHeaderDec(struct Codec_s *Codec, struct StreamChain_s *s, struct J2kParam_s *J2kParam);
byte4 TileHeaderDec(struct Tile_s *t, struct Codec_s *Codec, struct StreamChain_s *s, char mode,
                    struct J2kParam_s *J2kParam);
byte4 TileAdrCal(struct Tile_s *t, struct Siz_s *siz, byte4 index);
struct StreamChain_s *T1T2DataWrite(struct Tile_s *t, struct StreamChain_s *s, struct EECod_s *EECod);
byte4 J2k_DecMain(struct StreamChain_s *s, struct J2kParam_s *J2kParam);
byte4 J2kDec(struct J2kParam_s *J2kParam, struct Codec_s *_Codec, struct StreamChain_s *s,
             struct Image_s *OutStream);
void MainMarkerDestory(struct MainMarker_s *MainMarker);
struct Siz_s *SizEnc(byte4 width, byte4 height, ubyte2 numCmpts, struct J2kParam_s *J2kParam);
struct Siz_s *SizCreate(byte2 numCmpts);
byte4 SizSsiz(struct Siz_s *siz, ubyte2 numCmpts, struct Image_s *Stream);
byte4 CocDec(struct ECod_s *ECod, struct Siz_s *siz, struct StreamChain_s *s, byte4 TileNo);
byte4 PrcAdrCal(struct Tile_s *t, struct wavelet_s *wave1, ubyte2 comptsNo, struct EECod_s *EECod);
void PrcDestory(struct Tile_s *t, struct ECod_s *ECod, char ENC_DEC);
byte4 CblkAdrCal(struct Codec_s *Codec, struct EEPrc_s *EEPrc, struct EECod_s *EECod,
                 struct EEQcd_s *EEQcd);
void CblkDestory(struct Prc_s *Prc, char ENC_DEC);
byte4 PacketCreate(struct Tile_s *t, struct ECod_s *ECod, char ENCDEC);
void PacketDestory(struct Tile_s *t);
struct ECod_s *ECodCreate(Siz_t *siz, struct J2kParam_s *J2kParam);
void ECodDestory(struct ECod_s *ECod);
struct EPpm_s *EPpmCreate(byte4 numTile);
byte4 TileDec(struct J2kParam_s *J2kParam, struct Image_s *Image, struct StreamChain_s *s,
              struct Codec_s *Codec, struct mqdec_s *dec, byte4 tile_No, struct Image_s *ImageStream);
struct Siz_s *SizDec(struct StreamChain_s *codstream);
struct EQcd_s *EQCreate(struct ECod_s *ECod, struct Siz_s *Siz, char ENCDEC, struct J2kParam_s *J2kParam);
byte4 MarkerSizeCar(struct Siz_s *Siz, struct ECod_s *ECod, struct EQcd_s *EQcd);
byte4 PIbyteCar(struct Siz_s *Siz, struct ECod_s *ECod);
struct StreamChain_s *TileMarkerWrite(struct Siz_s *siz, struct ECod_s *ECod, struct EQcd_s *EQcd,
                                      struct Tile_s *t, struct StreamChain_s *s);

#if !Cplus
#	ifdef __cplusplus
extern "C" {
#	endif
extern byte2 *FlagCreate(void);
extern byte4 QcdEnc_ver3(struct Tile_s *t, struct EECod_s *EECod, struct EEQcd_s *EEQcd, byte2 ccc);
#	ifdef __cplusplus
}
#	endif
#endif

#endif
