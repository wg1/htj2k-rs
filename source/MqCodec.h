/*
Copyright (c) 2000-2021, ICT-Link
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the HTJ2K project.
*/

#ifndef mqcodec_h
#define mqcodec_h

#include "ImageUtil.h"
#include "ctx_enc.h"

#define MQ_DEBUG0 0 // ENCODER
#define MQ_DEBUG1 0 // DECORDER
#define MGPASS_MQ_OFF 1
#define MGPASS_MQ_ON 0
/********************************************************************************************************/
/********************************************************************************************************/
/********************************************************************************************************/
/********************************************************************************************************/
#define MAX_CONTEXTS 19
#define MAX_LENGTH 0x8000
#define CODE_BYTE_MAX 0x2000
#define J2K 0x01
//#define JBIG2 0x00
#define MQENC_PTERM 1
#define MQENC_DEFTERM1 2
#define MQENC_DEFTERM2 4
#define MQENC_JBIG2_PROCEDURE 3
#define TRUNCATE 0xFFFF
/********************************************************************************************************/
/********************************************************************************************************/
/********************************************************************************************************/
/********************************************************************************************************/
typedef struct mqenc_s {
	ubyte4 creg;
	byte4 p;
	uchar *cx;
	uchar *ind;      // 12 The state index.
	ubyte2 areg;     // 16 The A register.
	char ctreg;      // 18 The CT register. for both encoder and decoder
	uchar B_buf;     // 19 The byte buffer (i.e., the B variable in the standard). for only encoder
	char first_flag; // 20
	byte4 total_CT;  // 24
	byte4 num_CX;
	byte4 num_index;
} mqenc_t;

typedef struct mqdec_s {
	ubyte4 creg; /* The C register.  */
	uchar *ind;  /* The state index. */
	byte4 counter_limit;
	ubyte4 areg; /* The A register.  */
	byte2 ctreg; /* The CT register. for both encoder and decoder*/
	uchar B_buf; /* The byte buffer (i.e., the B variable in the standard). for only encoder*/
	byte4 seg_length;
	uchar term;
	byte4 next_cur_p;
} mqdec_t;

/********************************************************************************************************/
/********************************************************************************************************/
/********************************************************************************************************/
/********************************************************************************************************/

#endif
