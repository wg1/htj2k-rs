/*
Copyright (c) 2000-2021, ICT-Link
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the HTJ2K project.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ImageUtil.h"
#include "j2k2.h"
#include "MelCodec.h"
#include "vlc_codec.h"
#include "HT_cleanUp.h"

bool decode_SigProp(struct Image_s *Band, struct Cblk_s *Cblk, struct Codec_s *Codec,
                    struct StreamChain_s *str_sigp, char MagRef, struct StreamChain_s *str_mgrf,
                    uchar vcausal) {
	byte4 iii, jjj, i, j, tempHeight, tempWidth;
	byte4 width, height;
	byte4 height_;
	ubyte4 *sp_0, *sp_1, *sp_p, *sp_TS, *sp_0TS;
	uchar *rho_0, *rho_1, *rho_p, *rho_TS;
	byte4 col1step_sp, col4step_sp, row1step_sp;
	byte4 col1step_rho, col4step_rho;
	uchar mbr, mbr0;
	byte4 mbr1;
	byte4 P;
	uchar vcausal_mask;
	uchar left_mask, right_mask, up_mask, dn_mask, QuadTop_mask, QuadBottom_mask;

	Cblk->p--;
	P       = Cblk->p;
	width   = Cblk->tcbx1 - Cblk->tcbx0;
	height_ = height = Cblk->tcby1 - Cblk->tcby0;

	row1step_sp = Band->row1step;
	col1step_sp = Band->col1step;
	col4step_sp = col1step_sp * 4;

	col1step_rho = Codec->Image_rho->col1step;
	col4step_rho = col1step_rho * 4;

	sp_0TS = (ubyte4 *) Band->data;
	sp_0TS = &sp_0TS[(Cblk->tcby0 - Band->tby0) * col1step_sp + (Cblk->tcbx0 - Band->tbx0) * row1step_sp];
	sp_TS  = sp_0TS;
	rho_TS = (uchar *) Codec->Image_rho->data;
	rho_TS = &rho_TS[col1step_rho];
	rho_TS = &rho_TS[1];
	for (j = 0; j < height; j += 4, sp_TS += col4step_sp, rho_TS += col4step_rho) {
		if ((height - j) < 4)
			tempHeight = height - j;
		else
			tempHeight = 4;
		rho_p = rho_TS;
		sp_p  = sp_TS;
		for (i = 0; i < width; i += 4, rho_p = &rho_p[4], sp_p = &sp_p[4]) {
			if ((width - i) < 4)
				tempWidth = width - i;
			else
				tempWidth = 4;

			rho_0 = rho_p;
			sp_0  = sp_p;
			for (iii = 0; iii < tempWidth; iii++, sp_0++, rho_0++) {
				if ((i + iii) == 0)
					left_mask = 0;
				else
					left_mask = 1;
				if ((i + iii) == (width - 1))
					right_mask = 0;
				else
					right_mask = 1;
				rho_1 = rho_0;
				sp_1  = sp_0;
				for (jjj = 0; jjj < tempHeight;
				     jjj++, rho_1 = &rho_1[col1step_rho], sp_1 = &sp_1[col1step_sp]) {
					if ((jjj == 3) && vcausal)
						vcausal_mask = 0;
					else
						vcausal_mask = 1;
					if (jjj == 0)
						QuadTop_mask = 1;
					else
						QuadTop_mask = 0;
					if (jjj == 3)
						QuadBottom_mask = 0;
					else
						QuadBottom_mask = 1;
					if ((j + jjj) == 0)
						up_mask = 0;
					else
						up_mask = 1;
					if ((j + jjj) == (height - 1))
						dn_mask = 0;
					else
						dn_mask = 1;

					if (!rho_1[0]) {
						mbr0 = (uchar) (rho_1[-col1step_rho - 1] | rho_1[-col1step_rho]
						                | rho_1[-col1step_rho + 1] | rho_1[-1] | rho_1[1]
						                | ((rho_1[col1step_rho - 1] | rho_1[col1step_rho]
						                    | rho_1[col1step_rho + 1])
						                   * vcausal_mask));
						mbr0 &= 1;
						/*TopLeft*/
						if (((j + jjj) == 0) && ((i + iii) == 0)) {
							mbr1 = 0;
						}
						/*TopCenter*/
						else if (((j + jjj) == 0) && ((i + iii) < (width - 1))) {
							mbr1 = (sp_1[-1] * left_mask)
							       | (sp_1[col1step_sp - 1] * left_mask * dn_mask * QuadBottom_mask);
						} else if (((j + jjj) == 0) && ((i + iii) == (width - 1))) { /*TopRight*/
							mbr1 = (sp_1[-1] * left_mask)
							       | (sp_1[col1step_sp - 1] * left_mask * dn_mask * QuadBottom_mask);
						} else if (((j + jjj) < (height - 1)) && ((i + iii) == 0)) { /*CenterLeft*/
							mbr1 = (sp_1[-col1step_sp] * up_mask)
							       | (sp_1[-col1step_sp + 1] * up_mask * right_mask * QuadTop_mask);
						} else if (((j + jjj) < (height - 1))
						           && ((i + iii) < (width - 1))) { /*CenterCenter*/
							mbr1 = (sp_1[-col1step_sp - 1] * left_mask * up_mask)
							       | (sp_1[-col1step_sp] * up_mask)
							       | (sp_1[-col1step_sp + 1] * up_mask * right_mask * QuadTop_mask)
							       | (sp_1[-1])
							       | (sp_1[col1step_sp - 1] * left_mask * dn_mask * QuadBottom_mask);
						} else if (((j + jjj) < (height - 1))
						           && ((i + iii) == (width - 1))) { /*CenterRight*/
							mbr1 = (sp_1[-col1step_sp - 1] * up_mask * left_mask)
							       | (sp_1[-col1step_sp] * up_mask) | (sp_1[-1] * left_mask)
							       | (sp_1[col1step_sp - 1] * left_mask * dn_mask * QuadBottom_mask);
						} else if (((j + jjj) == (height - 1)) && ((i + iii) == 0)) { /*BottomLeft*/
							mbr1 = (sp_1[-col1step_sp] * up_mask)
							       | (sp_1[-col1step_sp + 1] * up_mask * right_mask * QuadTop_mask);
						} else if (((j + jjj) == (height - 1))
						           && ((i + iii) < (width - 1))) { /*BottomCenter*/
							mbr1 = (sp_1[-col1step_sp - 1] * up_mask * left_mask)
							       | (sp_1[-col1step_sp] * up_mask)
							       | (sp_1[-col1step_sp + 1] * up_mask * right_mask * QuadTop_mask)
							       | (sp_1[-1] * left_mask);
						} else if (((j + jjj) == (height - 1))
						           && ((i + iii) == (width - 1))) { /*BottomRight*/
							mbr1 = (sp_1[-col1step_sp - 1] * up_mask * left_mask)
							       | (sp_1[-col1step_sp] * up_mask) | (sp_1[-1] * left_mask);
						} else
							mbr1 = 0;
						mbr = (uchar) (mbr1 ? 1 : 0);
						mbr |= mbr0;
						if (mbr) {
							sp_1[0] = ((Ref_1Bit_LSB(str_sigp)) << P);
							if (sp_1[0])
								rho_1[0] = 0x12;
							else
								rho_1[0] = 2;
						}
					}
				}
			}
			// SigpSign
			rho_0 = rho_p;
			sp_0  = sp_p;
			for (iii = 0; iii < tempWidth; iii++, sp_0++, rho_0++) {
				rho_1 = rho_0;
				sp_1  = sp_0;
				for (jjj = 0; jjj < tempHeight;
				     jjj++, rho_1 = &rho_1[col1step_rho], sp_1 = &sp_1[col1step_sp]) {
					if (rho_1[0] == 0x12) sp_1[0] |= ((Ref_1Bit_LSB(str_sigp)) ? 0x80000000 : 0);
				}
			}
		}
	}

	if (MagRef) {
		// MagRef
		sp_0TS = (ubyte4 *) Band->data;
		sp_0TS =
		    &sp_0TS[(Cblk->tcby0 - Band->tby0) * col1step_sp + (Cblk->tcbx0 - Band->tbx0) * row1step_sp];
		sp_TS  = sp_0TS;
		rho_TS = (uchar *) Codec->Image_rho->data;
		rho_TS = &rho_TS[col1step_rho];
		rho_TS = &rho_TS[1];
		P      = Cblk->p;
		for (j = 0; j < height; j += 4, sp_TS += col4step_sp, rho_TS += col4step_rho) {
			rho_0 = rho_TS;
			sp_0  = sp_TS;
			if ((height - j) >= 4)
				tempHeight = 4;
			else
				tempHeight = height - j;
			for (i = 0; i < width; i++, sp_0++, rho_0++) {
				rho_1 = rho_0;
				sp_1  = sp_0;
				for (jjj = 0; jjj < tempHeight;
				     jjj++, rho_1 = &rho_1[col1step_rho], sp_1 = &sp_1[col1step_sp]) {
					if (rho_1[0] == 1) sp_1[0] |= (Ref_1Bit_Reverse_LSB(str_mgrf) << P);
				}
			}
		}
	}

	return EXIT_SUCCESS;
}

void initSigProp(struct StreamChain_s *str) {
	str->cur_p    = 0;
	str->bits     = 0;
	str->B_buf    = 0;
	str->lastbyte = 0;
}

void initMagRef(struct StreamChain_s *str, byte4 Lref) {
	str->cur_p    = Lref - 1;
	str->bits     = 0;
	str->lastbyte = 0xff;
	str->B_buf    = 0;
}