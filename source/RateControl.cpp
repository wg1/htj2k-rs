/*
Copyright (c) 2000-2021, ICT-Link
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the HTJ2K project.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "j2k2.h"
#include "ImageUtil.h"
#include "RateControl.h"

void calcrdslopes(struct Cblk_s *Cblk) {
	float *slope;
	float dd;
	float maxSlope, nowRdSlope;
	byte4 dr;
	byte4 i, j, pass;
	byte4 numslope;
	byte2 numPasses, numPasses2;
	bool flag;

	if (Cblk->NoStream == 1) return;
	if (Cblk->numPasses == 0) return;

	// Curve of ramda
	numPasses  = Cblk->numPasses;
	numPasses2 = (byte2) (numPasses + 1);
	numslope   = numPasses2 * numPasses2;
	slope      = new float[numslope];
	memset(slope, 0, sizeof(float) * numslope);
	for (pass = 0; pass < numPasses2; pass++) {
		for (i = 0; i < numPasses2; i++) {
			if (i < pass) {
				dd = Cblk->Distortion[i] - Cblk->Distortion[pass];
				dr = Cblk->Pass_Addr[pass] - Cblk->Pass_Addr[i];
				if (dr == 0) {
					slope[pass * numPasses2 + i] = -1;
				} else {
					slope[pass * numPasses2 + i] = dd / (float) dr;
				}
			} else {
				slope[pass * numPasses2 + i] = -1;
			}
		}
	}

	Cblk->RdSlope[0] = Cblk->Distortion[0];
	nowRdSlope       = Cblk->Distortion[0];
	i                = 0;
	j                = 1;
	while (i < numPasses2) {
		maxSlope = 0.0;
		flag     = 0;
		for (pass = j; pass < numPasses2; pass++) {
			if (maxSlope <= slope[pass * numPasses2 + i]) {
				maxSlope = slope[pass * numPasses2 + i];
				j        = pass;
				flag     = 1;
			}
		}
		pass = j;
		if ((nowRdSlope > maxSlope) && (flag)) {
			Cblk->RdSlope[pass] = maxSlope;
			nowRdSlope          = maxSlope;
			i                   = pass;
		} else
			i++;
	}
	Cblk->RdSlope[numPasses] = 0.0;

#if 0 // RATE_DEBUG4
	FILE *fp;
	fp=fopen("RdSlope.csv","a+");
	for(pass=0 ; pass<numPasses2 ; pass++){
		fprintf(fp,"%d,",Cblk->PassEndAddr[pass]);
	}
	fprintf(fp,"\n");
	for(pass=0 ; pass<numPasses2 ; pass++){
		fprintf(fp,"%f,",Cblk->Distortion[pass]);
	}
	fprintf(fp,"\n");
	for(pass=0 ; pass<numPasses2 ; pass++){
		fprintf(fp,"%f,",Cblk->RdSlope[pass]);
	}
	fprintf(fp,"\n\n");
#endif
#if 0
	FILE *fp1;
	fp1=fopen("Slope.csv","a+");
	for(pass=0;pass<numPasses2;pass++){
		for(i=0;i<numPasses2;i++){
			fprintf(fp1,"%f,",slope[pass*numPasses2+i]);
		}
		fprintf(fp1,"\n");
	}
	fprintf(fp1,"\n");
	fclose(fp1);
#endif

	delete[] slope;
}

byte4 Truncate2(struct Codec_s *Codec, byte4 byteLimit, byte4 MarkerByte, byte4 PIbyte) {
	struct Tile_s *t;
	struct Packet_s *P;
	struct Cblk_s *Cblk, *Cbe;
	byte2 ttt;
	char bbb;
	byte4 ppp;
	byte4 kkk;
	byte4 CodeStreamByte;
	byte2 pass;
	//	byte4 PIbyte2=0;
	float RdSlope;
	float maxRdSlope = 0.0;
	//	float minRdSlope=0.0;
	float absstep;
	byte2 dis_pass;
	byte4 Total_Length;
	//	byte4 numCblk=0;
	char flag;

	CodeStreamByte = byteLimit - (MarkerByte + PIbyte);
	if ((MarkerByte + PIbyte) > byteLimit) {
		printf("\nLimit vale is too small. So change the limit value.\n");
		printf("MarkerByte=%d\nPIbyte=%d\nMarkerByte+PIbyte=%d\nbyteLimit=%d->", MarkerByte, PIbyte,
		       (MarkerByte + PIbyte), byteLimit);
		byteLimit = (MarkerByte + PIbyte) + BYTE_LIMIT;
		printf("%d\n", byteLimit);
	}

	for (ttt = 0; ttt < Codec->numTiles; ttt++) {
		t = &(Codec->Tile[ttt]);
		for (ppp = 0; ppp < t->numPackets; ppp++) {
			for (bbb = 0; bbb < t->Packet[ppp].numband; bbb++) {
				for (kkk = 0; kkk < t->Packet[ppp].EPrc->Prc[bbb].numCblk; kkk++) {
					if (t->Packet[ppp].EPrc->Prc[bbb].Cblk[kkk].NoStream == 0) {
						calcrdslopes(&(t->Packet[ppp].EPrc->Prc[bbb].Cblk[kkk]));
					}
				}
			}
		}
	}

	for (ttt = 0; ttt < Codec->numTiles; ttt++) {
		t = &(Codec->Tile[ttt]);
		for (ppp = 0; ppp < t->numPackets; ppp++) {
			for (bbb = 0; bbb < t->Packet[ppp].numband; bbb++) {
				Cblk = t->Packet[ppp].EPrc->Prc[bbb].Cblk;
				for (kkk = 0; kkk < t->Packet[ppp].EPrc->Prc[bbb].numCblk; ++Cblk, kkk++) {
					if (!Cblk->NoStream) {
						if (Cblk->RdSlope[0] > maxRdSlope) maxRdSlope = Cblk->RdSlope[0];
					}
				}
			}
		}
	}
	absstep = (float) 0.001;

	for (RdSlope = 0.0; RdSlope <= maxRdSlope; RdSlope += absstep) {
		Total_Length = 0;
		for (ttt = 0; ttt < Codec->numTiles; ttt++) {
			t = &(Codec->Tile[ttt]);
			for (ppp = 0; ppp < t->numPackets; ppp++) {
				P = &(t->Packet[ppp]);
				for (bbb = 0; bbb < t->Packet[ppp].numband; bbb++) {
					Cblk = P->EPrc->Prc[bbb].Cblk;
					for (kkk = 0; kkk < P->EPrc->Prc[bbb].numCblk; kkk++, ++Cblk) {
						if (!Cblk->NoStream) {
							for (pass = Cblk->numPasses; pass >= 0; pass--) {
								if (RdSlope <= Cblk->RdSlope[pass]) {
									Total_Length += Cblk->Pass_Addr[pass];
									pass = -1;
								}
							}
						}
					}
				}
			}
		}
		if (CodeStreamByte > Total_Length) break;
	}
	printf("slope=,%f\n", RdSlope);

#if RATE_DEBUG4
	FILE *fp;
	fp = fopen("Before.csv", "w");
	fprintf(fp, "ppp, bbb, numberCblk,Mb,Zero,pass,Totalpass,Length,FirstLayer,NoStream\n");
	fprintf(fp, "BeforeTruncate\n");
	for (ttt = 0; ttt < Codec->numTiles; ttt++) {
		t = &(Codec->Tile[ttt]);
		for (ppp = 0; ppp < t->numPackets; ppp++) {
			P = &(t->Packet[ppp]);
			for (bbb = 0; bbb < P->numband; bbb++) {
				for (kkk = 0; kkk < P->EPrc->Prc[bbb].numCblk; kkk++) {
					Cblk = &P->EPrc->Prc[bbb].Cblk[kkk];
					fprintf(fp, "%d,%d,%d,%d,%d,%d,%d,%x,%x\n", ppp, bbb, kkk, Cblk->Mb, Cblk->zerobits,
					        Cblk->numPasses, Cblk->length, Cblk->firstLayer, Cblk->NoStream);
				}
			}
		}
	}
	fclose(fp);
#endif

	for (ttt = 0; ttt < Codec->numTiles; ttt++) {
		t = &(Codec->Tile[ttt]);
		for (ppp = 0; ppp < t->numPackets; ppp++) {
			P = &(t->Packet[ppp]);
			for (bbb = 0; bbb < P->numband; bbb++) {
				Cblk = P->EPrc->Prc[bbb].Cblk;
				Cbe  = &(P->EPrc->Prc[bbb].Cblk[P->EPrc->Prc[bbb].numCblk]);
				for (; Cblk != Cbe; ++Cblk) {
					dis_pass = 0;
					flag     = 0;
					if (!Cblk->NoStream) {
						for (pass = Cblk->numPasses; pass > 0; pass--) {
							if (RdSlope <= Cblk->RdSlope[pass]) {
								dis_pass = pass;
								pass     = -1;
								flag     = 1;
							}
						}
					}
					// Truncate
					if (flag) {
						Cblk->length    = Cblk->Pass_Addr[dis_pass];
						Cblk->numPasses = dis_pass;
						// 20190410				Cblk->Segment[0] = dis_pass-1;
						Cblk->firstLayer = 0x00;
					} else {
						Cblk->length     = 0;
						Cblk->numPasses  = 0;
						Cblk->firstLayer = -1; // 0xff;
						Cblk->NoStream   = 1;
					}
				}
			}
		}
	}
#if RATE_DEBUG4
	fp = fopen("After.csv", "w");
	fprintf(fp, "ppp, bbb, numberCblk,Mb,Zero,pass,Totalpass,Length,FirstLayer,NoStream\n");
	fprintf(fp, "After Truncate\n");
	for (ttt = 0; ttt < Codec->numTiles; ttt++) {
		t = &(Codec->Tile[ttt]);
		for (ppp = 0; ppp < t->numPackets; ppp++) {
			P = &(t->Packet[ppp]);
			for (bbb = 0; bbb < P->numband; bbb++) {
				for (kkk = 0; kkk < P->EPrc->Prc[bbb].numCblk; kkk++) {
					Cblk = &P->EPrc->Prc[bbb].Cblk[kkk];
					fprintf(fp, "%d,%d,%d,%d,%d,%d,%d,%x,%x\n", ppp, bbb, kkk, Cblk->Mb, Cblk->zerobits,
					        Cblk->numPasses, Cblk->length, Cblk->firstLayer, Cblk->NoStream);
				}
			}
		}
	}
	fclose(fp);
#endif

	return EXIT_SUCCESS;
}
