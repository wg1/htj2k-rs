/*
Copyright (c) 2000-2021, ICT-Link
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the HTJ2K project.
*/

#include <stdio.h>
#include <stdlib.h>
#include "j2k2.h"
#include "ImageUtil.h"
#include "annexA.h"
#include "annexH.h"
#include "wavelet_codec.h"
#include "t1codec.h"
#include "ctx_enc.h"

//[Func 6]
struct ERgn_s *ERgnCreate(ubyte2 numCmpts) {
	struct ERgn_s *ERgn;
	ubyte2 i;

	if (nullptr == (ERgn = new struct ERgn_s)) {
		printf("[ERgnCreate]:: struct ERgn_s* malloc error\n");
		return nullptr;
	}
	if (nullptr == (ERgn->EERgn = new struct EERgn_s[numCmpts])) {
		printf("[ERgnCreate]:: struct EERgn_s* malloc error\n");
		return nullptr;
	}
	ERgn->numCompts = numCmpts;
	for (i = 0; i < numCmpts; i++) {
		ERgn->EERgn[i].Lrgn  = 0x5;
		ERgn->EERgn[i].Crgn  = 0;
		ERgn->EERgn[i].Srgn  = 0;
		ERgn->EERgn[i].SPrgn = 0;
	}
	return ERgn;
}

void Mask53Create(struct Tile_s *t, ubyte2 ccc)
//(2x,2y)
//	LL=>{x}         * {y}
//	HL=>{x-1,x}     * {y}
//	LH=>{x}         * {y-1,y}
//	HH=>{x-1,x}     * {y-1,y}

//(2x+1,2y)
//	LL=>{x,x+1}     * {y}
//	HL=>{x-1,x,x+1} * {y}
//	LH=>{x,x+1}     * {y-1,y}
//	HH=>{x-1,x}     * {y-1,y}

//(2x,2y+1)
//	LL=>{x}         * {y,y+1}
//	HL=>{x-1,x}     * {y,y+1}
//	LH=>{x}         * {y-1,y,y+1}
//	HH=>{x-1,x}     * {y-1,y,y+1}

//(2x+1,2y+1)
//	LL=>{x,x+1}     * {y,y+1}
//	HL=>{x-1,x,x+1} * {y,y+1}
//	LH=>{x,x+1}     * {y-1,y,y+1}
//	HH=>{x-1,x,x+1} * {y-1,y,y+1}
{
	byte4 i, j, jj, ii;
	uchar rrr;
	byte4 xwidth;
	byte4 BandWidthLL, BandWidthHL, BandWidthLH, BandWidthHH;
	byte4 BandLengthLL, BandLengthHL, BandLengthLH, BandLengthHH;
	uchar RoiD;
	uchar NL;
	char B, LL = 0, HL = 1, LH = 2, HH = 3;
	NL = t->Cmpt[ccc].wave->NL;

	for (rrr = NL; rrr > 0; rrr--) {
		xwidth       = t->Cmpt[ccc].wave->MaskForROI[4 * rrr].col1step;
		BandWidthLL  = t->Cmpt[ccc].wave->MaskForROI[4 * (rrr - 1)].width;
		BandWidthHL  = t->Cmpt[ccc].wave->MaskForROI[4 * (rrr - 1) + 1].width;
		BandWidthLH  = t->Cmpt[ccc].wave->MaskForROI[4 * (rrr - 1) + 2].width;
		BandWidthHH  = t->Cmpt[ccc].wave->MaskForROI[4 * (rrr - 1) + 3].width;
		BandLengthLL = t->Cmpt[ccc].wave->MaskForROI[4 * (rrr - 1)].height;
		BandLengthHL = t->Cmpt[ccc].wave->MaskForROI[4 * (rrr - 1) + 1].height;
		BandLengthLH = t->Cmpt[ccc].wave->MaskForROI[4 * (rrr - 1) + 2].height;
		BandLengthHH = t->Cmpt[ccc].wave->MaskForROI[4 * (rrr - 1) + 3].height;
		//		BandWidthLL=t->Cmpt[ccc].wave->MaskForROI[4*(rrr-1)  ].col1step;
		//		BandWidthHL=t->Cmpt[ccc].wave->MaskForROI[4*(rrr-1)+1].col1step;
		//		BandWidthLH=t->Cmpt[ccc].wave->MaskForROI[4*(rrr-1)+2].col1step;
		//		BandWidthHH=t->Cmpt[ccc].wave->MaskForROI[4*(rrr-1)+3].col1step;
		//		BandLengthLL=t->Cmpt[ccc].wave->MaskForROI[4*(rrr-1)  ].collength;
		//		BandLengthHL=t->Cmpt[ccc].wave->MaskForROI[4*(rrr-1)+1].collength;
		//		BandLengthLH=t->Cmpt[ccc].wave->MaskForROI[4*(rrr-1)+2].collength;
		//		BandLengthHH=t->Cmpt[ccc].wave->MaskForROI[4*(rrr-1)+3].collength;
		for (j = t->Cmpt[ccc].wave->MaskForROI[4 * rrr].tby0, jj = 0;
		     j < t->Cmpt[ccc].wave->MaskForROI[4 * rrr].tby1; j++, jj++) {
			for (i = t->Cmpt[ccc].wave->MaskForROI[4 * rrr].tbx0, ii = 0;
			     i < t->Cmpt[ccc].wave->MaskForROI[4 * rrr].tbx1; i++, ii++) {
				if (!(j % 2) && !(i % 2))
					B = LL;
				else if (!(j % 2) && (i % 2))
					B = HL;
				else if ((j % 2) && !(i % 2))
					B = LH;
				else
					B = HH;
				if (t->Cmpt[ccc].wave->MaskForROI[4 * rrr].data[xwidth * jj + ii])
					RoiD = 1;
				else
					RoiD = 0;
				if (B == LL) {
					//(2x,2y)
					//	LL=>{x}         * {y}
					//	HL=>{x-1,x}     * {y}
					//	LH=>{x}         * {y-1,y}
					//	HH=>{x-1,x}     * {y-1,y}
					t->Cmpt[ccc].wave->MaskForROI[4 * (rrr - 1)].data[(jj / 2) * BandWidthLL + (ii / 2)] |=
					    RoiD; // LL
					t->Cmpt[ccc].wave->MaskForROI[4 * (rrr - 1) + 1].data[(jj / 2) * BandWidthHL
					                                                      + (ii / 2)] |= RoiD; // HL
					t->Cmpt[ccc].wave->MaskForROI[4 * (rrr - 1) + 2].data[(jj / 2) * BandWidthLH
					                                                      + (ii / 2)] |= RoiD; // LH
					t->Cmpt[ccc].wave->MaskForROI[4 * (rrr - 1) + 3].data[(jj / 2) * BandWidthHH
					                                                      + (ii / 2)] |= RoiD; // HH
					if (ii / 2 > 0) {
						t->Cmpt[ccc].wave->MaskForROI[4 * (rrr - 1) + 1].data[(jj / 2) * BandWidthHL
						                                                      + (ii / 2) - 1] |= RoiD; // HL
						t->Cmpt[ccc].wave->MaskForROI[4 * (rrr - 1) + 3].data[(jj / 2) * BandWidthHH
						                                                      + (ii / 2) - 1] |= RoiD; // HH
					}
					if (jj / 2 > 0) {
						t->Cmpt[ccc].wave->MaskForROI[4 * (rrr - 1) + 2].data[(jj / 2 - 1) * BandWidthLH
						                                                      + (ii / 2)] |= RoiD; // LH
						t->Cmpt[ccc].wave->MaskForROI[4 * (rrr - 1) + 3].data[(jj / 2 - 1) * BandWidthHH
						                                                      + (ii / 2)] |= RoiD; // HH
					}
					if (ii / 2 > 0 && jj / 2 > 0)
						t->Cmpt[ccc].wave->MaskForROI[4 * (rrr - 1) + 3].data[(jj / 2 - 1) * BandWidthHH
						                                                      + (ii / 2) - 1] |= RoiD; // HH
				} else if (B == HL) {
					//(2x+1,2y)
					//	LL=>{x,x+1}     * {y}
					//	HL=>{x-1,x,x+1} * {y}
					//	LH=>{x,x+1}     * {y-1,y}
					//	HH=>{x-1,x}     * {y-1,y}
					t->Cmpt[ccc].wave->MaskForROI[4 * (rrr - 1)].data[(jj / 2) * BandWidthLL + (ii / 2)] |=
					    RoiD; // LL
					if (ii / 2 + 1 < BandWidthLL)
						t->Cmpt[ccc].wave->MaskForROI[4 * (rrr - 1)].data[(jj / 2) * BandWidthLL + (ii / 2)
						                                                  + 1] |= RoiD; // LL
					if (ii / 2 > 0)
						t->Cmpt[ccc].wave->MaskForROI[4 * (rrr - 1) + 1].data[(jj / 2) * BandWidthHL
						                                                      + (ii / 2) - 1] |= RoiD; // HL
					t->Cmpt[ccc].wave->MaskForROI[4 * (rrr - 1) + 1].data[(jj / 2) * BandWidthHL
					                                                      + (ii / 2)] |= RoiD; // HL
					if (ii / 2 + 1 < BandWidthHL)
						t->Cmpt[ccc].wave->MaskForROI[4 * (rrr - 1) + 1].data[(jj / 2) * BandWidthHL
						                                                      + (ii / 2) + 1] |= RoiD; // HL
					if (jj / 2 > 0)
						t->Cmpt[ccc].wave->MaskForROI[4 * (rrr - 1) + 2].data[(jj / 2 - 1) * BandWidthLH
						                                                      + (ii / 2)] |= RoiD; // LH
					t->Cmpt[ccc].wave->MaskForROI[4 * (rrr - 1) + 2].data[(jj / 2) * BandWidthLH
					                                                      + (ii / 2)] |= RoiD; // LH
					if ((jj / 2 > 0) && (ii / 2 + 1 < BandLengthLH))
						t->Cmpt[ccc].wave->MaskForROI[4 * (rrr - 1) + 2].data[(jj / 2 - 1) * BandWidthLH
						                                                      + (ii / 2) + 1] |= RoiD; // LH
					if (ii / 2 + 1 < BandWidthLH)
						t->Cmpt[ccc].wave->MaskForROI[4 * (rrr - 1) + 2].data[(jj / 2) * BandWidthLH
						                                                      + (ii / 2) + 1] |= RoiD; // LH
					if ((jj / 2 > 0) && (ii / 2 > 0))
						t->Cmpt[ccc].wave->MaskForROI[4 * (rrr - 1) + 3].data[(jj / 2 - 1) * BandWidthHH
						                                                      + (ii / 2) - 1] |= RoiD; // HH
					if (ii / 2 > 0)
						t->Cmpt[ccc].wave->MaskForROI[4 * (rrr - 1) + 3].data[(jj / 2) * BandWidthHH
						                                                      + (ii / 2) - 1] |= RoiD; // HH
					if (jj / 2 > 0)
						t->Cmpt[ccc].wave->MaskForROI[4 * (rrr - 1) + 3].data[(jj / 2 - 1) * BandWidthHH
						                                                      + (ii / 2)] |= RoiD; // HH
					t->Cmpt[ccc].wave->MaskForROI[4 * (rrr - 1) + 3].data[(jj / 2) * BandWidthHH
					                                                      + (ii / 2)] |= RoiD; // HH
				} else if (B == LH) {
					//(2x,2y+1)
					//	LL=>{x}         * {y,y+1}
					//	HL=>{x-1,x}     * {y,y+1}
					//	LH=>{x}         * {y-1,y,y+1}
					//	HH=>{x-1,x}     * {y-1,y,y+1}
					t->Cmpt[ccc].wave->MaskForROI[4 * (rrr - 1)].data[(jj / 2) * BandWidthLL + (ii / 2)] |=
					    RoiD; // LL
					if (jj / 2 + 1 < BandLengthLL)
						t->Cmpt[ccc].wave->MaskForROI[4 * (rrr - 1)].data[(jj / 2 + 1) * BandWidthLL
						                                                  + (ii / 2)] |= RoiD; // LL
					t->Cmpt[ccc].wave->MaskForROI[4 * (rrr - 1) + 1].data[(jj / 2) * BandWidthHL
					                                                      + (ii / 2)] |= RoiD; // HL
					if (ii / 2 > 0)
						t->Cmpt[ccc].wave->MaskForROI[4 * (rrr - 1) + 1].data[(jj / 2) * BandWidthHL
						                                                      + (ii / 2) - 1] |= RoiD; // HL
					if (jj / 2 + 1 < BandLengthHL)
						t->Cmpt[ccc].wave->MaskForROI[4 * (rrr - 1) + 1].data[(jj / 2 + 1) * BandWidthHL
						                                                      + (ii / 2)] |= RoiD; // HL
					if ((jj / 2 + 1 < BandLengthHL) && (ii / 2 > 0))
						t->Cmpt[ccc].wave->MaskForROI[4 * (rrr - 1) + 1].data[(jj / 2 + 1) * BandWidthHL
						                                                      + (ii / 2) - 1] |= RoiD; // HL
					if (jj / 2 > 0)
						t->Cmpt[ccc].wave->MaskForROI[4 * (rrr - 1) + 2].data[(jj / 2 - 1) * BandWidthLH
						                                                      + (ii / 2)] |= RoiD; // LH
					t->Cmpt[ccc].wave->MaskForROI[4 * (rrr - 1) + 2].data[(jj / 2) * BandWidthLH
					                                                      + (ii / 2)] |= RoiD; // LH
					if (jj / 2 + 1 < BandLengthLH)
						t->Cmpt[ccc].wave->MaskForROI[4 * (rrr - 1) + 2].data[(jj / 2 + 1) * BandWidthLH
						                                                      + (ii / 2)] |= RoiD; // LH

					if ((jj / 2 > 0) && (ii / 2 > 0))
						t->Cmpt[ccc].wave->MaskForROI[4 * (rrr - 1) + 3].data[(jj / 2 - 1) * BandWidthHH
						                                                      + (ii / 2) - 1] |= RoiD; // HH
					if (ii / 2 > 0)
						t->Cmpt[ccc].wave->MaskForROI[4 * (rrr - 1) + 3].data[(jj / 2) * BandWidthHH
						                                                      + (ii / 2) - 1] |= RoiD; // HH
					if ((jj / 2 + 1 < BandLengthHH) && (ii / 2 > 0))
						t->Cmpt[ccc].wave->MaskForROI[4 * (rrr - 1) + 3].data[(jj / 2 + 1) * BandWidthHH
						                                                      + (ii / 2) - 1] |= RoiD; // HH
					if (jj / 2 > 0)
						t->Cmpt[ccc].wave->MaskForROI[4 * (rrr - 1) + 3].data[(jj / 2 - 1) * BandWidthHH
						                                                      + (ii / 2)] |= RoiD; // HH
					t->Cmpt[ccc].wave->MaskForROI[4 * (rrr - 1) + 3].data[(jj / 2) * BandWidthHH
					                                                      + (ii / 2)] |= RoiD; // HH
					if (jj / 2 + 1 < BandLengthHH)
						t->Cmpt[ccc].wave->MaskForROI[4 * (rrr - 1) + 3].data[(jj / 2 + 1) * BandWidthHH
						                                                      + (ii / 2)] |= RoiD; // HH
				} else if (B == HH) {
					//(2x+1,2y+1)
					//	LL=>{x,x+1}     * {y,y+1}
					//	HL=>{x-1,x,x+1} * {y,y+1}
					//	LH=>{x,x+1}     * {y-1,y,y+1}
					//	HH=>{x-1,x,x+1} * {y-1,y,y+1}
					t->Cmpt[ccc].wave->MaskForROI[4 * (rrr - 1)].data[(jj / 2) * BandWidthLL + (ii / 2)] |=
					    RoiD; // LL
					if (ii / 2 + 1 < BandWidthLL)
						t->Cmpt[ccc].wave->MaskForROI[4 * (rrr - 1)].data[(jj / 2) * BandWidthLL + (ii / 2)
						                                                  + 1] |= RoiD; // LL
					if (jj / 2 + 1 < BandLengthLL)
						t->Cmpt[ccc].wave->MaskForROI[4 * (rrr - 1)].data[(jj / 2 + 1) * BandWidthLL
						                                                  + (ii / 2)] |= RoiD; // LL
					if ((jj / 2 + 1 < BandLengthLL) && (ii / 2 + 1 < BandWidthLL))
						t->Cmpt[ccc].wave->MaskForROI[4 * (rrr - 1)].data[(jj / 2 + 1) * BandWidthLL
						                                                  + (ii / 2) + 1] |= RoiD; // LL
					if (ii / 2 > 0)
						t->Cmpt[ccc].wave->MaskForROI[4 * (rrr - 1) + 1].data[(jj / 2) * BandWidthHL
						                                                      + (ii / 2) - 1] |= RoiD; // HL
					t->Cmpt[ccc].wave->MaskForROI[4 * (rrr - 1) + 1].data[(jj / 2) * BandWidthHL
					                                                      + (ii / 2)] |= RoiD; // HL
					if (ii / 2 + 1 < BandWidthHL)
						t->Cmpt[ccc].wave->MaskForROI[4 * (rrr - 1) + 1].data[(jj / 2) * BandWidthHL
						                                                      + (ii / 2) + 1] |= RoiD; // HL
					if ((jj / 2 + 1 < BandLengthHL) && (ii / 2 > 0))
						t->Cmpt[ccc].wave->MaskForROI[4 * (rrr - 1) + 1].data[(jj / 2 + 1) * BandWidthHL
						                                                      + (ii / 2) - 1] |= RoiD; // HL
					if ((jj / 2 + 1 < BandLengthHL))
						t->Cmpt[ccc].wave->MaskForROI[4 * (rrr - 1) + 1].data[(jj / 2 + 1) * BandWidthHL
						                                                      + (ii / 2)] |= RoiD; // HL
					if ((jj / 2 + 1 < BandLengthHL) && (ii / 2 + 1 < BandWidthHL))
						t->Cmpt[ccc].wave->MaskForROI[4 * (rrr - 1) + 1].data[(jj / 2 + 1) * BandWidthHL
						                                                      + (ii / 2) + 1] |= RoiD; // HL
					if (jj / 2 > 0)
						t->Cmpt[ccc].wave->MaskForROI[4 * (rrr - 1) + 2].data[(jj / 2 - 1) * BandWidthLH
						                                                      + (ii / 2)] |= RoiD; // LH
					if ((jj / 2 > 0) && (ii / 2 + 1 < BandWidthLH))
						t->Cmpt[ccc].wave->MaskForROI[4 * (rrr - 1) + 2].data[(jj / 2 - 1) * BandWidthLH
						                                                      + (ii / 2) + 1] |= RoiD; // LH
					t->Cmpt[ccc].wave->MaskForROI[4 * (rrr - 1) + 2].data[(jj / 2) * BandWidthLH
					                                                      + (ii / 2)] |= RoiD; // LH
					if (ii / 2 + 1 < BandWidthLH)
						t->Cmpt[ccc].wave->MaskForROI[4 * (rrr - 1) + 2].data[(jj / 2) * BandWidthLH
						                                                      + (ii / 2) + 1] |= RoiD; // LH
					if (jj / 2 + 1 < BandLengthLH)
						t->Cmpt[ccc].wave->MaskForROI[4 * (rrr - 1) + 2].data[(jj / 2 + 1) * BandWidthLH
						                                                      + (ii / 2)] |= RoiD; // LH
					if ((jj / 2 + 1 < BandLengthLH) && (ii / 2 + 1 < BandWidthLH))
						t->Cmpt[ccc].wave->MaskForROI[4 * (rrr - 1) + 2].data[(jj / 2 + 1) * BandWidthLH
						                                                      + (ii / 2) + 1] |= RoiD; // LH
					if ((jj / 2 > 0) && (ii / 2 > 0))
						t->Cmpt[ccc].wave->MaskForROI[4 * (rrr - 1) + 3].data[(jj / 2 - 1) * BandWidthHH
						                                                      + (ii / 2) - 1] |= RoiD; // HH
					if (jj / 2 > 0)
						t->Cmpt[ccc].wave->MaskForROI[4 * (rrr - 1) + 3].data[(jj / 2 - 1) * BandWidthHH
						                                                      + (ii / 2)] |= RoiD; // HH
					if ((jj / 2 > 0) && (ii / 2 + 1 < BandWidthHH))
						t->Cmpt[ccc].wave->MaskForROI[4 * (rrr - 1) + 3].data[(jj / 2 - 1) * BandWidthHH
						                                                      + (ii / 2) + 1] |= RoiD; // HH
					if (ii / 2 > 0)
						t->Cmpt[ccc].wave->MaskForROI[4 * (rrr - 1) + 3].data[(jj / 2) * BandWidthHH
						                                                      + (ii / 2) - 1] |= RoiD; // HH
					t->Cmpt[ccc].wave->MaskForROI[4 * (rrr - 1) + 3].data[(jj / 2) * BandWidthHH
					                                                      + (ii / 2)] |= RoiD; // HH
					if (ii / 2 + 1 < BandWidthHH)
						t->Cmpt[ccc].wave->MaskForROI[4 * (rrr - 1) + 3].data[(jj / 2) * BandWidthHH
						                                                      + (ii / 2) + 1] |= RoiD; // HH
					if ((jj / 2 + 1 < BandLengthHH) && (ii / 2 > 0))
						t->Cmpt[ccc].wave->MaskForROI[4 * (rrr - 1) + 3].data[(jj / 2 + 1) * BandWidthHH
						                                                      + (ii / 2) - 1] |= RoiD; // HH
					if (jj / 2 + 1 < BandLengthHH)
						t->Cmpt[ccc].wave->MaskForROI[4 * (rrr - 1) + 3].data[(jj / 2 + 1) * BandWidthHH
						                                                      + (ii / 2)] |= RoiD; // HH
					if ((jj / 2 + 1 < BandLengthHH) && (ii / 2 + 1 < BandWidthHH))
						t->Cmpt[ccc].wave->MaskForROI[4 * (rrr - 1) + 3].data[(jj / 2 + 1) * BandWidthHH
						                                                      + (ii / 2) + 1] |= RoiD; // HH
				}
			}
		}
	}
}

void Mask97Create(struct Tile_s *t, ubyte2 numCmpts)
//(2x,2y)
//	LL=>{    x-1,x,x+1} * {    y-1,y,y+1}
//	HL=>{x-2,x-1,x,x+1} * {    y-1,y,y+1}
//	LH=>{    x-1,x,x+1} * {y-2,y-1,y,y+1}
//	HH=>{x-2,x-1,x,x+1} * {y-2,y-1,y,y+1}

//(2x+1,2y)
//	LL=>{    x-1,x,x+1,x+2} * {    y-1,y,y+1}
//	HL=>{x-2,x-1,x,x+1,x+2} * {    y-1,y,y+1}
//	LH=>{    x-1,x,x+1,x+2} * {y-2,y-1,y,y+1}
//	HH=>{x-2,x-1,x,x+1,x+2} * {y-2,y-1,y,y+1}

//(2x,2y+1)
//	LL=>{    x-1,x,x+1} * {    y-1,y,y+1,y+2}
//	HL=>{x-2,x-1,x,x+1} * {    y-1,y,y+1,y+2}
//	LH=>{    x-1,x,x+1} * {y-2,y-1,y,y+1,y+2}
//	HH=>{x-2,x-1,x,x+1} * {y-2,y-1,y,y+1,y+2}

//(2x+1,2y+1)
//	LL=>{    x-1,x,x+1,x+2} * {    y-1,y,y+1,y+2}
//	HL=>{x-2,x-1,x,x+1,x+2} * {    y-1,y,y+1,y+2}
//	LH=>{    x-1,x,x+1,x+2} * {y-2,y-1,y,y+1,y+2}
//	HH=>{x-2,x-1,x,x+1,x+2} * {y-2,y-1,y,y+1,y+2}
{
	byte4 i, j, jj, ii;
	uchar rrr;
	byte4 xwidth;
	byte4 BandWidthLL, BandWidthHL, BandWidthLH, BandWidthHH;
	byte4 BandLengthLL, BandLengthHL, BandLengthLH, BandLengthHH;
	uchar RoiD;
	uchar NL;
	char B, LL = 0, HL = 1, LH = 2, HH = 3;
	NL = t->Cmpt[numCmpts].wave->NL;

	for (rrr = NL; rrr > 0; rrr--) {
		xwidth       = t->Cmpt[numCmpts].wave->MaskForROI[4 * rrr].col1step;
		BandWidthLL  = t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1)].width;
		BandWidthHL  = t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1) + 1].width;
		BandWidthLH  = t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1) + 2].width;
		BandWidthHH  = t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1) + 3].width;
		BandLengthLL = t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1)].height;
		BandLengthHL = t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1) + 1].height;
		BandLengthLH = t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1) + 2].height;
		BandLengthHH = t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1) + 3].height;
		//		BandWidthLL=t->Cmpt[numCmpts].wave->MaskForROI[4*(rrr-1)  ].col1step;
		//		BandWidthHL=t->Cmpt[numCmpts].wave->MaskForROI[4*(rrr-1)+1].col1step;
		//		BandWidthLH=t->Cmpt[numCmpts].wave->MaskForROI[4*(rrr-1)+2].col1step;
		//		BandWidthHH=t->Cmpt[numCmpts].wave->MaskForROI[4*(rrr-1)+3].col1step;
		//		BandLengthLL=t->Cmpt[numCmpts].wave->MaskForROI[4*(rrr-1)  ].collength;
		//		BandLengthHL=t->Cmpt[numCmpts].wave->MaskForROI[4*(rrr-1)+1].collength;
		//		BandLengthLH=t->Cmpt[numCmpts].wave->MaskForROI[4*(rrr-1)+2].collength;
		//		BandLengthHH=t->Cmpt[numCmpts].wave->MaskForROI[4*(rrr-1)+3].collength;
		for (j = t->Cmpt[numCmpts].wave->MaskForROI[4 * rrr].tby0, jj = 0;
		     j < t->Cmpt[numCmpts].wave->MaskForROI[4 * rrr].tby1; j++, jj++) {
			for (i = t->Cmpt[numCmpts].wave->MaskForROI[4 * rrr].tbx0, ii = 0;
			     i < t->Cmpt[numCmpts].wave->MaskForROI[4 * rrr].tbx1; i++, ii++) {
				if (!(j % 2) && !(i % 2))
					B = LL;
				else if (!(j % 2) && (i % 2))
					B = HL;
				else if ((j % 2) && !(i % 2))
					B = LH;
				else
					B = HH;
				if (t->Cmpt[numCmpts].wave->MaskForROI[4 * rrr].data[xwidth * jj + ii])
					RoiD = 1;
				else
					RoiD = 0;
				if (B == LL) {
					//(2x,2y)
					//	LL=>{    x-1,x,x+1} * {    y-1,y,y+1}
					//	HL=>{x-2,x-1,x,x+1} * {    y-1,y,y+1}
					//	LH=>{    x-1,x,x+1} * {y-2,y-1,y,y+1}
					//	HH=>{x-2,x-1,x,x+1} * {y-2,y-1,y,y+1}
					if (jj / 2 - 2 >= 0) {
						if (ii / 2 - 1 >= 0)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 2]
							    .data[(jj / 2 - 2) * BandWidthLH + (ii / 2) - 1] |= RoiD; // LH
						t->Cmpt[numCmpts]
						    .wave->MaskForROI[4 * (rrr - 1) + 2]
						    .data[(jj / 2 - 2) * BandWidthLH + (ii / 2)] |= RoiD; // LH
						if ((ii / 2) + 1 < BandWidthLH)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 2]
							    .data[(jj / 2 - 2) * BandWidthLH + (ii / 2) + 1] |= RoiD; // LH
						if (ii / 2 - 2 >= 0)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 3]
							    .data[(jj / 2 - 2) * BandWidthHH + (ii / 2) - 2] |= RoiD; // HH
						if (ii / 2 - 1 >= 0)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 3]
							    .data[(jj / 2 - 2) * BandWidthHH + (ii / 2) - 1] |= RoiD; // HH
						t->Cmpt[numCmpts]
						    .wave->MaskForROI[4 * (rrr - 1) + 3]
						    .data[(jj / 2 - 2) * BandWidthHH + (ii / 2)] |= RoiD; // HH
						if ((ii / 2) + 1 < BandWidthHH)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 3]
							    .data[(jj / 2 - 2) * BandWidthHH + (ii / 2) + 1] |= RoiD; // HH
					}
					if ((jj / 2 - 1) >= 0) {
						if (ii / 2 - 1 >= 0)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1)]
							    .data[(jj / 2 - 1) * BandWidthLL + (ii / 2) - 1] |= RoiD; // LL
						t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1)].data[(jj / 2 - 1) * BandWidthLL
						                                                       + (ii / 2)] |= RoiD; // LL
						if (ii / 2 + 1 < BandWidthLL)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1)]
							    .data[(jj / 2 - 1) * BandWidthLL + (ii / 2) + 1] |= RoiD; // LL
						if (ii / 2 - 2 >= 0)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 1]
							    .data[(jj / 2 - 1) * BandWidthHL + (ii / 2) - 2] |= RoiD; // HL
						if (ii / 2 - 1 >= 0)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 1]
							    .data[(jj / 2 - 1) * BandWidthHL + (ii / 2) - 1] |= RoiD; // HL
						t->Cmpt[numCmpts]
						    .wave->MaskForROI[4 * (rrr - 1) + 1]
						    .data[(jj / 2 - 1) * BandWidthHL + (ii / 2)] |= RoiD; // HL
						if (ii / 2 + 1 < BandWidthHL)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 1]
							    .data[(jj / 2 - 1) * BandWidthHL + (ii / 2) + 1] |= RoiD; // HL
						if (ii / 2 - 1 >= 0)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 2]
							    .data[(jj / 2 - 1) * BandWidthLH + (ii / 2) - 1] |= RoiD; // LH
						t->Cmpt[numCmpts]
						    .wave->MaskForROI[4 * (rrr - 1) + 2]
						    .data[(jj / 2 - 1) * BandWidthLH + (ii / 2)] |= RoiD; // LH
						if (ii / 2 + 1 < BandWidthLH)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 2]
							    .data[(jj / 2 - 1) * BandWidthLH + (ii / 2) + 1] |= RoiD; // LH
						if (ii / 2 - 2 >= 0)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 3]
							    .data[(jj / 2 - 1) * BandWidthHH + (ii / 2) - 2] |= RoiD; // HH
						if (ii / 2 - 1 >= 0)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 3]
							    .data[(jj / 2 - 1) * BandWidthHH + (ii / 2) - 1] |= RoiD; // HH
						t->Cmpt[numCmpts]
						    .wave->MaskForROI[4 * (rrr - 1) + 3]
						    .data[(jj / 2 - 1) * BandWidthHH + (ii / 2)] |= RoiD; // HH
						if (ii / 2 + 1 < BandWidthHH)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 3]
							    .data[(jj / 2 - 1) * BandWidthHH + (ii / 2) + 1] |= RoiD; // HH
					}

					if (ii / 2 - 1 >= 0)
						t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1)].data[(jj / 2) * BandWidthLL
						                                                       + (ii / 2) - 1] |=
						    RoiD; // LL
					t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1)].data[(jj / 2) * BandWidthLL
					                                                       + (ii / 2)] |= RoiD; // LL
					if (ii / 2 + 1 < BandWidthLL)
						t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1)].data[(jj / 2) * BandWidthLL
						                                                       + (ii / 2) + 1] |=
						    RoiD; // LL
					if (ii / 2 - 2 >= 0)
						t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1) + 1].data[(jj / 2) * BandWidthHL
						                                                           + (ii / 2) - 2] |=
						    RoiD; // HL
					if (ii / 2 - 1 >= 0)
						t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1) + 1].data[(jj / 2) * BandWidthHL
						                                                           + (ii / 2) - 1] |=
						    RoiD; // HL
					t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1) + 1].data[(jj / 2) * BandWidthHL
					                                                           + (ii / 2)] |= RoiD; // HL
					if (ii / 2 + 1 < BandWidthLL)
						t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1) + 1].data[(jj / 2) * BandWidthHL
						                                                           + (ii / 2) + 1] |=
						    RoiD; // HL
					if (ii / 2 - 1 >= 0)
						t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1) + 2].data[(jj / 2) * BandWidthLH
						                                                           + (ii / 2) - 1] |=
						    RoiD; // LH
					t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1) + 2].data[(jj / 2) * BandWidthLH
					                                                           + (ii / 2)] |= RoiD; // LH
					if (ii / 2 + 1 < BandWidthLH)
						t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1) + 2].data[(jj / 2) * BandWidthLH
						                                                           + (ii / 2) + 1] |=
						    RoiD; // LH
					if (ii / 2 - 2 >= 0)
						t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1) + 3].data[(jj / 2) * BandWidthHH
						                                                           + (ii / 2) - 2] |=
						    RoiD; // HH
					if (ii / 2 - 1 >= 0)
						t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1) + 3].data[(jj / 2) * BandWidthHH
						                                                           + (ii / 2) - 1] |=
						    RoiD; // HH
					t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1) + 3].data[(jj / 2) * BandWidthHH
					                                                           + (ii / 2)] |= RoiD; // HH
					if (ii / 2 + 1 < BandWidthHH)
						t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1) + 3].data[(jj / 2) * BandWidthHH
						                                                           + (ii / 2) + 1] |=
						    RoiD; // HH

					if ((jj / 2 + 1) < BandLengthLL) {
						if (ii / 2 - 1 >= 0)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1)]
							    .data[(jj / 2 + 1) * BandWidthLL + (ii / 2) - 1] |= RoiD; // LL
						t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1)].data[(jj / 2 + 1) * BandWidthLL
						                                                       + (ii / 2)] |= RoiD; // LL
						if (ii / 2 + 1 < BandWidthLL)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1)]
							    .data[(jj / 2 + 1) * BandWidthLL + (ii / 2) + 1] |= RoiD; // LL
					}
					if ((jj / 2 + 1) < BandLengthHL) {
						if (ii / 2 - 2 >= 0)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 1]
							    .data[(jj / 2 + 1) * BandWidthHL + (ii / 2) - 2] |= RoiD; // HL
						if (ii / 2 - 1 >= 0)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 1]
							    .data[(jj / 2 + 1) * BandWidthHL + (ii / 2) - 1] |= RoiD; // HL
						t->Cmpt[numCmpts]
						    .wave->MaskForROI[4 * (rrr - 1) + 1]
						    .data[(jj / 2 + 1) * BandWidthHL + (ii / 2)] |= RoiD; // HL
						if (ii / 2 + 1 < BandWidthHL)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 1]
							    .data[(jj / 2 + 1) * BandWidthHL + (ii / 2) + 1] |= RoiD; // HL
					}
					if ((jj / 2 + 1) < BandLengthLH) {
						if (ii / 2 - 1 >= 0)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 2]
							    .data[(jj / 2 + 1) * BandWidthLH + (ii / 2) - 1] |= RoiD; // LH
						t->Cmpt[numCmpts]
						    .wave->MaskForROI[4 * (rrr - 1) + 2]
						    .data[(jj / 2 + 1) * BandWidthLH + (ii / 2)] |= RoiD; // LH
						if (ii / 2 + 1 < BandWidthLH)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 2]
							    .data[(jj / 2 + 1) * BandWidthLH + (ii / 2) + 1] |= RoiD; // LH
					}
					if ((jj / 2 + 1) < BandLengthHH) {
						if (ii / 2 - 2 >= 0)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 3]
							    .data[(jj / 2 + 1) * BandWidthHH + (ii / 2) - 2] |= RoiD; // HH
						if (ii / 2 - 1 >= 0)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 3]
							    .data[(jj / 2 + 1) * BandWidthHH + (ii / 2) - 1] |= RoiD; // HH
						t->Cmpt[numCmpts]
						    .wave->MaskForROI[4 * (rrr - 1) + 3]
						    .data[(jj / 2 + 1) * BandWidthHH + (ii / 2)] |= RoiD; // HH
						if (ii / 2 + 1 < BandWidthHH)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 3]
							    .data[(jj / 2 + 1) * BandWidthHH + (ii / 2) + 1] |= RoiD; // HH
					}
				} else if (B == HL) {
					//(2x+1,2y)
					//	LL=>{    x-1,x,x+1,x+2} * {    y-1,y,y+1}
					//	HL=>{x-2,x-1,x,x+1,x+2} * {    y-1,y,y+1}
					//	LH=>{    x-1,x,x+1,x+2} * {y-2,y-1,y,y+1}
					//	HH=>{x-2,x-1,x,x+1,x+2} * {y-2,y-1,y,y+1}
					if ((jj / 2 - 2) >= 0) {
						if (ii / 2 - 2 >= 0)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 3]
							    .data[(jj / 2 - 2) * BandWidthHH + (ii / 2) - 2] |= RoiD; // HH
						if (ii / 2 - 2 >= 0) {
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 2]
							    .data[(jj / 2 - 2) * BandWidthLH + (ii / 2) - 1] |= RoiD; // LH
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 3]
							    .data[(jj / 2 - 2) * BandWidthHH + (ii / 2) - 1] |= RoiD; // HH
						}
						t->Cmpt[numCmpts]
						    .wave->MaskForROI[4 * (rrr - 1) + 2]
						    .data[(jj / 2 - 2) * BandWidthLH + (ii / 2)] |= RoiD; // LH
						t->Cmpt[numCmpts]
						    .wave->MaskForROI[4 * (rrr - 1) + 3]
						    .data[(jj / 2 - 2) * BandWidthHH + (ii / 2)] |= RoiD; // HH
						if (ii / 2 + 1 < BandWidthLH)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 2]
							    .data[(jj / 2 - 2) * BandWidthLH + (ii / 2) + 1] |= RoiD; // LH
						if (ii / 2 + 1 < BandWidthHH)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 3]
							    .data[(jj / 2 - 2) * BandWidthHH + (ii / 2) + 1] |= RoiD; // HH
						if (ii / 2 + 2 < BandWidthLH)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 2]
							    .data[(jj / 2 - 2) * BandWidthLH + (ii / 2) + 2] |= RoiD; // LH
						if (ii / 2 + 2 < BandWidthHH)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 3]
							    .data[(jj / 2 - 2) * BandWidthHH + (ii / 2) + 2] |= RoiD; // HH
					}

					if ((jj / 2 - 1) >= 0) {
						if (ii / 2 - 2 >= 0) {
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 1]
							    .data[(jj / 2 - 1) * BandWidthHL + (ii / 2) - 2] |= RoiD; // HL
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 3]
							    .data[(jj / 2 - 1) * BandWidthHH + (ii / 2) - 2] |= RoiD; // HH
						}
						if (ii / 2 - 1 >= 0) {
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1)]
							    .data[(jj / 2 - 1) * BandWidthLL + (ii / 2) - 1] |= RoiD; // LL
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 2]
							    .data[(jj / 2 - 1) * BandWidthLH + (ii / 2) - 1] |= RoiD; // LH
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 1]
							    .data[(jj / 2 - 1) * BandWidthHL + (ii / 2) - 1] |= RoiD; // HL
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 3]
							    .data[(jj / 2 - 1) * BandWidthHH + (ii / 2) - 1] |= RoiD; // HH
						}
						t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1)].data[(jj / 2 - 1) * BandWidthLL
						                                                       + (ii / 2)] |= RoiD; // LL
						t->Cmpt[numCmpts]
						    .wave->MaskForROI[4 * (rrr - 1) + 1]
						    .data[(jj / 2 - 1) * BandWidthHL + (ii / 2)] |= RoiD; // HL
						t->Cmpt[numCmpts]
						    .wave->MaskForROI[4 * (rrr - 1) + 2]
						    .data[(jj / 2 - 1) * BandWidthLH + (ii / 2)] |= RoiD; // LH
						t->Cmpt[numCmpts]
						    .wave->MaskForROI[4 * (rrr - 1) + 3]
						    .data[(jj / 2 - 1) * BandWidthHH + (ii / 2)] |= RoiD; // HH

						if (ii / 2 + 1 < BandWidthLL)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1)]
							    .data[(jj / 2 - 1) * BandWidthLL + (ii / 2) + 1] |= RoiD; // LL
						if (ii / 2 + 1 < BandWidthHL)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 1]
							    .data[(jj / 2 - 1) * BandWidthHL + (ii / 2) + 1] |= RoiD; // HL
						if (ii / 2 + 1 < BandWidthLH)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 2]
							    .data[(jj / 2 - 1) * BandWidthLH + (ii / 2) + 1] |= RoiD; // LH
						if (ii / 2 + 1 < BandWidthHH)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 3]
							    .data[(jj / 2 - 1) * BandWidthHH + (ii / 2) + 1] |= RoiD; // HH

						if (ii / 2 + 2 < BandWidthLL)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1)]
							    .data[(jj / 2 - 1) * BandWidthLL + (ii / 2) + 2] |= RoiD; // LL
						if (ii / 2 + 2 < BandWidthHL)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 1]
							    .data[(jj / 2 - 1) * BandWidthHL + (ii / 2) + 2] |= RoiD; // HL
						if (ii / 2 + 2 < BandWidthLH)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 2]
							    .data[(jj / 2 - 1) * BandWidthLH + (ii / 2) + 2] |= RoiD; // LH
						if (ii / 2 + 2 < BandWidthHH)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 3]
							    .data[(jj / 2 - 1) * BandWidthHH + (ii / 2) + 2] |= RoiD; // HH
					}
					if (ii / 2 - 2 >= 0) {
						t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1) + 1].data[(jj / 2) * BandWidthHL
						                                                           + (ii / 2) - 2] |=
						    RoiD; // HL
						t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1) + 3].data[(jj / 2) * BandWidthHH
						                                                           + (ii / 2) - 2] |=
						    RoiD; // HH
					}
					if (ii / 2 - 1 >= 0) {
						t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1)].data[(jj / 2) * BandWidthLL
						                                                       + (ii / 2) - 1] |=
						    RoiD; // LL
						t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1) + 1].data[(jj / 2) * BandWidthHL
						                                                           + (ii / 2) - 1] |=
						    RoiD; // HL
						t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1) + 2].data[(jj / 2) * BandWidthLH
						                                                           + (ii / 2) - 1] |=
						    RoiD; // LH
						t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1) + 3].data[(jj / 2) * BandWidthHH
						                                                           + (ii / 2) - 1] |=
						    RoiD; // HH
					}
					t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1)].data[(jj / 2) * BandWidthLL
					                                                       + (ii / 2)] |= RoiD;
					t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1) + 1].data[(jj / 2) * BandWidthHL
					                                                           + (ii / 2)] |= RoiD;
					t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1) + 2].data[(jj / 2) * BandWidthLH
					                                                           + (ii / 2)] |= RoiD;
					t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1) + 3].data[(jj / 2) * BandWidthHH
					                                                           + (ii / 2)] |= RoiD;

					if (ii / 2 + 1 < BandWidthLL)
						t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1)].data[(jj / 2) * BandWidthLL
						                                                       + (ii / 2) + 1] |=
						    RoiD; // LL
					if (ii / 2 + 1 < BandWidthHL)
						t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1) + 1].data[(jj / 2) * BandWidthHL
						                                                           + (ii / 2) + 1] |=
						    RoiD; // HL
					if (ii / 2 + 1 < BandWidthLH)
						t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1) + 2].data[(jj / 2) * BandWidthLH
						                                                           + (ii / 2) + 1] |=
						    RoiD; // LH
					if (ii / 2 + 1 < BandWidthHH)
						t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1) + 3].data[(jj / 2) * BandWidthHH
						                                                           + (ii / 2) + 1] |=
						    RoiD; // HH

					if (ii / 2 + 2 < BandWidthLL)
						t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1)].data[(jj / 2) * BandWidthLL
						                                                       + (ii / 2) + 2] |=
						    RoiD; // LL
					if (ii / 2 + 2 < BandWidthHL)
						t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1) + 1].data[(jj / 2) * BandWidthHL
						                                                           + (ii / 2) + 2] |=
						    RoiD; // HL
					if (ii / 2 + 2 < BandWidthLH)
						t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1) + 2].data[(jj / 2) * BandWidthLH
						                                                           + (ii / 2) + 2] |=
						    RoiD; // LH
					if (ii / 2 + 2 < BandWidthHH)
						t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1) + 3].data[(jj / 2) * BandWidthHH
						                                                           + (ii / 2) + 2] |=
						    RoiD; // HH

					if (jj / 2 + 1 < BandLengthLL) {
						if (ii / 2 - 1 >= 0)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1)]
							    .data[(jj / 2 + 1) * BandWidthLL + (ii / 2) - 1] |= RoiD; // LL
						t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1)].data[(jj / 2 + 1) * BandWidthLL
						                                                       + (ii / 2)] |= RoiD; // LL
						if (ii / 2 + 1 < BandWidthLL)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1)]
							    .data[(jj / 2 + 1) * BandWidthLL + (ii / 2) + 1] |= RoiD; // LL
						if (ii / 2 + 2 < BandWidthLL)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1)]
							    .data[(jj / 2 + 1) * BandWidthLL + (ii / 2) + 2] |= RoiD; // LL
					}
					if (jj / 2 + 1 < BandLengthHL) {
						if (ii / 2 - 2 >= 0)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 1]
							    .data[(jj / 2 + 1) * BandWidthHL + (ii / 2) - 2] |= RoiD; // HL
						if (ii / 2 - 1 >= 0)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 1]
							    .data[(jj / 2 + 1) * BandWidthHL + (ii / 2) - 1] |= RoiD; // HL
						t->Cmpt[numCmpts]
						    .wave->MaskForROI[4 * (rrr - 1) + 1]
						    .data[(jj / 2 + 1) * BandWidthHL + (ii / 2)] |= RoiD; // HL
						if (ii / 2 + 1 < BandWidthHL)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 1]
							    .data[(jj / 2 + 1) * BandWidthHL + (ii / 2) + 1] |= RoiD; // HL
						if (ii / 2 + 2 < BandWidthHL)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 1]
							    .data[(jj / 2 + 1) * BandWidthHL + (ii / 2) + 2] |= RoiD; // HL
					}
					if (jj / 2 + 1 < BandLengthLH) {
						if (ii / 2 - 2 >= 0)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 3]
							    .data[(jj / 2 + 1) * BandWidthHH + (ii / 2) - 2] |= RoiD; // HH
						if (ii / 2 - 1 >= 0)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 2]
							    .data[(jj / 2 + 1) * BandWidthLH + (ii / 2) - 1] |= RoiD; // LH
						t->Cmpt[numCmpts]
						    .wave->MaskForROI[4 * (rrr - 1) + 2]
						    .data[(jj / 2 + 1) * BandWidthLH + (ii / 2)] |= RoiD; // LH
						if (ii / 2 + 1 < BandWidthLH)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 2]
							    .data[(jj / 2 + 1) * BandWidthLH + (ii / 2) + 1] |= RoiD; // LH
						if (ii / 2 + 2 < BandWidthLH)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 2]
							    .data[(jj / 2 + 1) * BandWidthLH + (ii / 2) + 2] |= RoiD; // LH
					}
					if (jj / 2 + 1 < BandLengthHH) {
						if (ii / 2 - 1 >= 0)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 3]
							    .data[(jj / 2 + 1) * BandWidthHH + (ii / 2) - 1] |= RoiD; // HH
						t->Cmpt[numCmpts]
						    .wave->MaskForROI[4 * (rrr - 1) + 3]
						    .data[(jj / 2 + 1) * BandWidthHH + (ii / 2)] |= RoiD; // HH
						if (ii / 2 + 1 < BandWidthHH)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 3]
							    .data[(jj / 2 + 1) * BandWidthHH + (ii / 2) + 1] |= RoiD; // HH
						if (ii / 2 + 2 < BandWidthHH)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 3]
							    .data[(jj / 2 + 1) * BandWidthHH + (ii / 2) + 2] |= RoiD; // HH
					}
				} else if (B == LH) {
					//(2x,2y+1)
					//	LL=>{    x-1,x,x+1} * {    y-1,y,y+1,y+2}
					//	HL=>{x-2,x-1,x,x+1} * {    y-1,y,y+1,y+2}
					//	LH=>{    x-1,x,x+1} * {y-2,y-1,y,y+1,y+2}
					//	HH=>{x-2,x-1,x,x+1} * {y-2,y-1,y,y+1,y+2}
					if (ii / 2 - 2 >= 0) {
						t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1) + 1].data[(jj / 2) * BandWidthHL
						                                                           + (ii / 2) - 2] |=
						    RoiD; // HL
						if (jj / 2 - 1 >= 0)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 1]
							    .data[(jj / 2 - 1) * BandWidthHL + (ii / 2) - 2] |= RoiD; // HL
						if (jj / 2 + 1 < BandLengthHL)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 1]
							    .data[(jj / 2 + 1) * BandWidthHL + (ii / 2) - 2] |= RoiD; // HL
						if (jj / 2 + 2 < BandLengthHL)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 1]
							    .data[(jj / 2 + 2) * BandWidthHL + (ii / 2) - 2] |= RoiD; // HL
						if (jj / 2 - 2 >= 0)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 3]
							    .data[(jj / 2 - 2) * BandWidthHH + (ii / 2) - 2] |= RoiD; // HH
						t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1) + 3].data[(jj / 2) * BandWidthHH
						                                                           + (ii / 2) - 2] |=
						    RoiD; // HH
						if (jj / 2 - 1 >= 0)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 3]
							    .data[(jj / 2 - 1) * BandWidthHH + (ii / 2) - 2] |= RoiD; // HH
						if (jj / 2 + 1 < BandLengthHH)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 3]
							    .data[(jj / 2 + 1) * BandWidthHH + (ii / 2) - 2] |= RoiD; // HH
						if (jj / 2 + 2 < BandLengthHH)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 3]
							    .data[(jj / 2 + 2) * BandWidthHH + (ii / 2) - 2] |= RoiD; // HH
					}

					if (ii / 2 - 2 >= 0) {
						if (jj / 2 - 1 >= 0)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1)]
							    .data[(jj / 2 - 1) * BandWidthLL + (ii / 2) - 1] |= RoiD; // LL
						t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1)].data[(jj / 2) * BandWidthLL
						                                                       + (ii / 2) - 1] |=
						    RoiD; // LL
						if (jj / 2 + 1 < BandLengthLL)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1)]
							    .data[(jj / 2 + 1) * BandWidthLL + (ii / 2) - 1] |= RoiD; // LL
						if (jj / 2 + 2 < BandLengthLL)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1)]
							    .data[(jj / 2 + 2) * BandWidthLL + (ii / 2) - 1] |= RoiD; // LL
						if (jj / 2 - 1 >= 0)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 1]
							    .data[(jj / 2 - 1) * BandWidthHL + (ii / 2) - 1] |= RoiD; // HL
						t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1) + 1].data[(jj / 2) * BandWidthHL
						                                                           + (ii / 2) - 1] |=
						    RoiD; // HL
						if (jj / 2 + 1 < BandLengthHL)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 1]
							    .data[(jj / 2 + 1) * BandWidthHL + (ii / 2) - 1] |= RoiD; // HL
						if (jj / 2 + 2 < BandLengthHL)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 1]
							    .data[(jj / 2 + 2) * BandWidthHL + (ii / 2) - 1] |= RoiD; // HL
						if (jj / 2 - 2 >= 0)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 2]
							    .data[(jj / 2 - 2) * BandWidthLH + (ii / 2) - 1] |= RoiD; // LH
						if (jj / 2 - 1 >= 0)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 2]
							    .data[(jj / 2 - 1) * BandWidthLH + (ii / 2) - 1] |= RoiD; // LH
						t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1) + 2].data[(jj / 2) * BandWidthLH
						                                                           + (ii / 2) - 1] |=
						    RoiD; // LH
						if (jj / 2 + 1 < BandLengthLH)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 2]
							    .data[(jj / 2 + 1) * BandWidthLH + (ii / 2) - 1] |= RoiD; // LH
						if (jj / 2 + 2 < BandLengthLH)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 2]
							    .data[(jj / 2 + 2) * BandWidthLH + (ii / 2) - 1] |= RoiD; // LH
						if (jj / 2 - 2 >= 0)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 3]
							    .data[(jj / 2 - 2) * BandWidthHH + (ii / 2) - 1] |= RoiD; // HH
						if (jj / 2 - 1 >= 0)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 3]
							    .data[(jj / 2 - 1) * BandWidthHH + (ii / 2) - 1] |= RoiD; // HH
						t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1) + 3].data[(jj / 2) * BandWidthHH
						                                                           + (ii / 2) - 1] |=
						    RoiD; // HH
						if (jj / 2 + 1 < BandLengthHH)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 3]
							    .data[(jj / 2 + 1) * BandWidthHH + (ii / 2) - 1] |= RoiD; // HH
						if (jj / 2 + 2 < BandLengthHH)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 3]
							    .data[(jj / 2 + 2) * BandWidthHH + (ii / 2) - 1] |= RoiD; // HH
					}

					if (jj / 2 - 1 >= 0)
						t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1)].data[(jj / 2 - 1) * BandWidthLL
						                                                       + (ii / 2)] |= RoiD; // LL
					t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1)].data[(jj / 2) * BandWidthLL
					                                                       + (ii / 2)] |= RoiD; // LL
					if (jj / 2 + 1 < BandLengthLL)
						t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1)].data[(jj / 2 + 1) * BandWidthLL
						                                                       + (ii / 2)] |= RoiD; // LL
					if (jj / 2 + 2 < BandLengthLL)
						t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1)].data[(jj / 2 + 2) * BandWidthLL
						                                                       + (ii / 2)] |= RoiD; // LL
					if (jj / 2 - 1 >= 0)
						t->Cmpt[numCmpts]
						    .wave->MaskForROI[4 * (rrr - 1) + 1]
						    .data[(jj / 2 - 1) * BandWidthHL + (ii / 2)] |= RoiD; // HL
					t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1) + 1].data[(jj / 2) * BandWidthHL
					                                                           + (ii / 2)] |= RoiD; // HL
					if (jj / 2 + 1 < BandLengthHL)
						t->Cmpt[numCmpts]
						    .wave->MaskForROI[4 * (rrr - 1) + 1]
						    .data[(jj / 2 + 1) * BandWidthHL + (ii / 2)] |= RoiD; // HL
					if (jj / 2 + 2 < BandLengthHL)
						t->Cmpt[numCmpts]
						    .wave->MaskForROI[4 * (rrr - 1) + 1]
						    .data[(jj / 2 + 2) * BandWidthHL + (ii / 2)] |= RoiD; // HL
					if (jj / 2 - 2 >= 0)
						t->Cmpt[numCmpts]
						    .wave->MaskForROI[4 * (rrr - 1) + 2]
						    .data[(jj / 2 - 2) * BandWidthLH + (ii / 2)] |= RoiD; // LH
					if (jj / 2 - 1 >= 0)
						t->Cmpt[numCmpts]
						    .wave->MaskForROI[4 * (rrr - 1) + 2]
						    .data[(jj / 2 - 1) * BandWidthLH + (ii / 2)] |= RoiD; // LH
					t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1) + 2].data[(jj / 2) * BandWidthLH
					                                                           + (ii / 2)] |= RoiD; // LH
					if (jj / 2 + 1 < BandLengthLH)
						t->Cmpt[numCmpts]
						    .wave->MaskForROI[4 * (rrr - 1) + 2]
						    .data[(jj / 2 + 1) * BandWidthLH + (ii / 2)] |= RoiD; // LH
					if (jj / 2 + 2 < BandLengthLH)
						t->Cmpt[numCmpts]
						    .wave->MaskForROI[4 * (rrr - 1) + 2]
						    .data[(jj / 2 + 2) * BandWidthLH + (ii / 2)] |= RoiD; // LH
					if (jj / 2 - 2 >= 0)
						t->Cmpt[numCmpts]
						    .wave->MaskForROI[4 * (rrr - 1) + 3]
						    .data[(jj / 2 - 2) * BandWidthHH + (ii / 2)] |= RoiD; // HH
					if (jj / 2 - 1 >= 0)
						t->Cmpt[numCmpts]
						    .wave->MaskForROI[4 * (rrr - 1) + 3]
						    .data[(jj / 2 - 1) * BandWidthHH + (ii / 2)] |= RoiD; // HH
					t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1) + 3].data[(jj / 2) * BandWidthHH
					                                                           + (ii / 2)] |= RoiD; // HH
					if (jj / 2 + 1 < BandLengthHH)
						t->Cmpt[numCmpts]
						    .wave->MaskForROI[4 * (rrr - 1) + 3]
						    .data[(jj / 2 + 1) * BandWidthHH + (ii / 2)] |= RoiD; // HH
					if (jj / 2 + 2 < BandLengthHH)
						t->Cmpt[numCmpts]
						    .wave->MaskForROI[4 * (rrr - 1) + 3]
						    .data[(jj / 2 + 2) * BandWidthHH + (ii / 2)] |= RoiD; // HH

					if (ii / 2 + 1 < BandWidthLL) {
						if (jj / 2 - 1 >= 0)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1)]
							    .data[(jj / 2 - 1) * BandWidthLL + (ii / 2) + 1] |= RoiD; // LL
						t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1)].data[(jj / 2) * BandWidthLL
						                                                       + (ii / 2) + 1] |=
						    RoiD; // LL
						if (jj / 2 + 1 < BandLengthLL)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1)]
							    .data[(jj / 2 + 1) * BandWidthLL + (ii / 2) + 1] |= RoiD; // LL
						if (jj / 2 + 2 < BandLengthLL)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1)]
							    .data[(jj / 2 + 2) * BandWidthLL + (ii / 2) + 1] |= RoiD; // LL
					}
					if (ii / 2 + 1 < BandWidthHL) {
						if (jj / 2 - 1 >= 0)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 1]
							    .data[(jj / 2 - 1) * BandWidthHL + (ii / 2) + 1] |= RoiD; // HL
						t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1) + 1].data[(jj / 2) * BandWidthHL
						                                                           + (ii / 2) + 1] |=
						    RoiD; // HL
						if (jj / 2 + 1 < BandLengthHL)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 1]
							    .data[(jj / 2 + 1) * BandWidthHL + (ii / 2) + 1] |= RoiD; // HL
						if (jj / 2 + 2 < BandLengthHL)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 1]
							    .data[(jj / 2 + 2) * BandWidthHL + (ii / 2) + 1] |= RoiD; // HL
					}
					if (ii / 2 + 1 < BandWidthLH) {
						if (jj / 2 - 2 >= 0)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 2]
							    .data[(jj / 2 - 2) * BandWidthLH + (ii / 2) + 1] |= RoiD; // LH
						if (jj / 2 - 1 >= 0)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 2]
							    .data[(jj / 2 - 1) * BandWidthLH + (ii / 2) + 1] |= RoiD; // LH
						t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1) + 2].data[(jj / 2) * BandWidthLH
						                                                           + (ii / 2) + 1] |=
						    RoiD; // LH
						if (jj / 2 + 1 < BandLengthLH)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 2]
							    .data[(jj / 2 + 1) * BandWidthLH + (ii / 2) + 1] |= RoiD; // LH
						if (jj / 2 + 2 < BandLengthLH)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 2]
							    .data[(jj / 2 + 2) * BandWidthLH + (ii / 2) + 1] |= RoiD; // LH
					}
					if (ii / 2 + 1 < BandWidthHH) {
						if (jj / 2 - 2 >= 0)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 3]
							    .data[(jj / 2 - 2) * BandWidthHH + (ii / 2) + 1] |= RoiD; // HH
						if (jj / 2 - 1 >= 0)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 3]
							    .data[(jj / 2 - 1) * BandWidthHH + (ii / 2) + 1] |= RoiD; // HH
						t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1) + 3].data[(jj / 2) * BandWidthHH
						                                                           + (ii / 2) + 1] |=
						    RoiD; // HH
						if (jj / 2 + 1 < BandLengthHH)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 3]
							    .data[(jj / 2 + 1) * BandWidthHH + (ii / 2) + 1] |= RoiD; // HH
						if (jj / 2 + 2 < BandLengthHH)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 3]
							    .data[(jj / 2 + 2) * BandWidthHH + (ii / 2) + 1] |= RoiD; // HH
					}
				} else if (B == HH) {
					//(2x+1,2y+1)
					//	LL=>{    x-1,x,x+1,x+2} * {    y-1,y,y+1,y+2}
					//	HL=>{x-2,x-1,x,x+1,x+2} * {    y-1,y,y+1,y+2}
					//	LH=>{    x-1,x,x+1,x+2} * {y-2,y-1,y,y+1,y+2}
					//	HH=>{x-2,x-1,x,x+1,x+2} * {y-2,y-1,y,y+1,y+2}
					if (jj / 2 - 2 >= 0) {
						if (ii / 2 - 1 >= 0)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 2]
							    .data[(jj / 2 - 2) * BandWidthLH + (ii / 2) - 1] |= RoiD; // LH
						t->Cmpt[numCmpts]
						    .wave->MaskForROI[4 * (rrr - 1) + 2]
						    .data[(jj / 2 - 2) * BandWidthLH + (ii / 2)] |= RoiD; // LH
						if (ii / 2 + 1 < BandWidthLH)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 2]
							    .data[(jj / 2 - 2) * BandWidthLH + (ii / 2) + 1] |= RoiD; // LH
						if (ii / 2 + 2 < BandWidthLH)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 2]
							    .data[(jj / 2 - 2) * BandWidthLH + (ii / 2) + 2] |= RoiD; // LH
						if (ii / 2 - 2 >= 0)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 3]
							    .data[(jj / 2 - 2) * BandWidthHH + (ii / 2) - 2] |= RoiD; // HH
						if (ii / 2 - 1 >= 0)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 3]
							    .data[(jj / 2 - 2) * BandWidthHH + (ii / 2) - 1] |= RoiD; // HH
						t->Cmpt[numCmpts]
						    .wave->MaskForROI[4 * (rrr - 1) + 3]
						    .data[(jj / 2 - 2) * BandWidthHH + (ii / 2)] |= RoiD; // HH
						if (ii / 2 + 1 < BandWidthHH)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 3]
							    .data[(jj / 2 - 2) * BandWidthHH + (ii / 2) + 1] |= RoiD; // HH
						if (ii / 2 + 2 < BandWidthHH)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 3]
							    .data[(jj / 2 - 2) * BandWidthHH + (ii / 2) + 2] |= RoiD; // HH
					}

					if (jj / 2 - 1 >= 0) {
						if (ii / 2 - 1 >= 0)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1)]
							    .data[(jj / 2 - 1) * BandWidthLL + (ii / 2) - 1] |= RoiD; // LL
						t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1)].data[(jj / 2 - 1) * BandWidthLL
						                                                       + (ii / 2)] |= RoiD; // LL
						if (ii / 2 + 1 < BandWidthLL)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1)]
							    .data[(jj / 2 - 1) * BandWidthLL + (ii / 2) + 1] |= RoiD; // LL
						if (ii / 2 + 2 < BandWidthLL)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1)]
							    .data[(jj / 2 - 1) * BandWidthLL + (ii / 2) + 2] |= RoiD; // LL
						if (ii / 2 - 1 >= 0)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 1]
							    .data[(jj / 2 - 1) * BandWidthHL + (ii / 2) - 2] |= RoiD; // HL
						if (ii / 2 - 1 >= 0)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 1]
							    .data[(jj / 2 - 1) * BandWidthHL + (ii / 2) - 1] |= RoiD; // HL
						t->Cmpt[numCmpts]
						    .wave->MaskForROI[4 * (rrr - 1) + 1]
						    .data[(jj / 2 - 1) * BandWidthHL + (ii / 2)] |= RoiD; // HL
						if (ii / 2 + 1 < BandWidthHL)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 1]
							    .data[(jj / 2 - 1) * BandWidthHL + (ii / 2) + 1] |= RoiD; // HL
						if (ii / 2 + 2 < BandWidthHL)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 1]
							    .data[(jj / 2 - 1) * BandWidthHL + (ii / 2) + 2] |= RoiD; // HL
						if (ii / 2 - 1 >= 0)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 2]
							    .data[(jj / 2 - 1) * BandWidthLH + (ii / 2) - 1] |= RoiD; // LH
						t->Cmpt[numCmpts]
						    .wave->MaskForROI[4 * (rrr - 1) + 2]
						    .data[(jj / 2 - 1) * BandWidthLH + (ii / 2)] |= RoiD; // LH
						if (ii / 2 + 1 < BandWidthLH)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 2]
							    .data[(jj / 2 - 1) * BandWidthLH + (ii / 2) + 1] |= RoiD; // LH
						if (ii / 2 + 2 < BandWidthLH)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 2]
							    .data[(jj / 2 - 1) * BandWidthLH + (ii / 2) + 2] |= RoiD; // LH
						if (ii / 2 - 1 >= 0)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 3]
							    .data[(jj / 2 - 1) * BandWidthHH + (ii / 2) - 2] |= RoiD; // HH
						if (ii / 2 - 1 >= 0)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 3]
							    .data[(jj / 2 - 1) * BandWidthHH + (ii / 2) - 1] |= RoiD; // HH
						t->Cmpt[numCmpts]
						    .wave->MaskForROI[4 * (rrr - 1) + 3]
						    .data[(jj / 2 - 1) * BandWidthHH + (ii / 2)] |= RoiD; // HH
						if (ii / 2 + 1 < BandWidthHH)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 3]
							    .data[(jj / 2 - 1) * BandWidthHH + (ii / 2) + 1] |= RoiD; // HH
						if (ii / 2 + 2 < BandWidthHH)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 3]
							    .data[(jj / 2 - 1) * BandWidthHH + (ii / 2) + 2] |= RoiD; // HH
					}

					if (ii / 2 - 1 >= 0)
						t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1)].data[(jj / 2) * BandWidthLL
						                                                       + (ii / 2) - 1] |=
						    RoiD; // LL
					t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1)].data[(jj / 2) * BandWidthLL
					                                                       + (ii / 2)] |= RoiD; // LL
					if (ii / 2 + 1 < BandWidthLL)
						t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1)].data[(jj / 2) * BandWidthLL
						                                                       + (ii / 2) + 1] |=
						    RoiD; // LL
					if (ii / 2 + 2 < BandWidthLL)
						t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1)].data[(jj / 2) * BandWidthLL
						                                                       + (ii / 2) + 2] |=
						    RoiD; // LL
					if (ii / 2 - 2 >= 0)
						t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1) + 1].data[(jj / 2) * BandWidthHL
						                                                           + (ii / 2) - 2] |=
						    RoiD; // HL
					if (ii / 2 - 1 >= 0)
						t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1) + 1].data[(jj / 2) * BandWidthHL
						                                                           + (ii / 2) - 1] |=
						    RoiD; // HL
					t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1) + 1].data[(jj / 2) * BandWidthHL
					                                                           + (ii / 2)] |= RoiD; // HL
					if (ii / 2 + 1 < BandWidthHL)
						t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1) + 1].data[(jj / 2) * BandWidthHL
						                                                           + (ii / 2) + 1] |=
						    RoiD; // HL
					if (ii / 2 + 2 < BandWidthHL)
						t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1) + 1].data[(jj / 2) * BandWidthHL
						                                                           + (ii / 2) + 2] |=
						    RoiD; // HL
					if (ii / 2 - 1 >= 0)
						t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1) + 2].data[(jj / 2) * BandWidthLH
						                                                           + (ii / 2) - 1] |=
						    RoiD; // LH
					t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1) + 2].data[(jj / 2) * BandWidthLH
					                                                           + (ii / 2)] |= RoiD; // LH
					if (ii / 2 + 1 < BandWidthLH)
						t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1) + 2].data[(jj / 2) * BandWidthLH
						                                                           + (ii / 2) + 1] |=
						    RoiD; // LH
					if (ii / 2 + 2 < BandWidthLH)
						t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1) + 2].data[(jj / 2) * BandWidthLH
						                                                           + (ii / 2) + 2] |=
						    RoiD; // LH
					if (ii / 2 - 2 >= 0)
						t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1) + 3].data[(jj / 2) * BandWidthHH
						                                                           + (ii / 2) - 2] |=
						    RoiD; // HH
					if (ii / 2 - 1 >= 0)
						t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1) + 3].data[(jj / 2) * BandWidthHH
						                                                           + (ii / 2) - 1] |=
						    RoiD; // HH
					t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1) + 3].data[(jj / 2) * BandWidthHH
					                                                           + (ii / 2)] |= RoiD; // HH
					if (ii / 2 + 1 < BandWidthHH)
						t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1) + 3].data[(jj / 2) * BandWidthHH
						                                                           + (ii / 2) + 1] |=
						    RoiD; // HH
					if (ii / 2 + 2 < BandWidthHH)
						t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1) + 3].data[(jj / 2) * BandWidthHH
						                                                           + (ii / 2) + 2] |=
						    RoiD; // HH

					if (jj / 2 + 1 < BandLengthLL) {
						if (ii / 2 - 1 >= 0)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1)]
							    .data[(jj / 2 + 1) * BandWidthLL + (ii / 2) - 1] |= RoiD; // LL
						t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1)].data[(jj / 2 + 1) * BandWidthLL
						                                                       + (ii / 2)] |= RoiD; // LL
						if (ii / 2 + 1 < BandWidthLL)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1)]
							    .data[(jj / 2 + 1) * BandWidthLL + (ii / 2) + 1] |= RoiD; // LL
						if (ii / 2 + 2 < BandWidthLL)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1)]
							    .data[(jj / 2 + 1) * BandWidthLL + (ii / 2) + 2] |= RoiD; // LL
					}
					if (jj / 2 + 1 < BandLengthHL) {
						if (ii / 2 - 2 >= 0)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 1]
							    .data[(jj / 2 + 1) * BandWidthHL + (ii / 2) - 2] |= RoiD; // HL
						if (ii / 2 - 1 >= 0)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 1]
							    .data[(jj / 2 + 1) * BandWidthHL + (ii / 2) - 1] |= RoiD; // HL
						t->Cmpt[numCmpts]
						    .wave->MaskForROI[4 * (rrr - 1) + 1]
						    .data[(jj / 2 + 1) * BandWidthHL + (ii / 2)] |= RoiD; // HL
						if (ii / 2 + 1 < BandWidthHL)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 1]
							    .data[(jj / 2 + 1) * BandWidthHL + (ii / 2) + 1] |= RoiD; // HL
						if (ii / 2 + 2 < BandWidthHL)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 1]
							    .data[(jj / 2 + 1) * BandWidthHL + (ii / 2) + 2] |= RoiD; // HL
					}
					if (jj / 2 + 1 < BandLengthLH) {
						if (ii / 2 - 1 >= 0)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 2]
							    .data[(jj / 2 + 1) * BandWidthLH + (ii / 2) - 1] |= RoiD; // LH
						t->Cmpt[numCmpts]
						    .wave->MaskForROI[4 * (rrr - 1) + 2]
						    .data[(jj / 2 + 1) * BandWidthLH + (ii / 2)] |= RoiD; // LH
						if (ii / 2 + 1 < BandWidthLH)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 2]
							    .data[(jj / 2 + 1) * BandWidthLH + (ii / 2) + 1] |= RoiD; // LH
						if (ii / 2 + 2 < BandWidthLH)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 2]
							    .data[(jj / 2 + 1) * BandWidthLH + (ii / 2) + 2] |= RoiD; // LH
					}
					if (jj / 2 + 1 < BandLengthLH) {
						if (ii / 2 - 2 >= 0)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 3]
							    .data[(jj / 2 + 1) * BandWidthHH + (ii / 2) - 2] |= RoiD; // HH
						if (ii / 2 - 1 >= 0)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 3]
							    .data[(jj / 2 + 1) * BandWidthHH + (ii / 2) - 1] |= RoiD; // HH
						t->Cmpt[numCmpts]
						    .wave->MaskForROI[4 * (rrr - 1) + 3]
						    .data[(jj / 2 + 1) * BandWidthHH + (ii / 2)] |= RoiD; // HH
						if (ii / 2 + 1 < BandWidthHH)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 3]
							    .data[(jj / 2 + 1) * BandWidthHH + (ii / 2) + 1] |= RoiD; // HH
						if (ii / 2 + 2 < BandWidthHH)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 3]
							    .data[(jj / 2 + 1) * BandWidthHH + (ii / 2) + 2] |= RoiD; // HH
					}

					if (jj / 2 + 2 < BandLengthLL) {
						if (ii / 2 - 1 >= 0)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1)]
							    .data[(jj / 2 + 2) * BandWidthLL + (ii / 2) - 1] |= RoiD; // LL
						t->Cmpt[numCmpts].wave->MaskForROI[4 * (rrr - 1)].data[(jj / 2 + 2) * BandWidthLL
						                                                       + (ii / 2)] |= RoiD; // LL
						if (ii / 2 + 1 < BandWidthLL)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1)]
							    .data[(jj / 2 + 2) * BandWidthLL + (ii / 2) + 1] |= RoiD; // LL
						if (ii / 2 + 2 < BandWidthLL)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1)]
							    .data[(jj / 2 + 2) * BandWidthLL + (ii / 2) + 2] |= RoiD; // LL
					}
					if (jj / 2 + 2 < BandLengthHL) {
						if (ii / 2 - 2 >= 0)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 1]
							    .data[(jj / 2 + 2) * BandWidthHL + (ii / 2) - 2] |= RoiD; // HL
						if (ii / 2 - 1 >= 0)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 1]
							    .data[(jj / 2 + 2) * BandWidthHL + (ii / 2) - 1] |= RoiD; // HL
						t->Cmpt[numCmpts]
						    .wave->MaskForROI[4 * (rrr - 1) + 1]
						    .data[(jj / 2 + 2) * BandWidthHL + (ii / 2)] |= RoiD; // HL
						if (ii / 2 + 1 < BandWidthHL)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 1]
							    .data[(jj / 2 + 2) * BandWidthHL + (ii / 2) + 1] |= RoiD; // HL
						if (ii / 2 + 2 < BandWidthHL)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 1]
							    .data[(jj / 2 + 2) * BandWidthHL + (ii / 2) + 2] |= RoiD; // HL
					}
					if (jj / 2 + 2 < BandLengthLH) {
						if (ii / 2 - 1 >= 0)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 2]
							    .data[(jj / 2 + 2) * BandWidthLH + (ii / 2) - 1] |= RoiD; // LH
						t->Cmpt[numCmpts]
						    .wave->MaskForROI[4 * (rrr - 1) + 2]
						    .data[(jj / 2 + 2) * BandWidthLH + (ii / 2)] |= RoiD; // LH
						if (ii / 2 + 1 < BandWidthLH)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 2]
							    .data[(jj / 2 + 2) * BandWidthLH + (ii / 2) + 1] |= RoiD; // LH
						if (ii / 2 + 2 < BandWidthLH)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 2]
							    .data[(jj / 2 + 2) * BandWidthLH + (ii / 2) + 2] |= RoiD; // LH
					}
					if (jj / 2 + 2 < BandLengthHH) {
						if (ii / 2 - 2 >= 0)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 3]
							    .data[(jj / 2 + 2) * BandWidthHH + (ii / 2) - 2] |= RoiD; // HH
						if (ii / 2 - 1 >= 0)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 3]
							    .data[(jj / 2 + 2) * BandWidthHH + (ii / 2) - 1] |= RoiD; // HH
						t->Cmpt[numCmpts]
						    .wave->MaskForROI[4 * (rrr - 1) + 3]
						    .data[(jj / 2 + 2) * BandWidthHH + (ii / 2)] |= RoiD; // HH
						if (ii / 2 + 1 < BandWidthHH)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 3]
							    .data[(jj / 2 + 2) * BandWidthHH + (ii / 2) + 1] |= RoiD; // HH
						if (ii / 2 + 2 < BandWidthHH)
							t->Cmpt[numCmpts]
							    .wave->MaskForROI[4 * (rrr - 1) + 3]
							    .data[(jj / 2 + 2) * BandWidthHH + (ii / 2) + 2] |= RoiD; // HH
					}
				}
			}
		}
	}
}

void Roi_S_ValueCal(struct EECod_s *EECod, struct EEQcd_s *EEQcd, struct ERgn_s *ERgn, ubyte2 ccc) {
	uchar bbb;

	for (bbb = 0; bbb < (uchar) (EECod->DecompLev * 3 + 1); bbb++) {
		if (ERgn->EERgn[ccc].SPrgn < (EEQcd->G + EEQcd->e[bbb] - 1))
			ERgn->EERgn[ccc].SPrgn = (uchar) (EEQcd->G + EEQcd->e[bbb] - 1);
	}
}

byte4 Roi_S_ShiftUP(struct Tile_s *t, struct ERgn_s *ERgn, ubyte2 ccc) {
	byte4 *D_TS, *D_;
	byte4 *R_TS, *R_;
	byte4 iii, jjj;
	byte4 col1step, col1stepR;
	char bbb;
	uchar ROI_shift;

	ROI_shift = ERgn->EERgn[ccc].SPrgn;

	for (bbb = 0; bbb < t->Cmpt[ccc].wave->numBands; bbb++) {
		col1step  = t->Cmpt[ccc].wave->wave[bbb].col1step;
		col1stepR = ERgn->EERgn[ccc].Wave_Roi->wave[bbb].col1step;
		D_TS      = t->Cmpt[ccc].wave->wave[bbb].data;
		R_TS      = ERgn->EERgn[ccc].Wave_Roi->wave[bbb].data;
		for (jjj = t->Cmpt[ccc].wave->wave[bbb].tby0; jjj < t->Cmpt[ccc].wave->wave[bbb].tby1;
		     jjj++, D_TS += col1step, R_TS += col1stepR) {
			D_ = D_TS;
			R_ = R_TS;
			for (iii = t->Cmpt[ccc].wave->wave[bbb].tbx0; iii < t->Cmpt[ccc].wave->wave[bbb].tbx1;
			     iii++, ++D_, ++R_) {
				if (*R_) {
					if (((*D_) & 0x80000000))
						*D_ = (((*D_) & 0x7fffffff) << ROI_shift) | 0x80000000;
					else
						*D_ = (((*D_) & 0x7fffffff) << ROI_shift);
				}
			}
		}
	}
	return EXIT_SUCCESS;
}

void WaveROI_IL(struct Image_s *image, byte4 lll, struct Image_s *ll_d, struct Image_s *ll,
                struct Image_s *hl, struct Image_s *lh, struct Image_s *hh) {
	byte4 height, width;
	byte4 i, j, jj;
	byte4 col1step, col1step2, row1step, row2step;
	byte4 xs_L, xs_H, ys_L, ys_H;
	byte4 *D2;
	byte4 *D, *D_TS;

	width    = image->tbx1 - image->tbx0;
	height   = image->tby1 - image->tby0;
	col1step = image->col1step;
	row1step = image->row1step;
	row2step = row1step * 2;

	if (image->tbx0 & 1) {
		xs_L = 1;
		xs_H = 0;
	} // odd
	else {
		xs_L = 0;
		xs_H = 1;
	} // even
	if (image->tby0 & 1) {
		ys_L = 1;
		ys_H = 0;
	} // odd
	else {
		ys_L = 0;
		ys_H = 1;
	} // even

	// ll
	if (lll != 1) {
		D_TS      = ll_d->data;
		col1step2 = ll_d->col1step;
	} else {
		D_TS      = ll->data;
		col1step2 = ll->col1step;
	}
	for (j = ys_L, jj = 0; j < height; j += 2, jj++) {
		D  = &D_TS[jj * col1step2];
		D2 = &image->data[j * col1step + xs_L * row1step];
		for (i = xs_L; i < width; i += 2, ++D, D2 += row2step) {
			if (*D2)
				*D = 1;
			else
				*D = 0;
		}
	}

	// hl
	D_TS = hl->data;
	for (j = ys_L, jj = 0; j < height; j += 2, jj++) {
		D  = &D_TS[jj * hl->col1step];
		D2 = &image->data[j * col1step + xs_H * row1step];
		for (i = xs_H; i < width; i += 2, ++D, D2 += row2step) {
			if (*D2)
				*D = 1;
			else
				*D = 0;
		}
	}

	// lh
	D_TS = lh->data;
	for (j = ys_H, jj = 0; j < height; j += 2, jj++) {
		D  = &D_TS[jj * lh->col1step];
		D2 = &image->data[j * col1step + xs_L * row1step];
		for (i = xs_L; i < width; i += 2, ++D, D2 += row2step) {
			if (*D2)
				*D = 1;
			else
				*D = 0;
		}
	}

	// hh
	D_TS = hh->data;
	for (j = ys_H, jj = 0; j < height; j += 2, jj++) {
		D  = &D_TS[jj * hh->col1step];
		D2 = &image->data[j * col1step + xs_H * row1step];
		for (i = xs_H; i < width; i += 2, ++D, D2 += row2step) {
			if (*D2)
				*D = 1;
			else
				*D = 0;
		}
	}
}

byte4 Wave_ROI(struct wavelet_s *Wave) {
	char lll;
	char NL;

	NL = Wave->NL;

	for (lll = NL; lll > 0; lll--) {
		WaveROI_IL(&Wave->ll_d[lll], lll, &Wave->ll_d[lll - 1], &Wave->wave[lll * 3 - 3],
		           &Wave->wave[lll * 3 - 2], &Wave->wave[lll * 3 - 1], &Wave->wave[lll * 3]);
	}

	return EXIT_SUCCESS;
}
