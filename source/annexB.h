/*
Copyright (c) 2000-2021, ICT-Link
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the HTJ2K project.
*/

#ifndef ANNEX_B_H
#define ANNEX_B_H

//#define NO_LIMIT 0x7fffffff
#define NO_LIMIT -1

byte4 TagtreeCreate2(
    struct Tile_s *t /*, struct EECod_s *EECod, struct EEQcd_s *EEQcd, struct Codec_s *codec*/, byte4 Ps,
    byte4 Pe);
struct Cblk_s *CreateCblk(byte4 numCblk, char rrr, char bbb, struct Codec_s *Codec, struct EECod_s *EECod);
byte4 ProgCnt(struct Codec_s *Codec, struct Tile_s *Tile, uchar tilePartNo, struct wavelet_s *Wave,
              struct ECod_s *ECod,
              /*struct StreamChain_s *PocMain, struct StreamChain_s *PocTile,*/ byte2 *Cppp, uchar *flagD,
              uchar Control);
byte4 LRCP_cnt(byte4 PacketNo, uchar RSpoc, uchar REpoc, byte2 LYSpoc, byte2 LYEpoc, byte2 CSpoc,
               byte2 CEpoc, struct Codec_s *Codec, struct Tile_s *t, struct wavelet_s *Wave, uchar *flagD,
               byte4 colcstep, byte4 colrstep, byte4 colpstep, byte4 colkstep);
byte4 RLCP_cnt(byte4 PacketNo, uchar RSpoc, uchar REpoc, byte2 LYSpoc, byte2 LYEpoc, byte2 CSpoc,
               byte2 CEpoc, struct Codec_s *Codec, struct Tile_s *t, struct wavelet_s *Wave, uchar *flagD,
               byte4 colcstep, byte4 colrstep, byte4 colpstep, byte4 colkstep);
byte4 RPCL_cnt(byte4 PacketNo, uchar RSpoc, uchar REpoc, byte2 LYSpoc, byte2 LYEpoc, byte2 CSpoc,
               byte2 CEpoc, byte2 *Cppp, struct Codec_s *Codec, struct Tile_s *t, struct wavelet_s *Wave,
               uchar *flagD, byte4 colcstep, byte4 colrstep, byte4 colpstep, byte4 colkstep);
byte4 PCRL_cnt(byte4 PacketNo, uchar RSpoc, uchar REpoc, byte2 LYSpoc, byte2 LYEpoc, byte2 CSpoc,
               byte2 CEpoc, byte2 *Cppp, struct Codec_s *Codec, struct Tile_s *t, struct wavelet_s *Wave,
               uchar *flagD, byte4 colcstep, byte4 colrstep, byte4 colpstep, byte4 colkstep);
byte4 CPRL_cnt(byte4 PacketNo, uchar RSpoc, uchar REpoc, byte2 LYSpoc, byte2 LYEpoc, byte2 CSpoc,
               byte2 CEpoc, byte2 *Cppp, struct Codec_s *Codec, struct Tile_s *t, struct wavelet_s *Wave,
               uchar *flagD, byte4 colcstep, byte4 colrstep, byte4 colpstep, byte4 colkstep);

byte4 DecMainBody(struct Tile_s *t, struct mqdec_s *dec, struct EECod_s *EECod, struct Codec_s *Codec,
                  struct StreamChain_s *stream, ubyte2 ccc);

#endif
