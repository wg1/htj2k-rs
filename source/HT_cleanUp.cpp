/*
Copyright (c) 2000-2021, ICT-Link
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the HTJ2K project.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ImageUtil.h"
#include "j2k2.h"
#include "MelCodec.h"
#include "vlc_codec.h"
#include "HT_cleanUp.h"

#define OLDCX 0

char Bittop(char startT, char endT, byte4 D) {
	byte4 iii;
	if (D == 0)
		return 0;
	else if (D == 1)
		return 1;
	else {
		iii = startT;
		while (iii > endT) {
			if (mask1[iii] & (D - 1)) return (char) (iii + 1);
			iii--;
		}
	}
	return 0;
}

char Bittop2(char startT, char endT, byte4 D) {
	char iii;
	if (D < 0)
		return -1;
	else {
		iii = startT;
		while (iii > endT) {
			if (mask1[iii] & D) return iii;
			iii--;
		}
		if (iii < 0) iii = 0;
	}
	return iii;
}

byte4 encode_cln(struct J2kParam_s *J2kParam, struct Cblk_s *Cblk, struct Image_s *Image_mex2, byte4 width,
                 byte4 height, struct RRawCodec_s *codecR, struct MelCodec_s *codecM,
                 struct fRawCodec_s *codecf, struct StreamChain_s *str_rraw, struct StreamChain_s *str_melc,
                 struct StreamChain_s *str_fraw) {
	byte4 iii, jjj;
	char e_nw;
	char e0, e1, e2, e3;
	char Kenw, Ken, Kene, Kenf, Y_Line;
	byte4 frawD0, frawD1, frawD2, frawD3;
	uchar frawL0, frawL1, frawL2, frawL3;
	uchar rho, nRho;
	uchar Uq, Uq0, u_off, u_off0, u_off1;
	byte4 CwD, CwD0, CwD1;
	uchar CwL;
	uchar melFlag;
	uchar CX0, CX1, CX;
	byte4 *mel_D;
	byte4 col1step, col2step, row1step;
	ubyte2 qinf0, qinf1;
	uchar *mex0_p, *mex0_TS, *mex1_p, *mex1_TS;
	uchar qmex0, qmex1;
	uchar epcironK;
	byte4 *BandD;
	uchar msbits2_0, msbits2_1, msbits2_2, msbits2_3;
	char kappa, gamma, Uq00;
	uchar EmbPattern;
	uchar MelRun;
	byte4 js, is;
	byte4 *in0_p, *in1_p, *in0_TS, *in1_TS;

#if 0
	FILE	*fp_e1e3_in=nullptr, *fp_e1e3_out=nullptr;
	fp_e1e3_in  = fopen( "eie3_in.csv" , "a+" );
	fp_e1e3_out = fopen( "eie3_out.csv", "a+" );
#endif

	col1step = Cblk->Band->col1step;
	col2step = 2 * col1step;
	row1step = Cblk->Band->row1step;
	BandD    = (byte4 *) Cblk->Band->data;
	js       = Cblk->tcby0 - Cblk->Band->tby0;
	is       = Cblk->tcbx0 - Cblk->Band->tbx0;
	BandD    = &BandD[js * col1step + is * row1step];
	in0_TS   = BandD;
	in1_TS   = &BandD[col1step];

	mex0_TS = (uchar *) Image_mex2->data;
	memset(mex0_TS, 0, sizeof(uchar) * (Image_mex2->col1step + 2));
	mex0_TS = &mex0_TS[1];
	mex1_TS = &mex0_TS[col1step + 4];
	memset(mex1_TS, 0, sizeof(uchar) * (Image_mex2->col1step + 2));
	mex1_TS = &mex1_TS[1];

	mel_D = new byte4[1024];
	memset(mel_D, 0, sizeof(byte4) * 1024);

	mex0_p = mex0_TS;
	mex1_p = mex1_TS;
	in0_p  = in0_TS;
	in1_p  = in1_TS;
	jjj    = 0;
	Y_Line = 0;
	e_nw   = 0;
	e0     = 0;
	e1     = 0;
	e2     = 0;
	e3     = 0;
	nRho   = 0;
	for (iii = 0; iii < width;
	     iii += 4, mex0_p = &mex0_p[4], mex1_p = &mex1_p[4], in0_p = &in0_p[4], in1_p = &in1_p[4]) {
		/*Quad even side*/
		Kenw = e_nw;
		Ken  = mex1_p[0];
		Kene = mex1_p[1];
		Kenf = mex1_p[2];
		CX0 = CX = (uchar) (((e0 | e1) ? 1 : 0) + (e2 ? 2 : 0) + (e3 ? 4 : 0));
		CX1      = (uchar) (((e_nw | mex1_p[0]) ? 1 : 0) + ((mex1_p[1] | mex1_p[2]) ? 4 : 0)
                       + ((e2 | e3) ? 2 : 0)); // e2,e3 are "0" when left boundary. And e2,e3 are came from
		                                       // Quad odd side caluculation.
		kappa = (e_nw > Ken ? e_nw : Ken);
		kappa = (kappa > Kene ? kappa : Kene);
		kappa = (kappa > Kenf ? kappa : Kenf);
		kappa -= 1;
		kappa = (char) (kappa > 1 ? kappa : 1);
		e0    = Bittop(30, 0, (in0_p[0] & 0x7fffffff));
		e1    = Bittop(30, 0, (in1_p[0] & 0x7fffffff));
		e2    = Bittop(30, 0, (in0_p[1] & 0x7fffffff));
		e3    = Bittop(30, 0, (in1_p[1] & 0x7fffffff));
		rho   = (uchar) ((e0 ? 1 : 0) + (e1 ? 2 : 0) + (e2 ? 4 : 0) + (e3 ? 8 : 0));
		gamma = (char) (((rho & (rho - 1)) == 0) ? 0 : 1);
		if (!gamma) kappa = 1;
		e_nw      = mex1_p[1];
		mex0_p[0] = e0;
		mex1_p[0] = e1;
		mex0_p[1] = e2;
		mex1_p[1] = e3;
#if 0
		fprintf( fp_e1e3_out,"%x,", ( (e1<<5) + e3 ) );
		fprintf( fp_e1e3_in, "0," );
#endif
		Uq0        = (e0 > e1) ? e0 : e1;
		Uq0        = (Uq0 > e2) ? Uq0 : e2;
		Uq0        = (Uq0 > e3) ? Uq0 : e3;
		Uq00       = (char) (Uq0 ? 1 : 0);
		EmbPattern = (uchar) ((((e0 == Uq0) ? 1 : 0) + ((e1 == Uq0) ? 2 : 0) + ((e2 == Uq0) ? 4 : 0)
		                       + ((e3 == Uq0) ? 8 : 0))
		                      * Uq00);
		qmex0      = (uchar) ((Uq0 < kappa ? 0 : Uq0 - kappa) * Uq00);

		qinf1 = (ubyte2) ((CX << 8) + (rho << 4) + EmbPattern);
		qinf0 = (ubyte2) ((CX << 4) + rho);
		CwD1  = Enc_uff1_Table0[qinf1];
		CwD0  = Enc_uff0_Table0[qinf0];

		u_off0 = (uchar) (qmex0 ? 1 : 0);

		Uq = (uchar) (qmex0 + kappa);
		if (CX == 0) {
			if (rho == 0)
				mel_D[codecM->mel_count]++;
			else
				codecM->mel_count++;
		}
		MelRun = (uchar) ((CX ? 0 : 2) + (rho ? 0 : 1));

		if (u_off0) {
			CwD      = CwD1;
			epcironK = (uchar) ((CwD >> 12) & 0xf);
		} else {
			CwD      = CwD0;
			epcironK = 0;
		}
		CwL = (uchar) (CwD & 0xf);
		CwD = (CwD >> 4) & 0xff;
		if (nullptr == (str_rraw = Write_nBits(str_rraw, codecR, CwD, CwL))) {
			printf("[encode_cln]:: Write_nBits error\n");
		}

		msbits2_0 = (uchar) (Uq * ((rho & 1) ? 1 : 0) - (epcironK & 1));
		msbits2_1 = (uchar) (Uq * ((rho & 2) ? 1 : 0) - ((epcironK & 2) >> 1));
		msbits2_2 = (uchar) (Uq * ((rho & 4) ? 1 : 0) - ((epcironK & 4) >> 2));
		msbits2_3 = (uchar) (Uq * ((rho & 8) ? 1 : 0) - ((epcironK & 8) >> 3));
		if (rho & 1) {
			str_fraw = Enc_MagSgn(J2kParam, str_fraw, codecf, msbits2_0, in0_p[0]);
			frawD0   = J2kParam->frawD;
			frawL0   = J2kParam->frawL;
		} else {
			frawD0 = 0;
			frawL0 = 0;
		}
		if (rho & 2) {
			str_fraw = Enc_MagSgn(J2kParam, str_fraw, codecf, msbits2_1, in1_p[0]);
			frawD1   = J2kParam->frawD;
			frawL1   = J2kParam->frawL;
		} else {
			frawD1 = 0;
			frawL1 = 0;
		}
		if (rho & 4) {
			str_fraw = Enc_MagSgn(J2kParam, str_fraw, codecf, msbits2_2, in0_p[1]);
			frawD2   = J2kParam->frawD;
			frawL2   = J2kParam->frawL;
		} else {
			frawD2 = 0;
			frawL2 = 0;
		}
		if (rho & 8) {
			str_fraw = Enc_MagSgn(J2kParam, str_fraw, codecf, msbits2_3, in1_p[1]);
			frawD3   = J2kParam->frawD;
			frawL3   = J2kParam->frawL;
		} else {
			frawD3 = 0;
			frawL3 = 0;
		}

		nRho = rho;
		/*Quad odd side*/
		Kenw = e_nw;
		Ken  = mex1_p[2];
		Kene = mex1_p[3];
		Kenf = mex1_p[4];
		CX0 = CX = (uchar) (((e0 | e1) ? 1 : 0) + (e2 ? 2 : 0) + (e3 ? 4 : 0));
		CX1      = (uchar) (((e_nw | mex1_p[2]) ? 1 : 0) + ((mex1_p[3] | mex1_p[4]) ? 4 : 0)
                       + ((e2 | e3) ? 2 : 0));
		kappa    = (e_nw > Ken ? e_nw : Ken);
		kappa    = (kappa > Kene ? kappa : Kene);
		kappa    = (kappa > Kenf ? kappa : Kenf);
		kappa -= 1;
		kappa = (char) (kappa > 1 ? kappa : 1);
		e0    = Bittop(30, 0, (in0_p[2] & 0x7fffffff));
		e1    = Bittop(30, 0, (in1_p[2] & 0x7fffffff));
		e2    = Bittop(30, 0, (in0_p[3] & 0x7fffffff));
		e3    = Bittop(30, 0, (in1_p[3] & 0x7fffffff));
		rho   = (uchar) ((e0 ? 1 : 0) + (e1 ? 2 : 0) + (e2 ? 4 : 0) + (e3 ? 8 : 0));
		gamma = (char) (((rho & (rho - 1)) == 0) ? 0 : 1);
		if (!gamma) kappa = 1;
		e_nw      = mex1_p[3];
		mex0_p[2] = e0;
		mex1_p[2] = e1;
		mex0_p[3] = e2;
		mex1_p[3] = e3;
#if 0
		fprintf( fp_e1e3_out,"%x,", ( (e1<<5) + e3 ) );
		fprintf( fp_e1e3_in, "0," );
#endif

		Uq0        = (e0 > e1) ? e0 : e1;
		Uq0        = (Uq0 > e2) ? Uq0 : e2;
		Uq0        = (Uq0 > e3) ? Uq0 : e3;
		Uq00       = (char) (Uq0 ? 1 : 0);
		EmbPattern = (uchar) ((((e0 == Uq0) ? 1 : 0) + ((e1 == Uq0) ? 2 : 0) + ((e2 == Uq0) ? 4 : 0)
		                       + ((e3 == Uq0) ? 8 : 0))
		                      * Uq00);
		qmex1      = (uchar) ((Uq0 < kappa ? 0 : Uq0 - kappa) * Uq00);

		qinf1 = (ubyte2) ((CX << 8) + (rho << 4) + EmbPattern);
		qinf0 = (ubyte2) ((CX << 4) + rho);
		CwD1  = Enc_uff1_Table0[qinf1];
		CwD0  = Enc_uff0_Table0[qinf0];

		u_off1 = (uchar) (qmex1 ? 2 : 0);

		Uq = (uchar) (qmex1 + kappa);
		if (CX == 0) {
			if (rho == 0)
				mel_D[codecM->mel_count]++;
			else
				codecM->mel_count++;
		}
		MelRun = (uchar) ((CX ? 0 : 2) + (rho ? 0 : 1));

		if (u_off1) {
			CwD      = CwD1;
			epcironK = (uchar) ((CwD >> 12) & 0xf);
		} else {
			CwD      = CwD0;
			epcironK = 0;
		}
		CwL = (uchar) (CwD & 0xf);
		CwD = (CwD >> 4) & 0xff;
		if (nullptr == (str_rraw = Write_nBits(str_rraw, codecR, CwD, CwL))) {
			printf("[encode_cln]:: Write_nBits error\n");
		}

		msbits2_0 = (uchar) (Uq * ((rho & 1) ? 1 : 0) - (epcironK & 1));
		msbits2_1 = (uchar) (Uq * ((rho & 2) ? 1 : 0) - ((epcironK & 2) >> 1));
		msbits2_2 = (uchar) (Uq * ((rho & 4) ? 1 : 0) - ((epcironK & 4) >> 2));
		msbits2_3 = (uchar) (Uq * ((rho & 8) ? 1 : 0) - ((epcironK & 8) >> 3));
		if (rho & 1) {
			str_fraw = Enc_MagSgn(J2kParam, str_fraw, codecf, msbits2_0, in0_p[2]);
			frawD0   = J2kParam->frawD;
			frawL0   = J2kParam->frawL;
		} else {
			frawD0 = 0;
			frawL0 = 0;
		}
		if (rho & 2) {
			str_fraw = Enc_MagSgn(J2kParam, str_fraw, codecf, msbits2_1, in1_p[2]);
			frawD1   = J2kParam->frawD;
			frawL1   = J2kParam->frawL;
		} else {
			frawD1 = 0;
			frawL1 = 0;
		}
		if (rho & 4) {
			str_fraw = Enc_MagSgn(J2kParam, str_fraw, codecf, msbits2_2, in0_p[3]);
			frawD2   = J2kParam->frawD;
			frawL2   = J2kParam->frawL;
		} else {
			frawD2 = 0;
			frawL2 = 0;
		}
		if (rho & 8) {
			str_fraw = Enc_MagSgn(J2kParam, str_fraw, codecf, msbits2_3, in1_p[3]);
			frawD3   = J2kParam->frawD;
			frawL3   = J2kParam->frawL;
		} else {
			frawD3 = 0;
			frawL3 = 0;
		}

		u_off = (uchar) (u_off1 + u_off0);
		if ((qmex0 > 0) && (qmex1 > 0)) {
			melFlag = (uchar) ((((qmex0 > FBC_U0_VAL_I) && (qmex1 > FBC_U0_VAL_I)) ? 1 : 0));
			if (melFlag) {
				codecM->mel_count++;
				u_off++;
			} else
				mel_D[codecM->mel_count]++;
		}

		str_rraw = Enc_U(str_rraw, codecR, u_off, qmex0, qmex1, jjj);
		nRho     = rho;
	}
#if 0
	fprintf( fp_e1e3_in,  "\n" );
	fprintf( fp_e1e3_out, "\n" );
#endif

	Y_Line = 1;
	in0_TS += col2step;
	in1_TS += col2step;
	for (jjj = 2; jjj < height; jjj += 2, in0_TS += col2step, in1_TS += col2step) {
		mex0_p = mex0_TS;
		mex1_p = mex1_TS;
		in0_p  = in0_TS;
		in1_p  = in1_TS;
		e_nw   = 0;
		e0     = 0;
		e1     = 0;
		e2     = 0;
		e3     = 0;
		nRho   = 0;
		for (iii = 0; iii < width;
		     iii += 4, mex0_p = &mex0_p[4], mex1_p = &mex1_p[4], in0_p = &in0_p[4], in1_p = &in1_p[4]) {
			/*Quad even side*/
			Kenw = e_nw;
			Ken  = mex1_p[0];
			Kene = mex1_p[1];
			Kenf = mex1_p[2];
			CX0  = (uchar) (((e0 | e1) ? 1 : 0) + (e2 ? 2 : 0) + (e3 ? 4 : 0));
			CX = CX1 = (uchar) (((e_nw | mex1_p[0]) ? 1 : 0) + ((mex1_p[1] | mex1_p[2]) ? 4 : 0)
			                    + ((e2 | e3) ? 2 : 0)); // e2,e3 are "0" when left boundary. And e2,e3 are
			                                            // came from Quad odd side caluculation.
			kappa = (e_nw > mex1_p[0] ? e_nw : mex1_p[0]);
			kappa = (kappa > mex1_p[1] ? kappa : mex1_p[1]);
			kappa = (kappa > mex1_p[2] ? kappa : mex1_p[2]);
			kappa -= 1;
			kappa = (char) (kappa > 1 ? kappa : 1);
			e0    = Bittop(30, 0, (in0_p[0] & 0x7fffffff));
			e1    = Bittop(30, 0, (in1_p[0] & 0x7fffffff));
			e2    = Bittop(30, 0, (in0_p[1] & 0x7fffffff));
			e3    = Bittop(30, 0, (in1_p[1] & 0x7fffffff));
			rho   = (uchar) ((e0 ? 1 : 0) + (e1 ? 2 : 0) + (e2 ? 4 : 0) + (e3 ? 8 : 0));
			gamma = (char) (((rho & (rho - 1)) == 0) ? 0 : 1);
			if (!gamma) kappa = 1;
			e_nw      = mex1_p[1];
			mex0_p[0] = e0;
			mex1_p[0] = e1;
			mex0_p[1] = e2;
			mex1_p[1] = e3;
#if 0
			fprintf( fp_e1e3_out,"%x,", ( (e1<<5) + e3 ) );
			fprintf( fp_e1e3_in, "%x,", ( (Ken<<5)+ Kene));
#endif

			Uq0        = (e0 > e1) ? e0 : e1;
			Uq0        = (Uq0 > e2) ? Uq0 : e2;
			Uq0        = (Uq0 > e3) ? Uq0 : e3;
			Uq00       = (char) (Uq0 ? 1 : 0);
			EmbPattern = (uchar) ((((e0 == Uq0) ? 1 : 0) + ((e1 == Uq0) ? 2 : 0) + ((e2 == Uq0) ? 4 : 0)
			                       + ((e3 == Uq0) ? 8 : 0))
			                      * Uq00);
			qmex0      = (uchar) ((Uq0 < kappa ? 0 : Uq0 - kappa) * Uq00);

			qinf1 = (ubyte2) ((CX << 8) + (rho << 4) + EmbPattern);
			qinf0 = (ubyte2) ((CX << 4) + rho);
			CwD1  = Enc_uff1_Table1[qinf1];
			CwD0  = Enc_uff0_Table1[qinf0];

			u_off0 = (uchar) (qmex0 ? 1 : 0);

			Uq = (uchar) (qmex0 + kappa);
			if (CX == 0) {
				if (rho == 0)
					mel_D[codecM->mel_count]++;
				else
					codecM->mel_count++;
			}
			MelRun = (uchar) ((CX ? 0 : 2) + (rho ? 0 : 1));

			if (u_off0) {
				CwD      = CwD1;
				epcironK = (uchar) ((CwD >> 12) & 0xf);
			} else {
				CwD      = CwD0;
				epcironK = 0;
			}
			CwL      = (uchar) (CwD & 0xf);
			CwD      = (CwD >> 4) & 0xff;
			str_rraw = Write_nBits(str_rraw, codecR, CwD, CwL);

			msbits2_0 = (uchar) (Uq * ((rho & 1) ? 1 : 0) - ((epcironK & 1)));
			msbits2_1 = (uchar) (Uq * ((rho & 2) ? 1 : 0) - ((epcironK & 2) >> 1));
			msbits2_2 = (uchar) (Uq * ((rho & 4) ? 1 : 0) - ((epcironK & 4) >> 2));
			msbits2_3 = (uchar) (Uq * ((rho & 8) ? 1 : 0) - ((epcironK & 8) >> 3));
			if (rho & 1) {
				str_fraw = Enc_MagSgn(J2kParam, str_fraw, codecf, msbits2_0, in0_p[0]);
				frawD0   = J2kParam->frawD;
				frawL0   = J2kParam->frawL;
			} else {
				frawD0 = 0;
				frawL0 = 0;
			}
			if (rho & 2) {
				str_fraw = Enc_MagSgn(J2kParam, str_fraw, codecf, msbits2_1, in1_p[0]);
				frawD1   = J2kParam->frawD;
				frawL1   = J2kParam->frawL;
			} else {
				frawD1 = 0;
				frawL1 = 0;
			}
			if (rho & 4) {
				str_fraw = Enc_MagSgn(J2kParam, str_fraw, codecf, msbits2_2, in0_p[1]);
				frawD2   = J2kParam->frawD;
				frawL2   = J2kParam->frawL;
			} else {
				frawD2 = 0;
				frawL2 = 0;
			}
			if (rho & 8) {
				str_fraw = Enc_MagSgn(J2kParam, str_fraw, codecf, msbits2_3, in1_p[1]);
				frawD3   = J2kParam->frawD;
				frawL3   = J2kParam->frawL;
			} else {
				frawD3 = 0;
				frawL3 = 0;
			}
			nRho = rho;

			/*Quad odd side*/
			Kenw = e_nw;
			Ken  = mex1_p[2];
			Kene = mex1_p[3];
			Kenf = mex1_p[4];
			CX0  = (uchar) (((e0 | e1) ? 1 : 0) + (e2 ? 2 : 0) + (e3 ? 4 : 0));
			CX = CX1 = (uchar) (((e_nw | mex1_p[2]) ? 1 : 0) + ((mex1_p[3] | mex1_p[4]) ? 4 : 0)
			                    + ((e2 | e3) ? 2 : 0));
			kappa    = (e_nw > mex1_p[2] ? e_nw : mex1_p[2]);
			kappa    = (kappa > mex1_p[3] ? kappa : mex1_p[3]);
			kappa    = (kappa > mex1_p[4] ? kappa : mex1_p[4]);
			kappa -= 1;
			kappa = (char) (kappa > 1 ? kappa : 1);
			e0    = Bittop(30, 0, (in0_p[2] & 0x7fffffff));
			e1    = Bittop(30, 0, (in1_p[2] & 0x7fffffff));
			e2    = Bittop(30, 0, (in0_p[3] & 0x7fffffff));
			e3    = Bittop(30, 0, (in1_p[3] & 0x7fffffff));
#if 0
			fprintf( fp_e1e3_out,"%x,", ( (e1<<5) + e3 ) );
			fprintf( fp_e1e3_in, "%x,", ( (Ken<<5)+ Kene));
#endif

			rho   = (uchar) ((e0 ? 1 : 0) + (e1 ? 2 : 0) + (e2 ? 4 : 0) + (e3 ? 8 : 0));
			gamma = (char) (((rho & (rho - 1)) == 0) ? 0 : 1);
			if (!gamma) kappa = 1;
			e_nw      = mex1_p[3];
			mex0_p[2] = e0;
			mex1_p[2] = e1;
			mex0_p[3] = e2;
			mex1_p[3] = e3;

			Uq0        = (e0 > e1) ? e0 : e1;
			Uq0        = (Uq0 > e2) ? Uq0 : e2;
			Uq0        = (Uq0 > e3) ? Uq0 : e3;
			Uq00       = (char) (Uq0 ? 1 : 0);
			EmbPattern = (uchar) ((((e0 == Uq0) ? 1 : 0) + ((e1 == Uq0) ? 2 : 0) + ((e2 == Uq0) ? 4 : 0)
			                       + ((e3 == Uq0) ? 8 : 0))
			                      * Uq00);
			qmex1      = (uchar) ((Uq0 < kappa ? 0 : Uq0 - kappa) * Uq00);

			qinf1 = (ubyte2) ((CX << 8) + (rho << 4) + EmbPattern);
			qinf0 = (ubyte2) ((CX << 4) + rho);
			CwD1  = Enc_uff1_Table1[qinf1];
			CwD0  = Enc_uff0_Table1[qinf0];

			u_off1 = (uchar) (qmex1 ? 2 : 0);

			Uq = (uchar) (qmex1 + kappa);
			if (CX == 0) {
				if (rho == 0)
					mel_D[codecM->mel_count]++;
				else
					codecM->mel_count++;
			}

			if (u_off1) {
				CwD      = CwD1;
				epcironK = (uchar) ((CwD >> 12) & 0xf);
			} else {
				CwD      = CwD0;
				epcironK = 0;
			}
			CwL      = (uchar) (CwD & 0xf);
			CwD      = (CwD >> 4) & 0xff;
			str_rraw = Write_nBits(str_rraw, codecR, CwD, CwL);

			msbits2_0 = (uchar) (Uq * ((rho & 1) ? 1 : 0) - (epcironK & 1));
			msbits2_1 = (uchar) (Uq * ((rho & 2) ? 1 : 0) - ((epcironK & 2) >> 1));
			msbits2_2 = (uchar) (Uq * ((rho & 4) ? 1 : 0) - ((epcironK & 4) >> 2));
			msbits2_3 = (uchar) (Uq * ((rho & 8) ? 1 : 0) - ((epcironK & 8) >> 3));
			if (rho & 1) {
				str_fraw = Enc_MagSgn(J2kParam, str_fraw, codecf, msbits2_0, in0_p[2]);
				frawD0   = J2kParam->frawD;
				frawL0   = J2kParam->frawL;
			} else {
				frawD0 = 0;
				frawL0 = 0;
			}
			if (rho & 2) {
				str_fraw = Enc_MagSgn(J2kParam, str_fraw, codecf, msbits2_1, in1_p[2]);
				frawD1   = J2kParam->frawD;
				frawL1   = J2kParam->frawL;
			} else {
				frawD1 = 0;
				frawL1 = 0;
			}
			if (rho & 4) {
				str_fraw = Enc_MagSgn(J2kParam, str_fraw, codecf, msbits2_2, in0_p[3]);
				frawD2   = J2kParam->frawD;
				frawL2   = J2kParam->frawL;
			} else {
				frawD2 = 0;
				frawL2 = 0;
			}
			if (rho & 8) {
				str_fraw = Enc_MagSgn(J2kParam, str_fraw, codecf, msbits2_3, in1_p[3]);
				frawD3   = J2kParam->frawD;
				frawL3   = J2kParam->frawL;
			} else {
				frawD3 = 0;
				frawL3 = 0;
			}
			u_off    = (uchar) (u_off1 + u_off0);
			str_rraw = Enc_U(str_rraw, codecR, u_off, qmex0, qmex1, jjj);
			nRho     = rho;
		}
#if 0
		fprintf( fp_e1e3_in,  "\n" );
		fprintf( fp_e1e3_out, "\n" );
#endif
	}
#if 0
	fprintf( fp_e1e3_in,  "\n" );
	fprintf( fp_e1e3_out, "\n" );
	fclose( fp_e1e3_in  );
	fclose( fp_e1e3_out );
#endif

	if (mel_D[codecM->mel_count]) codecM->mel_count++;

	for (jjj = 0; jjj < codecM->mel_count; jjj++)
		str_melc = EncMelCoder(str_melc, codecM, mel_D[jjj]);

	delete[] mel_D;

	return EXIT_SUCCESS;
}

byte4 decode_cln(
    struct Image_s *Band, struct Cblk_s *Cblk, struct Codec_s *Codec, struct Image_s *Image_qinf,
    struct Image_s *Image_qmex, struct Image_s *Image_msbits, struct Image_s *Image_mex,
    struct Image_s *Image_sigma, struct Image_s *Image_epcironK, struct Image_s *Image_epciron1,
    /*bool refined,*/ struct RRawCodec_s *codecR, struct MelCodec_s *codecM, struct fRawCodec_s *codecf,
    struct StreamChain_s *str_rraw, struct StreamChain_s *str_melc,
    struct StreamChain_s *str_fraw /*, byte4 T, ubyte2 ccc, uchar rrr, byte4 ppp, uchar bbb, byte4 kkk*/) {
	byte4 iii, jjj, i;
	byte4 mel_run, val2;
	uchar inf_p2, inf_c2, inf_n2;
	uchar rho;
	uchar e_nw, e_n, e_ne, e_nf;
	uchar ms2;
	byte4 myu, myu_;
	ubyte4 implicit_2, smag2, pattern;
	uchar u_plus, epciron1;
	uchar smag_bits2;
	byte2 p_max;
	uchar qinf_val;
	byte4 width, height;
	uchar CX, CX_0;
	byte4 CodeStream_CX;
	byte4 height_;
	char Sel;
	uchar temp2; //= Cblk->Mb+8;
	ubyte4 *sp_p, *sp_TS, *sp_0TS;
	uchar *qinf_p, *qinf_c, *qinf_cTS, *qinf_pTS;
	uchar *rho_TS, *rho_p;
	uchar *qmex_p, *qmex_TS;
	uchar *epcironK_p, *epcironK_TS;
	uchar *epciron1_p, *epciron1_TS;
	uchar *msbits_p, *msbits_TS;
	uchar *mex_p, *mex_c, *mex_pTS, *mex_cTS;
	uchar eK, e1, uoff, uoff0, uoff1;
	ubyte2 *sigma_p, *sigma_TS;
	byte4 col1step_sp, col2step_sp, row1step_sp;
	byte4 col1step_qinf;
	byte4 col1step_qmex, col1step_epcironK, col1step_epciron1, col1step_rho, col2step_rho;
	byte4 col1step_msbits;
	byte4 col1step_mex;
	byte4 col1step_sigma, col2step_sigma;
	uchar *Cx_D, *Cx_TS;
	struct Image_s *Image_msbits2;
	uchar *msbits2_p, *msbits2_TS;
	byte4 col1step_msbits2;
	uchar kappa, gamma, Uq;

	temp2   = (uchar) (Cblk->Mb + 8);
	width   = Cblk->tcbx1 - Cblk->tcbx0;
	height_ = height = Cblk->tcby1 - Cblk->tcby0;

	Cx_D = new uchar[width + 1];
	memset(Cx_D, 0, sizeof(uchar) * (width + 1));
	Cx_TS = Cx_D;

	col1step_sp = Band->col1step;
	col2step_sp = col1step_sp * 2;
	row1step_sp = Band->row1step;
	sp_0TS      = (ubyte4 *) Band->data;
	sp_0TS = &sp_0TS[(Cblk->tcby0 - Band->tby0) * col1step_sp + (Cblk->tcbx0 - Band->tbx0) * row1step_sp];
	sp_TS  = sp_0TS;
	sp_p   = sp_TS;

	memset(Image_qinf->Pdata, 0, sizeof(uchar) * Image_qinf->numData);
	col1step_qinf = Image_qinf->col1step;
	qinf_cTS      = (uchar *) Image_qinf->data;
	qinf_c        = qinf_cTS;

	memset(Codec->Image_rho->Pdata, 0, sizeof(uchar) * Codec->Image_rho->numData);
	col1step_rho = Codec->Image_rho->col1step;
	col2step_rho = col1step_rho * 2;

	memset(Image_qmex->Pdata, 0, sizeof(uchar) * Image_qmex->numData);
	qmex_TS       = (uchar *) Image_qmex->data;
	qmex_p        = qmex_TS;
	col1step_qmex = Image_qmex->col1step;

	memset(Image_epcironK->Pdata, 0, sizeof(uchar) * Image_epcironK->numData);
	epcironK_TS       = (uchar *) Image_epcironK->data;
	epcironK_p        = epcironK_TS;
	col1step_epcironK = Image_epcironK->col1step;

	memset(Image_epciron1->Pdata, 0, sizeof(uchar) * Image_epciron1->numData);
	epciron1_TS       = (uchar *) Image_epciron1->data;
	epciron1_p        = epciron1_TS;
	col1step_epciron1 = Image_epciron1->col1step;

	memset(Image_msbits->Pdata, 0, sizeof(uchar) * Image_msbits->numData);
	col1step_msbits = Image_msbits->col1step;
	msbits_TS       = (uchar *) Image_msbits->data;
	msbits_p        = msbits_TS;

	memset(Image_mex->Pdata, 0, sizeof(uchar) * Image_mex->numData);
	col1step_mex = Image_mex->col1step;
	mex_pTS      = (uchar *) Image_mex->data;
	mex_cTS      = &mex_pTS[col1step_mex];
	mex_p        = mex_pTS;
	mex_c        = mex_cTS;

	memset(Image_sigma->Pdata, 0, sizeof(byte2) * Image_sigma->numData);
	col1step_sigma = Image_sigma->col1step;
	col2step_sigma = 2 * col1step_sigma;
	sigma_TS       = (ubyte2 *) Image_sigma->data;
	sigma_p        = sigma_TS;

	Image_msbits2 =
	    ImageCreate(nullptr, (width * 2), ((height + 1) / 2), 0, (width * 2), 0, ((height + 1) / 2), CHAR);

	col1step_msbits2 = Image_msbits2->col1step;
	msbits2_TS       = (uchar *) Image_msbits2->data;
	msbits2_p        = msbits_TS;

#if ICT_Link08
	FILE *fp_myu, *fp_msbits, *fp_E_mex, *fp_Uq_kappa, *fp_u, *fp_Cx, *fp_uoff, *fp_rho, *fp_sp, *fp_kappa,
	    *fp_epciron1, *fp_epcironK, *fp_rraw, *fp_melc, *fp_fraw, *fp_output;
	fp_myu      = fopen("dec_myu.csv", "a+");
	fp_msbits   = fopen("dec_msbits.csv", "a+");
	fp_E_mex    = fopen("dec_E_mex.csv", "a+");
	fp_Uq_kappa = fopen("dec_Uq_kappa.csv", "a+");
	fp_u        = fopen("dec_u_qmex.csv", "a+");
	fp_Cx       = fopen("dec_Cx.csv", "a+");
	fp_uoff     = fopen("dec_uoff.csv", "a+");
	fp_rho      = fopen("dec_rho.csv", "a+");
	fp_sp       = fopen("dec_sp.csv", "a+");
	fp_kappa    = fopen("dec_kappa.csv", "a+");
	fp_epcironK = fopen("dec_epcironK.csv", "a+");
	fp_epciron1 = fopen("dec_epciron1.csv", "a+");
	fp_rraw     = fopen("dec_rraw.csv", "a+");
	fp_fraw     = fopen("dec_fraw.csv", "a+");
#endif

	codecM->mel_state = FBC_AZC_INITIAL_MEL_STATE_I;
	if (-1 == (mel_run = DecMelCoder(str_melc, codecM))) {
		printf("[decode_cln]:: DecMelCoder Error.\n");
		return EXIT_FAILURE;
	}
	codecR->qinf_val = 0;

	qmex_p     = qmex_TS;
	qinf_c     = qinf_cTS;
	epciron1_p = epciron1_TS;
	epcironK_p = epcironK_TS;
	CX         = 0;
	jjj        = 0;
	for (iii = 0; iii < width; iii += 4, qinf_c = &qinf_c[2], qmex_p = &qmex_p[2],
	    epciron1_p = &epciron1_p[2], epcironK_p = &epcironK_p[2]) {
#if ICT_Link08
		fprintf(fp_Cx, "%d,", CX);
		CX_0 = CX;
#endif
		codecR->CwD   = (uchar) (codecR->Creg & 0x7f);
		CodeStream_CX = (byte4) ((CX << 7) + codecR->CwD);
		if (CX == 0) {
			mel_run--;
			if (mel_run >= 0) {
				codecR->CwL = 0;
				uoff0 = codecR->u_off = (uchar) 0;
				rho = qinf_c[0] = 0;
				eK = epcironK_p[0] = 0;
				e1 = epciron1_p[0] = 0;
				CX = (uchar) (((qinf_c[0] & 1) | ((qinf_c[0] & 2) >> 1)) + ((qinf_c[0] & 4) ? 2 : 0)
				              + ((qinf_c[0] & 8) >> 1));
			} else {
				if (-1 == (mel_run = DecMelCoder(str_melc, codecM))) {
					printf("[decode_cln]:: DecMelCoder Error.\n");
					return EXIT_FAILURE;
				}
				codecR->CwL = dec_vlc_table_0[CodeStream_CX][4];
				uoff0 = codecR->u_off = (uchar) dec_vlc_table_0[CodeStream_CX][0];
				rho = qinf_c[0] = dec_vlc_table_0[CodeStream_CX][3];
				eK = epcironK_p[0] = dec_vlc_table_0[CodeStream_CX][1];
				e1 = epciron1_p[0] = dec_vlc_table_0[CodeStream_CX][2];
				CX = (uchar) (((qinf_c[0] & 1) | ((qinf_c[0] & 2) >> 1)) + ((qinf_c[0] & 4) ? 2 : 0)
				              + ((qinf_c[0] & 8) >> 1));
			}
		} else {
			codecR->CwL = dec_vlc_table_0[CodeStream_CX][4];
			uoff0 = codecR->u_off = (uchar) dec_vlc_table_0[CodeStream_CX][0];
			rho = qinf_c[0] = dec_vlc_table_0[CodeStream_CX][3];
			eK = epcironK_p[0] = dec_vlc_table_0[CodeStream_CX][1];
			e1 = epciron1_p[0] = dec_vlc_table_0[CodeStream_CX][2];
			CX = (uchar) (((qinf_c[0] & 1) | ((qinf_c[0] & 2) >> 1)) + ((qinf_c[0] & 4) ? 2 : 0)
			              + ((qinf_c[0] & 8) >> 1));
		}
		Load_nBits(str_rraw, codecR, codecR->CwL);
#if ICT_Link08
		fprintf(fp_rho, "%d,", rho);
		fprintf(fp_Cx, "%d,", CX);
		fprintf(fp_uoff, "%d,", uoff0);
		fprintf(fp_epcironK, "%d,", eK);
		fprintf(fp_epciron1, "%d,", e1);
		fprintf(fp_rraw, "%d, %d, %d, %d, %d, %d, %d, %d, %d\n", jjj, iii, CX_0, codecR->CwL,
		        (codecR->CwD & mask6[codecR->CwL]), rho, uoff0, eK, e1);
#endif
		if ((iii + 2) < width) {
			CX_0          = CX;
			codecR->CwD   = (uchar) (codecR->Creg & 0x7f);
			CodeStream_CX = (byte4) ((CX << 7) + codecR->CwD);

			if (CX == 0) {
				mel_run--;
				if (mel_run >= 0) {
					codecR->CwL   = 0;
					uoff1         = 0;
					codecR->u_off = (uchar) (codecR->u_off + uoff1);
					rho = qinf_c[1] = 0;
					eK = epcironK_p[1] = 0;
					e1 = epciron1_p[1] = 0;
					CX = (uchar) (((qinf_c[1] & 1) | ((qinf_c[1] & 2) >> 1)) + ((qinf_c[1] & 4) ? 2 : 0)
					              + ((qinf_c[1] & 8) >> 1));
				} else {
					if (-1 == (mel_run = DecMelCoder(str_melc, codecM))) {
						printf("[decode_cln]:: DecMelCoder Error.\n");
						return EXIT_FAILURE;
					}
					codecR->CwL   = dec_vlc_table_0[CodeStream_CX][4];
					uoff1         = (uchar) ((dec_vlc_table_0[CodeStream_CX][0] << 1));
					codecR->u_off = (uchar) (codecR->u_off + (dec_vlc_table_0[CodeStream_CX][0] << 1));
					rho = qinf_c[1] = dec_vlc_table_0[CodeStream_CX][3];
					eK = epcironK_p[1] = dec_vlc_table_0[CodeStream_CX][1];
					e1 = epciron1_p[1] = dec_vlc_table_0[CodeStream_CX][2];
					CX = (uchar) (((qinf_c[1] & 1) | ((qinf_c[1] & 2) >> 1)) + ((qinf_c[1] & 4) ? 2 : 0)
					              + ((qinf_c[1] & 8) >> 1));
				}
			} else {
				codecR->CwL   = dec_vlc_table_0[CodeStream_CX][4];
				uoff1         = (uchar) (dec_vlc_table_0[CodeStream_CX][0] << 1);
				codecR->u_off = (uchar) (codecR->u_off + (dec_vlc_table_0[CodeStream_CX][0] << 1));
				rho = qinf_c[1] = dec_vlc_table_0[CodeStream_CX][3];
				eK = epcironK_p[1] = dec_vlc_table_0[CodeStream_CX][1];
				e1 = epciron1_p[1] = dec_vlc_table_0[CodeStream_CX][2];
				CX = (uchar) (((qinf_c[1] & 1) | ((qinf_c[1] & 2) >> 1)) + ((qinf_c[1] & 4) ? 2 : 0)
				              + ((qinf_c[1] & 8) >> 1));
			}
			Load_nBits(str_rraw, codecR, codecR->CwL);
#if ICT_Link08
			fprintf(fp_rho, "%d,", rho);
			fprintf(fp_uoff, "%d,", uoff1);
			fprintf(fp_epcironK, "%d,", eK);
			fprintf(fp_epciron1, "%d,", e1);
			fprintf(fp_rraw, "%d, %d, %d, %d, %d, %d, %d, %d, %d\n", jjj, (iii + 2), CX_0, codecR->CwL,
			        (codecR->CwD & mask6[codecR->CwL]), rho, uoff1, eK, e1);
#endif
			uoff = (uchar) (uoff0 + uoff1);
			if ((mel_run -= ((codecR->u_off + 1) >> 2)) < 0) {
				codecR->u_off += 1;
				uoff += 1;
				if (-1 == (mel_run = DecMelCoder(str_melc, codecM))) {
					printf("[decode_cln]:: DecMelCoder Error.\n");
					return EXIT_FAILURE;
				}
			}
#if ICT_Link08
			fprintf(fp_uoff, "%d,", codecR->u_off);
#endif
		}
		Dec_U(str_rraw, codecR, qmex_p, /*0,*/ jjj);
#if ICT_Link08
		fprintf(fp_u, "u_off/qmex, %d, %d, %d, %d\n", jjj, codecR->u_off, qmex_p[0], qmex_p[1]);
		fprintf(fp_rraw, "%d, %d, %d, %d, %d\n", jjj, (iii + 2), uoff, qmex_p[0], qmex_p[1]);
#endif
	}
#if ICT_Link08
	fprintf(fp_rho, "\n");
	fprintf(fp_Cx, "\n");
	fprintf(fp_uoff, "\n");
	fprintf(fp_epcironK, "\n");
	fprintf(fp_epciron1, "\n");
#endif
	qinf_pTS = qinf_cTS;
	qinf_cTS += col1step_qinf;
	qmex_TS += col1step_qmex;
	epciron1_TS += col1step_epciron1;
	epcironK_TS += col1step_epcironK;
	Sel = 1;
	for (jjj = 2; jjj < height; jjj += 2, qinf_pTS += col1step_qinf, qinf_cTS += col1step_qinf,
	    qmex_TS += col1step_qmex, epciron1_TS += col1step_epciron1, epcironK_TS += col1step_epcironK) {
		qinf_p = qinf_pTS;
		inf_p2 = 0;
		inf_c2 = qinf_p[0];
		Cx_D   = Cx_TS;
		for (iii = 0; iii < width; iii += 2, qinf_p++, Cx_D++) {
			inf_n2  = qinf_p[1];
			(*Cx_D) = (uchar) (((((inf_p2 & 8) | (inf_c2 & 2)) ? 0x1 : 0)
			                    | (((inf_c2 & 8) | (inf_n2 & 2)) ? 0x4 : 0)));
			inf_p2  = inf_c2;
			inf_c2  = inf_n2;
		}

		qinf_c           = qinf_cTS;
		qmex_p           = qmex_TS;
		epciron1_p       = epciron1_TS;
		epcironK_p       = epcironK_TS;
		codecR->qinf_val = qinf_c[0];
		qinf_val         = qinf_c[0];
		Cx_D             = Cx_TS;
		for (iii = 0; iii < width; iii += 4, qinf_c = &qinf_c[2], Cx_D = &Cx_D[2], qmex_p = &qmex_p[2],
		    epciron1_p = &epciron1_p[2], epcironK_p = &epcironK_p[2]) {
			CX            = Cx_D[0];
			CX_0          = CX;
			codecR->CwD   = (byte4) (codecR->Creg & 0x7f);
			CodeStream_CX = (CX << 7) | codecR->CwD;
			if (CX == 0) {
				mel_run--;
				if (mel_run >= 0) {
					codecR->CwL = 0;
					uoff0 = codecR->u_off = (uchar) 0;
					rho = qinf_c[0] = 0;
					eK = epcironK_p[0] = 0;
					e1 = epciron1_p[0] = 0;
					Cx_D[1] |= ((qinf_c[0] & 0xc) ? 2 : 0);
				} else {
					if (-1 == (mel_run = DecMelCoder(str_melc, codecM))) {
						printf("[decode_cln]:: DecMelCoder Error.\n");
						return EXIT_FAILURE;
					}
					codecR->CwL = dec_vlc_table_1[CodeStream_CX][4];
					uoff0 = codecR->u_off = (uchar) dec_vlc_table_1[CodeStream_CX][0];
					rho = qinf_c[0] = dec_vlc_table_1[CodeStream_CX][3];
					eK = epcironK_p[0] = dec_vlc_table_1[CodeStream_CX][1];
					e1 = epciron1_p[0] = dec_vlc_table_1[CodeStream_CX][2];
					Cx_D[1] |= ((qinf_c[0] & 0xc) ? 2 : 0);
				}
			} else {
				codecR->CwL = dec_vlc_table_1[CodeStream_CX][4];
				uoff0 = codecR->u_off = (uchar) dec_vlc_table_1[CodeStream_CX][0];
				rho = qinf_c[0] = dec_vlc_table_1[CodeStream_CX][3];
				eK = epcironK_p[0] = dec_vlc_table_1[CodeStream_CX][1];
				e1 = epciron1_p[0] = dec_vlc_table_1[CodeStream_CX][2];
				Cx_D[1] |= ((qinf_c[0] & 0xc) ? 2 : 0);
			}
			Load_nBits(str_rraw, codecR, codecR->CwL);
#if ICT_Link08
			fprintf(fp_Cx, "%d,", CX);
			fprintf(fp_rho, "%d,", rho);
			fprintf(fp_uoff, "%d,", uoff0);
			fprintf(fp_epcironK, "%d,", epcironK_p[0]);
			fprintf(fp_epciron1, "%d,", epciron1_p[0]);
			fprintf(fp_rraw, "%d, %d, %d, %d, %d, %d, %d, %d, %d\n", jjj, iii, CX_0, codecR->CwL,
			        (codecR->CwD & mask6[codecR->CwL]), rho, uoff0, eK, e1);
#endif
			uoff1 = 0;
			if ((iii + 2) < width) {
				CX            = Cx_D[1];
				CX_0          = CX;
				codecR->CwD   = (byte4) (codecR->Creg & 0x7f);
				CodeStream_CX = (CX << 7) | codecR->CwD;
				if (CX == 0) {
					mel_run--;
					if (mel_run >= 0) {
						codecR->CwL = 0;
						uoff1       = 0;
						codecR->u_off += 0;
						rho = qinf_c[1] = 0;
						eK = epcironK_p[1] = 0;
						e1 = epciron1_p[1] = 0;
						Cx_D[2] |= ((qinf_c[1] & 0xc) ? 2 : 0);
					} else {
						if (-1 == (mel_run = DecMelCoder(str_melc, codecM))) {
							printf("[decode_cln]:: DecMelCoder Error.\n");
							return EXIT_FAILURE;
						}
						codecR->CwL   = dec_vlc_table_1[CodeStream_CX][4];
						uoff1         = (uchar) (dec_vlc_table_1[CodeStream_CX][0] << 1);
						codecR->u_off = (uchar) (codecR->u_off + (dec_vlc_table_1[CodeStream_CX][0] << 1));
						rho = qinf_c[1] = dec_vlc_table_1[CodeStream_CX][3];
						eK = epcironK_p[1] = dec_vlc_table_1[CodeStream_CX][1];
						e1 = epciron1_p[1] = dec_vlc_table_1[CodeStream_CX][2];
						Cx_D[2] |= ((qinf_c[1] & 0xc) ? 2 : 0);
					}
				} else {
					codecR->CwL   = dec_vlc_table_1[CodeStream_CX][4];
					uoff1         = (uchar) (dec_vlc_table_1[CodeStream_CX][0] << 1);
					codecR->u_off = (uchar) (codecR->u_off + (dec_vlc_table_1[CodeStream_CX][0] << 1));
					rho = qinf_c[1] = dec_vlc_table_1[CodeStream_CX][3];
					eK = epcironK_p[1] = dec_vlc_table_1[CodeStream_CX][1];
					e1 = epciron1_p[1] = dec_vlc_table_1[CodeStream_CX][2];
					Cx_D[2] |= ((qinf_c[1] & 0xc) ? 2 : 0);
				}
				Load_nBits(str_rraw, codecR, codecR->CwL);
#if ICT_Link08
				fprintf(fp_Cx, "%d,", CX);
				fprintf(fp_rho, "%d,", rho);
				fprintf(fp_uoff, "%d,", uoff1);
				fprintf(fp_epcironK, "%d,", epcironK_p[1]);
				fprintf(fp_epciron1, "%d,", epciron1_p[1]);
				fprintf(fp_rraw, "%d, %d, %d, %d, %d, %d, %d, %d, %d\n", jjj, (iii + 2), CX_0, codecR->CwL,
				        (codecR->CwD & mask6[codecR->CwL]), rho, uoff1, eK, e1);
#endif
			}
			uoff = (uchar) (uoff0 + uoff1);
			Dec_U(str_rraw, codecR, qmex_p, /*0,*/ jjj);
#if ICT_Link08
			fprintf(fp_u, "u_off/qmex, %d, %d, %d, %d\n", jjj, codecR->u_off, qmex_p[0], qmex_p[1]);
			fprintf(fp_rraw, "%d, %d, %d, %d, %d\n", jjj, (iii + 2), uoff, qmex_p[0], qmex_p[1]);
#endif
		}
#if ICT_Link08
		fprintf(fp_Cx, "\n");
		fprintf(fp_rho, "\n");
		fprintf(fp_uoff, "\n");
		fprintf(fp_epcironK, "\n");
		fprintf(fp_epciron1, "\n");
#endif
	}
#if ICT_Link08
	fprintf(fp_Cx, "\n\n");
	fprintf(fp_rho, "\n\n");
	fprintf(fp_uoff, "\n\n");
	fprintf(fp_epcironK, "\n\n");
	fprintf(fp_epciron1, "\n\n");
	fprintf(fp_u, "\n\n");
	fprintf(fp_rraw, "\n\n");
	fclose(fp_Cx);
	fclose(fp_rho);
	fclose(fp_uoff);
	fclose(fp_epcironK);
	fclose(fp_epciron1);
	fclose(fp_u);
	fclose(fp_rraw);
#endif

	qmex_TS     = (uchar *) Image_qmex->data;
	msbits_TS   = (uchar *) Image_msbits->data;
	msbits2_TS  = (uchar *) Image_msbits2->data;
	qinf_cTS    = (uchar *) Image_qinf->data;
	mex_cTS     = (uchar *) Image_mex->data;
	epcironK_TS = (uchar *) Image_epcironK->data;
	epciron1_TS = (uchar *) Image_epciron1->data;
	sp_TS       = sp_0TS;
	rho_TS      = (uchar *) Codec->Image_rho->data;
	rho_TS      = &rho_TS[col1step_rho];
	for (jjj     = 0; jjj < height; jjj += 2, sp_TS += col2step_sp, qmex_TS += col1step_qmex,
	    qinf_pTS = qinf_cTS, qinf_cTS += col1step_qinf, mex_pTS = mex_cTS, mex_cTS += col1step_mex,
	    msbits_TS += col1step_msbits, msbits2_TS += col1step_msbits2, epcironK_TS += col1step_epcironK,
	    epciron1_TS += col1step_epciron1, rho_TS += col2step_rho) {
		qmex_p     = qmex_TS;
		msbits_p   = msbits_TS;
		msbits2_p  = msbits2_TS;
		qinf_p     = qinf_pTS;
		qinf_c     = qinf_cTS;
		mex_p      = mex_pTS;
		mex_c      = mex_cTS;
		epcironK_p = epcironK_TS;
		epciron1_p = epciron1_TS;
		rho_p      = rho_TS;
		rho_p      = &rho_p[1];
		if (jjj == 0) {
			kappa = 1; // kappa is fixed "1" in first quad line.
			for (iii = 0; iii < width; iii += 2, qmex_p++, msbits_p++, msbits2_p = &msbits2_p[4], qinf_c++,
			    mex_c += 4, epcironK_p++, epciron1_p++, rho_p                    = &rho_p[2]) {
#if ICT_Link08
				fprintf(fp_kappa, "%d,%d,0,0,0,0,%d\n", jjj, iii, kappa);
#endif

				Uq = msbits_p[0]        = (uchar) (qmex_p[0] + kappa); // Uq
				rho                     = (uchar) (qinf_c[0] & 0xf);
				msbits2_p[0]            = (uchar) (Uq * ((rho & 1) ? 1 : 0) - (epcironK_p[0] & 1)); // m
				msbits2_p[1]            = (uchar) (Uq * ((rho & 2) ? 1 : 0) - ((epcironK_p[0] & 2) >> 1));
				msbits2_p[2]            = (uchar) (Uq * ((rho & 4) ? 1 : 0) - ((epcironK_p[0] & 4) >> 2));
				msbits2_p[3]            = (uchar) (Uq * ((rho & 8) ? 1 : 0) - ((epcironK_p[0] & 8) >> 3));
				rho_p[0]                = (uchar) (rho & 1);
				rho_p[col1step_rho]     = (uchar) ((rho & 2) >> 1);
				rho_p[1]                = (uchar) ((rho & 4) >> 2);
				rho_p[col1step_rho + 1] = (uchar) ((rho & 8) >> 3);
#if ICT_Link08
				fprintf(fp_Uq_kappa, "%d,%d,", Uq, kappa);
				fprintf(fp_msbits, "%d,%d,%d,%d,", msbits2_p[0], msbits2_p[1], msbits2_p[2], msbits2_p[3]);
#endif
			}
#if ICT_Link08
			fprintf(fp_Uq_kappa, "\n");
			fprintf(fp_msbits, "\n");
#endif
		} else {
			e_nw = 0;
			e_n  = mex_p[1];
			for (iii = 0; iii < width; iii += 2, qmex_p++, msbits_p++, msbits2_p = &msbits2_p[4], qinf_p++,
			    qinf_c++, mex_p += 4, mex_c += 4, epcironK_p++, rho_p = &rho_p[2]) {
				rho  = qinf_c[0];
				e_ne = mex_p[3];
				e_nf = mex_p[5];
				ms2  = 0;
#if ICT_Link08
				fprintf(fp_kappa, "%d,%d,%d,%d,%d,%d,", jjj, iii, e_nw, e_n, e_ne, e_nf);
#endif
				if (rho != 0) {
					u_plus = qmex_p[0]; // E
					p_max  = (e_n > e_ne) ? e_n : e_ne;
					e_nw   = (e_nw > e_nf) ? e_nw : e_nf;
					p_max  = (e_nw > p_max) ? e_nw : p_max;
					p_max--;
					gamma = (uchar) (((rho & (rho - 1)) == 0) ? 0 : 1); // gamma:(p23-Formula(6)
					p_max = (byte2) (p_max * gamma);
					kappa = (uchar) ((p_max > 1) ? p_max : 1); // kappa:p23 -Formular(5)
				} else
					kappa = 1;
#if ICT_Link08
				fprintf(fp_kappa, "%d\n", kappa);
#endif
				Uq = msbits_p[0]        = (uchar) (kappa + qmex_p[0]); // Uq
				rho                     = (uchar) (qinf_c[0] & 0xf);
				rho_p[0]                = (uchar) ((rho & 1));
				rho_p[col1step_rho]     = (uchar) ((rho & 2) >> 1);
				rho_p[1]                = (uchar) ((rho & 4) >> 2);
				rho_p[col1step_rho + 1] = (uchar) ((rho & 8) >> 3);

				msbits2_p[0] = (uchar) (Uq * ((rho & 1) ? 1 : 0) - (epcironK_p[0] & 1));
				msbits2_p[1] = (uchar) (Uq * ((rho & 2) ? 1 : 0) - ((epcironK_p[0] & 2) >> 1));
				msbits2_p[2] = (uchar) (Uq * ((rho & 4) ? 1 : 0) - ((epcironK_p[0] & 4) >> 2));
				msbits2_p[3] = (uchar) (Uq * ((rho & 8) ? 1 : 0) - ((epcironK_p[0] & 8) >> 3));
				e_nw         = e_ne;
				e_n          = e_nf;
#if ICT_Link08
				fprintf(fp_Uq_kappa, "%d,%d,", Uq, kappa);
				fprintf(fp_msbits, "%d,%d,%d,%d,", msbits2_p[0], msbits2_p[1], msbits2_p[2], msbits2_p[3]);
#endif
			}
#if ICT_Link08
			fprintf(fp_Uq_kappa, "\n");
			fprintf(fp_msbits, "\n");
#endif
		}

		if (Cblk->p > 0) {
			qinf_c     = qinf_cTS;
			mex_c      = mex_cTS;
			msbits_p   = msbits_TS;
			msbits2_p  = msbits2_TS;
			epciron1_p = epciron1_TS;
			sp_p       = sp_TS;
			e_nw       = 0; //�C�����
			for (iii = 0; iii < width;
			     iii += 2, msbits_p++, msbits2_p += 4, mex_c += 4, sp_p += 2, qinf_c++, epciron1_p++) {
				pattern  = qinf_c[0];
				epciron1 = epciron1_p[0];
				for (i = 0; i < 4; i += 2, pattern = (pattern >> 2), epciron1 >>= 2) {
					myu_ = 0;
					if (pattern & 1) {
						smag_bits2 = msbits2_p[i];
						if (smag_bits2) {
							myu_ = dec_MagSgn(str_fraw, codecf, smag_bits2, (uchar) (epciron1 & 1));
							val2 = (myu_ & 1) << 31;
							myu  = myu_ >> 1;
							myu++;
							smag2       = myu << (Cblk->p);
							sp_p[i / 2] = (val2 | smag2);
							mex_c[i]    = mex_Cal(myu, 31);
#if ICT_Link08
							if (val2)
								fprintf(fp_sp, "%d\n", -1 * myu);
							else
								fprintf(fp_sp, "%d\n", myu);
#endif
						}
					} else {
						myu  = 0;
						val2 = 0;
					}
#if ICT_Link08
					fprintf(fp_myu, "%d,", myu_);
#endif
					myu_ = 0;
					if (pattern & 2) {
						smag_bits2 = msbits2_p[i + 1];
						if (smag_bits2) {
							myu_ = dec_MagSgn(str_fraw, codecf, smag_bits2, (uchar) ((epciron1 & 2) >> 1));
							val2 = (myu_ & 1) << 31;
							myu  = myu_ >> 1;
							myu++;
							smag2                     = myu << (Cblk->p);
							sp_p[i / 2 + col1step_sp] = (val2 | smag2);
							mex_c[i + 1]              = mex_Cal(myu, 31);
#if ICT_Link08
							if (val2)
								fprintf(fp_sp, "%d\n", -1 * myu);
							else
								fprintf(fp_sp, "%d\n", myu);
#endif
						}
					} else {
						myu  = 0;
						val2 = 0;
					}
#if ICT_Link08
					fprintf(fp_myu, "%d,", myu_);
#endif
				}
#if ICT_Link08
				fprintf(fp_E_mex, "%d,%d,%d,%d,%d,%d\n", jjj, iii, mex_c[0], mex_c[1], mex_c[2], mex_c[3]);
#endif
			}
#if ICT_Link08
			fprintf(fp_myu, "\n");
#endif
		} else {
			qinf_c   = qinf_cTS;
			mex_c    = mex_cTS;
			msbits_p = msbits_TS;
			sp_p     = sp_TS;
			sigma_p  = sigma_TS;
			for (iii = 0; iii < width;
			     iii += 4, msbits_p += 2, mex_c += 8, sp_p += 4, sigma_p++, qinf_c += 2) {
				pattern = qinf_c[0];
				pattern |= (qinf_c[1] << 4);
				for (int i = 0; pattern != 0; i++, pattern >>= 2) {
					if (!(pattern & 3)) continue;
					smag_bits2 = msbits_p[i / 2];
					if (pattern & 1) {
						implicit_2 = smag_bits2 >> 7;
						smag_bits2 &= 0x7F;
						if (smag_bits2 > 31) return EXIT_FAILURE;
						implicit_2 <<= smag_bits2;
						smag2 = consume_word(str_fraw, codecf, smag_bits2);
						val2  = (byte4) ((smag2 & 1) << 31);
						smag2 &= ~(0xFFFFFFFF << smag_bits2);
						smag2 |= (implicit_2 | 1);
						mex_c[2 * i] = Bittop2(temp2, 0, smag2);
						smag2 += 2;
						smag2 >>= 1;
						sp_p[i] = (val2 | (byte4) smag2);
					}

					smag_bits2 = msbits_p[i / 2];
					if (pattern & 2) {
						implicit_2 = smag_bits2 >> 7;
						smag_bits2 &= 0x7F;
						if (smag_bits2 > 31) return EXIT_FAILURE;
						implicit_2 <<= smag_bits2;
						smag2 = consume_word(str_fraw, codecf, smag_bits2);
						val2  = (byte4) ((smag2 & 1) << 31);
						smag2 &= ~(0xFFFFFFFF << smag_bits2);
						smag2 |= (implicit_2 | 1);
						mex_c[2 * i + 1] = Bittop2(temp2, 0, smag2);
						smag2 += 2;
						smag2 >>= 1;
						sp_p[col1step_sp + i] = (val2 | (byte4) smag2);
					}
				}
			}
		}
	}
#if ICT_Link08
	sp_TS     = sp_0TS;
	fp_output = fopen("dec_out.csv", "a+");
	for (jjj = 0; jjj < height; jjj++, sp_TS += col1step_sp) {
		sp_p = sp_TS;
		for (iii = 0; iii < width; iii++, ++sp_p) {
			if ((*sp_p) & 0x80000000)
				fprintf(fp_output, "%d,", -1 * (((*sp_p) & 0x7fffffff) >> Cblk->p));
			else
				fprintf(fp_output, "%d,", (((*sp_p) & 0x7fffffff) >> Cblk->p));
		}
		fprintf(fp_output, "\n");
	}
	fprintf(fp_output, "\n\n");
	fclose(fp_output);
	fprintf(fp_E_mex, "\n\n");
	fprintf(fp_Uq_kappa, "\n\n");
	fprintf(fp_msbits, "\n\n");
	fprintf(fp_myu, "\n\n");
	fprintf(fp_sp, "\n\n");
	fprintf(fp_kappa, "\n\n");
	fclose(fp_E_mex);
	fclose(fp_Uq_kappa);
	fclose(fp_msbits);
	fclose(fp_myu);
	fclose(fp_sp);
	fclose(fp_kappa);
	fp_melc = fopen("dec_mel.csv", "a+");
	fprintf(fp_melc, "\n\n");
	fclose(fp_melc);
	fp_fraw = fopen("dec_fraw.csv", "a+");
	fprintf(fp_fraw, "\n\n");
	fclose(fp_fraw);
#endif
	ImageDestory(Image_msbits2);
	return EXIT_SUCCESS;
}

uchar mex_Cal(ubyte4 myu, uchar startB) {
	ubyte4 i;
	uchar mex, flag = 1;

	if (myu == 1)
		mex = 1;
	else {
		myu = myu * 2 - 1;
		i   = (1 << startB);
		mex = startB;
		while (flag) {
			if (i & myu) {
				flag = 0;
				break;
			}
			i >>= 1;
			mex--;
		}
		if (myu & (i - 1)) mex++;
	}
	return mex;
}
