/*
Copyright (c) 2000-2021, ICT-Link
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the HTJ2K project.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "j2k2.h"
#include "MelCodec.h"
#include "ImageUtil.h"

/*****************************************************************************/
/*****************************************************************************/
/***************** MEL *******************************************************/
/*****************************************************************************/
struct MelCodec_s *MelCodecCreate() {
	struct MelCodec_s *codecM;
	byte4 i;

	//	mel_state_Length = FBC_MEL_STATES2;
	//	printf( "[MelCodecCreate]:: In\n" );

	if (nullptr == (codecM = new struct MelCodec_s)) {
		printf("[MelCodecCreate] :: codecM create error\n");
		return nullptr;
	}
	codecM->dec_Lut_Length = FBC_MEL_STATES2 * 64;
	codecM->enc_Lut_Length = FBC_MEL_LAST2 * 64;
	codecM->Limits         = FBC_MEL_ENC_FAST_LIM2;
	//	printf( "[MelCodecCreate]:: enc_Lut_Length= %d dec_Lut_Length= %d\n", codecM->enc_Lut_Length,
	//codecM->dec_Lut_Length );

	if (nullptr == (codecM->fbc_mel_decode_lut = new ubyte2[codecM->dec_Lut_Length])) {
		printf("[MelCodecCreate] :: codecM->fbc_mel_decode_lut create error\n");
		return nullptr;
	}
	//	memcpy( codecM->fbc_mel_decode_lut, MEL_DECODE_LUT, sizeof(ubyte2)*dec_Lut_Length );

	if (nullptr == (codecM->fbc_mel_enc_C_lut = new ubyte2[codecM->enc_Lut_Length])) {
		printf("[MelCodecCreate] :: codecM->fbc_mel_enc_C_lut create error\n");
		return nullptr;
	}
	//	memcpy( codecM->fbc_mel_enc_C_lut, MEL_ENC_C_LUT, sizeof(ubyte2)*enc_Lut_Length );

	if (nullptr == (codecM->fbc_mel_enc_U_lut = new uchar[codecM->enc_Lut_Length])) {
		printf("[MelCodecCreate] :: codecM->fbc_mel_enc_U_lut create error\n");
		return nullptr;
	}
	//	memcpy( codecM->fbc_mel_enc_U_lut, MEL_ENC_U_LUT, sizeof(uchar)*enc_Lut_Length );
	for (i = 0; i < codecM->enc_Lut_Length; i++) {
		codecM->fbc_mel_enc_U_lut[i] = (uchar) MEL_ENC_U_LUT[i];
		codecM->fbc_mel_enc_C_lut[i] = (ubyte2) MEL_ENC_C_LUT[i];
	}
	for (i = 0; i < codecM->dec_Lut_Length; i++) {
		codecM->fbc_mel_decode_lut[i] = MEL_DECODE_LUT[i];
	}

	//	printf( "enc_Lut_Length= %d dec_Lut_Length= %d complete\n", codecM->enc_Lut_Length,
	//codecM->dec_Lut_Length );

	return codecM;
}

byte4 MelCodecDestroy(struct MelCodec_s *codecM) {
	//	printf("[MelCodecDestroy] in\n" );
	delete[] codecM->fbc_mel_decode_lut;
	//	printf("[MelCodecDestroy] codecM->fbc_mel_decode_lut complete\n" );
	delete[] codecM->fbc_mel_enc_C_lut;
	//	printf("[MelCodecDestroy] codecM->fbc_mel_enc_C_lut complete\n" );
	delete[] codecM->fbc_mel_enc_U_lut;
	//	printf("[MelCodecDestroy] codecM->fbc_mel_enc_U_lut complete\n" );
	delete codecM;
	return EXIT_SUCCESS;
}

void EncMelStart(struct MelCodec_s *codecM) {
	codecM->Creg      = 0;
	codecM->ctreg     = 0;
	codecM->mel_state = FBC_AZC_INITIAL_MEL_STATE2;
	codecM->mel_count = 0;
	codecM->last_byte = 0;
}

byte4 DecMelStart(struct StreamChain_s *str_melc, struct MelCodec_s *codecM) {
	ubyte4 val;

	str_melc->buf[str_melc->buf_length - 1] = 0xff;
	str_melc->buf[str_melc->buf_length - 2] |= 0x0f;
	codecM->Creg      = 0;
	codecM->ctreg     = 16;
	codecM->last_byte = 0;
	val               = (FBC_AZC_INITIAL_MEL_STATE2);

	if (EXIT_FAILURE == Load_Stream(str_melc, codecM, 0, (FBC_AZC_INITIAL_MEL_STATE2))) {
		printf("[DecMelStart]:: Load_Stream error\n");
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}

struct StreamChain_s *terminate(struct StreamChain_s *str_melc, struct MelCodec_s *codecM) {
	if (codecM->ctreg) {
		if (codecM->last_byte == 0xff) {
			if (nullptr
			    == (str_melc =
			            Stream1ByteWrite(str_melc, (uchar) ((codecM->Creg << (7 - codecM->ctreg)) & 0xFF),
			                             str_melc->buf_length))) {
				printf("[terminate]:: Stream1ByteWrite 1 error\n");
				return nullptr;
			}
		} else {
			if (nullptr
			    == (str_melc =
			            Stream1ByteWrite(str_melc, (uchar) ((codecM->Creg << (8 - codecM->ctreg)) & 0xFF),
			                             str_melc->buf_length))) {
				printf("[terminate]:: Stream1ByteWrite 2 error\n");
				return nullptr;
			}
		}
	}
	return str_melc;
}

struct StreamChain_s *Write_Stream(struct StreamChain_s *str_melc, struct MelCodec_s *codecM, uchar CwL,
                                   ubyte4 CwD) {
	if (CwL == 0) return str_melc;

	codecM->ctreg = (uchar) (codecM->ctreg + CwL);
	codecM->Creg <<= CwL;
	codecM->Creg |= CwD;
	while (codecM->ctreg >= 8) {
		if (codecM->last_byte == 0xff) {
			codecM->ctreg -= 7;
			codecM->last_byte = (uchar) ((codecM->Creg >> codecM->ctreg) & 0x7f);
		} else {
			codecM->ctreg -= 8;
			codecM->last_byte = (uchar) ((codecM->Creg >> codecM->ctreg) & 0xff);
		}
		if (codecM->ctreg) codecM->Creg &= mask6[codecM->ctreg];
		if (nullptr == (str_melc = Stream1ByteWrite(str_melc, codecM->last_byte, str_melc->buf_length))) {
			printf("[Write_Stream]:: Stream1ByteWrite error\n");
			return nullptr;
		}
	}
	return str_melc;
}

byte4 Load_Stream(struct StreamChain_s *str_melc, struct MelCodec_s *codecM, uchar CwL, uchar nMelState) {
	uchar val;

	codecM->ctreg = (uchar) (codecM->ctreg + CwL);
	codecM->Creg <<= CwL;
	codecM->mel_state = nMelState;
	if (codecM->ctreg >= 8) {
		if (codecM->last_byte == 0xff) {
			if (str_melc->buf_length - 1 <= str_melc->cur_p)
				val = 0;
			else
				val = Ref_1Byte(str_melc);
			if (EXIT_FAILURE == str_melc->ERROR) {
				printf("[Load_Stream]:: val error1 val=%02x\n", val);
			}
			val &= 0x7f;
			codecM->ctreg -= 7;
			codecM->Creg |= (val << codecM->ctreg);
		} else {
			if (str_melc->buf_length - 1 <= str_melc->cur_p)
				val = 0;
			else
				val = Ref_1Byte(str_melc);
			if (EXIT_FAILURE == str_melc->ERROR) {
				printf("[Load_Stream]:: val error2 val=%02x\n", val);
			}
			codecM->ctreg -= 8;
			codecM->Creg |= (val << codecM->ctreg);
		}
		codecM->last_byte = val;
	}
	return EXIT_SUCCESS;
}

struct StreamChain_s *EncMelCoder(struct StreamChain_s *str_melc, struct MelCodec_s *codecM, byte4 run) {
	byte4 idx, uval, cval;
	ubyte2 CwD;
	uchar CwL;

	if (run < 0) {
		printf("[EncMelCoder]:: input data run is negative. This codec is only valid for positive "
		       "value.run = %d\n",
		       run);
		return nullptr;
	}

	idx = codecM->mel_state << 6;
	if (run < codecM->Limits) {
		idx += run;
		if (codecM->enc_Lut_Length <= idx) {
			printf("[EncMelCoder]:: idx error. idx=%d, run\n", idx, run);
			return nullptr;
		}

		uval              = codecM->fbc_mel_enc_U_lut[idx];
		CwD               = codecM->fbc_mel_enc_C_lut[idx];
		codecM->mel_state = (ubyte4) (uval >> 4);
		CwL               = (uchar) ((uval & 0xf) + 1);
		str_melc          = Write_Stream(str_melc, codecM, CwL, CwD);
	} else {
		idx += (run < 63) ? run : 63;
		if (codecM->enc_Lut_Length <= idx) {
			printf("[EncMelCoder]:: idx error. idx=%d, run\n", idx, run);
			return nullptr;
		}
		uval = codecM->fbc_mel_enc_U_lut[idx];
		cval = codecM->fbc_mel_enc_C_lut[idx];
		CwL  = (uchar) (uval & 0xf);
		run -= (cval & 0x00FF);
		cval >>= 8;
		codecM->mel_state = (ubyte4) (uval >> 4);
		CwL               = (uchar) (CwL + (run >> cval));
		run &= (1 << cval) - 1;
		while (CwL > 8) {
			str_melc = Write_Stream(str_melc, codecM, 6, 0x3f);
			CwL -= 6;
		}
		str_melc = Write_Stream(str_melc, codecM, CwL, ((1 << CwL) - 1));
		str_melc = Write_Stream(str_melc, codecM, (uchar) (cval + 1), run);
	}
	return str_melc;
}

byte4 DecMelCoder(struct StreamChain_s *str_melc, struct MelCodec_s *codecM) {
	ubyte4 vlc, lval;
	byte4 run;
	uchar CodeStream;
	uchar CwL;

	CodeStream = (uchar) ((codecM->Creg >> 10) & 0x3f);
	lval       = (codecM->mel_state << 6) + CodeStream;
	vlc        = codecM->fbc_mel_decode_lut[lval];
	CwL        = (uchar) (vlc & 0x7);
	if (EXIT_FAILURE == Load_Stream(str_melc, codecM, CwL, (uchar) ((vlc >> 4) & 0xf))) {
		printf("[DecMelCoder]:: Load_Stream error1.\n");
		return -1;
	}
	run = vlc >> 8;
	if (vlc & 8) {
		do {
			CodeStream = (uchar) ((codecM->Creg >> 10) & 0x3f);
			lval       = (codecM->mel_state << 6) + CodeStream;
			vlc        = codecM->fbc_mel_decode_lut[lval];
			CwL        = (uchar) (vlc & 0x7);
			if (EXIT_FAILURE == Load_Stream(str_melc, codecM, CwL, (uchar) ((vlc >> 4) & 0xf))) {
				printf("[DecMelCoder]:: Load_Stream error2.\n");
				return -1;
			}
			run += (vlc >> 8);
		} while ((vlc & 8) && (run < 1024));
	}
	return run;
}
