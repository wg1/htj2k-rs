/*
Copyright (c) 2000-2021, ICT-Link
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the HTJ2K project.
*/

#ifndef CLEANUP_H
#define CLEANUP_H

#include "ImageUtil.h"

#define FBC_U0_VAL_I 2
#define FBC_AZC_INITIAL_MEL_STATE_I 0

char Bittop2(char startT, char endT, byte4 D);
char Bittop(char startT, char endT, byte4 D);
void prepare_bit_plane(struct Cblk_s *Cblk, struct Image_s *CbData, struct Image_s *Image_mex,
                       struct Image_s *Image_refine, uchar bittop, byte2 ccc, uchar rrr, uchar bbb,
                       byte4 kkk /*, char *debug_fname*/);
byte4 encode_cln(
    struct J2kParam_s *J2kParam, struct Cblk_s *Cblk,
    /*struct Image_s *In, struct Image_s *Image_qinf, struct Image_s *Image_qmex, struct Image_s
       *Image_msbits,*/
    struct Image_s *Image_mex,
    /*struct Image_s *Image_sigma, struct Image_s *Image_epcironK, struct Image_s *Image_epciron1,*/
    byte4 width, byte4 height, struct RRawCodec_s *codecR, struct MelCodec_s *codecM,
    struct fRawCodec_s *codecf, struct StreamChain_s *str_rraw, struct StreamChain_s *str_melc,
    struct StreamChain_s
        *str_fraw /*, char *fnameTemp, byte2 ccc, uchar rrr, uchar bbb, byte4 kkk, char *debug_fname*/);
byte4 decode_cln(struct Image_s *Band, struct Cblk_s *Cblk, struct Codec_s *Codec,
                 struct Image_s *Image_qinf, struct Image_s *Image_qmex, struct Image_s *Image_msbits,
                 struct Image_s *Image_mex, struct Image_s *Image_sigma, struct Image_s *Image_epcironK,
                 struct Image_s *Image_epciron1, /*bool refined,*/ struct RRawCodec_s *codecR,
                 struct MelCodec_s *codecM, struct fRawCodec_s *codecf, struct StreamChain_s *str_rraw,
                 struct StreamChain_s *str_melc, struct StreamChain_s *str_fraw /*, char mode1,*/
                 /* byte4 T, ubyte2 ccc, uchar rrr, byte4 ppp, uchar bbb, byte4 kkk, char *debug_fname*/);
// void	Dec_bit_plane( byte4 *Data_, byte4 col1step, byte4 height, byte4 width, byte4 p);
uchar mex_Cal(ubyte4 myu, uchar startB);
bool decode_SigProp(struct Image_s *Band, struct Cblk_s *Cblk, struct Codec_s *Codec,
                    struct StreamChain_s *str_sigp, char MagRef, struct StreamChain_s *str_mgrf,
                    uchar vcausal /*, byte2 ccc, uchar rrr, uchar bbb, byte4 kkk*/);
void initSigProp(struct StreamChain_s *str);
void initMagRef(struct StreamChain_s *str, byte4 Lref);
#endif // CLEANUP_H
