/*
Copyright (c) 2000-2021, ICT-Link
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the HTJ2K project.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "j2k2.h"
#include "ImageUtil.h"
#include "wavelet_codec.h"
#include "Icc.h"
#include "t2codec.h"
#include "annexA.h"
#include "t1codec.h"
#include "ctx_enc.h"
#include "MqCodec.h"
#include "annexB.h"

uchar NumberOfPassesDec(struct StreamChain_s *s) {
	uchar n;
	if (Ref_1Bit(s)) { // 1
		if (Ref_1Bit(s)) {
			n = (uchar) Ref_nBits(s, 2);
			if (n != 3)
				return (uchar) (n + 3); // 11**
			else {
				n = (uchar) Ref_nBits(s, 5);
				if (n != 0x1f)
					return (uchar) (n + 6); // 1111 **** *
				else
					return (uchar) (Ref_nBits(s, 7) + 37); // 1111 1111 1*** ****
			}
		} else
			return 2; // 10
	} else
		return 1; // 0
}

byte4 TagTreeDec(/*struct Tagtree_s *tree,*/ struct TagtreeNode_s *leaf, char threshold,
                 struct StreamChain_s *s) {
	struct TagtreeNode_s *nodeP, *node;
	char value;

	node = leaf;
	while ((node->parent != NULL) && (node->parent->state != FIXED)) {
		nodeP        = node->parent;
		nodeP->child = node;
		node         = nodeP;
	}

	if (node->state == YET)
		value = node->known;
	else if (node->state == UNHANDLE) {
		if (node->parent != NULL)
			value = node->parent->value;
		else
			value = 0;
	} else
		return EXIT_FAILURE;

	for (;; node = node->child) {
		if (value > threshold) {
			node->state = YET;
			node->known = value;
			goto OUT;
		}
		while (!Ref_1Bit(s)) {
			value++;
			if (value > threshold) {
				node->state = YET;
				node->known = value;
				goto OUT;
			}
		}
		node->value = value;
		node->state = FIXED;
		if (node->child == NULL) goto OUT;
	}
OUT:
	return EXIT_SUCCESS;
}

byte4 LblockChangeDec(struct StreamChain_s *s) {
	byte4 n;
	n = 0;
	while (Ref_1Bit(s)) {
		n++;
	}
	return n;
}

struct Packet_s *PacketInfoDec(struct Packet_s *Ps, struct StreamChain_s *strPI,
                               struct StreamChain_s *strPC, struct EECod_s *EECod, uchar ppm_flag) {
	struct Cblk_s *Cbs, *Cbe;
	struct Tagtree_s *inclTree, *zeroTree;
	struct TagtreeNode_s *node_Zero, *node_incl;
	uchar numBands, bbb;
	uchar seg;
	uchar t1;
	char NoPackets = 0;
	char SopOn, EphOn;
	byte4 kkk;
	byte4 Saddr, length;
	byte4 add_bits, add_bits2;
	byte4 lll, total_numPasses, xx, num_SegmentPass, next_numSegmentPass, new_passes;
	byte4 usedPasses = 0, residualPasses;
	uchar flag       = 0;

	if (Ps->cmptsNo == 2 && Ps->resoNo == 1) flag = flag;

	SopOn = EECod->SopOn;
	EphOn = EECod->EphOn;
	EECod->DecompLev;
	numBands     = Ps->numband;
	strPI->cur_p = Ps->SadrPI;
	strPC->cur_p = Ps->Sadr_PacketCode;

	if (SopOn && (!ppm_flag)) {
		if (Ref_2Byte(strPI) != SOP) {
			printf("[PacketInfoDec]::SOP missing error Packet number= %d\n", Ps->packetNo);
			return NULL;
		}
		Ref_2Byte(strPI);
		Ps->packetNo = Ref_2Byte(strPI);
	}
	strPI->stream_type = NoDiscard;
	if (!Ref_1Bit(strPI)) {
		Ref_StuffBits(strPI, JPEG2000);
		if (EphOn) {
			if (Ref_2Byte(strPI) != EPH) {
				printf("[PacketInfoDec]::EPH missing error, when enpty packet.\n");
				return NULL;
			}
		}
		for (bbb = 0; bbb < numBands; bbb++) {
			Cbs = &Ps->EPrc->Prc[bbb].Cblk[0];
			Cbe = &Ps->EPrc->Prc[bbb].Cblk[Ps->EPrc->Prc[bbb].numCblk];
			for (; Cbs != Cbe; ++Cbs) {
				Cbs->Nb = Cbs->Mb;
			}
		}
		Ps->NoPacket = 0;
		if (ppm_flag) {
			Ps->EadrPI = strPI->cur_p;
			if (SopOn) {
				if (SOP != Ref_2Byte(strPC)) {
					printf("[PacketInfoDec]::PPM SOP missing error, when enpty packet.\n");
					return NULL;
				}
				Ref_2Byte(strPC);
				Ref_2Byte(strPC);
			}
			Ps->Sadr_PacketCode = strPC->cur_p;
			Ps->Eadr_PacketCode = Ps->Sadr_PacketCode;
			strPI->stream_type  = JPEG2000;
		} else {
			Ps->EadrPI          = strPI->cur_p;
			Ps->Sadr_PacketCode = Ps->EadrPI;
			Ps->Eadr_PacketCode = Ps->EadrPI;
			strPI->stream_type  = JPEG2000;
		}
		return Ps;
	}
	strPI->stream_type = JPEG2000;
	Ps->NoPacket       = 1;

	for (bbb = 0; bbb < numBands; bbb++) {
		inclTree  = Ps->EPrc->Prc[bbb].incltree;
		zeroTree  = Ps->EPrc->Prc[bbb].zerotree;
		Cbs       = &Ps->EPrc->Prc[bbb].Cblk[0];
		Cbe       = &Ps->EPrc->Prc[bbb].Cblk[Ps->EPrc->Prc[bbb].numCblk];
		node_Zero = &zeroTree->node[0];
		node_incl = &inclTree->node[0];
		for (kkk = 0; Cbs != Cbe; kkk++, ++Cbs, ++node_Zero, ++node_incl) {
			if ((Cbs->tcbx1 - Cbs->tcbx0) * (Cbs->tcby1 - Cbs->tcby0)) {
				Cbs->numPasses  = 0;
				total_numPasses = 0;
				for (lll = 0; lll < Ps->LayerNo; lll++)
					total_numPasses += Cbs->Layer_passes[lll];

				if (Cbs->included) {
					if (!Ref_1Bit(strPI)) {
						Cbs->Last_numSegment = Cbs->numSegment;
						goto NEXTCb;
					}
				} else {
					TagTreeDec(node_incl, (char) Ps->LayerNo, strPI);
					if ((node_incl->state == FIXED) && (node_incl->value == Ps->LayerNo))
						Cbs->decOn = 1;
					else
						Cbs->decOn = 0;

					if (Cbs->decOn) {
						TagTreeDec(node_Zero, (char) CHARMAX, strPI);
						Cbs->zerobits = node_Zero->value;
						Cbs->Nb       = Cbs->zerobits;
						Cbs->included = 1;
					} else {
						Cbs->zerobits = (char) Cbs->Mb;
						Cbs->Nb       = Cbs->Mb;
						goto NEXTCb;
					}
				}
				Cbs->numPasses                 = NumberOfPassesDec(strPI);
				Cbs->Layer_passes[Ps->LayerNo] = Cbs->numPasses;
				Cbs->Nb     = (ubyte2) (Cbs->zerobits + (ceil2(total_numPasses - 1, 3) + 1));
				Cbs->Lblock = (uchar) (Cbs->Lblock + LblockChangeDec(strPI));
				seg = Cbs->Last_numSegment = Cbs->numSegment;

				if (Cbs->HT_mode & 0x80) { // all passes are terminated.
					if (Cbs->HT_firstCleanUP == 0) {
						xx = (total_numPasses + Cbs->numPasses - 1) % 3;
						Cbs->numSegment++;

						num_SegmentPass = Cbs->numPasses - xx;
						if (num_SegmentPass <= 0) num_SegmentPass = Cbs->numPasses;
						add_bits  = floorlog2(num_SegmentPass);
						add_bits2 = floorlog2(Cbs->numPasses);
						t1        = (uchar) (Cbs->Lblock + add_bits);
						seg       = Cbs->Last_numSegment;
						length    = Ref_nBits(strPI, t1);
						if (!length) {
							num_SegmentPass = Cbs->numPasses;
							length          = Ref_nBits(strPI, (uchar) (add_bits2 - add_bits));
						}
						if (length) {
							if ((Cbs->HT_mode & 0x10) && ((Cbs->HT_mode & 3) == 3)) {
								if ((length > 1) && (Cbs->Lblock > 3) && !(length >> (t1 - 1)))
									new_passes = Cbs->numPasses - num_SegmentPass;
								else {
									length <<= (add_bits2 - add_bits);
									length += Ref_nBits(strPI, (uchar) (add_bits2 - add_bits));
									Cbs->HT_mode &= 0x7f;
									new_passes = 0;
								}
							} else
								new_passes = Cbs->numPasses - num_SegmentPass;
						} else
							new_passes = Cbs->numPasses - num_SegmentPass;
						Cbs->Seg_passes[seg] = (ubyte2) num_SegmentPass;
						Cbs->Seg_Length[seg] = length;

						if (new_passes > 0) {
							next_numSegmentPass = 2;
							seg++;
							if (new_passes > 1)
								num_SegmentPass = next_numSegmentPass;
							else
								num_SegmentPass = 1;
							t1                   = (uchar) (Cbs->Lblock + num_SegmentPass - 1);
							Cbs->Seg_Length[seg] = Ref_nBits(strPI, t1);
							Cbs->Seg_passes[seg] = (ubyte2) num_SegmentPass;
							length += Cbs->Seg_Length[seg];
							Cbs->numSegment++;
						}
						if (length) Cbs->HT_firstCleanUP = 1;
					} else {
						if ((total_numPasses % 3) == 0) {
							// cleanUp
							add_bits             = 0;
							t1                   = (uchar) (Cbs->Lblock + add_bits);
							seg                  = Cbs->Last_numSegment;
							Cbs->Seg_Length[seg] = Ref_nBits(strPI, t1);
							Cbs->Seg_passes[seg] = 1;
							length               = Cbs->Seg_Length[seg];
							if (Cbs->numPasses > 1) {
								t1 = Cbs->Lblock;
								seg++;
								Cbs->Seg_Length[seg] = Ref_nBits(strPI, t1);
								Cbs->Seg_passes[seg] = Cbs->numPasses;
								length += Cbs->Seg_Length[seg];
							}
						} else if ((total_numPasses % 3) == 1) {
							// not cleanUp. SigProf only. only 1 segment is added.
							add_bits             = (Cbs->numPasses > 1) ? 1 : 0;
							t1                   = (uchar) (Cbs->Lblock + add_bits);
							seg                  = Cbs->Last_numSegment;
							Cbs->Seg_Length[seg] = Ref_nBits(strPI, t1);
							Cbs->Seg_passes[seg] = Cbs->numPasses; // 1;
							length               = Cbs->Seg_Length[seg];
						} else {
							// not cleanUp. SigProf only. only 1 segment is added.
							add_bits             = (Cbs->numPasses > 1) ? 1 : 0;
							t1                   = (uchar) (Cbs->Lblock + add_bits);
							seg                  = Cbs->Last_numSegment;
							Cbs->Seg_Length[seg] = Ref_nBits(strPI, t1);
							Cbs->Seg_passes[seg] = Cbs->numPasses; // 1;
							length               = Cbs->Seg_Length[seg];
						}
					}
					Cbs->numSegment++;
					if (Cbs->numSegment >= 64) {
						printf("[PacketInfoDec] :: HTj2K Segment number over error (%d)\n",
						       Cbs->numSegment);
						return NULL;
					}
				} else if (EECod->CbTerm) { // all passes are terminated.
					Cbs->numSegment = (uchar) (Cbs->numSegment + Cbs->numPasses);
					length          = 0;
					t1              = Cbs->Lblock;
					for (seg = Cbs->Last_numSegment; seg < Cbs->numSegment; seg++) {
						Cbs->Seg_Length[seg] = Ref_nBits(strPI, t1);
						Cbs->Seg_passes[seg] = 1; // seg;
						length += Cbs->Seg_Length[seg];
					}
				} else if ((!EECod->CbTerm) && EECod->CbBypass) {
					length         = 0;
					residualPasses = Cbs->numPasses;
					// 1st flow
					if (!Cbs->firstLayer) {
						total_numPasses = Cbs->total_numPasses + Cbs->numPasses;
						if (total_numPasses < 10) {
							usedPasses      = Cbs->numPasses;
							Cbs->firstLayer = 0; // next next is Cln
						} else {
							usedPasses      = 10 - Cbs->total_numPasses;
							Cbs->firstLayer = 2; // next next is Cln
						}
						t1                   = (uchar) (Cbs->Lblock + floorlog2(usedPasses));
						Cbs->Seg_passes[seg] = (ubyte2) usedPasses;
						Cbs->Seg_Length[seg] = Ref_nBits(strPI, t1);
						length += Cbs->Seg_Length[seg];
						Cbs->numSegment++;
						seg++;
						residualPasses -= usedPasses;
					}
					// 2dn flow
					if (residualPasses) {
						while (residualPasses) {
							if (residualPasses == 1) {
								switch (Cbs->firstLayer) {
									case 1:
										usedPasses      = 1;
										Cbs->firstLayer = 2;
										break;
									case 2:
										usedPasses      = 1;
										Cbs->firstLayer = 0x11;
										break;
									case 0x11:
										usedPasses      = 1;
										Cbs->firstLayer = 1; // 2;20191017
										break;
									default:
										break;
								}
							} else {
								switch (Cbs->firstLayer) {
									case 1:
										usedPasses      = 1;
										Cbs->firstLayer = 2;
										break;
									case 2:
										usedPasses      = 2;
										Cbs->firstLayer = 1;
										break;
									case 0x11:
										usedPasses      = 1;
										Cbs->firstLayer = 1;
										break;
									default:
										break;
								}
							}
							t1                   = (uchar) (Cbs->Lblock + floorlog2(usedPasses));
							Cbs->Seg_passes[seg] = (ubyte2) usedPasses;
							Cbs->Seg_Length[seg] = Ref_nBits(strPI, t1);
							length += Cbs->Seg_Length[seg];
							Cbs->numSegment++;
							seg++;
							residualPasses -= usedPasses;
						}
					}
				} else {
					t1                   = (uchar) (Cbs->Lblock + floorlog2(Cbs->numPasses));
					Cbs->Seg_Length[seg] = length = Ref_nBits(strPI, t1);
					Cbs->Seg_passes[seg]          = Cbs->numPasses;
					Cbs->numSegment++;
				}
				Cbs->total_numPasses += Cbs->numPasses;
				Cbs->Layer_Length[Ps->LayerNo] = length;
			NEXTCb:;
			}
		}
		Cbs = &Ps->EPrc->Prc[bbb].Cblk[0];
		Cbe = &Ps->EPrc->Prc[bbb].Cblk[Ps->EPrc->Prc[bbb].numCblk];
	}

	Ref_StuffBits(strPI, JPEG2000);
	if (EphOn) {
		if (EPH != Ref_2Byte(strPI)) {
			printf("[PacketInfoDec]::EPH missing error, when normal packet.\nPackets number=%x",
			       Ps->packetNo);
			return NULL;
		}
	}
	Ps->EadrPI = strPI->cur_p;

	if (ppm_flag) {
		strPC->cur_p = Ps->Sadr_PacketCode;
		if (SopOn) {
			if (Ref_2Byte(strPC) != SOP) {
				printf("[PacketInfoDec]::SOP missing error Packet number= %d\n", Ps->packetNo);
				return NULL;
			}
			Ref_2Byte(strPC);
			Ps->packetNo        = Ref_2Byte(strPC);
			Ps->Sadr_PacketCode = strPC->cur_p;
		}
		Ps->Eadr_PacketCode = Ps->Sadr_PacketCode;
	} else {
		Ps->Sadr_PacketCode = Ps->EadrPI;
		Ps->Eadr_PacketCode = Ps->EadrPI;
	}

	Saddr = Ps->Sadr_PacketCode;
	for (bbb = 0; bbb < numBands; bbb++) {
		Cbs = &Ps->EPrc->Prc[bbb].Cblk[0];
		Cbe = &Ps->EPrc->Prc[bbb].Cblk[Ps->EPrc->Prc[bbb].numCblk];
		for (; Cbs != Cbe; ++Cbs) {
			Cbs->Layer_Saddr[Ps->LayerNo] = Saddr;
			for (seg = Cbs->Last_numSegment; seg < Cbs->numSegment; seg++) {
				Cbs->Seg_Saddr[seg] = Saddr;
				Saddr += Cbs->Seg_Length[seg];
			}
			Ps->Eadr_PacketCode += Cbs->Layer_Length[Ps->LayerNo];
			if (Cbs->decOn) NoPackets = 1;
		}
	}

	Ps->NoPacket = NoPackets;
	return Ps;
}
