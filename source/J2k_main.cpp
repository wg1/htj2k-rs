/*
Copyright (c) 2000-2021, ICT-Link
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the HTJ2K project.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "j2k2.h"
#include "ImageUtil.h"

byte4 main(int argc, char **argv) {
	FILE *fp;
	char fname1[256], fname2[256], fileForm1[5], fileForm2[5];
	char encdec     = 0;
	ubyte2 numCmpts = 0;
	struct StreamChain_s *CodeStream;
	struct StreamChain_s *str;
	struct J2kParam_s *J2kParam;
	struct Image_s *Image1 = nullptr;
	byte4 i, j;
	byte4 rateD;
	byte4 buf_length;
	char numBitDipth = 1;
	byte4 width = 0, height = 0;
	uchar PrcW, PrcH;
	uchar Sign   = 0;
	char RawMode = 0;

	if (argc < 8) {
		printf("j2k3 -i [in-file] -o [out-file] -f[input file extension] -F [output file extension]\n");
		return EXIT_SUCCESS;
	} else if ((!strcmp(argv[1], "-h")) || (!strcmp(argv[1], "-H"))) {
		printf("j2k3 -i [in-file] -o [out-file] -f[input file extension] -F [output file extension]\n");
		return EXIT_SUCCESS;
	}

	if (nullptr == (J2kParam = J2kParamCreate())) {
		printf("[main] J2kParamCreate error\n");
		return EXIT_FAILURE;
	}

	i = 1;
	while (i < argc) {
		if ((!strcmp(argv[i], "-i"))) {
			strcpy(fname1, argv[i + 1]);
			i += 2;
		} else if ((!strcmp(argv[i], "-f"))) {
			strcpy(fileForm1, argv[i + 1]);
			i += 2;
		} else if ((!strcmp(argv[i], "-o"))) {
			strcpy(J2kParam->fname, argv[i + 1]);
			strcpy(fname2, argv[i + 1]);
			i += 2;
		} else if ((!strcmp(argv[i], "-F"))) {
			strcpy(J2kParam->fileForm, argv[i + 1]);
			strcpy(fileForm2, argv[i + 1]);
			i += 2;
		} else if ((!strcmp(argv[i], "-HTJ2K"))) {
			J2kParam->HTJ2K = 1;
			i++;
		} else if ((!strcmp(argv[i], "-PROGORDER"))) {
			if (!strcmp(argv[i + 1], "LRCP"))
				J2kParam->ProgOrder = 0;
			else if (!strcmp(argv[i + 1], "RLCP"))
				J2kParam->ProgOrder = 1;
			else if (!strcmp(argv[i + 1], "RPCL"))
				J2kParam->ProgOrder = 2;
			else if (!strcmp(argv[i + 1], "PCRL"))
				J2kParam->ProgOrder = 3;
			else if (!strcmp(argv[i + 1], "CPRL"))
				J2kParam->ProgOrder = 4;
			i += 2;
		} else if ((!strcmp(argv[i], "-NUMLAYER"))) {
			J2kParam->numLayer = (byte2) atoi(argv[i + 1]);
			i += 2;
		} else if ((!strcmp(argv[i], "-TILE_X"))) {
			J2kParam->Tile_x = atoi(argv[i + 1]);
			i += 2;
		} else if ((!strcmp(argv[i], "-TILE_Y"))) {
			J2kParam->Tile_y = atoi(argv[i + 1]);
			i += 2;
		} else if ((!strcmp(argv[i], "-MC"))) {
			if (!strcmp(argv[i + 1], "OFF")) J2kParam->MC = 0;
			if (!strcmp(argv[i + 1], "off")) J2kParam->MC = 0;
			if (!strcmp(argv[i + 1], "ON")) J2kParam->MC = 1;
			if (!strcmp(argv[i + 1], "on")) J2kParam->MC = 1;
			i += 2;
		} else if ((!strcmp(argv[i], "-DECOMPLEV"))) {
			J2kParam->DecompLev = (char) atoi(argv[i + 1]);
			i += 2;
		} else if ((!strcmp(argv[i], "-CB_W"))) {
			J2kParam->CbW = (uchar) atoi(argv[i + 1]);
			i += 2;
		} else if ((!strcmp(argv[i], "-CB_H"))) {
			J2kParam->CbH = (uchar) atoi(argv[i + 1]);
			i += 2;
		} else if ((!strcmp(argv[i], "-CB_BYPASS"))) {
			if (!strcmp(argv[i + 1], "ON"))
				J2kParam->CbBypass = 4;
			else if (!strcmp(argv[i + 1], "on"))
				J2kParam->CbBypass = 4;
			else
				J2kParam->CbBypass = (uchar) atoi(argv[i + 1]);
			if ((0 <= J2kParam->CbBypass) && (J2kParam->CbBypass < 5)) {
			} else {
				printf("[main] Lazy parameter input error %d\n", J2kParam->CbBypass);
				return EXIT_FAILURE;
			}
			i += 2;
		} else if ((!strcmp(argv[i], "-CB_RESET"))) {
			if (!strcmp(argv[i + 1], "ON")) J2kParam->CbReset = 1;
			if (!strcmp(argv[i + 1], "on")) J2kParam->CbReset = 1;
			i += 2;
		} else if ((!strcmp(argv[i], "-CB_TERM"))) {
			if (!strcmp(argv[i + 1], "ON")) J2kParam->CbTerm = 1;
			if (!strcmp(argv[i + 1], "on")) J2kParam->CbTerm = 1;
			i += 2;
		} else if ((!strcmp(argv[i], "-CB_VCAUSAL"))) {
			if (!strcmp(argv[i + 1], "ON")) J2kParam->CbVcausal = 1;
			if (!strcmp(argv[i + 1], "on")) J2kParam->CbVcausal = 1;
			i += 2;
		} else if ((!strcmp(argv[i], "-CB_PTERM"))) {
			if (!strcmp(argv[i + 1], "ON")) J2kParam->CbPreterm = 1;
			if (!strcmp(argv[i + 1], "on")) J2kParam->CbPreterm = 1;
			i += 2;
		} else if ((!strcmp(argv[i], "-CB_SEGMENTSYMBOL"))) {
			if (!strcmp(argv[i + 1], "ON")) J2kParam->CbSegmentSymbol = 1;
			if (!strcmp(argv[i + 1], "on")) J2kParam->CbSegmentSymbol = 1;
			i += 2;
		} else if ((!strcmp(argv[i], "-FILTER"))) {
			if (!strcmp(argv[i + 1], "97")) J2kParam->Filter = FILTER97;
			if (!strcmp(argv[i + 1], "53")) J2kParam->Filter = FILTER53;
			i += 2;
		} else if ((!strcmp(argv[i], "-SOP"))) {
			if (!strcmp(argv[i + 1], "OFF")) J2kParam->SopOn = 0;
			if (!strcmp(argv[i + 1], "off")) J2kParam->SopOn = 0;
			i += 2;
		} else if ((!strcmp(argv[i], "-EPH"))) {
			if (!strcmp(argv[i + 1], "OFF")) J2kParam->EphOn = 0;
			if (!strcmp(argv[i + 1], "off")) J2kParam->EphOn = 0;
			i += 2;
		} else if ((!strcmp(argv[i], "-SQCD"))) {
			if (!strcmp(argv[i + 1], "NoQ")) J2kParam->Sqcd = 0;
			if (!strcmp(argv[i + 1], "DERIVED")) J2kParam->Sqcd = 1;
			if (!strcmp(argv[i + 1], "derived")) J2kParam->Sqcd = 1;
			if (!strcmp(argv[i + 1], "TABLE")) J2kParam->Sqcd = 2;
			if (!strcmp(argv[i + 1], "table")) J2kParam->Sqcd = 2;
			if (J2kParam->Sqcd == 2) {
				J2kParam->Q_select = (char) (atoi(argv[i + 2]));
				i += 3;
			} else
				i += 2;
		} else if ((!strcmp(argv[i], "-ROI"))) {
			J2kParam->Roi_fname = new char[256];
			J2kParam->ROI       = 1;
			strcpy(J2kParam->Roi_fname, argv[i + 1]);
			i += 2;
		} else if ((!strcmp(argv[i], "-RateMode"))) {
			if (!strcmp(argv[i + 1], "byteLimit")) J2kParam->RateControl = RATE_CONTROL_ByteLimit; // 2
			if (!strcmp(argv[i + 1], "rate")) J2kParam->RateControl = RATE_CONTROL_RateValue;      // 1
			if (!strcmp(argv[i + 1], "off")) J2kParam->RateControl = RATE_CONTROL_OFF;             // 0
			i += 2;
		} else if ((!strcmp(argv[i], "-Rate"))) {
			rateD                 = atoi(argv[i + 1]);
			J2kParam->rate        = (float) ((float) rateD / (float) 1000.0);
			J2kParam->Filter      = FILTER97;
			J2kParam->RateControl = RATE_CONTROL_RateValue;
			i += 2;
		} else if ((!strcmp(argv[i], "-ByteLimit"))) {
			J2kParam->byteLimit   = atoi(argv[i + 1]);
			J2kParam->Filter      = FILTER97;
			J2kParam->RateControl = RATE_CONTROL_ByteLimit;
			i += 2;
		} else if ((!strcmp(argv[i], "-Qselect"))) {
			J2kParam->Q_select = (char) atoi(argv[i + 1]);
			i += 2;
		} else if ((!strcmp(argv[i], "-Q0"))) {
			J2kParam->Q_value0 = (byte2) atoi(argv[i + 1]);
			i += 2;
		} else if ((!strcmp(argv[i], "-Q1"))) {
			J2kParam->Q_value1 = (byte2) atoi(argv[i + 1]);
			i += 2;
		} else if ((!strcmp(argv[i], "-W"))) {
			width = atoi(argv[i + 1]);
			RawMode++;
			i += 2;
		} else if ((!strcmp(argv[i], "-H"))) {
			height = atoi(argv[i + 1]);
			RawMode++;
			i += 2;
		} else if ((!strcmp(argv[i], "-numCmpts"))) {
			numCmpts = (ubyte2) atoi(argv[i + 1]);
			RawMode++;
			i += 2;
		} else if ((!strcmp(argv[i], "-numBitDipth"))) {
			numBitDipth = (char) atoi(argv[i + 1]);
			RawMode++;
			i += 2;
		} else if ((!strcmp(argv[i], "-Sign"))) {
			Sign = (uchar) atoi(argv[i + 1]);
			RawMode++;
			i += 2;
		} else if ((!strcmp(argv[i], "-PrcW"))) {
			PrcW = (uchar) atoi(argv[i + 1]);
			for (j = 0; j < 16; j++)
				J2kParam->PrcW[j] = PrcW;
			i += 2;
		} else if ((!strcmp(argv[i], "-PrcH"))) {
			PrcH = (uchar) atoi(argv[i + 1]);
			for (j = 0; j < 16; j++)
				J2kParam->PrcH[j] = PrcH;
			i += 2;
		} else if ((!strcmp(argv[i], "-MC_OFF"))) { // only for decoder
			J2kParam->MC_OFF = 1;
			i++;
		} else if ((!strcmp(argv[i], "-Sign_OFF"))) {
			J2kParam->Sign_OFF = 1;
			i++;
		} else if ((!strcmp(argv[i], "-DEC_DecompLev"))) {
			J2kParam->DEC_DecompLev = (char) atoi(argv[i + 1]);
			i += 2;
		} else if ((!strcmp(argv[i], "-debug"))) {
			J2kParam->debugFlag = 1;
			strcpy(J2kParam->debugFname, fname2);
			i++;
		}
		//		else if( (!strcmp(argv[i],"-HW")) ) {
		//			J2kParam->HW=1;
		//			J2kParam->HW_DecimalPoint = (char)atoi( argv[i+1] );
		//			J2kParam->HW_DisCard = (char)atoi( argv[i+2] );
		//			i+=3;
		//		}
		//		else if( (!strcmp(argv[i],"-HT_ALPHA_TEST")) ) {
		//			J2kParam->HT_ALPHA_TEST=1;
		//			J2kParam->HT_ccc= (ubyte2)atoi(argv[i+1] );
		//			J2kParam->HT_rrr= (char)atoi(argv[i+2] );
		//			i+=3;
		//		}
		//		else if( (!strcmp(argv[i],"-HT_BETA_TEST")) ) {
		//			J2kParam->HT_BETA_TEST=1;
		//			J2kParam->HT_ccc= (ubyte2)atoi(argv[i+1] );
		//			J2kParam->HT_rrr= (char)atoi(argv[i+2] );
		//			i+=3;
		//		}
		//		else if( (!strcmp(argv[i],"-HT_GAMMA_TEST")) ) {
		//			J2kParam->HT_GAMMA_TEST=1;
		//			J2kParam->HT_ccc= (ubyte2)atoi(argv[i+1] );
		//			J2kParam->HT_rrr= (char)atoi(argv[i+2] );
		//			i+=3;
		//		}
		//		else if( (!strcmp(argv[i],"-HT_DELTA_TEST")) ) {
		//			J2kParam->HT_DELTA_TEST=1;
		//			J2kParam->HT_ccc= (ubyte2)atoi(argv[i+1] );
		//			J2kParam->HT_rrr= (char)atoi(argv[i+2] );
		//			i+=3;
		//		}
		//		else if( (!strcmp(argv[i],"-HT_KAPPA0_TEST")) ) {
		//			J2kParam->HT_KAPPA0_TEST=1;
		//			J2kParam->HT_ccc= (ubyte2)atoi(argv[i+1] );
		//			J2kParam->HT_rrr= (char)atoi(argv[i+2] );
		//			i+=3;
		//		}
		//		else if( (!strcmp(argv[i],"-HT_KAPPA1_TEST")) ) {
		//			J2kParam->HT_KAPPA1_TEST=1;
		//			J2kParam->HT_ccc= (ubyte2)atoi(argv[i+1] );
		//			J2kParam->HT_rrr= (char)atoi(argv[i+2] );
		//			i+=3;
		//		}
		//		else if( (!strcmp(argv[i],"-HT_WAVE_TEST")) ) {
		//			J2kParam->HT_WAVE_TEST=1;
		//			J2kParam->HT_DelayAlpha = (byte4)atoi(argv[i+1] );
		//			J2kParam->HT_DelayBeta = (byte4)atoi(argv[i+2] );
		//			J2kParam->HT_DelayGamma = (byte4)atoi(argv[i+3] );
		//			J2kParam->HT_DelayDelta = (byte4)atoi(argv[i+4] );
		//			J2kParam->HT_DelayKappa0 = (byte4)atoi(argv[i+5] );
		//			J2kParam->HT_DelayKappa1 = (byte4)atoi(argv[i+6] );
		//			J2kParam->HT_TG = (byte4)atoi(argv[i+7] );
		//			J2kParam->HT_Width_Margin = (byte4)atoi(argv[i+8] );
		//			J2kParam->HT_Height_Margin = (byte4)atoi(argv[i+9] );
		//			i+=10;
		//		}
		//		else if( (!strcmp(argv[i],"-HT_CleanUP_CX0_TEST")) ) {
		//			J2kParam->HT_CleanUP_CX0_TEST=1;
		//			J2kParam->HT_ccc= (ubyte2)atoi(argv[i+1] );
		//			J2kParam->HT_rrr= (char)atoi(argv[i+2] );
		//			J2kParam->HT_bbb= (char)atoi(argv[i+3] );
		//			J2kParam->HT_kkk= (byte4)atoi(argv[i+4] );
		//			i+=5;
		//		}
		//		else if( (!strcmp(argv[i],"-HT_CleanUP_ALL_TEST")) ) {
		//			J2kParam->HT_CleanUP_ALL_TEST=1;
		//			J2kParam->HT_ccc= (ubyte2)atoi(argv[i+1] );
		//			J2kParam->HT_rrr= (char)atoi(argv[i+2] );
		//			J2kParam->HT_bbb= (char)atoi(argv[i+3] );
		//			J2kParam->HT_kkk= (byte4)atoi(argv[i+4] );
		//			i+=5;
		//		}
		else
			i++;
	}

	encdec = (!strcmp(fileForm2, "j2c")) || (!strcmp(fileForm2, "j2k"));
	strcat(fname1, ".");
	strcat(fname1, fileForm1);
	strcat(fname2, ".");
	strcat(fname2, fileForm2);
	printf("IN=[%s],OUT=[%s]\n", fname1, fname2);

	if (J2kParam->debugFlag) {
		FILE *fp_log;
		strcat(J2kParam->debugFname, ".log");
		fp_log = fopen(J2kParam->debugFname, "w");
		fprintf(fp_log, "IN=[%s],OUT=[%s]\n", fname1, fname2);
		fprintf(fp_log, "log=[%s]\n", J2kParam->debugFname);
		fclose(fp_log);
		printf("log=[%s]\n", J2kParam->debugFname);
	}

	// encoder
	if (encdec) {
		if (!(strcmp(fileForm1, "bmp") && strcmp(fileForm1, "BMP") && strcmp(fileForm1, "Bmp"))) {
			Image1           = (struct Image_s *) LoadBmp(fname1);
			J2kParam->metric = METRIC_RGB;
		} else if (!(strcmp(fileForm1, "tif") && strcmp(fileForm1, "TIF") && strcmp(fileForm1, "Tif"))) {
			if (nullptr == (Image1 = (struct Image_s *) LoadTif(fname1))) {
				printf("[main] input file load erroe\n");
				return EXIT_FAILURE;
			}
			J2kParam->metric = METRIC_RGB;
		} else if (!(strcmp(fileForm1, "ppm") && strcmp(fileForm1, "PPM") && strcmp(fileForm1, "Pgm"))) {
			if (nullptr == (Image1 = (struct Image_s *) LoadPpm(fname1, TT_BIG_ENDIAN))) {
				printf("[main]:: input file load erroe\n");
				return EXIT_FAILURE;
			}
			J2kParam->metric = METRIC_RGB;
		} else if (!(strcmp(fileForm1, "raw") && strcmp(fileForm1, "RAW") && strcmp(fileForm1, "Raw"))) {
			if ((RawMode != 4) && (RawMode != 5)) {
				printf("[main]:: input file load erroe\n");
				return EXIT_FAILURE;
			}
			if (nullptr
			    == (Image1 =
			            (struct Image_s *) LoadRAW(fname1, numCmpts, numBitDipth, width, height, Sign))) {
				printf("[main] input file load erroe\n");
				return EXIT_FAILURE;
			}
			J2kParam->metric = METRIC_RGB;
		}

		if (nullptr == (str = StreamChainMake(nullptr, BUF_LENGTH, JPEG2000))) {
			printf("[main]:: StreamChainMake error\n");
			return EXIT_FAILURE;
		}

		if (EXIT_SUCCESS != J2K_EncMain(Image1, str, J2kParam)) {
			printf("[main]:: J2K_EncMain error\n");
			return EXIT_FAILURE;
		}

		if (EXIT_SUCCESS != StreamToFile(fname2, str)) {
			printf("[main]:: StreamToFile error\n");
			return EXIT_FAILURE;
		}
	}
	// decoder
	else {
		if (NULL == (fp = fopen(fname1, "rb"))) {
			printf("[main]:: j2c file open error!\n");
			return EXIT_FAILURE;
		}
		fseek(fp, 0, SEEK_END);
		buf_length = ftell(fp);
		fclose(fp);

		CodeStream = nullptr;
		if (nullptr == (CodeStream = StreamChainMake(CodeStream, buf_length, JPEG2000))) {
			printf("[main]:: StreamChainMake error, when decode code stream make.\n");
			return EXIT_FAILURE;
		}
		CodeStream->cur_p = 0;
		fp                = fopen(fname1, "rb");
		fread(&CodeStream->buf[0], sizeof(uchar), buf_length, fp);
		fclose(fp);

		if (EXIT_SUCCESS != J2k_DecMain(CodeStream, J2kParam)) {
			printf("[main]:: J2k_DecMain error!\n");
			return EXIT_FAILURE;
		}
	}
	printf("successly complete!\n");
	return EXIT_SUCCESS;
}
