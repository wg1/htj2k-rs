/*
Copyright (c) 2000-2021, ICT-Link
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the HTJ2K project.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "j2k2.h"
#include "ImageUtil.h"
#include "wavelet_codec.h"
#include "Icc.h"
#include "t2codec.h"
#include "annexA.h"
#include "annexB.h"
#include "t1codec.h"
#include "annexE.h"
#include "annexH.h"
#include "MqCodec.h"
#include "RateControl.h"
#include "vlc_codec.h"
#include "MelCodec.h"

byte4 MainHeaderDec(struct Codec_s *Codec, struct StreamChain_s *s, struct J2kParam_s *J2kParam) {
	struct Com_s *Com;
	byte4 Coc_missingAddr = 0, Qcd_missingAddr = 0, Qcc_missingAddr = 0;
	byte4 ini_address;
	ubyte2 Marker;
	char flag         = 1;
	char flagstateCOD = 0, flagstateQCD = 0, firststatePPM = 0;
	char MissingMarker = 0;
	char RGN_flag = 0, POC_flag = 0;

	// 0:
	// 1:COD done QCD yet
	// 2:COD done QCD done
	// 3:COD yet  QCD yet
	// 4:COD yet  QCD done

	//	printf("[MainHeaderDec]\n");

	ini_address = s->cur_p;
	while (flag) {
		Marker = Ref_2Byte(s);
		switch (Marker) {
			case SIZ:
				break;
			case COD: // 0xff52
				if (!flagstateCOD) {
					Codec->ECod  = ECodDec(s, Codec, J2kParam);
					flagstateCOD = 1;
					if (MissingMarker & 2) s->cur_p = Qcd_missingAddr;
				} else
					MarkerSkip(s);
				break;
			case COC:
				if (flagstateCOD) {
					ECocDec(Codec->ECod, s, -1, Codec);
					MissingMarker &= 0xfe;
				} else {
					Coc_missingAddr = s->cur_p - 2;
					MissingMarker |= 1;
					MarkerSkip(s);
				}
				break;
			case QCD: // 0xff5c
				if (flagstateCOD) {
					Codec->EQcd  = EQcdDec(Codec->ECod, s, Codec->Siz);
					flagstateQCD = 1;
					MissingMarker &= 0xfd;
				} else {
					Qcd_missingAddr = s->cur_p - 2;
					MissingMarker |= 2;
					MarkerSkip(s);
				}
				break;
			case QCC: // 0xff5d
				if (flagstateQCD) {
					EQccMarDec(Codec->EQcd, Codec->ECod, s, NULL);
					MissingMarker &= 0xfb;
				} else {
					Qcc_missingAddr = s->cur_p - 2;
					MissingMarker |= 4;
					MarkerSkip(s);
				}
				break;
			case COM: // 0xff64
				Com = ComDec(s);
				break;
			case CAP:
				Codec->Cap = CapDec(s /* Codec*/);
				break;
			case CPF:
				MarkerSkip(s);
				break;
			case RGN:
				if (RGN_flag == 0) {
					if (nullptr == (Codec->ERgn = ERgnCreate(Codec->ECod->numCompts))) {
						printf("[MainHeaderDec]::ERgnCreate error\n");
						return EXIT_FAILURE;
					}
					RGN_flag = 1;
				}
				if (EXIT_FAILURE == ERgnMarDec(Codec->ERgn, s)) {
					printf("[MainHeaderDec]::ERgnMarDec error\n");
					return EXIT_FAILURE;
				}
				break;
			case POC: // 0xff5f
				POC_flag = 1;
				if (nullptr == (Codec->Poc = PocDec(nullptr, s, Codec))) {
					printf("[MainHeaderDec]::PocDec error\n");
					return EXIT_FAILURE;
				}
				break;
			case PPM:
				if (firststatePPM == 0) {
					firststatePPM    = 1;
					Codec->ppm_Saddr = new byte4[Codec->Siz->numTiles * 256];
					memset(Codec->ppm_Saddr, 0, sizeof(byte4) * Codec->Siz->numTiles * 256);
					Codec->ppm_Saddr2 = new byte4[Codec->Siz->numTiles * 256];
					memset(Codec->ppm_Saddr2, 0, sizeof(byte4) * Codec->Siz->numTiles * 256);
					Codec->ppm_Length = new byte4[Codec->Siz->numTiles * 256];
					memset(Codec->ppm_Length, 0, sizeof(byte4) * Codec->Siz->numTiles * 256);
					Codec->ppm_Length2 = new byte4[Codec->Siz->numTiles * 256];
					memset(Codec->ppm_Length2, 0, sizeof(byte4) * Codec->Siz->numTiles * 256);
				}
				PpmDec(Codec, s);
				break;
			case TLM:
				MarkerSkip(s);
				break;
			case PLM:
				MarkerSkip(s);
				break;
			case CRG: // 0xff63
				MarkerSkip(s);
				break;
			case SOT:
				if (EXIT_FAILURE == SotDec(Codec, s)) {
					printf("[MainHeaderDec]::SotDec error\n");
					return EXIT_FAILURE;
				}
				break;
			case 0xff30:
				break;
			case EOC:
				flag = 0;
				break;
			default:
				printf("[MainHeaderDec]::Invalid marker happend error Marker=,%x\n", Marker);
				return EXIT_FAILURE;
				break;
		}
	}

	if (J2kParam->debugFlag) {
		FILE *fp_log;
		byte4 tileCmpts;
		ubyte2 ccc;
		uchar rrr, bbb2, bbb = 0;

		fp_log = fopen(J2kParam->debugFname, "a+");
		fprintf(fp_log, "[MAIN_HEADER]\n");
		fprintf(fp_log, "[SIZ]\n");
		fprintf(fp_log, "Csiz=%d ", Codec->Siz->Csiz);
		fprintf(fp_log, "Rsiz=%x(h) ", Codec->Siz->Rsiz);
		fprintf(fp_log, "Xsiz=%d ", Codec->Siz->Xsiz);
		fprintf(fp_log, "Ysiz=%d ", Codec->Siz->Ysiz);
		fprintf(fp_log, "XTsiz=%d ", Codec->Siz->XTsiz);
		fprintf(fp_log, "YTsiz=%d ", Codec->Siz->YTsiz);
		fprintf(fp_log, "XOsiz=%d ", Codec->Siz->XOsiz);
		fprintf(fp_log, "YOsiz=%d ", Codec->Siz->YOsiz);
		fprintf(fp_log, "XTOsiz=%d ", Codec->Siz->XTOsiz);
		fprintf(fp_log, "YTOsiz=%d\n", Codec->Siz->YTOsiz);
		for (ccc = 0; ccc < Codec->Siz->Csiz; ccc++) {
			fprintf(fp_log, "<Ssiz=%x(h) ", (Codec->Siz->Ssiz[ccc]) & 0xff);
			fprintf(fp_log, "XRsiz=%d ", (Codec->Siz->XRsiz[ccc]) & 0xff);
			fprintf(fp_log, "YRsiz=%d>", (Codec->Siz->YRsiz[ccc]) & 0xff);
		}
		fprintf(fp_log, "\n[COD]\n");
		for (ccc = 0; ccc < Codec->Siz->Csiz; ccc++) {
			tileCmpts = ccc * Codec->ECod->numTiles;
			fprintf(fp_log, "DecompLev=%d ", Codec->ECod->EECod[tileCmpts].DecompLev);
			fprintf(fp_log, "Cbw=%d ", Codec->ECod->EECod[tileCmpts].CbW);
			fprintf(fp_log, "Cbh=%d ", Codec->ECod->EECod[tileCmpts].CbH);
			fprintf(fp_log, "CbBypass=%d ", Codec->ECod->EECod[tileCmpts].CbBypass);
			fprintf(fp_log, "CbReset=%d ", Codec->ECod->EECod[tileCmpts].CbReset);
			fprintf(fp_log, "CbTerm=%d ", Codec->ECod->EECod[tileCmpts].CbTerm);
			fprintf(fp_log, "CbVcausal=%d ", Codec->ECod->EECod[tileCmpts].CbVcausal);
			fprintf(fp_log, "CbPreterm=%d ", Codec->ECod->EECod[tileCmpts].CbPreterm);
			fprintf(fp_log, "CbSegmentSymbol=%d ", Codec->ECod->EECod[tileCmpts].CbSegmentSymbol);
			fprintf(fp_log, "EphOn=%d ", Codec->ECod->EECod[tileCmpts].EphOn);
			fprintf(fp_log, "SopOn=%d ", Codec->ECod->EECod[tileCmpts].SopOn);
			fprintf(fp_log, "ProgOrder=%d ", Codec->ECod->EECod[tileCmpts].ProgOrder);
			fprintf(fp_log, "numLayer=%d ", Codec->ECod->EECod[tileCmpts].numLayer);
			fprintf(fp_log, "MC=%d ", Codec->ECod->EECod[tileCmpts].MC);
			fprintf(fp_log, "Scod=0x%x ", Codec->ECod->EECod[tileCmpts].Scod);
			fprintf(fp_log, "Filter=%d\n", Codec->ECod->EECod[tileCmpts].Filter);
			if (Codec->ECod->EECod[tileCmpts].Scod & 1) {
				for (rrr = 0; rrr <= Codec->ECod->EECod[tileCmpts].DecompLev; rrr++) {
					fprintf(fp_log, "\t\tPrcW[%d]=%d,PrcH[%d]=%d\n", rrr,
					        Codec->ECod->EECod[tileCmpts].PrcW[rrr], rrr,
					        Codec->ECod->EECod[tileCmpts].PrcH[rrr]);
				}
			}
		}
		fprintf(fp_log, "[QOD]\n");
		for (ccc = 0; ccc < Codec->Siz->Csiz; ccc++) {
			tileCmpts = ccc * Codec->ECod->numTiles;
			bbb2      = 0;
			fprintf(fp_log, "\te[%d]=%d,", bbb2, Codec->EQcd->EEQcd[tileCmpts].e[bbb2]);
			fprintf(fp_log, "myu[%d]=%x\n", bbb2, Codec->EQcd->EEQcd[tileCmpts].myu[bbb2]);
			bbb2++;
			for (rrr = 1; rrr <= Codec->ECod->EECod[tileCmpts].DecompLev; rrr++) {
				for (bbb = 0; bbb < 3; bbb++) {
					fprintf(fp_log, "\te[%d]=%d,", bbb2, Codec->EQcd->EEQcd[tileCmpts].e[bbb2]);
					fprintf(fp_log, "myu[%d]=%x\n", bbb2, Codec->EQcd->EEQcd[tileCmpts].myu[bbb2]);
					bbb2++;
				}
			}
			fprintf(fp_log, "\tG=%d,", Codec->EQcd->EEQcd[tileCmpts].G);
			fprintf(fp_log, "Sqcd=%d\n", Codec->EQcd->EEQcd[tileCmpts].Sqcd);
		}
		if (RGN_flag) {
			fprintf(fp_log, "[RGN]\n");
			for (ccc = 0; ccc < Codec->Siz->Csiz; ccc++) {
				fprintf(fp_log, "ccc=%d,ROI-Compts=%d, MarkerLength=%d, Srgn=%d, shift=%d\n", ccc,
				        Codec->ERgn->EERgn[ccc].Crgn, Codec->ERgn->EERgn[ccc].Lrgn,
				        Codec->ERgn->EERgn[ccc].Srgn, Codec->ERgn->EERgn[ccc].SPrgn);
			}
		}
		if (Codec->ppm_flag) fprintf(fp_log, "[PPM]\n");
		if (POC_flag) fprintf(fp_log, "[POC]\n");
		fclose(fp_log);
	}

	return EXIT_SUCCESS;
}

byte4 TileHeaderDec(struct Tile_s *t, struct Codec_s *Codec, struct StreamChain_s *s, char mode,
                    struct J2kParam_s *J2kParam) {
	ubyte2 Marker;
	ubyte2 Lppt;
	uchar Zppt;
	char flag     = 1;
	char COD_flag = 0, QCD_flag = 0, COC_flag = 0, QCC_flag = 0, RGN_flag = 0, POC_flag = 0;

	//	printf("[TileHeaderDec]\n");

	flag = 1;
	while (flag) {
		Marker = Ref_2Byte(s);
		switch (Marker) {
			case COD:
				COD_flag = 1;
				if (mode)
					ECodDecTilePart(s, Codec->Siz, Codec->ECod, t->T);
				else
					MarkerSkip(s);
				break;
			case COC:
				COC_flag = 1;
				if (mode)
					ECocDec(Codec->ECod, s, t->T, Codec);
				else
					MarkerSkip(s);
				break;
			case QCD:
				QCD_flag = 1;
				if (mode)
					EQcdDecTilePart(Codec->EQcd, Codec->ECod, s, t->T, Codec->Siz);
				else
					MarkerSkip(s);
				break;
			case QCC:
				QCC_flag = 1;
				if (mode)
					EQccMarDec(Codec->EQcd, Codec->ECod, s, t->T);
				else
					MarkerSkip(s);
				break;
			case SOD:
				flag = 0;
				break;
			case PPT:
				if (mode) {
					Lppt       = Ref_2Byte(s);
					Zppt       = Ref_1Byte(s);
					t->str_ppt = StreamChainMake_B(t->str_ppt, s->buf, s->cur_p, Lppt, JPEG2000);
					t->ppt_flag++;
					s->cur_p += (Lppt - 3);
				} else
					MarkerSkip(s);
				break;
			case RGN: // 0xff5e
				RGN_flag = 1;
				if (mode) {
					if (nullptr == (t->ERgn = ERgnCreate(Codec->ECod->numCompts))) {
						printf("[TileHeaderDec]::ERgnCreate error\n");
						return EXIT_FAILURE;
					}
					ERgnMarDec(t->ERgn, s);
				} else
					MarkerSkip(s);
				break;
			case POC:
				POC_flag = 1;
				if (mode) {
					if (nullptr == (Codec->tilePoc = PocDec(Codec->tilePoc, s, Codec))) {
						printf("[TileHeaderDec]::PocDec error\n");
						return EXIT_FAILURE;
					}
				} else {
					//				printf( "[TileHeaderDec] skip Codec->tilePoc=%x\n", Codec->tilePoc );
					MarkerSkip(s);
				}
				break;
			case PLT:
				MarkerSkip(s);
				break;
			case COM:
				MarkerSkip(s);
				break;
			default:
				printf("[TileHeaderDec]:: vail marker happend\n");
				return EXIT_FAILURE;
				break;
		}
	}
	if (mode) {
		if (J2kParam->debugFlag) {
			FILE *fp_log;
			byte4 tileCmpts;
			ubyte2 ccc;
			uchar rrr, bbb2, bbb;
			fp_log = fopen(J2kParam->debugFname, "a+");
			fprintf(fp_log, "\n[TILE_HEADER] TileNo=%d\n", t->T);
			if (COD_flag || COC_flag) {
				fprintf(fp_log, "[TILE COD](%d)\n", t->T);
				for (ccc = 0; ccc < Codec->Siz->Csiz; ccc++) {
					tileCmpts = ccc * Codec->ECod->numTiles + t->T;
					fprintf(fp_log, "<< COD/COC[tileCmpts = %d]>>\n", tileCmpts);
					fprintf(fp_log, "DecompLev=%d ", Codec->ECod->EECod[tileCmpts].DecompLev);
					fprintf(fp_log, "Cbw=%d ", Codec->ECod->EECod[tileCmpts].CbW);
					fprintf(fp_log, "Cbh=%d ", Codec->ECod->EECod[tileCmpts].CbH);
					fprintf(fp_log, "CbBypass=%d ", Codec->ECod->EECod[tileCmpts].CbBypass);
					fprintf(fp_log, "CbReset=%d ", Codec->ECod->EECod[tileCmpts].CbReset);
					fprintf(fp_log, "CbTerm=%d ", Codec->ECod->EECod[tileCmpts].CbTerm);
					fprintf(fp_log, "CbVcausal=%d ", Codec->ECod->EECod[tileCmpts].CbVcausal);
					fprintf(fp_log, "CbPreterm=%d ", Codec->ECod->EECod[tileCmpts].CbPreterm);
					fprintf(fp_log, "CbSegmentSymbol=%d ", Codec->ECod->EECod[tileCmpts].CbSegmentSymbol);
					fprintf(fp_log, "EphOn=%d ", Codec->ECod->EECod[tileCmpts].EphOn);
					fprintf(fp_log, "SopOn=%d ", Codec->ECod->EECod[tileCmpts].SopOn);
					fprintf(fp_log, "ProgOrder=%d ", Codec->ECod->EECod[tileCmpts].ProgOrder);
					fprintf(fp_log, "numLayer=%d ", Codec->ECod->EECod[tileCmpts].numLayer);
					fprintf(fp_log, "MC=%d ", Codec->ECod->EECod[tileCmpts].MC);
					fprintf(fp_log, "Scod=0x%x ", Codec->ECod->EECod[tileCmpts].Scod);
					fprintf(fp_log, "Filter=%d\n", Codec->ECod->EECod[tileCmpts].Filter);
					if (Codec->ECod->EECod[tileCmpts].Scod & 1) {
						for (rrr = 0; rrr <= Codec->ECod->EECod[tileCmpts].DecompLev; rrr++) {
							fprintf(fp_log, "\t\tPrcW[%d]=%d,PrcH[%d]=%d\n", rrr,
							        Codec->ECod->EECod[tileCmpts].PrcW[rrr], rrr,
							        Codec->ECod->EECod[tileCmpts].PrcH[rrr]);
						}
					}
				}
			}
			if (QCD_flag || QCC_flag) {
				fprintf(fp_log, "[TILE QOD](%d)\n", t->T);
				for (ccc = 0; ccc < Codec->Siz->Csiz; ccc++) {
					tileCmpts = ccc * Codec->ECod->numTiles + t->T;
					bbb2      = 0;
					fprintf(fp_log, "<< QCD e myu [tileCmpts = %d]>>\n", tileCmpts);
					fprintf(fp_log, "\te[%d]=%d,", bbb2, Codec->EQcd->EEQcd[tileCmpts].e[bbb2]);
					fprintf(fp_log, "myu[%d]=%x\n", bbb2, Codec->EQcd->EEQcd[tileCmpts].myu[bbb2]);
					bbb2++;
					for (rrr = 1; rrr <= Codec->ECod->EECod[tileCmpts].DecompLev; rrr++) {
						for (bbb = 0; bbb < 3; bbb++) {
							fprintf(fp_log, "\te[%d]=%d,", bbb2, Codec->EQcd->EEQcd[tileCmpts].e[bbb2]);
							fprintf(fp_log, "myu[%d]=%x\n", bbb2, Codec->EQcd->EEQcd[tileCmpts].myu[bbb2]);
							bbb2++;
						}
					}
					fprintf(fp_log, "\tG=%d,", Codec->EQcd->EEQcd[tileCmpts].G);
					fprintf(fp_log, "Sqcd=%d\n", Codec->EQcd->EEQcd[tileCmpts].Sqcd);
				}
			}
			if (RGN_flag) {
				fprintf(fp_log, "[Tile RGN]\n");
				for (ccc = 0; ccc < Codec->Siz->Csiz; ccc++) {
					fprintf(fp_log, "ccc=%d,ROI-Compts=%d, MarkerLength=%d, Srgn=%d, shift=%d\n", ccc,
					        t->ERgn->EERgn[ccc].Crgn, t->ERgn->EERgn[ccc].Lrgn, t->ERgn->EERgn[ccc].Srgn,
					        t->ERgn->EERgn[ccc].SPrgn);
				}
			}
			if (t->ppt_flag) fprintf(fp_log, "[PPT] %d\n", t->ppt_flag);
			if (POC_flag) fprintf(fp_log, "[POC]\n");
			fclose(fp_log);
		}
	}
	return EXIT_SUCCESS;
}

struct Packet_s *PacketInfoControl(struct J2kParam_s *J2kParam, struct Codec_s *Codec, struct Tile_s *Tile,
                                   /*struct wavelet_s *Wave, byte4 TilePartNo,*/ struct Packet_s *Ps,
                                   /*uchar *ON_reso,*/ struct StreamChain_s *strPI, byte4 PI_Saddr,
                                   struct StreamChain_s *strPC, byte4 Tile_Saddr,
                                   byte4 Tile_Eaddr /*, uchar FlagFlag*/) {
	uchar flag;
	struct EECod_s *EECod = nullptr;
	byte4 Saddr_PI, Saddr_PC;
	byte4 packetNo;
	uchar HTJ2K;
	FILE *fp_log;

	//	printf("[PacketInfoControl]\n" );

	strPI->cur_p = PI_Saddr;
	HTJ2K        = (uchar) (((Codec->Siz->Rsiz & 0x4000) >> 14));
	flag         = 0;
	Saddr_PI     = PI_Saddr;
	Saddr_PC     = Tile_Saddr;
	if (strPI->cur_p != Tile_Eaddr) flag = 1;
	while (flag) {
		EECod               = &Codec->ECod->EECod[Ps->cmptsNo * Codec->ECod->numTiles + Tile->T];
		Ps->SadrPI          = Saddr_PI;
		Ps->Sadr_PacketCode = Saddr_PC;
		packetNo            = Ps->packetNo;
		if (nullptr
		    == (Ps = PacketInfoDec(
		            Ps, strPI, strPC, EECod,
		            (Codec->ppm_flag
		             || Tile->ppt_flag)))) { // PacketInfoDec�͒P��p�P�b�g�̃f�R�[�h��Cblk�ւ�Zero,numPasses,Cblk->length�̔z�M�p�X�P�ʂɃA�h���X�ƃ����O�X���i�[�����B
			printf("[TileDec]::PacketInfoDec (Tile number=%d Packet_Number=%d) Error\n", Tile->T, packetNo);
			return nullptr;
		}
		if (Codec->ppm_flag || Tile->ppt_flag) {
			Saddr_PI = Ps->EadrPI;
			Saddr_PC = Ps->Eadr_PacketCode;
		} else {
			Saddr_PI = Ps->Eadr_PacketCode;
			Saddr_PC = Ps->Eadr_PacketCode;
		}
		if (Tile_Eaddr == Ps->Eadr_PacketCode) {
			if (J2kParam->debugFlag) {
				fp_log = fopen(J2kParam->debugFname, "a+");
				fprintf(fp_log, "This tile(tile_number=%d) packets information dec is completely SUCCESS\n",
				        Tile->T);
				fclose(fp_log);
			}
			flag = 0;
		} else {
			if (Tile->numPackets - 1 == Ps->packetNo) {
				printf("This tile(tile_number=%d) packets information dec is FAILURE. Exit\n", Tile->T);
				return nullptr;
			}
		}
		++Ps;
	}
	return Ps;
}

byte4 DecMainBody(struct Tile_s *t, struct mqdec_s *dec, struct EECod_s *EECod, struct Codec_s *Codec,
                  struct StreamChain_s *stream, ubyte2 ccc) {
	struct ERgn_s *ERgn;
	struct EERgn_s *EERgn;
	struct EPrc_s *EPrc;
	struct Cblk_s *sCb, *eCb;
	byte4 kkk, numCblks;
	byte4 ppp, numPrc;
	uchar rrr, bbb;
	uchar *ctx_table;

	//	printf("[DecMainBody]::\n" );

	ERgn  = nullptr;
	EERgn = nullptr;
	if (t->ERgn != nullptr) {
		ERgn  = t->ERgn;
		EERgn = &ERgn->EERgn[ccc];
	} else {
		if (Codec->ERgn == nullptr) {
			ERgn  = nullptr;
			EERgn = nullptr;
		} else {
			ERgn  = Codec->ERgn;
			EERgn = &ERgn->EERgn[ccc];
		}
	}
	for (rrr = 0; rrr <= EECod->DecompLev; rrr++) {
		numPrc = t->Cmpt[ccc].EEPrc[rrr].numPrc;
		for (ppp = 0; ppp < numPrc; ppp++) {
			EPrc = &(t->Cmpt[ccc].EEPrc[rrr].EPrc[ppp]);
			for (bbb = 0; bbb < t->Cmpt[ccc].EEPrc[rrr].numBand; bbb++) {
				if (t->Cmpt[ccc].EEPrc[rrr].numBand == 1) {
					ctx_table = lhll_table2;
				} else {
					switch (bbb) {
						case 0:
							ctx_table = hl_table2;
							break;
						case 1:
							ctx_table = lhll_table2;
							break;
						case 2:
							ctx_table = hh_table2;
							break;
						default:
							return EXIT_FAILURE;
					}
				}
				numCblks = EPrc->Prc[bbb].numCblk;
				sCb      = &EPrc->Prc[bbb].Cblk[0];
				eCb      = &EPrc->Prc[bbb].Cblk[numCblks];
				for (kkk = 0; sCb != eCb; ++sCb, kkk++) {
					if (sCb->decOn) {
						if (sCb->HT_mode & 0x80) {
							if (EXIT_SUCCESS != T1Dec_HTJ2K(sCb, Codec, EECod, EERgn, stream, ccc)) {
								printf("[DecMainBody]:: T1Dec_HTJ2K Error(TileNo=%d ccc=%d rrr=%d ppp=%d "
								       "bbb=%d kkk=%d)\n",
								       t->T, ccc, rrr, ppp, bbb, kkk);
								return EXIT_FAILURE;
							}
						} else {
							if (EXIT_SUCCESS
							    != T1Dec(sCb, Codec, EECod, EERgn, dec, ctx_table, stream, ccc)) {
								printf("[DecMainBody]:: T1Dec Error(TileNo=%d ccc=%d rrr=%d ppp=%d bbb=%d "
								       "kkk=%d)\n",
								       t->T, ccc, rrr, ppp, bbb, kkk);
								return EXIT_FAILURE;
							}
						}
					}
				}
			}
		}
	}

	return EXIT_SUCCESS;
}

byte4 TileDec(struct J2kParam_s *J2kParam, struct Image_s *Image, struct StreamChain_s *s,
              struct Codec_s *Codec, struct mqdec_s *dec, byte4 tile_No, struct Image_s *ImageStream) {
	ubyte2 sot;
	ubyte2 Lsot, Isot;
	byte4 Psot;
	uchar TPsot, TNsot;
	struct wavelet_s *Wave = nullptr;
	struct Tile_s *Tile;
	struct EECod_s *EECod = nullptr;
	struct EEQcd_s *EEQcd;
	struct Packet_s *Ps, *Pe;
	struct StreamChain_s *str_ppm;
	byte4 Packet_Number, numPackets;
	byte4 numberOfPackets, maxDecompLev, maxnumPasses, maxnumPrc /*, PacketsCountS*/;
	byte2 *Cppp  = nullptr;
	uchar *flagD = nullptr;
	ubyte2 ccc;
	uchar rrr;
	uchar *ON_reso;
	uchar NL, NL_c;
	bool flag;
	byte4 Tile_Saddr, Tile_Eaddr;
	byte4 PI_Saddr;
	byte4 PacketNo;
	uchar FlagFlag = 0;
	uchar ttt, numTilePart;

	//	printf("[TileDec]\n" );

	Tile       = &Codec->Tile[tile_No];
	Tile->MAGB = 0;
	if (Codec->Cap != nullptr) Tile->MAGB = Codec->Cap->MAGB;
	numTilePart = Codec->Sot->numTilePart[tile_No];
	for (ttt = 0; ttt < numTilePart; ttt++) {
		s->cur_p = Codec->Sot->Saddr[tile_No][ttt];
		sot      = Ref_2Byte(s);
		Lsot     = Ref_2Byte(s);
		if (Lsot != 10) return EXIT_FAILURE;
		Isot  = Ref_2Byte(s);
		Psot  = Ref_4Byte(s);
		TPsot = Ref_1Byte(s);
		TNsot = Ref_1Byte(s);

		if (ttt == 0 && Codec->tilePoc != nullptr) {
			delete Codec->tilePoc;
			Codec->tilePoc = nullptr;
		}

		if (Codec->ppm_flag) {
			if (Codec->ppm_Length2[Tile->T]) {
				Tile->str_ppt = StreamChainMake(
				    nullptr, (Codec->ppm_Length[Tile->T] + Codec->ppm_Length2[Tile->T] - 4), JPEG2000);
				memcpy(Tile->str_ppt->buf, &s->buf[Codec->ppm_Saddr[Tile->T] + 4],
				       sizeof(uchar) * (Codec->ppm_Length[Tile->T] - 4));
				memcpy(&Tile->str_ppt->buf[Codec->ppm_Length[Tile->T] - 4],
				       &s->buf[Codec->ppm_Saddr2[Tile->T]], sizeof(uchar) * Codec->ppm_Length2[Tile->T]);
			} else
				Tile->str_ppt = StreamChainMake_B(Tile->str_ppt, s->buf, Codec->Sot->PI_Saddr[tile_No][ttt],
				                                  Codec->Sot->PI_Length[tile_No][ttt], JPEG2000);
		}
		TileHeaderDec(Tile, Codec, s, 1, J2kParam);
		//		printf( "[after TileHeaderDec] ttt=%d\n", ttt);
		if (!ttt) {
			Wave = new struct wavelet_s[Tile->numCompts];
			for (ccc = 0; ccc < Tile->numCompts; ccc++) {
				NL = Codec->ECod->EECod[ccc * Codec->ECod->numTiles + Tile->T].DecompLev;
				Wavelet_Create(&Wave[ccc], (Tile->Cmpt[ccc].tcx1 - Tile->Cmpt[ccc].tcx0),
				               (Tile->Cmpt[ccc].tcy1 - Tile->Cmpt[ccc].tcy0), NL);
			}
			for (ccc = 0; ccc < Tile->numCompts; ccc++) {
				EECod             = &Codec->ECod->EECod[ccc * Codec->ECod->numTiles + Tile->T];
				EEQcd             = &Codec->EQcd->EEQcd[ccc * Codec->ECod->numTiles + Tile->T];
				NL_c              = EECod->DecompLev;
				Image[ccc].tbx0   = Tile->Cmpt[ccc].tcx0;
				Image[ccc].tbx1   = Tile->Cmpt[ccc].tcx1;
				Image[ccc].tby0   = Tile->Cmpt[ccc].tcy0;
				Image[ccc].tby1   = Tile->Cmpt[ccc].tcy1;
				Image[ccc].height = Image[ccc].tby1 - Image[ccc].tby0;
				Image[ccc].width  = Image[ccc].tbx1 - Image[ccc].tbx0;
				WaveAdrCal(&Wave[ccc], /*Tile,*/ &Image[ccc], EECod, FlagFlag);
				Tile->Cmpt[ccc].data = (struct Image_s *) &Image[ccc];
				Tile->Cmpt[ccc].wave = &Wave[ccc];
				PrcAdrCal(Tile, &Wave[ccc], ccc, EECod /*, FlagFlag*/);
				if (EXIT_SUCCESS
				    != CblkAdrCal(/*J2kParam,*/ Codec, Tile->Cmpt[ccc].EEPrc, EECod,
				                  EEQcd /*, ccc, FlagFlag*/)) {
					printf("[TileDec] CblkAdrCal(ccc=%d)error\n", ccc);
					return EXIT_FAILURE;
				}
			}
			numPackets    = PacketCreate(Tile, Codec->ECod, 0);
			Ps            = &Tile->Packet[0];
			Pe            = &Tile->Packet[Tile->numPackets];
			Packet_Number = 0;

			maxDecompLev = 0;
			maxnumPasses = 0;
			for (ccc = 0; ccc < Codec->Siz->Csiz; ccc++) {
				maxDecompLev =
				    (maxDecompLev > Codec->ECod->EECod[ccc * Codec->ECod->numTiles + Tile->T].DecompLev)
				        ? maxDecompLev
				        : Codec->ECod->EECod[ccc * Codec->ECod->numTiles + Tile->T].DecompLev;
				maxnumPasses =
				    (maxnumPasses > Codec->ECod->EECod[ccc * Codec->ECod->numTiles + Tile->T].numLayer)
				        ? maxnumPasses
				        : Codec->ECod->EECod[ccc * Codec->ECod->numTiles + Tile->T].numLayer;
			}
			maxDecompLev++;
			maxnumPrc = 0;
			for (ccc = 0; ccc < Codec->Siz->Csiz; ccc++)
				for (rrr = 0; rrr <= Codec->ECod->EECod[ccc * Codec->ECod->numTiles + Tile->T].DecompLev;
				     rrr++)
					maxnumPrc = (maxnumPrc > Tile->Cmpt[ccc].EEPrc[rrr].numPrc)
					                ? maxnumPrc
					                : Tile->Cmpt[ccc].EEPrc[rrr].numPrc;

			Cppp = new byte2[Codec->Siz->Csiz * maxDecompLev];
			memset(Cppp, 0, sizeof(byte2) * Codec->Siz->Csiz * maxDecompLev);
			numberOfPackets = Codec->Siz->Csiz * maxDecompLev * maxnumPasses * maxnumPrc;
			flagD           = new uchar[numberOfPackets];
			memset(flagD, 0, sizeof(uchar) * numberOfPackets);
		}
		if (EXIT_SUCCESS != ProgCnt(Codec, Tile, ttt, Wave, Codec->ECod, Cppp, flagD, 1)) {
			printf("[TileDec]::ProgCnt(ttt=%d)error\n", Tile->T);
			return EXIT_FAILURE;
		}
	}

	if (EXIT_SUCCESS != ProgCnt(Codec, Tile, ttt, Wave, Codec->ECod, Cppp, flagD, 0)) {
		printf("[TileDec]::ProgCnt(ttt=%d)error\n", Tile->T);
		return EXIT_FAILURE;
	}

	TagtreeCreate2(Tile, 0, Tile->numPackets);

	Ps      = &Tile->Packet[0];
	flag    = 0;
	ON_reso = new uchar[Codec->Siz->Csiz];
	memset(ON_reso, 0xff, sizeof(uchar) * Codec->Siz->Csiz);
	PacketNo = 0;
	for (ttt = 0; ttt < numTilePart; ttt++) {
		s->cur_p   = Codec->Sot->Saddr[tile_No][ttt];
		Tile_Eaddr = Codec->Sot->Saddr[tile_No][ttt] + Codec->Sot->Psot[tile_No][ttt];
		Ref_2Byte(s);
		Ref_2Byte(s);
		Ref_2Byte(s);
		Ref_4Byte(s);
		Ref_1Byte(s);
		Ref_1Byte(s);
		TileHeaderDec(Tile, Codec, s, 0, J2kParam);
		if (Codec->ppm_flag || Tile->ppt_flag) {
			str_ppm  = Tile->str_ppt;
			PI_Saddr = str_ppm->cur_p;
		} else {
			str_ppm  = s;
			PI_Saddr = s->cur_p;
		}
		Tile_Saddr = s->cur_p; // after TileHeaderDec out, s->cur_p is pointing SODmarker out.
		if (nullptr
		    == (Ps = PacketInfoControl(J2kParam, Codec, Tile, Ps, str_ppm, PI_Saddr, s, Tile_Saddr,
		                               Tile_Eaddr))) {
			printf("[TileDec]:: PacketInfoControl error. Tile_no=%d TilePartNo=%d\n", Tile->T, ttt);
			return EXIT_FAILURE;
		}
	}

	for (ccc = 0; ccc < Codec->Siz->Csiz; ccc++) {
		EECod                   = &Codec->ECod->EECod[ccc * Codec->ECod->numTiles + Tile->T];
		EEQcd                   = &Codec->EQcd->EEQcd[ccc * Codec->EQcd->numTiles + Tile->T];
		NL                      = EECod->DecompLev;
		Wave[ccc].ll_d[NL].data = Image[ccc].data;
		Wave[ccc].ll_d[NL].type = Image[ccc].type;
		Wavelet_Init(&Wave[ccc]);
		if (EXIT_SUCCESS != DecMainBody(Tile, dec, EECod, Codec, s, ccc)) {
			printf("[TileDec]::DecMainBody  Error ccc=%d\n", ccc);
			return EXIT_FAILURE;
		}
		WaveDec(EECod, EEQcd, &Wave[ccc] /*, Tile->T, ccc*/);
	}

	if ((Codec->Siz->Csiz == 3) && (Codec->ECod->EECod[Tile->T].MC) && (J2kParam->MC_OFF == 0)) {
		ImageStream->row1step = 3;
		if (Codec->ECod->EECod[Tile->T].Filter == FILTER53) {
			ccc = 0;
			if (J2kParam->DEC_DecompLev != -1)
				NL = (uchar) (Codec->ECod->EECod[ccc * Codec->ECod->numTiles + Tile->T].DecompLev
				              - J2kParam->DEC_DecompLev);
			else
				NL = Codec->ECod->EECod[ccc * Codec->ECod->numTiles + Tile->T].DecompLev;
			YUVtoRGB_3(Codec, Codec->Siz, &Wave[0].ll_d[NL], &Wave[1].ll_d[NL], &Wave[2].ll_d[NL],
			           ImageStream, J2kParam->metric);
		} else {
			ccc = 0;
			if (J2kParam->DEC_DecompLev != -1)
				NL = (uchar) (Codec->ECod->EECod[ccc * Codec->ECod->numTiles + Tile->T].DecompLev
				              - J2kParam->DEC_DecompLev);
			else
				NL = Codec->ECod->EECod[ccc * Codec->ECod->numTiles + Tile->T].DecompLev;
			ICCDec3(Codec, Tile, Codec->Siz, &Wave[0].ll_d[NL], &Wave[1].ll_d[NL], &Wave[2].ll_d[NL],
			        ImageStream, J2kParam->metric);
		}
	} else {
		if (Codec->ECod->EECod[Tile->T].MC) {
			ccc = 0;
			if (J2kParam->DEC_DecompLev != -1)
				NL = (uchar) (Codec->ECod->EECod[ccc * Codec->ECod->numTiles + Tile->T].DecompLev
				              - J2kParam->DEC_DecompLev);
			else
				NL = Codec->ECod->EECod[ccc * Codec->ECod->numTiles + Tile->T].DecompLev;
			if (Codec->ECod->EECod[Tile->T].Filter == FILTER53)
				YUVtoRGB_4(Codec, Codec->Siz, &Wave[0].ll_d[NL], &Wave[1].ll_d[NL], &Wave[2].ll_d[NL],
				           ImageStream);
			else
				ICCDec4(Codec, Codec->Siz, &Wave[0].ll_d[NL], &Wave[1].ll_d[NL], &Wave[2].ll_d[NL],
				        ImageStream);

			for (ccc = 3; ccc < Codec->Siz->Csiz; ccc++) {
				if (J2kParam->DEC_DecompLev != -1)
					NL = (uchar) (Codec->ECod->EECod[ccc * Codec->ECod->numTiles + Tile->T].DecompLev
					              - J2kParam->DEC_DecompLev);
				else
					NL = Codec->ECod->EECod[ccc * Codec->ECod->numTiles + Tile->T].DecompLev;
				DcDec3(Codec, Tile, &Wave[ccc].ll_d[NL], ccc, ImageStream);
			}
		} else {
			for (ccc = 0; ccc < Codec->Siz->Csiz; ccc++) {
				if (J2kParam->DEC_DecompLev != -1)
					NL = (uchar) (Codec->ECod->EECod[ccc * Codec->ECod->numTiles + Tile->T].DecompLev
					              - J2kParam->DEC_DecompLev);
				else
					NL = Codec->ECod->EECod[ccc * Codec->ECod->numTiles + Tile->T].DecompLev;
				DcDec3(Codec, Tile, &Wave[ccc].ll_d[NL], ccc, ImageStream);
			}
		}
	}

#if ICT_Link11
	fPrint_Image(ImageStream, "DC_ICC_YUV.csv");
#endif

	if (Tile->ERgn != nullptr) {
		delete[] Tile->ERgn->EERgn;
		delete Tile->ERgn;
		Tile->ERgn = nullptr;
	}
	Wavelet_Destory(Wave);
	delete[] ON_reso;
	ON_reso = nullptr;
	return EXIT_SUCCESS;
}

//[Func 12]
byte4 J2kDec(struct J2kParam_s *J2kParam, struct Codec_s *Codec, struct StreamChain_s *s,
             struct Image_s *ImageStream) {
	byte4 tile_No;
	struct Tile_s *t;
	struct Image_s *Image;
	struct mqdec_s *dec;
	byte4 x1, y1;
	ubyte2 ccc;
	Image = new struct Image_s[Codec->Siz->Csiz];
	for (ccc = 0; ccc < Codec->Siz->Csiz; ccc++) {
		x1 = (Codec->Siz->Xdim < Codec->Siz->XTsiz) ? Codec->Siz->Xdim : Codec->Siz->XTsiz;
		y1 = (Codec->Siz->Ydim < Codec->Siz->YTsiz) ? Codec->Siz->Ydim : Codec->Siz->YTsiz;
		x1 = ceil2(x1, Codec->Siz->XRsiz[ccc]);
		y1 = ceil2(y1, Codec->Siz->YRsiz[ccc]);
		ImageCreate(&Image[ccc], x1, y1, 0, x1, 0, y1, BYTE4);
	}
	if (nullptr == (dec = Mqdec_Create(MAX_CONTEXTS))) {
		printf("[J2kDec]:: Mqdec_Create error\n");
		return EXIT_FAILURE;
	}

	for (tile_No = 0; tile_No < Codec->numTiles; tile_No++) {
		t = &Codec->Tile[tile_No];
		TileAdrCal(t, Codec->Siz, tile_No);
		if (EXIT_SUCCESS != TileDec(J2kParam, Image, s, Codec, dec, tile_No, ImageStream)) {
			printf("[J2kDec] :: TileDec error\n");
			return EXIT_FAILURE;
		}
	}

	for (ccc = 0; ccc < Codec->Siz->Csiz; ccc++) {
		delete[] Image[ccc].Pdata;
	}
	delete Image;

	return EXIT_SUCCESS;
}

byte4 J2k_DecMain(struct StreamChain_s *str, struct J2kParam_s *J2kParam) {
	struct Codec_s *Codec;
	struct Siz_s *Siz;
	struct Image_s *ImageStream;
	byte4 fwidth, fheight;
	byte4 Cbwidth, Cbheight;
	ubyte2 ccc, Ssiz_flag;
	char rrr;
	ubyte4 len;
	char fname[256];
	byte4 numData, width, height /*, tx0, tx1, ty0, ty1*/;
	uchar *ImageD;

	if (SOC != Ref_2Byte(str)) {
		printf("[J2k_DecMain] :: SOC Error.\n");
		return EXIT_FAILURE;
	}
	if (SIZ != Ref_2Byte(str)) {
		printf("[J2k_DecMain] :: SIZ Marker Error.\n");
		return EXIT_FAILURE;
	}
	if (nullptr == (Siz = SizDec(str))) {
		printf("[J2k_DecMain]:: SizDec error\n");
		return EXIT_FAILURE;
	}
	Codec      = CreateCodec(J2kParam);
	Codec->Siz = Siz;

	Codec->Sot = SotCreate(Codec->Siz);

	if (Codec->Siz->Rsiz & 0x4000)
		J2kParam->HTJ2K = 1;
	else
		J2kParam->HTJ2K = 0;
	Codec->XTSiz    = Codec->Siz->XTsiz;
	Codec->YTSiz    = Codec->Siz->YTsiz;
	Codec->numTiles = Codec->Siz->numTiles;

	if (EXIT_FAILURE == MainHeaderDec(Codec, str, /*Codec->Siz,*/ J2kParam)) {
		printf("[DecMain] MainHeaderDec error\n");
		return EXIT_FAILURE;
	}

	width  = 0; // Codec->Siz->Xdim;
	height = 0; // Codec->Siz->Ydim;
	for (ccc = 0; ccc < Codec->Siz->Csiz; ccc++) {
		width  = width < ceil2(Codec->Siz->Xdim, Codec->Siz->XRsiz[ccc])
		             ? ceil2(Codec->Siz->Xdim, Codec->Siz->XRsiz[ccc])
		             : width;
		height = height < ceil2(Codec->Siz->Ydim, Codec->Siz->YRsiz[ccc])
		             ? ceil2(Codec->Siz->Ydim, Codec->Siz->YRsiz[ccc])
		             : height;
	}

	Ssiz_flag = 0;
	for (ccc = 0; ccc < Codec->Siz->Csiz; ccc++) {
		if ((Codec->Siz->Ssiz[ccc] & 0x7f) == 0)
			Ssiz_flag = (ubyte2) (Ssiz_flag == 0 ? 0 : Ssiz_flag);
		else if ((Codec->Siz->Ssiz[ccc] & 0x7f) < 8)
			Ssiz_flag = (ubyte2) (Ssiz_flag <= 1 ? 1 : Ssiz_flag);
		else if ((Codec->Siz->Ssiz[ccc] & 0x7f) < 16)
			Ssiz_flag = (ubyte2) (Ssiz_flag <= 2 ? 2 : Ssiz_flag);
		else if ((Codec->Siz->Ssiz[ccc] & 0x7f) < 32)
			Ssiz_flag = (ubyte2) 4;
		else {
			printf(" [J2k_DecMain] ::Siz->Ssiz=%d This value is inhibited.\n", Codec->Siz->Ssiz[ccc]);
			return EXIT_FAILURE;
		}
	}

	if (Ssiz_flag == 0) {
		ImageStream =
		    ImageCreate(nullptr, ((width + 7) / 8), height * Codec->Siz->Csiz, 0, width, 0, height, BIT1);
	} else if (Ssiz_flag == 1) {
		if ((Codec->Siz->Csiz == 3) && (Codec->ECod->EECod[0].MC == 1) && (J2kParam->MC_OFF == 0)) {
			ImageStream = ImageCreate(nullptr, width * Codec->Siz->Csiz, height, 0, width, 0, height, CHAR);
			ImageStream->width = width;
		} else
			ImageStream = ImageCreate(nullptr, width, height * Codec->Siz->Csiz, 0, width, 0, height, CHAR);
	} else if (Ssiz_flag == 2) {
		if ((Codec->Siz->Csiz == 3) && (Codec->ECod->EECod[0].MC == 1) && (J2kParam->MC_OFF == 0)) {
			ImageStream =
			    ImageCreate(nullptr, width * Codec->Siz->Csiz, height, 0, width, 0, height, BYTE2);
			ImageStream->width = width;
		} else
			ImageStream =
			    ImageCreate(nullptr, width, height * Codec->Siz->Csiz, 0, width, 0, height, BYTE2);
	} else if (Ssiz_flag == 4) {
		if ((Codec->Siz->Csiz == 3) && (Codec->ECod->EECod[0].MC == 1) && (J2kParam->MC_OFF == 0)) {
			ImageStream =
			    ImageCreate(nullptr, width * Codec->Siz->Csiz, height, 0, width, 0, height, BYTE4);
			ImageStream->width = width;
		} else
			ImageStream =
			    ImageCreate(nullptr, width, height * Codec->Siz->Csiz, 0, width, 0, height, BYTE4);
	} else {
		ImageStream = nullptr;
		printf("[J2k_DecMain]:: ImageStream create Error.\n");
		return EXIT_FAILURE;
	}
	if (nullptr == (Codec->Tile = TileCreate(Codec, 1))) { // new struct Tile_s [Codec->numTiles];
		printf("[J2k_DecMain]:: TileCreate error.\n");
		return EXIT_FAILURE;
	}
	Cbwidth  = 4 << Codec->ECod->EECod[0].CbW;
	Cbheight = 4 << Codec->ECod->EECod[0].CbH;
	fwidth   = Cbwidth + 8;
	fheight  = Cbheight + 8;
	if (!J2kParam->HTJ2K) {
		Codec->flag             = ImageCreate(nullptr, fheight, fwidth, 0, fheight, 0, fwidth, BYTE2);
		Codec->flag->row1step   = Codec->flag->col1step;
		Codec->flag->col1step   = 1;
		Codec->CbData           = ImageCreate(nullptr, Cbheight, Cbwidth, 0, Cbheight, 0, Cbwidth, BYTE4);
		Codec->CbData->row1step = Codec->CbData->col1step;
		Codec->CbData->col1step = 1;
		Codec->Image_mex        = nullptr;
		Codec->Image_msbits     = nullptr;
		Codec->Image_qinf       = nullptr;
		Codec->Image_qmex       = nullptr;
		Codec->Image_sigma      = nullptr;
		Codec->Image_rho        = nullptr;
		Codec->codecf           = nullptr;
		Codec->codecR           = nullptr;
		Codec->codecM           = nullptr;
		Codec->Image_epciron1   = nullptr;
		Codec->Image_epcironK   = nullptr;
	} else {
		Codec->flag             = ImageCreate(nullptr, fheight, fwidth, 0, fheight, 0, fwidth, BYTE2);
		Codec->flag->row1step   = Codec->flag->col1step;
		Codec->flag->col1step   = 1;
		Codec->CbData           = ImageCreate(nullptr, Cbheight, Cbwidth, 0, Cbheight, 0, Cbwidth, BYTE4);
		Codec->CbData->row1step = Codec->CbData->col1step;
		Codec->CbData->col1step = 1;
		Codec->Image_qinf       = ImageCreate(nullptr, ((Cbwidth + 1) / 2), ((Cbheight + 1) / 2), 0,
                                        ((Cbwidth + 1) / 2), 0, ((Cbheight + 1) / 2), CHAR);
		Codec->Image_qmex       = ImageCreate(nullptr, ((Cbwidth + 1) / 2), ((Cbheight + 1) / 2), 0,
                                        ((Cbwidth + 1) / 2), 0, ((Cbheight + 1) / 2), CHAR);
		Codec->Image_msbits     = ImageCreate(nullptr, ((Cbwidth + 1) / 2), ((Cbheight + 1) / 2), 0,
                                          ((Cbwidth + 1) / 2), 0, ((Cbheight + 1) / 2), CHAR);
		Codec->Image_mex   = ImageCreate(nullptr, (Cbwidth * 2), ((Cbheight + 1) / 2), 0, (Cbwidth * 2), 0,
                                       ((Cbheight + 1) / 2), CHAR);
		Codec->Image_sigma = ImageCreate(nullptr, (Cbwidth + 3) / 4, (Cbheight + 1) / 2, 0,
		                                 (Cbwidth + 3) / 4, 0, (Cbheight + 1) / 2, BYTE2);
		Codec->Image_epcironK = ImageCreate(nullptr, ((Cbwidth + 1) / 2), ((Cbheight + 1) / 2), 0,
		                                    ((Cbwidth + 1) / 2), 0, ((Cbheight + 1) / 2), CHAR);
		Codec->Image_epciron1 = ImageCreate(nullptr, ((Cbwidth + 1) / 2), ((Cbheight + 1) / 2), 0,
		                                    ((Cbwidth + 1) / 2), 0, ((Cbheight + 1) / 2), CHAR);
		Codec->Image_rho      = ImageCreate(nullptr, fheight, fwidth, 0, fheight, 0, fwidth, CHAR);
		Codec->Image_msbits2  = ImageCreate(nullptr, ((Cbwidth + 1) / 2), ((Cbheight + 1) / 2), 0,
                                           ((Cbwidth + 1) / 2), 0, ((Cbheight + 1) / 2), CHAR);
		Codec->codecf         = new fRawCodec_t; // osamu:for VC 2015
		Codec->codecR         = RRawCodecCreate();
		Codec->codecM         = MelCodecCreate();
	}

	if (EXIT_SUCCESS != J2kDec(J2kParam, Codec, str, ImageStream)) {
		printf("[DecMain]:: Erro in J2kDec.\n");
		if (J2kParam->HTJ2K) {
			MelCodecDestroy(Codec->codecM);
			delete Codec->codecf;
			RRawCodecDestroy(Codec->codecR);
			ImageDestory(Codec->Image_mex);
			ImageDestory(Codec->Image_msbits);
			ImageDestory(Codec->Image_qinf);
			ImageDestory(Codec->Image_qmex);
			ImageDestory(Codec->Image_sigma);
		} else {
			ImageDestory(Codec->flag);
			ImageDestory(Codec->CbData);
		}
		DestroyCodec(Codec);
		return EXIT_FAILURE;
	}

	// malloc free
	if (J2kParam->HTJ2K) {
		MelCodecDestroy(Codec->codecM);
		delete Codec->codecf;
		RRawCodecDestroy(Codec->codecR);
		ImageDestory(Codec->Image_mex);
		ImageDestory(Codec->Image_msbits);
		ImageDestory(Codec->Image_qinf);
		ImageDestory(Codec->Image_qmex);
		ImageDestory(Codec->Image_sigma);
	} else {
		ImageDestory(Codec->flag);
		ImageDestory(Codec->CbData);
	}

	if (!(strcmp(J2kParam->fileForm, "bmp") && strcmp(J2kParam->fileForm, "BMP")
	      && strcmp(J2kParam->fileForm, "Bmp"))) {
		if ((Codec->Siz->Csiz == 3) && (Codec->ECod->EECod[0].MC)) {
			strcat(J2kParam->fname, ".");
			strcat(J2kParam->fname, J2kParam->fileForm);
			ImageStream->row1step = 3;
			SaveBmp777(J2kParam->fname, ImageStream);
		} else if (Codec->Siz->Csiz == 1) {
			strcat(J2kParam->fname, ".");
			strcat(J2kParam->fname, J2kParam->fileForm);
			SaveBmp777(J2kParam->fname, ImageStream);
		} else {
			numData = ImageStream->col1step * (ImageStream->tby1 - ImageStream->tby0);
			for (ccc = 0; ccc < Codec->Siz->Csiz; ccc++) {
				width  = Codec->Siz->Xdim; // ImageStream->width;
				height = Codec->Siz->Ydim; //(ImageStream->tby1-ImageStream->tby0);
				width  = ceil2(width, Codec->Siz->XRsiz[ccc]);
				height = ceil2(height, Codec->Siz->YRsiz[ccc]);
				ImageD = (uchar *) ImageStream->data;
				ImageD = &ImageD[ccc * numData];
				strcpy(fname, J2kParam->fname);
				strcat(fname, "_");
				len            = strlen(fname);
				fname[len]     = (char) (ccc + 0x30);
				fname[len + 1] = 0;
				strcat(fname, ".");
				strcat(fname, J2kParam->fileForm);
				SaveBmp778(fname, ImageD, width, height, ImageStream->col1step);
			}
		}
	} else if (!(strcmp(J2kParam->fileForm, "tif") && strcmp(J2kParam->fileForm, "TIF")
	             && strcmp(J2kParam->fileForm, "Tif"))) {
		uchar Bitdipth;
		if ((Codec->Siz->Csiz == 3) && (Codec->ECod->EECod[0].MC)) {
			Bitdipth = 0;
			for (ccc = 0; ccc < Codec->Siz->Csiz; ccc++) {
				if (Bitdipth < (Codec->Siz->Ssiz[ccc] & 0x7f + 1))
					Bitdipth = (uchar) (Codec->Siz->Ssiz[ccc] & 0x7f + 1);
			}

			strcat(J2kParam->fname, ".");
			strcat(J2kParam->fname, J2kParam->fileForm);
			ImageStream->row1step = 3;
			SaveTiff((uchar) ImageStream->row1step, ImageStream->width,
			         (ImageStream->width * ImageStream->row1step), ImageStream->height, Bitdipth,
			         J2kParam->fname, ImageStream, 0);
		} else if (Codec->Siz->Csiz == 1) {
			Bitdipth = (uchar) (Codec->Siz->Ssiz[0] & 0x7f + 1);
			if (Codec->Siz->XRsiz[0] * Codec->Siz->YRsiz[0] == 1) {
				strcat(J2kParam->fname, ".");
				strcat(J2kParam->fname, J2kParam->fileForm);
				ImageStream->row1step = 1;
				SaveTiff((uchar) ImageStream->row1step, ImageStream->width, ImageStream->width,
				         ImageStream->height, Bitdipth, J2kParam->fname, ImageStream, 0);
			} else {
				width  = ImageStream->width;
				height = (ImageStream->tby1 - ImageStream->tby0);
				width  = ceil2(width, Codec->Siz->XRsiz[0]);
				height = ceil2(height, Codec->Siz->YRsiz[0]);
				strcat(J2kParam->fname, ".");
				strcat(J2kParam->fname, J2kParam->fileForm);
				SaveTiff((uchar) ImageStream->row1step, width, width, height, Bitdipth, J2kParam->fname,
				         ImageStream, 0);
			}
		} else {
			numData = ImageStream->col1step * (ImageStream->tby1 - ImageStream->tby0);
			for (ccc = 0; ccc < Codec->Siz->Csiz; ccc++) {
				Bitdipth = (uchar) ((Codec->Siz->Ssiz[ccc] & 0x3f) + 1);
				width    = ImageStream->width;
				height   = (ImageStream->tby1 - ImageStream->tby0);
				width    = ceil2(width, Codec->Siz->XRsiz[ccc]);
				height   = ceil2(height, Codec->Siz->YRsiz[ccc]);
				strcpy(fname, J2kParam->fname);
				strcat(fname, "_");
				len            = strlen(fname);
				fname[len]     = (uchar) (ccc + 0x30);
				fname[len + 1] = 0;
				strcat(fname, ".");
				strcat(fname, J2kParam->fileForm);
				SaveTiff778((uchar) ImageStream->row1step, width, width, height, Bitdipth, fname,
				            ImageStream, (numData * ccc), 0);
			}
		}

	} else if (!(strcmp(J2kParam->fileForm, "ppm") && strcmp(J2kParam->fileForm, "PPM"))) {
		uchar Bitdipth;
		if (ImageStream->type == BYTE4)
			Bitdipth = 32;
		else if (ImageStream->type == BYTE2)
			Bitdipth = 16;
		else if (ImageStream->type == CHAR)
			Bitdipth = 8;
		else
			Bitdipth = 1;
		SavePpm(J2kParam->fname, ImageStream, Bitdipth);
	} else if (!(strcmp(J2kParam->fileForm, "raw") && strcmp(J2kParam->fileForm, "RAW")
	             && strcmp(J2kParam->fileForm, "Raw"))) {
		uchar Bitdipth;
		if ((Codec->Siz->Csiz == 3) && (Codec->ECod->EECod[0].MC) && (J2kParam->MC_OFF == 0)) {
			Bitdipth = 0;
			for (ccc = 0; ccc < Codec->Siz->Csiz; ccc++) {
				if (Bitdipth < (Codec->Siz->Ssiz[ccc] & 0x7f + 1))
					Bitdipth = (uchar) (Codec->Siz->Ssiz[ccc] & 0x7f + 1);
			}
			strcat(J2kParam->fname, ".");
			strcat(J2kParam->fname, J2kParam->fileForm);
			ImageStream->row1step = 3;
			SaveRAW((ImageStream->width * ImageStream->row1step), ImageStream->height, Bitdipth,
			        J2kParam->fname, ImageStream);
		} else if (Codec->Siz->Csiz == 1) {
			Bitdipth = (uchar) ((Codec->Siz->Ssiz[0] & 0x7f) + 1);
			if (Codec->Siz->XRsiz[0] * Codec->Siz->YRsiz[0] == 1) {
				width  = ImageStream->width;
				height = (ImageStream->tby1 - ImageStream->tby0);
				if (J2kParam->DEC_DecompLev != -1) {
					for (rrr = 0; rrr < J2kParam->DEC_DecompLev; rrr++) {
						width  = ceil2(width, 2);
						height = ceil2(height, 2);
					}
				}
				strcat(J2kParam->fname, ".");
				strcat(J2kParam->fname, J2kParam->fileForm);
				ImageStream->row1step = 1;
				//				SaveRAW(ImageStream->width, ImageStream->height, Bitdipth, J2kParam->fname,
				//ImageStream );
				SaveRAW(width, height, Bitdipth, J2kParam->fname, ImageStream);
			} else {
				width  = ImageStream->width;
				height = (ImageStream->tby1 - ImageStream->tby0);
				if (J2kParam->DEC_DecompLev != -1) {
					for (rrr = 0; rrr < J2kParam->DEC_DecompLev; rrr++) {
						width  = ceil2(width, 2);
						height = ceil2(height, 2);
					}
				}
				strcat(J2kParam->fname, ".");
				strcat(J2kParam->fname, J2kParam->fileForm);
				SaveRAW(width, height, Bitdipth, J2kParam->fname, ImageStream);
			}
		} else {
			numData = ImageStream->col1step * (ImageStream->tby1 - ImageStream->tby0);
			for (ccc = 0; ccc < Codec->Siz->Csiz; ccc++) {
				Bitdipth = (uchar) ((Codec->Siz->Ssiz[ccc] & 0x3f) + 1);
				width    = Codec->Siz->Xdim; // ImageStream->width;
				height   = Codec->Siz->Ydim; //(ImageStream->tby1-ImageStream->tby0);
				width    = ceil2(width, Codec->Siz->XRsiz[ccc]);
				height   = ceil2(height, Codec->Siz->YRsiz[ccc]);
				if (J2kParam->DEC_DecompLev != -1) {
					for (rrr = 0; rrr < J2kParam->DEC_DecompLev; rrr++) {
						width  = ceil2(width, 2);
						height = ceil2(height, 2);
					}
				}
				strcpy(fname, J2kParam->fname);
				strcat(fname, "_");
				len = strlen(fname);
				if (ccc <= 9) {
					fname[len]     = (char) (ccc + 0x30);
					fname[len + 1] = 0;
				} else {
					ubyte2 ccc6, ccc5, ccc4, ccc3, ccc2, ccc1;
					ccc6           = ccc;
					ccc5           = (char) (ccc6 / 10000);
					ccc6           = (char) (ccc6 - (ccc5 * 10000));
					ccc4           = (char) (ccc6 / 1000);
					ccc6           = (char) (ccc6 - (ccc4 * 1000));
					ccc3           = (char) (ccc6 / 100);
					ccc6           = (char) (ccc6 - (ccc3 * 100));
					ccc2           = (char) (ccc6 / 10);
					ccc6           = (char) (ccc6 - (ccc2 * 10));
					ccc1           = (char) (ccc6 % 10);
					fname[len + 0] = (char) (ccc5 + 0x30);
					fname[len + 1] = (char) (ccc4 + 0x30);
					fname[len + 2] = (char) (ccc3 + 0x30);
					fname[len + 3] = (char) (ccc2 + 0x30);
					fname[len + 4] = (char) (ccc1 + 0x30);
					fname[len + 5] = 0;
				}
				strcat(fname, ".");
				strcat(fname, J2kParam->fileForm);
				SaveRAW778(width, height, Bitdipth, fname, ImageStream, (numData * ccc));
			}

			if (Codec->Siz->Csiz > 9) {
				strcpy(fname, J2kParam->fname);
				strcat(fname, "_all");
				strcat(fname, ".");
				strcat(fname, J2kParam->fileForm);
				Bitdipth = 0;
				for (ccc = 0; ccc < Codec->Siz->Csiz; ccc++)
					Bitdipth = (uchar) ((Codec->Siz->Ssiz[ccc] & 0x3f + 1) > Bitdipth
					                        ? (Codec->Siz->Ssiz[ccc] & 0x3f + 1)
					                        : Bitdipth);
				SaveRAW778(width, (height * Codec->Siz->Csiz), Bitdipth, fname, ImageStream, 0);
			}
		}
	}
	return EXIT_SUCCESS;
}
