/*
Copyright (c) 2000-2021, ICT-Link
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the HTJ2K project.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ImageUtil.h"
#include "ctx_enc.h"
#include "t1codec.h"
#include "MqCodec.h"
#include "j2k2.h"
#include "wavelet_codec.h"
#include "HT_cleanUp.h"
#include "vlc_codec.h"
#include "MelCodec.h"

struct StreamChain_s *mqdec_bytein(struct mqdec_s *dec, struct StreamChain_s *str)
// figureC-19
{
	if (str->cur_p < dec->counter_limit) {
		if (str->cur_p == str->buf_length) {
			if (str->child == nullptr) {
				printf("[mqdec_bytein]:: str->child is not exist. This is error. str->cur_p=%x "
				       "dec->counter_limit=%x\n",
				       str->cur_p, dec->counter_limit);
				return nullptr;
			} else {
				while (str->child != nullptr) {
					str = str->child;
					if (str->cur_p < str->buf_length) break;
					if (str->buf_length == dec->counter_limit) {
						dec->creg += 0xff00;
						dec->ctreg = 1;
						return str;
					}
				}
			}
			str->bits    = 8;
			str->total_p = str->parent->total_p;
		}
		if (dec->B_buf == 0xff) {
			if (str->buf[str->cur_p] > 0x8f) {
				dec->creg += 0xff00;
				dec->ctreg = 1;
			} else {
				dec->B_buf = str->buf[str->cur_p];
				dec->creg += (byte4) (dec->B_buf << 9);
				dec->ctreg = 2;
				str->cur_p++;
				str->total_p++;
			}
		} else {
			dec->B_buf = str->buf[str->cur_p];
			dec->creg += (byte4) (dec->B_buf << 8);
			dec->ctreg = 1;
			str->cur_p++;
			str->total_p++;
		}
	} else {
		dec->creg += 0xff00;
		dec->ctreg = 1;
	}

	return str;
}

struct StreamChain_s *Term(struct StreamChain_s *str, uchar mode) {
	if (mode == CLNPASS) {
		str->cur_p++;
		if (str->cur_p == str->buf_length) {
			if (str->child != nullptr) {
				str          = str->child;
				str->cur_p   = 0;
				str->total_p = 0;
				str->bits    = 8;
			}
		} else {
			str->bits = 8;
		}
	} else if (mode == REFPASS) {
		if (str->bits != 8) {
			str->cur_p++;
			str->total_p++;
			if (str->child != nullptr) {
				str          = str->child;
				str->cur_p   = 0;
				str->total_p = str->parent->total_p;
				str->bits    = 8;
			}
			str->bits = 8;
		}
	}
	return str;
}

struct StreamChain_s *mqdec_init(struct mqdec_s *dec, ubyte4 code_length, struct StreamChain_s *str)
// figureC-20
{
	dec->counter_limit = str->cur_p + code_length;
	if (code_length) {
		dec->B_buf = str->buf[str->cur_p];
		str->cur_p++;
		str->total_p++;
	} else {
		dec->B_buf = 0xff;
	}

	dec->creg = (ubyte4) (dec->B_buf << 16); // dec->b<<16
	if (nullptr == (str = mqdec_bytein(dec, str))) {
		printf("[mqdec_init] :: mqdec_bytein error\n");
		return nullptr;
	}
	dec->creg <<= 7;
	dec->ctreg <<= 7;
	return str;
}
struct StreamChain_s *mqdec_init_B(struct mqdec_s *dec, byte4 code_length, byte4 Eaddr,
                                   struct StreamChain_s *str)
// figureC-20
{
	dec->counter_limit = Eaddr;
	if (code_length) {
		dec->B_buf = str->buf[str->cur_p];
		str->cur_p++;
		str->total_p++;
	} else {
		dec->B_buf = 0xFF;
		//		if(str->child!=NULL)
		//			return	str->child;
		//		else
		//			return str;
	}

	dec->creg = (ubyte4) (dec->B_buf << 16); // dec->b<<16
	if (nullptr == (str = mqdec_bytein(dec, str))) {
		printf("[mqdec_init_B] :: mqdec_bytein error\n");
		return nullptr;
	}
	dec->creg <<= 7;
	dec->ctreg <<= 7;
	return str;
}

void mq_Index_init(struct mqdec_s *dec) {
	memset(dec->ind, 0, sizeof(char) * MAX_CONTEXTS * 2);
	dec->ind[CTXUNI2] = 46 * 4;
	dec->ind[CTXRUN2] = 3 * 4;
	dec->ind[0]       = 4 * 4;
}

struct mqdec_s *Mqdec_Create(byte4 max_contexts) {
	struct mqdec_s *dec;
	if (nullptr == (dec = new struct mqdec_s)) {
		printf("[Mqdec_Create]:: mqdec_s malloc error!\n");
		return nullptr;
	}
	if (nullptr == (dec->ind = new uchar[max_contexts * 2 + 256])) {
		printf("[Mqdec_Create]:: dec->ind malloc error!\n");
		return nullptr;
	}
	return dec;
}

uchar MQ_Dec2(char CX, struct mqdec_s *dec, struct StreamChain_s *str, ubyte4 *QeIndexTable_) {
	uchar index2;
	uchar index;
	ubyte4 Qe;
	uchar D;
	uchar MPS_D;

	dec->areg &= 0xffff0000;
	index2 = (uchar) (dec->ind[CX] & 0xfe);
	index  = (uchar) (index2 / 4);
	MPS_D  = (uchar) (dec->ind[CX] & 1);
	Qe     = QeIndexTable_[index] & 0xffff0000;
	dec->areg -= Qe;
	if (dec->creg < Qe) {
		if (dec->areg >= Qe) {
			dec->areg    = Qe; // D1 LPS
			D            = (uchar) (MPS_D ^ 1);
			dec->ind[CX] = (uchar) (QeIndexTable_[index] >> 8);
			dec->ind[CX] ^= MPS_D;
		} else {
			dec->areg    = Qe; // D5 MPS
			D            = MPS_D;
			dec->ind[CX] = (uchar) (QeIndexTable_[index] | MPS_D);
		}
		do {
			if (dec->ctreg == 0x100) {
				if (nullptr == (str = mqdec_bytein(dec, str))) {
					printf("[MQ_Dec2]:: mqdec_bytein error.\n");
					return EXIT_FAILURE;
				}
			}
			dec->areg <<= 1;
			dec->creg <<= 1;
			dec->ctreg <<= 1;
		} while (!(dec->areg & 0x80000000));
	} else {
		dec->creg -= Qe;
		if (!(dec->areg & 0x80000000)) {
			if (dec->areg < Qe) {
				D            = (uchar) (MPS_D ^ 1); // D2 LPS
				dec->ind[CX] = (uchar) ((QeIndexTable_[index] >> 8) ^ MPS_D);
			} else {
				D            = MPS_D; // D4 MPS
				dec->ind[CX] = (uchar) (QeIndexTable_[index] | MPS_D);
			}
			do {
				if (dec->ctreg == 0x100) {
					if (nullptr == (str = mqdec_bytein(dec, str))) {
						printf("[MQ_Dec2]:: mqdec_bytein error.\n");
						return EXIT_FAILURE;
					}
				}
				dec->areg <<= 1;
				dec->creg <<= 1;
				dec->ctreg <<= 1;
			} while (!(dec->areg & 0x80000000));
		} else
			D = MPS_D; // D3 MPS
	}
	return D;
}

struct StreamChain_s *MQ_Dec(uchar *D, char CX, struct mqdec_s *dec, struct StreamChain_s *str,
                             ubyte4 *QeIndexTable_) {
	uchar index2;
	uchar index;
	ubyte4 Qe;
	uchar MPS_D;

	dec->areg &= 0xffff0000;
	index2 = (uchar) (dec->ind[CX] & 0xfe);
	index  = (uchar) (index2 / 4);
	MPS_D  = (uchar) (dec->ind[CX] & 1);
	Qe     = QeIndexTable_[index] & 0xffff0000;
	dec->areg -= Qe;
	if (dec->creg < Qe) {
		if (dec->areg >= Qe) {
			dec->areg    = Qe; // D1 LPS
			*D           = (uchar) (MPS_D ^ 1);
			dec->ind[CX] = (uchar) (QeIndexTable_[index] >> 8);
			dec->ind[CX] ^= MPS_D;
		} else {
			dec->areg    = Qe; // D5 MPS
			*D           = MPS_D;
			dec->ind[CX] = (uchar) (QeIndexTable_[index] | MPS_D);
		}
		do {
			if (dec->ctreg == 0x100) {
				if (nullptr == (str = mqdec_bytein(dec, str))) {
					printf("[MQ_Dec]:: mqdec_bytein error.\n");
					return nullptr;
				}
			}
			dec->areg <<= 1;
			dec->creg <<= 1;
			dec->ctreg <<= 1;
		} while (!(dec->areg & 0x80000000));
	} else {
		dec->creg -= Qe;
		if (!(dec->areg & 0x80000000)) {
			if (dec->areg < Qe) {
				*D           = (uchar) (MPS_D ^ 1); // D2 LPS
				dec->ind[CX] = (uchar) ((QeIndexTable_[index] >> 8) ^ MPS_D);
			} else {
				*D           = MPS_D; // D4 MPS
				dec->ind[CX] = (uchar) (QeIndexTable_[index] | MPS_D);
			}
			do {
				if (dec->ctreg == 0x100) {
					if (nullptr == (str = mqdec_bytein(dec, str))) {
						printf("[MQ_Dec]:: mqdec_bytein error.\n");
						return nullptr;
					}
				}
				dec->areg <<= 1;
				dec->creg <<= 1;
				dec->ctreg <<= 1;
			} while (!(dec->areg & 0x80000000));
		} else
			*D = MPS_D; // D3 MPS
	}
	return str;
}

struct StreamChain_s *MQ_Dec_Lazy(uchar *D, byte4 counter_limit, struct StreamChain_s *str) {
	if (str->cur_p < counter_limit) {
		if (str->cur_p == str->buf_length) {
			if (str->child != nullptr) {
				str           = str->child;
				str->total_p  = str->parent->total_p;
				str->bits     = 8;
				str->lastbyte = str->parent->lastbyte;
			} else {
				printf("[MQ_Dec_Lazy] :: str->child is not exist.str is %x str->parent is %x\n", str,
				       str->parent);
				return nullptr;
			}
		}
		if (str->lastbyte == 0xff) {
			if (str->stream_type == JPEG2000) {
				if (str->bits == 8) {
					str->lastbyte = str->buf[str->cur_p];
					str->bits--;
				}
			}
		}
		*D = (uchar) (str->buf[str->cur_p] & mask[str->bits] ? 1 : 0);
		str->bits--;
		if (str->bits == 0) {
			str->lastbyte = str->buf[str->cur_p];
			str->cur_p++;
			str->total_p++;
			str->bits = 8;
		}
	} else
		*D = 1;

	return str;
}

byte4 T1Dec(struct Cblk_s *Cblk, struct Codec_s *Codec, struct EECod_s *EECod, struct EERgn_s *EERgn,
            struct mqdec_s *dec, uchar *ctx_table, struct StreamChain_s *s, ubyte2 ccc) {
	uchar bittop;
	ubyte2 pass, pass2, numPasses, numSegments = 0;
	byte2 shift;
	char onROI;
	char Lazy;
	byte4 seg, seg2;
	ubyte2 Layer;
	byte4 code_length;
	byte4 code_length3[96], code_Saddr3[96], code_Eaddr3[96], code_Chain3[96];
	ubyte2 code_passes3[96];
	byte4 row1stepCb, col1stepCb;
	byte4 row1stepf, col1stepf;
	byte2 width, height, height1, height2;
	byte4 ii, jj;
	byte4 *data_;
	byte4 x0, x1, y0, y1;
	ubyte4 one;
	struct StreamChain_s *str;
	byte4 nextSaddr, Eaddr;
	byte4 Half_Bit;
	uchar Chain_Flag, SigMag_F;
	uchar FlagFlag  = 0;
	uchar BreakFlag = 0;

	height = (byte2) (Cblk->tcby1 - Cblk->tcby0);
	width  = (byte2) (Cblk->tcbx1 - Cblk->tcbx0);
	if (height % 4) {
		height1 = (byte2) (height / 4 + 1);
		height2 = (byte2) (height1 * 4 - height);
	} else {
		height1 = (byte2) (height / 4);
		height2 = 0;
	}

	// CbData
	memset(Codec->CbData->Pdata, 0x00, sizeof(byte4) * (Codec->CbData->numData));
	data_                 = (byte4 *) Codec->CbData->data;
	row1stepCb            = Codec->CbData->row1step;
	col1stepCb            = Codec->CbData->col1step;
	Codec->CbData->tbx0   = 0;
	Codec->CbData->tbx1   = Cblk->tcbx1 - Cblk->tcbx0;
	Codec->CbData->tby0   = 0;
	Codec->CbData->tby1   = Cblk->tcby1 - Cblk->tcby0;
	Codec->CbData->width  = Codec->CbData->tbx1;
	Codec->CbData->height = Codec->CbData->tby1;
	// flag
	x0                = Cblk->tcbx0 - Cblk->Band->tbx0;
	x1                = Cblk->tcbx1 - Cblk->Band->tbx0;
	y0                = Cblk->tcby0 - Cblk->Band->tby0;
	y1                = Cblk->tcby1 - Cblk->Band->tby0;
	Codec->flag->tbx0 = 1;
	Codec->flag->tbx1 = 1 + (x1 - x0);
	Codec->flag->tby0 = 4;
	Codec->flag->tby1 = 4 + (y1 - y0);
	col1stepf         = Codec->flag->col1step;
	row1stepf         = Codec->flag->row1step;
	Cblk->flags       = Codec->flag;
	memset(Codec->flag->Pdata, 0, sizeof(ubyte2) * Codec->flag->numData);

	shift = 0;
	onROI = 0;
	if (EERgn != nullptr) {
		if (EERgn->Crgn == ccc) {
			shift = EERgn->SPrgn;
			onROI = 1;
		}
	}
	bittop = (uchar) (31 - 1 - Cblk->zerobits);
	one    = (ubyte4) (1 << bittop);

	//	printf( "[T1Dec] tcbx0=%d tcbx1=%d tcby0=%d tcby1=%d\n", Cblk->tcbx0, Cblk->tcbx1, Cblk->tcby0,
	//Cblk->tcby1);

	if ((!EECod->CbTerm) && (!EECod->CbBypass)) {
		code_length = 0;
		numPasses   = 0;
		for (Layer = 0; Layer < EECod->numLayer; Layer++) {
			code_length += Cblk->Layer_Length[Layer];
			numPasses = (ubyte2) (numPasses + Cblk->Layer_passes[Layer]);
		}
		if (code_length) {
			str = nullptr;
			if (nullptr == (str = StreamChainMake(str, code_length, JPEG2000))) {
				printf("[T1Dec]:: StreamchainMake error, when code_length!=0\n");
				return EXIT_FAILURE;
			}
			str->cur_p = 0;
			for (Layer = 0; Layer < EECod->numLayer; Layer++) {
				memcpy(&str->buf[str->cur_p], &s->buf[Cblk->Layer_Saddr[Layer]],
				       sizeof(uchar) * Cblk->Layer_Length[Layer]);
				str->cur_p += Cblk->Layer_Length[Layer];
			}
			str->cur_p = 0;
		} else {
			str = nullptr;
			if (nullptr == (str = StreamChainMake(str, 1, JPEG2000))) {
				printf("[T1Dec]::StreamchainMake error, when code_length==0\n");
				return EXIT_FAILURE;
			}
			str->cur_p = 0;
		}
		mq_Index_init(dec);
		if (nullptr == mqdec_init(dec, code_length, str)) {
			printf("[T1Dec]:: mqdec_init error\n");
			return EXIT_FAILURE;
		}
		dec->areg = 0x80000000;
		for (pass = 0; pass < numPasses; pass++) {
			switch (pass % 3) {
				case 1:
					if (EECod->CbVcausal) {
						if (nullptr
						    == (str = dec_sigpass_vcausal(Cblk, Codec->CbData, bittop, ctx_table, dec, str,
						                                  FlagFlag))) {
							printf("[T1Dec] :: dec_sigpass_vcausal error pass=%d bittop=%d\n", pass,
							       bittop);
							return EXIT_FAILURE;
						}
					} else {
						if (nullptr
						    == (str = dec_sigpass(Cblk, Codec->CbData, bittop, ctx_table, dec, str,
						                          FlagFlag))) {
							printf("[T1Dec] :: dec_sigpass error pass=%d bittop=%d\n", pass, bittop);
							return EXIT_FAILURE;
						}
					}
					if (EECod->CbReset) mq_Index_init(dec);
					BreakFlag = 1;
					break;
				case 2:
					if (EECod->CbVcausal) {
						if (nullptr
						    == (str =
						            dec_magpass_vcausal(Cblk, Codec->CbData, bittop, dec, str, FlagFlag))) {
							printf("[T1Dec] :: dec_magpass_vcausal error pass=%d bittop=%d\n", pass,
							       bittop);
							return EXIT_FAILURE;
						}
					} else {
						if (nullptr
						    == (str = dec_magpass(Cblk, Codec->CbData, bittop, dec, str, FlagFlag))) {
							printf("[T1Dec] :: dec_magpass error pass=%d bittop=%d\n", pass, bittop);
							return EXIT_FAILURE;
						}
					}
					if (EECod->CbReset) mq_Index_init(dec);
					BreakFlag = 1;
					break;
				case 0:
					if (EECod->CbVcausal) {
						if (nullptr
						    == (str = dec_clnpass_vcausal(Cblk, Codec->CbData, bittop, ctx_table, dec, str,
						                                  EECod->CbSegmentSymbol, FlagFlag))) {
							printf("[T1Dec]:: dec_clnpass_vcausal error pass=%d bittop=%d\n", pass, bittop);
							return EXIT_FAILURE;
						}
					} else {
						if (nullptr
						    == (str = dec_clnpass(Cblk, Codec->CbData, bittop, ctx_table, dec, str,
						                          EECod->CbSegmentSymbol, FlagFlag))) {
							printf("[T1Dec]:: dec_clnpass error pass=%d bittop=%d\n", pass, bittop);
							return EXIT_FAILURE;
						}
					}
					if (EECod->CbReset) mq_Index_init(dec);

					BreakFlag = 0;
					bittop--;
					one >>= 1;
					break;
			}
		}
		StreamChainDestory(str);
	} else if ((!EECod->CbTerm) && (EECod->CbBypass)) {
		memset(code_length3, 0, sizeof(byte4) * 96);
		memset(code_Saddr3, 0, sizeof(byte4) * 96);
		memset(code_Eaddr3, 0, sizeof(byte4) * 96);
		memset(code_Chain3, 0, sizeof(byte4) * 96);
		memset(code_passes3, 0, sizeof(ubyte2) * 96);
		str         = nullptr;
		numPasses   = 0;
		code_length = 0;
		Chain_Flag  = 0;
		SigMag_F    = 0;
		for (seg = 0, seg2 = 0; seg < Cblk->numSegment; seg++) {
			str =
			    StreamChainMake_B(str, s->buf, Cblk->Seg_Saddr[seg], Cblk->Seg_Length[seg], s->stream_type);
			numPasses = (ubyte2) (numPasses + Cblk->Seg_passes[seg]);
			code_length += Cblk->Seg_Length[seg];
			if (numPasses < 10) {
				code_length3[seg2] += Cblk->Seg_Length[seg];
				code_passes3[seg2] = numPasses;
				code_Eaddr3[seg2]  = Cblk->Seg_Saddr[seg] + Cblk->Seg_Length[seg];
				if ((Chain_Flag == 0) && Cblk->Seg_Length[seg]) {
					code_Saddr3[seg2] = Cblk->Seg_Saddr[seg];
					code_Chain3[seg2] = str->numChains;
					Chain_Flag        = 1;
				}
			} else if (numPasses == 10) {
				code_length3[seg2] = code_length;
				code_passes3[seg2] = numPasses;
				code_Eaddr3[seg2]  = Cblk->Seg_Saddr[seg] + Cblk->Seg_Length[seg];
				if ((Chain_Flag == 0) && Cblk->Seg_Length[seg]) {
					code_Saddr3[seg2] = Cblk->Seg_Saddr[seg];
					code_Chain3[seg2] = str->numChains;
					Chain_Flag        = 1;
				}
				seg2++;
			} else if (numPasses % 3 == 2) { // Significant pass
				code_length3[seg2] = Cblk->Seg_Length[seg];
				code_passes3[seg2] = numPasses;
				code_Saddr3[seg2]  = Cblk->Seg_Saddr[seg];
				code_Chain3[seg2]  = str->numChains;
				SigMag_F           = 1;
			} else if (numPasses % 3 == 0) { // Magnitude pass
				code_length3[seg2] += Cblk->Seg_Length[seg];
				code_passes3[seg2] = numPasses;
				if ((!SigMag_F) || (code_length3[seg2] == 0)) {
					code_Saddr3[seg2] = Cblk->Seg_Saddr[seg];
					code_Chain3[seg2] = str->numChains;
				}
				code_Eaddr3[seg2] = Cblk->Seg_Saddr[seg] + Cblk->Seg_Length[seg];
				SigMag_F          = 0;
				seg2++;
			} else if (numPasses % 3 == 1) { // CleanUp pss
				code_length3[seg2] = Cblk->Seg_Length[seg];
				code_passes3[seg2] = numPasses;
				code_Saddr3[seg2]  = Cblk->Seg_Saddr[seg];
				code_Eaddr3[seg2]  = Cblk->Seg_Saddr[seg] + Cblk->Seg_Length[seg];
				code_Chain3[seg2]  = str->numChains;
				seg2++;
			}
		}
		if (numPasses == 0) {
			numSegments = 0;
		} else if (numPasses <= 10)
			numSegments = 1;
		else if (((numPasses - 10) % 3) == 1) {
			numSegments = 1;
			numSegments = (ubyte2) (numSegments + (((numPasses - 10) / 3) * 2));
			numSegments = (ubyte2) (numSegments + (((numPasses - 10) % 3) ? 1 : 0));
		} else if (((numPasses - 10) % 3) == 2) {
			numSegments = 1;
			numSegments = (ubyte2) (numSegments + (((numPasses - 10) / 3) * 2));
			numSegments = (ubyte2) (numSegments + (((numPasses - 10) % 3) ? 1 : 0));
		} else if (((numPasses - 10) % 3) == 0) {
			numSegments = 1;
			numSegments = (ubyte2) (numSegments + (((numPasses - 10) / 3) * 2));
			numSegments = (ubyte2) (numSegments + (((numPasses - 10) % 3) ? 1 : 0));
		}

		str  = StreamChainParentSearch(str);
		pass = 0;
		mq_Index_init(dec);
		dec->areg = 0x80000000;
		numPasses = 0;
		for (seg2 = 0; seg2 < numSegments; seg2++) {
			str = StreamChainParentSearch(str);
			while (code_Chain3[seg2] != str->numChains) {
				str = str->child;
			}

			str->cur_p = code_Saddr3[seg2];
			str->bits  = 8;
			if (!seg2) {
				str       = mqdec_init_B(dec, code_length3[seg2], code_Eaddr3[seg2], str);
				dec->areg = 0x80000000;
			}
			for (; pass < code_passes3[seg2]; pass++) {
				Lazy = (char) ((pass < 10) ? 0 : 1);
				switch (pass % 3) {
					case 1:
						if (Lazy) {
							// patch_0 by osamu start
							if (EECod->CbVcausal) {
								if (nullptr
								    == (str = dec_sigpass_Lazy_vcausal(Cblk, Codec->CbData, bittop,
								                                       code_Eaddr3[seg2], str))) {
									printf("[T1Dec] :: dec_sigpass_Lazy_vcausal error (pass=%d)\n", pass);
									return EXIT_FAILURE;
								}
							} else {
								if (nullptr
								    == (str = dec_sigpass_Lazy(Cblk, Codec->CbData, bittop,
								                               code_Eaddr3[seg2], str, FlagFlag))) {
									printf("[T1Dec]:: dec_sigpass_Lazy error (pass=%d)\n", pass);
									return EXIT_FAILURE;
								}
							}
						} else {
							if (EECod->CbVcausal) {
								if (nullptr
								    == (str = dec_sigpass_vcausal(Cblk, Codec->CbData, bittop, ctx_table,
								                                  dec, str, FlagFlag))) {
									printf("[T1Dec]:: dec_sigpass_vcausal error (pass=%d)\n", pass);
									return EXIT_FAILURE;
								}
							} else {
								if (nullptr
								    == (str = dec_sigpass(Cblk, Codec->CbData, bittop, ctx_table, dec, str,
								                          FlagFlag))) {
									printf("[T1Dec]:: dec_sigpass error (pass=%d)\n", pass);
									return EXIT_FAILURE;
								}
							}
						}
						if (EECod->CbReset) mq_Index_init(dec);
						BreakFlag = 1;
						break;
					case 2:
						if (Lazy) {
							// patch_2 by osamu start
							if (EECod->CbVcausal) {
								if (nullptr
								    == (str = dec_magpass_Lazy_vcausal(Cblk, Codec->CbData, bittop,
								                                       code_Eaddr3[seg2], str))) {
									printf("[T1Dec] :: dec_magpass_Lazy error (pass=%d)\n", pass);
									return EXIT_FAILURE;
								}
							} else {
								if (nullptr
								    == (str = dec_magpass_Lazy(Cblk, Codec->CbData, bittop,
								                               code_Eaddr3[seg2], str, FlagFlag))) {
									printf("[T1Dec]:: dec_magpass_Lazy error (pass=%d)\n", pass);
									return EXIT_FAILURE;
								}
							}
							// patch_2 by osamu end
						} else {
							// patch_3 by osamu start
							if (EECod->CbVcausal) {
								if (nullptr
								    == (str = dec_magpass_vcausal(Cblk, Codec->CbData, bittop, dec, str,
								                                  FlagFlag))) {
									printf("[T1Dec] :: dec_magpass_vcausal error (pass=%d)\n", pass);
									return EXIT_FAILURE;
								}
							} else {
								if (nullptr
								    == (str =
								            dec_magpass(Cblk, Codec->CbData, bittop, dec, str, FlagFlag))) {
									printf("[T1Dec] :: dec_magpass error. (pass=%d)\n", pass);
									return EXIT_FAILURE;
								}
							}
						}
						if (EECod->CbReset) mq_Index_init(dec);
						BreakFlag = 1;
						break;
					case 0:
						if (Lazy) {
							str       = mqdec_init_B(dec, code_length3[seg2], code_Eaddr3[seg2], str);
							dec->areg = 0x80000000;
						}

						// patch_4 by osamu start
						if (EECod->CbVcausal) {
							if (nullptr
							    == (str = dec_clnpass_vcausal(Cblk, Codec->CbData, bittop, ctx_table, dec,
							                                  str, EECod->CbSegmentSymbol, FlagFlag))) {
								printf("[T1Dec]:: dec_clnpass_vcausal error pass=%d bittop=%d\n", pass,
								       bittop);
								return EXIT_FAILURE;
							}
						} else {
							if (nullptr
							    == (str = dec_clnpass(Cblk, Codec->CbData, bittop, ctx_table, dec, str,
							                          EECod->CbSegmentSymbol, FlagFlag))) {
								printf("[T1Dec] :: dec_clnpass error pass=%d bittop=%d\n", pass, bittop);
								return EXIT_FAILURE;
							}
						}
						if (EECod->CbReset) mq_Index_init(dec);
						BreakFlag = 0;
						bittop--;
						one >>= 1;
						break;
				}
			}
		}
	} else {
		code_length = 0;
		numPasses   = 0;
		for (Layer = 0; Layer < EECod->numLayer; Layer++) {
			code_length += Cblk->Layer_Length[Layer];
			numPasses = (ubyte2) (numPasses + Cblk->Layer_passes[Layer]);
		}
		if (code_length) {
			str = nullptr;
			if (nullptr == (str = StreamChainMake(str, code_length, JPEG2000))) {
				printf("[T1Dec]:: StreamchainMake error, when code_length!=0\n");
				return EXIT_FAILURE;
			}
			str->cur_p = 0;
			for (Layer = 0; Layer < EECod->numLayer; Layer++) {
				memcpy(&str->buf[str->cur_p], &s->buf[Cblk->Layer_Saddr[Layer]],
				       sizeof(uchar) * Cblk->Layer_Length[Layer]);
				str->cur_p += Cblk->Layer_Length[Layer];
			}
			str->cur_p = 0;
		} else {
			str = nullptr;
			if (nullptr == (str = StreamChainMake(str, 1, JPEG2000))) {
				printf("[T1Dec]:: StreamchainMake error, when code_length==0\n");
				return EXIT_FAILURE;
			}
			str->cur_p = 0;
		}
		pass = 0;
		mq_Index_init(dec);
		dec->areg = 0x80000000;
		nextSaddr = 0;
		for (seg = 0; seg < Cblk->numSegment; seg++) {
			str->cur_p  = nextSaddr;
			str->bits   = 8;
			code_length = Cblk->Seg_Length[seg];
			Eaddr       = str->cur_p + code_length;
			nextSaddr += code_length;
			for (pass2 = 0; pass2 < Cblk->Seg_passes[seg]; pass2++, pass++) {
				Lazy = 0;
				if (EECod->CbBypass) {
					Lazy = (char) ((pass < 10) ? 0 : 1);
				}
				if (Lazy) {
				} else {
					if (nullptr == (str = mqdec_init(dec, code_length, str))) {
						printf("[T1Dec] :: mqdec_init error.\n");
						return EXIT_FAILURE;
					}
					dec->areg = 0x80000000;
				}
				switch (pass % 3) {
					case 1:
						if (Lazy && (!EECod->CbVcausal)) {
							if (nullptr
							    == (str = dec_sigpass_Lazy(Cblk, Codec->CbData, bittop, Eaddr, str,
							                               FlagFlag))) {
								printf("[T1Dec] :: dec_sigpass_Lazy error (pass=%d)\n", pass);
								return EXIT_FAILURE;
							}
						} else if (Lazy && EECod->CbVcausal) {
							if (nullptr
							    == (str = dec_sigpass_Lazy_vcausal(Cblk, Codec->CbData, bittop, Eaddr,
							                                       str))) {
								printf("[T1Dec] :: dec_sigpass_Lazy_vcausal error pass=%d bittop=%d\n",
								       pass, bittop);
								return EXIT_FAILURE;
							}
						} else if ((!Lazy) && EECod->CbVcausal) {
							if (nullptr
							    == (str = dec_sigpass_vcausal(Cblk, Codec->CbData, bittop, ctx_table, dec,
							                                  str, FlagFlag))) {
								printf("[T1Dec] :: dec_sigpass_vcausal error pass=%d bittop=%d\n", pass,
								       bittop);
								return EXIT_FAILURE;
							}
						} else {
							if (nullptr
							    == (str = dec_sigpass(Cblk, Codec->CbData, bittop, ctx_table, dec, str,
							                          FlagFlag))) {
								printf("[T1Dec] :: dec_sigpass error pass=%d bittop=%d\n", pass, bittop);
								return EXIT_FAILURE;
							}
						}
						if (EECod->CbReset) mq_Index_init(dec);
						BreakFlag = 1;
						break;
					case 2:
						if (Lazy && (EECod->CbVcausal)) {
							if (nullptr
							    == (str = dec_magpass_Lazy_vcausal(Cblk, Codec->CbData, bittop, Eaddr,
							                                       str))) {
								printf("[T1Dec]:: dec_magpass_vcausal error pass=%d bittop=%d\n", pass,
								       bittop);
								return EXIT_FAILURE;
							}
						} else if (Lazy && (!EECod->CbVcausal)) {
							if (nullptr
							    == (str = dec_magpass_Lazy(Cblk, Codec->CbData, bittop, Eaddr, str,
							                               FlagFlag))) {
								printf("[T1Dec]:: dec_magpass_Lazy error (pass=%d)\n", pass);
								return EXIT_FAILURE;
							}
						} else if ((!Lazy) && EECod->CbVcausal) {
							if (nullptr
							    == (str = dec_magpass_vcausal(Cblk, Codec->CbData, bittop, dec, str,
							                                  FlagFlag))) {
								printf("[T1Dec]:: dec_magpass_vcausal error pass=%d bittop=%d\n", pass,
								       bittop);
								return EXIT_FAILURE;
							}
						} else {
							if (nullptr
							    == (str = dec_magpass(Cblk, Codec->CbData, bittop, dec, str, FlagFlag))) {
								printf("[T1Dec]:: dec_magpass error. pass=%d bittop=%d\n", pass, bittop);
								return EXIT_FAILURE;
							}
						}
						if (EECod->CbReset) mq_Index_init(dec);
						BreakFlag = 1;
						break;
					case 0:
						if (Lazy) {
							str       = mqdec_init_B(dec, code_length, Eaddr, str);
							dec->areg = 0x80000000;
						}
						if (EECod->CbVcausal) {
							if (nullptr
							    == (str = dec_clnpass_vcausal(Cblk, Codec->CbData, bittop, ctx_table, dec,
							                                  str, EECod->CbSegmentSymbol, FlagFlag))) {
								printf("[T1Dec]:: dec_clnpass_vcausal error pass=%d bittop=%d\n", pass,
								       bittop);
								return EXIT_FAILURE;
							}
						} else {
							if (nullptr
							    == (str = dec_clnpass(Cblk, Codec->CbData, bittop, ctx_table, dec, str,
							                          EECod->CbSegmentSymbol, FlagFlag))) {
								printf("[T1Dec]:: dec_clnpass error pass=%d bittop=%d\n", pass, bittop);
								return EXIT_FAILURE;
							}
						}
						if (EECod->CbReset) mq_Index_init(dec);
						BreakFlag = 0;
						bittop--;
						one >>= 1;
						break;
				}
			}
		}
	}

	if (onROI) {
		for (jj = Codec->CbData->tby0; jj < Codec->CbData->tby1; jj++) {
			for (ii = Codec->CbData->tbx0; ii < Codec->CbData->tbx1; ii++) {
				if (mask_roi[Cblk->Mb] & Codec->CbData->data[jj * col1stepCb + ii * row1stepCb]) {
				} else {
					if (Codec->CbData->data[jj * col1stepCb + ii * row1stepCb] & 0x80000000) {
						Codec->CbData->data[jj * col1stepCb + ii * row1stepCb] =
						    Codec->CbData->data[jj * col1stepCb + ii * row1stepCb] & 0x7fffffff;
						Codec->CbData->data[jj * col1stepCb + ii * row1stepCb] =
						    (Codec->CbData->data[jj * col1stepCb + ii * row1stepCb] << shift); // Cblk->Mb;
						if (Codec->CbData->data[jj * col1stepCb + ii * row1stepCb])
							Codec->CbData->data[jj * col1stepCb + ii * row1stepCb] |= 0x80000000;
					} else
						Codec->CbData->data[jj * col1stepCb + ii * row1stepCb] <<= shift; // Cblk->Mb;
				}
			}
		}
	}

	Half_Bit = one >> BreakFlag;
	// r(Half data add or sub)
	for (jj = Codec->CbData->tby0; jj < Codec->CbData->tby1; jj++) {
		for (ii = Codec->CbData->tbx0; ii < Codec->CbData->tbx1; ii++) {
			if (Codec->CbData->data[jj * col1stepCb + ii * row1stepCb])
				Codec->CbData->data[jj * col1stepCb + ii * row1stepCb] |= Half_Bit;
		}
	}

	jj = Cblk->tcby0 - Cblk->Band->tby0;
	ii = Cblk->tcbx0 - Cblk->Band->tbx0;
	DecWaveIL53_4(&Cblk->Band->data[jj * Cblk->Band->col1step + ii * Cblk->Band->row1step],
	              Cblk->Band->col1step, Codec->CbData);

	return EXIT_SUCCESS;
}

#if 0
byte4 T1Dec_HTJ2K( struct Cblk_s *Cblk, struct Codec_s *Codec, struct EECod_s *EECod, struct EERgn_s *EERgn, struct StreamChain_s *s, ubyte2 ccc )
{
	byte2	shift;
	char	onROI;
	byte2	Z1_Layer=-1, Z2_Layer=-1, Z3_Layer=-1;
	byte2	width, height,height1,height2;
	bool	refined;
	byte4	Lcup, Scup, Pcup, Lref;
	struct	RRawCodec_s *codecR;
	struct	MelCodec_s *codecM;
	struct	fRawCodec_s *codecf;
	struct	StreamChain_s *str_rraw;
	struct	StreamChain_s *str_melc;
	struct	StreamChain_s *str_fraw;
	struct	StreamChain_s *str_sigp;
	struct	StreamChain_s *str_mgrf;
	struct	Image_s *Image_qinf=Codec->Image_qinf;
	struct	Image_s *Image_qmex=Codec->Image_qmex;
	struct	Image_s *Image_epcironK=Codec->Image_epcironK;
	struct	Image_s *Image_epciron1=Codec->Image_epciron1;
	struct	Image_s *Image_msbits=Codec->Image_msbits;;
	struct	Image_s *Image_mex=Codec->Image_mex;
	struct	Image_s *Image_sigma=Codec->Image_sigma; 
	byte4	rraw_melc_Length_F=0, fraw_Length_F=0;
	uchar	error_flag;
	byte4	jjj,iii, col1step_Band;
	byte4	*D_TS, *D_;
	uchar	seg;
	uchar	Z1_flag=0, Z2_flag=0, Z3_flag=0;
	char	MagRef;
	char	s_blk, temp_s_blk0, temp_s_blk1;
	char	s_blk2=0;
	ubyte2	lll, sum, PlaceHolderPass;
	byte4	roimask;
	char	SignBit;
	byte4	Half_Bit;

	height = (byte2)( Cblk->tcby1-Cblk->tcby0 );
	width  = (byte2)( Cblk->tcbx1-Cblk->tcbx0 );
	if( height%4 ){
		height1 = (byte2)( height/4+1 );
		height2 = (byte2)( height1*4 - height );
	}
	else{
		height1 = (byte2)( height/4 );
		height2 = 0;
	}

	lll=0;
	sum=0;

	while(	!Cblk->Layer_Length[lll] ){
		sum = (ubyte2)( sum+Cblk->Layer_passes[lll] );
		lll++;
	}
	sum = (ubyte2)( sum+Cblk->Layer_passes[lll] );

	temp_s_blk0 = (char)( sum/3 );
	temp_s_blk1 = (char)( sum%3 );
	s_blk = (char)( temp_s_blk0 - ((temp_s_blk1)? 0:1) );
	Cblk->s_blk = (uchar)( Cblk->zerobits + s_blk );
	Cblk->Nb = (ubyte2)( Cblk->s_blk+1 );


	shift = 0;
	onROI = 0;
	if(EERgn!=NULL){
		if( EERgn->Crgn==ccc){
			shift = EERgn->SPrgn;
			onROI = 1;
		}
	}

	MagRef=0;
	sum=0;
	PlaceHolderPass=0;
	for(seg=0 ; seg<Cblk->numSegment ; seg++ ){
		if(!Z1_flag){
			sum = (ubyte2)( sum+Cblk->Seg_passes[seg] );
			if( Cblk->Seg_Length[seg] ){
				PlaceHolderPass = (ubyte2)( sum/3 );
				sum = (ubyte2)( sum-PlaceHolderPass*3 );
				PlaceHolderPass--;
				s_blk2 = (char)ceil2( PlaceHolderPass, 3) ;
				Z1_Layer = seg;
				Z1_flag=1;
			}
		}
		else{
			sum = (ubyte2)( sum+Cblk->Seg_passes[seg] );
			if( sum && (!Z2_flag) ){
				if( sum==2 ){
					Z2_Layer = seg;
					Z2_flag=1;
				}
				else if(sum==3){
					Z2_Layer = seg;
					Z2_flag=1;
					Z3_flag=2;
				}
			}
			else{
				if( Z2_flag && (!Z3_flag)){
					if( sum==3 ){
						Z3_Layer = seg;
						Z3_flag=1;
					}
				}
			}
		}
	}
	if( sum==3 )
		MagRef=1;
	Lcup = Cblk->Seg_Length[Z1_Layer];
	Cblk->melc_Length = Scup = (s->buf[Cblk->Seg_Saddr[Z1_Layer]+Cblk->Seg_Length[Z1_Layer]-1]<<4) + (s->buf[Cblk->Seg_Saddr[Z1_Layer]+Cblk->Seg_Length[Z1_Layer]-2]&0xf);
	Pcup = Lcup - Scup;

	//fraw
	if(!Pcup)
		fraw_Length_F=1;
	if(NULL == (str_fraw=StreamChainMake( NULL, (Pcup+fraw_Length_F), JPEG2000)) ){
		printf("[T1Dec_HTJ2K] :: str_fraw malloc error. (Z1_Layer=%d Lcup(=Seg_Length)=%d Scup(=melc_Length (%x(h) %x(h)) )=%d Pcup=%d s->cur_p=%x(h) Seg_Saddr=%x(h) Seg_Length=%x(h) )\n", Z1_Layer, Lcup, Scup, (Cblk->Seg_Saddr[Z1_Layer]+Cblk->Seg_Length[Z1_Layer]-1), (Cblk->Seg_Saddr[Z1_Layer]+Cblk->Seg_Length[Z1_Layer]-2), Pcup, s->cur_p, Cblk->Seg_Saddr[Z1_Layer], Cblk->Seg_Length[Z1_Layer] );
		return	EXIT_FAILURE;
	}
	if(Pcup){
		memcpy(&str_fraw->buf[0] ,&s->buf[Cblk->Seg_Saddr[Z1_Layer]], sizeof(uchar)*Pcup );
		str_fraw->total_p = str_fraw->buf_length;
	}
	else{
		str_fraw->buf[0]=0xff;
		str_fraw->total_p = str_fraw->buf_length;
	}
	//melc
	if(!Scup)
		rraw_melc_Length_F=1;
	if(NULL == (str_melc=StreamChainMake( NULL, (Scup+rraw_melc_Length_F), JPEG2000)) ){
		printf("[T1Dec_HTJ2K] :: str_melc malloc error. (Lcup=%d Scup=%d Pcup=%d)\n", Lcup, Scup, Pcup );
		return	EXIT_FAILURE;
	}
	if(Scup){
		memcpy( &str_melc->buf[0], &s->buf[Cblk->Seg_Saddr[Z1_Layer]+Pcup], sizeof(uchar)*Scup );
		str_melc->total_p = str_melc->buf_length;
	}
	//rraw
	if(NULL == (str_rraw=StreamChainMake( NULL, (Scup+rraw_melc_Length_F), JPEG2000)) ){
		printf("[T1Dec_HTJ2K] :: str_rraw malloc error. (Lcup=%d Scup=%d Pcup=%d)\n", Lcup, Scup, Pcup );
		return	EXIT_FAILURE;
	}
	if(Scup){
		memcpy( &str_rraw->buf[0], &s->buf[Cblk->Seg_Saddr[Z1_Layer]+Pcup], sizeof(uchar)*Scup );
		str_rraw->total_p = str_rraw->buf_length;
		str_rraw->cur_p = str_rraw->buf_length-1;
	}
	str_melc->cur_p=str_melc->buf_length;
	str_fraw->cur_p=str_fraw->buf_length;

	codecR = Codec->codecR;//RRawCodecCreate();
	codecf = Codec->codecf;//new struct fRawCodec_s;
	codecM = Codec->codecM;//MelCodecCreate();
	str_fraw->cur_p=0;
	str_melc->cur_p=0;
	DecfRawStart( str_fraw, codecf );
	DecMelStart( str_melc, codecM );
	DecRRawStart( str_rraw, codecR );

	refined=0;
	if(onROI){
		if( Z2_flag )
			Cblk->p= 31-(Cblk->s_blk+1);//Cblk->p;
		else
			Cblk->p= 31-(Cblk->s_blk+1);//Cblk->p;
	}
	else{
		if( Z2_flag )
			Cblk->p= 31-(Cblk->s_blk+1);//Cblk->p;
		else
			Cblk->p= 31-(Cblk->s_blk+1);//Cblk->p;
	}

	decode_cln(	Cblk->Band, Cblk, Codec, Image_qinf, Image_qmex, Image_msbits, Image_mex, Image_sigma, Image_epcironK, Image_epciron1, codecR, codecM, codecf, str_rraw, str_melc, str_fraw );



	if(Z2_flag){
		if( Z2_Layer==-1 )
			Lref=0;
		else
			Lref = Cblk->Seg_Length[Z2_Layer];
		if( Z3_flag==1 )
			Lref+= Cblk->Seg_Length[Z3_Layer];

		if( Lref==0 )
			str_sigp=NULL;
		else{
			if(NULL == (str_sigp=StreamChainMake( NULL, Lref, JPEG2000 )) ){
				printf("[T1Dec_HTJ2K] :: str_sigp malloc error. (Lref=%d)\n", Lref );
				return	EXIT_FAILURE;
			}
			if( Z2_Layer!=-1 )
				memcpy(&str_sigp->buf[0] ,&s->buf[Cblk->Seg_Saddr[Z2_Layer]], sizeof(uchar)*Cblk->Seg_Length[Z2_Layer] );
			if( Z3_flag )
				if( Z3_Layer!=-1 )
					memcpy(&str_sigp->buf[Cblk->Seg_Length[Z2_Layer]] ,&s->buf[Cblk->Seg_Saddr[Z3_Layer]], sizeof(uchar)*Cblk->Seg_Length[Z3_Layer] );
			initSigProp( str_sigp );
		}

		if(MagRef){
			if( Lref==0 )
				str_mgrf=NULL;
			else{
				if(NULL == (str_mgrf=StreamChainMake( NULL, (Lref), JPEG2000)) ){
					printf("[T1Dec_HTJ2K] :: str_mgrf malloc error. (Lref=%d)\n", Lref );
					return	EXIT_FAILURE;
				}
				memcpy( &str_mgrf->buf[0], &str_sigp->buf[0], sizeof(uchar)*Lref );
				initMagRef( str_mgrf, Lref );
			}
		}
		else
			str_mgrf=NULL;

		decode_SigProp(	Cblk->Band, Cblk, Codec, str_sigp, MagRef, str_mgrf, EECod->CbVcausal );
		Cblk->Nb++;
	}

	error_flag=0;

	SignBit = (char)( (Codec->Siz->Ssiz[ccc]&0x80) ? 2:1 );

	if(onROI){
		roimask=-1;
		roimask<<=(Cblk->p+shift);
		height = (byte2)( Cblk->tcby1-Cblk->tcby0 );
		width  = (byte2)( Cblk->tcbx1-Cblk->tcbx0 );
		col1step_Band = Cblk->Band->col1step;
		D_TS = (byte4 *)Cblk->Band->data;
		D_TS = &D_TS[(Cblk->tcby0-Cblk->Band->tby0)*Cblk->Band->col1step + (Cblk->tcbx0-Cblk->Band->tbx0)*Cblk->Band->row1step];
		for( jjj=0 ; jjj<height ; jjj++, D_TS+=col1step_Band ){
			D_=D_TS;
			for( iii=0 ; iii<width ; iii++, ++D_ ){
				if(mask_roi[Cblk->Mb]&(*D_) ){
				}
				else{
					if( (*D_)&0x80000000 ){
						*D_ = (*D_)&0x7fffffff;
						*D_ = ((*D_)<<shift);//Cblk->Mb;
						if(*D_)
							*D_ = (*D_) | 0x80000000;
					}
					else
						(*D_) = ((*D_)<<shift);//Cblk->Mb;
				}
			}
		}
	}

	//r(Half data add or sub)
	Half_Bit =1<<(30-Cblk->Nb);
	col1step_Band = Cblk->Band->col1step;
	D_TS = (byte4 *)Cblk->Band->data;
	D_TS = &D_TS[(Cblk->tcby0-Cblk->Band->tby0)*Cblk->Band->col1step + (Cblk->tcbx0-Cblk->Band->tbx0)*Cblk->Band->row1step];
	for( jjj=0 ; jjj<height ; jjj++, D_TS+=col1step_Band ){
		D_=D_TS;
		for( iii=0 ; iii<width ; iii++, ++D_ ){
			if(*D_)
				*D_ |= Half_Bit;
		}
	}

	return EXIT_SUCCESS;
}
#else
byte4 T1Dec_HTJ2K(struct Cblk_s *Cblk, struct Codec_s *Codec, struct EECod_s *EECod, struct EERgn_s *EERgn,
                  struct StreamChain_s *s, ubyte2 ccc) {
	byte2 shift;
	char onROI;
	byte2 Z1_Layer = -1, Z2_Layer = -1, Z3_Layer = -1;
	byte2 width, height, height1, height2;
	bool refined;
	byte4 Lcup, Scup, Pcup, Lref;
	struct RRawCodec_s *codecR;
	struct MelCodec_s *codecM;
	struct fRawCodec_s *codecf;
	struct StreamChain_s *str_rraw;
	struct StreamChain_s *str_melc;
	struct StreamChain_s *str_fraw;
	struct StreamChain_s *str_sigp;
	struct StreamChain_s *str_mgrf;
	struct Image_s *Image_qinf     = Codec->Image_qinf;
	struct Image_s *Image_qmex     = Codec->Image_qmex;
	struct Image_s *Image_epcironK = Codec->Image_epcironK;
	struct Image_s *Image_epciron1 = Codec->Image_epciron1;
	struct Image_s *Image_msbits   = Codec->Image_msbits;
	;
	struct Image_s *Image_mex   = Codec->Image_mex;
	struct Image_s *Image_sigma = Codec->Image_sigma;
	byte4 rraw_melc_Length_F = 0, fraw_Length_F = 0;
	uchar error_flag;
	byte4 jjj, iii, col1step_Band;
	byte4 *D_TS, *D_;
	uchar seg;
	uchar Z1_flag = 0, Z2_flag = 0, Z3_flag = 0;
	char MagRef;
	char s_blk, temp_s_blk0, temp_s_blk1;
	char s_blk2 = 0;
	ubyte2 lll, sum, PlaceHolderPass;
	byte4 roimask;
	char SignBit;
	byte4 Half_Bit;

	height = (byte2) (Cblk->tcby1 - Cblk->tcby0);
	width  = (byte2) (Cblk->tcbx1 - Cblk->tcbx0);
	if (height % 4) {
		height1 = (byte2) (height / 4 + 1);
		height2 = (byte2) (height1 * 4 - height);
	} else {
		height1 = (byte2) (height / 4);
		height2 = 0;
	}

	lll = 0;
	sum = 0;

	while (!Cblk->Layer_Length[lll]) {
		sum = (ubyte2) (sum + Cblk->Layer_passes[lll]);
		lll++;
	}
	sum = (ubyte2) (sum + Cblk->Layer_passes[lll]);

	temp_s_blk0 = (char) (sum / 3);
	temp_s_blk1 = (char) (sum % 3);
	s_blk       = (char) (temp_s_blk0 - ((temp_s_blk1) ? 0 : 1));
	Cblk->s_blk = (uchar) (Cblk->zerobits + s_blk);
	Cblk->Nb    = (ubyte2) (Cblk->s_blk + 1);

	shift = 0;
	onROI = 0;
	if (EERgn != nullptr) {
		if (EERgn->Crgn == ccc) {
			shift = EERgn->SPrgn;
			onROI = 1;
		}
	}

	MagRef          = 0;
	sum             = 0;
	PlaceHolderPass = 0;
	for (seg = 0; seg < Cblk->numSegment; seg++) {
		if (!Z1_flag) {
			sum = (ubyte2) (sum + Cblk->Seg_passes[seg]);
			if (Cblk->Seg_Length[seg]) {
				PlaceHolderPass = (ubyte2) (sum / 3);
				sum             = (ubyte2) (sum - PlaceHolderPass * 3);
				PlaceHolderPass--;
				s_blk2   = (char) ceil2(PlaceHolderPass, 3);
				Z1_Layer = seg;
				Z1_flag  = 1;
			}
		} else {
			sum = (ubyte2) (sum + Cblk->Seg_passes[seg]);
			if (sum && (!Z2_flag)) {
				if (sum == 2) {
					Z2_Layer = seg;
					Z2_flag  = 1;
				} else if (sum == 3) {
					Z2_Layer = seg;
					Z2_flag  = 1;
					Z3_flag  = 2;
				}
			} else {
				if (Z2_flag && (!Z3_flag)) {
					if (sum == 3) {
						Z3_Layer = seg;
						Z3_flag  = 1;
					}
				}
			}
		}
	}
	if (sum == 3) MagRef = 1;
	Lcup              = Cblk->Seg_Length[Z1_Layer];
	Cblk->melc_Length = Scup = (s->buf[Cblk->Seg_Saddr[Z1_Layer] + Cblk->Seg_Length[Z1_Layer] - 1] << 4)
	                           + (s->buf[Cblk->Seg_Saddr[Z1_Layer] + Cblk->Seg_Length[Z1_Layer] - 2] & 0xf);
	Pcup = Lcup - Scup;

	// fraw
	if (!Pcup) fraw_Length_F = 1;
	if (nullptr == (str_fraw = StreamChainMake(nullptr, (Pcup + fraw_Length_F), JPEG2000))) {
		printf("[T1Dec_HTJ2K]:: str_fraw malloc error. (Z1_Layer=%d Lcup(=Seg_Length)=%d Scup(=melc_Length "
		       "(%x(h) %x(h)) )=%d Pcup=%d s->cur_p=%x(h) Seg_Saddr=%x(h) Seg_Length=%x(h) )\n",
		       Z1_Layer, Lcup, Scup, (Cblk->Seg_Saddr[Z1_Layer] + Cblk->Seg_Length[Z1_Layer] - 1),
		       (Cblk->Seg_Saddr[Z1_Layer] + Cblk->Seg_Length[Z1_Layer] - 2), Pcup, s->cur_p,
		       Cblk->Seg_Saddr[Z1_Layer], Cblk->Seg_Length[Z1_Layer]);
		return EXIT_FAILURE;
	}
	if (Pcup) {
		memcpy(&str_fraw->buf[0], &s->buf[Cblk->Seg_Saddr[Z1_Layer]], sizeof(uchar) * Pcup);
		str_fraw->total_p = str_fraw->buf_length;
	} else {
		str_fraw->buf[0]  = 0xff;
		str_fraw->total_p = str_fraw->buf_length;
	}
	// melc
	if (!Scup) rraw_melc_Length_F = 1;
	if (nullptr == (str_melc = StreamChainMake(nullptr, (Scup + rraw_melc_Length_F), JPEG2000))) {
		printf("[T1Dec_HTJ2K]:: str_melc malloc error. (Lcup=%d Scup=%d Pcup=%d)\n", Lcup, Scup, Pcup);
		return EXIT_FAILURE;
	}
	if (Scup) {
		memcpy(&str_melc->buf[0], &s->buf[Cblk->Seg_Saddr[Z1_Layer] + Pcup], sizeof(uchar) * Scup);
		str_melc->total_p = str_melc->buf_length;
	}
	// rraw
	if (nullptr == (str_rraw = StreamChainMake(nullptr, (Scup + rraw_melc_Length_F), JPEG2000))) {
		printf("[T1Dec_HTJ2K]:: str_rraw malloc error. (Lcup=%d Scup=%d Pcup=%d)\n", Lcup, Scup, Pcup);
		return EXIT_FAILURE;
	}
	if (Scup) {
		memcpy(&str_rraw->buf[0], &s->buf[Cblk->Seg_Saddr[Z1_Layer] + Pcup], sizeof(uchar) * Scup);
		str_rraw->total_p = str_rraw->buf_length;
		str_rraw->cur_p   = str_rraw->buf_length - 1;
	}
	str_melc->cur_p = str_melc->buf_length;
	str_fraw->cur_p = str_fraw->buf_length;

	codecR          = Codec->codecR; // RRawCodecCreate();
	codecf          = Codec->codecf; // new struct fRawCodec_s;
	codecM          = Codec->codecM; // MelCodecCreate();
	str_fraw->cur_p = 0;
	str_melc->cur_p = 0;
	DecfRawStart(str_fraw, codecf);
	if (EXIT_FAILURE == DecMelStart(str_melc, codecM)) {
		printf("[T1Dec_HTJ2K]:: DecMelStart error. \n");
		return EXIT_FAILURE;
	}
	DecRRawStart(str_rraw, codecR);

	refined = 0;
	if (onROI) {
		if (Z2_flag)
			Cblk->p = 31 - (Cblk->s_blk + 1); // Cblk->p;
		else
			Cblk->p = 31 - (Cblk->s_blk + 1); // Cblk->p;
	} else {
		if (Z2_flag)
			Cblk->p = 31 - (Cblk->s_blk + 1); // Cblk->p;
		else
			Cblk->p = 31 - (Cblk->s_blk + 1); // Cblk->p;
	}

	if (EXIT_FAILURE
	    == decode_cln(Cblk->Band, Cblk, Codec, Image_qinf, Image_qmex, Image_msbits, Image_mex, Image_sigma,
	                  Image_epcironK, Image_epciron1, codecR, codecM, codecf, str_rraw, str_melc,
	                  str_fraw)) {
		printf("[T1Dec_HTJ2K] decode_cln error.\n");
		return EXIT_FAILURE;
	}

	if (Z2_flag) {
		if (Z2_Layer == -1)
			Lref = 0;
		else
			Lref = Cblk->Seg_Length[Z2_Layer];
		if (Z3_flag == 1) Lref += Cblk->Seg_Length[Z3_Layer];

		if (Lref == 0)
			str_sigp = nullptr;
		else {
			if (nullptr == (str_sigp = StreamChainMake(nullptr, Lref, JPEG2000))) {
				printf("[T1Dec_HTJ2K]:: str_sigp malloc error. (Lref=%d)\n", Lref);
				return EXIT_FAILURE;
			}
			if (Z2_Layer != -1)
				memcpy(&str_sigp->buf[0], &s->buf[Cblk->Seg_Saddr[Z2_Layer]],
				       sizeof(uchar) * Cblk->Seg_Length[Z2_Layer]);
			if (Z3_flag)
				if (Z3_Layer != -1)
					memcpy(&str_sigp->buf[Cblk->Seg_Length[Z2_Layer]], &s->buf[Cblk->Seg_Saddr[Z3_Layer]],
					       sizeof(uchar) * Cblk->Seg_Length[Z3_Layer]);
			initSigProp(str_sigp);
		}

		if (MagRef) {
			if (Lref == 0)
				str_mgrf = nullptr;
			else {
				if (nullptr == (str_mgrf = StreamChainMake(nullptr, (Lref), JPEG2000))) {
					printf("[T1Dec_HTJ2K]:: str_mgrf malloc error. (Lref=%d)\n", Lref);
					return EXIT_FAILURE;
				}
				memcpy(&str_mgrf->buf[0], &str_sigp->buf[0], sizeof(uchar) * Lref);
				initMagRef(str_mgrf, Lref);
			}
		} else
			str_mgrf = nullptr;
		if (str_sigp != nullptr) {
			decode_SigProp(Cblk->Band, Cblk, Codec, str_sigp, MagRef, str_mgrf, EECod->CbVcausal);
		}
		Cblk->Nb++;
	}

	error_flag = 0;

	SignBit = (char) ((Codec->Siz->Ssiz[ccc] & 0x80) ? 2 : 1);

	if (onROI) {
		roimask = -1;
		roimask <<= (Cblk->p + shift);
		height        = (byte2) (Cblk->tcby1 - Cblk->tcby0);
		width         = (byte2) (Cblk->tcbx1 - Cblk->tcbx0);
		col1step_Band = Cblk->Band->col1step;
		D_TS          = (byte4 *) Cblk->Band->data;
		D_TS          = &D_TS[(Cblk->tcby0 - Cblk->Band->tby0) * Cblk->Band->col1step
                     + (Cblk->tcbx0 - Cblk->Band->tbx0) * Cblk->Band->row1step];
		for (jjj = 0; jjj < height; jjj++, D_TS += col1step_Band) {
			D_ = D_TS;
			for (iii = 0; iii < width; iii++, ++D_) {
				if (mask_roi[Cblk->Mb] & (*D_)) {
				} else {
					if ((*D_) & 0x80000000) {
						*D_ = (*D_) & 0x7fffffff;
						*D_ = ((*D_) << shift); // Cblk->Mb;
						if (*D_) *D_ = (*D_) | 0x80000000;
					} else
						(*D_) = ((*D_) << shift); // Cblk->Mb;
				}
			}
		}
	}

	// r(Half data add or sub)
	Half_Bit      = 1 << (30 - Cblk->Nb);
	col1step_Band = Cblk->Band->col1step;
	D_TS          = (byte4 *) Cblk->Band->data;
	D_TS          = &D_TS[(Cblk->tcby0 - Cblk->Band->tby0) * Cblk->Band->col1step
                 + (Cblk->tcbx0 - Cblk->Band->tbx0) * Cblk->Band->row1step];
	for (jjj = 0; jjj < height; jjj++, D_TS += col1step_Band) {
		D_ = D_TS;
		for (iii = 0; iii < width; iii++, ++D_) {
			if (*D_) *D_ |= Half_Bit;
		}
	}

	return EXIT_SUCCESS;
}
#endif

void DecWaveIL53_4(byte4 *Data_, byte4 col1step, struct Image_s *CbData) {
	byte4 *data_, *D_TS, *D_;
	byte4 j, i;

	data_ = CbData->data;
	D_TS  = Data_;
	for (j = CbData->tby0; j < CbData->tby1; j++, D_TS += col1step) {
		D_ = D_TS;
		for (i = CbData->tbx0; i < CbData->tbx1; i++, ++D_)
			*D_ = data_[j * CbData->col1step + i * CbData->row1step];
	}
}
