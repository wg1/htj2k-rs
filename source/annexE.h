/*
Copyright (c) 2000-2021, ICT-Link
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the HTJ2K project.
*/

#ifndef ANNEX_E_H
#define ANNEX_E_H

#define QQ_DEBUG1 0 // DECODE
#define QQ_DEBUG2 0 // ENCODE
#define MYU_OFFSET 2048
#define MYU_SHIFT 11
#define QQ_r 0.5

// Table 0
static char EEE0_LL[33] = {
    0x000,          // Dummy
    0,     0, 0, 0, // 1LL,2LL,3LL,4LL
    0,     0, 0, 0, // 5LL,6LL,7LL,8LL
    0,     0, 0, 0, // 9LL,10LL,11LL,12LL
    0,     0, 0, 0, // 13LL,14LL,15LL,16LL
    0,     0, 0, 0, // 17LL,18LL,19LL,20LL
    0,     0, 0, 0, // 21LL,22LL,23LL,24LL
    0,     0, 0, 0, // 25LL,26LL,27LL,28LL
    0,     0, 0, 0  // 29LL,30LL,31LL,32LL
};

static char EEE0_HLLHHH[33][3] = {
    {0, 0, 0}, // Dummy
    {0, 0, 0}, // 1HL,1LH,1HH
    {0, 0, 0}, // 2HL,2LH,2HH
    {0, 0, 0}, // 3HL,3LH,3HH
    {0, 0, 0}, // 4HL,4LH,4HH
    {0, 0, 0}, // 5HL,5LH,5HH
    {0, 0, 0}, // 6HL,6LH,6HH
    {0, 0, 0}, // 7HL,7LH,7HH
    {0, 0, 0}, // 8HL,8LH,8HH
    {0, 0, 0}, // 9HL,9LH,9HH
    {0, 0, 0}, // 10HL,10LH,10HH
    {0, 0, 0}, // 11HL,11LH,11HH
    {0, 0, 0}, // 12HL,12LH,12HH
    {0, 0, 0}, // 13HL,13LH,13HH
    {0, 0, 0}, // 14HL,14LH,14HH
    {0, 0, 0}, // 15HL,15LH,15HH
    {0, 0, 0}, // 16HL,16LH,16HH
    {0, 0, 0}, // 17HL,17LH,17HH
    {0, 0, 0}, // 18HL,18LH,18HH
    {0, 0, 0}, // 19HL,19LH,19HH
    {0, 0, 0}, // 20HL,20LH,20HH
    {0, 0, 0}, // 21HL,21LH,21HH
    {0, 0, 0}, // 22HL,22LH,22HH
    {0, 0, 0}, // 23HL,23LH,23HH
    {0, 0, 0}, // 24HL,24LH,24HH
    {0, 0, 0}, // 25HL,25LH,25HH
    {0, 0, 0}, // 26HL,26LH,26HH
    {0, 0, 0}, // 27HL,27LH,27HH
    {0, 0, 0}, // 28HL,28LH,28HH
    {0, 0, 0}, // 29HL,29LH,29HH
    {0, 0, 0}, // 30HL,30LH,30HH
    {0, 0, 0}, // 31HL,31LH,31HH
    {0, 0, 0}  // 32HL,32LH,32HH
};

static ubyte2 MYU0_LL[33] = {
    0x000,                      // Dummy
    0x000, 0x000, 0x000, 0x000, // 1LL,2LL,3LL,4LL
    0x000, 0x000, 0x000, 0x000, // 5LL,6LL,7LL,8LL
    0x000, 0x000, 0x000, 0x000, // 9LL,10LL,11LL,12LL
    0x000, 0x000, 0x000, 0x000, // 13LL,14LL,15LL,16LL
    0x000, 0x000, 0x000, 0x000, // 17LL,18LL,19LL,20LL
    0x000, 0x000, 0x000, 0x000, // 21LL,22LL,23LL,24LL
    0x000, 0x000, 0x000, 0x000, // 25LL,26LL,27LL,28LL
    0x000, 0x000, 0x000, 0x000  // 29LL,30LL,31LL,32LL
};

static ubyte2 MYU0_HLLHHH[33][3] = {
    {0x000, 0x000, 0x000}, // Dummy
    {0x000, 0x000, 0x000}, // 1HL,1LH,1HH
    {0x000, 0x000, 0x000}, // 2HL,2LH,2HH
    {0x000, 0x000, 0x000}, // 3HL,3LH,3HH
    {0x000, 0x000, 0x000}, // 4HL,4LH,4HH
    {0x000, 0x000, 0x000}, // 5HL,5LH,5HH
    {0x000, 0x000, 0x000}, // 6HL,6LH,6HH
    {0x000, 0x000, 0x000}, // 7HL,7LH,7HH
    {0x000, 0x000, 0x000}, // 8HL,8LH,8HH
    {0x000, 0x000, 0x000}, // 9HL,9LH,9HH
    {0x000, 0x000, 0x000}, // 10HL,10LH,10HH
    {0x000, 0x000, 0x000}, // 11HL,11LH,11HH
    {0x000, 0x000, 0x000}, // 12HL,12LH,12HH
    {0x000, 0x000, 0x000}, // 13HL,13LH,13HH
    {0x000, 0x000, 0x000}, // 14HL,14LH,14HH
    {0x000, 0x000, 0x000}, // 15HL,15LH,15HH
    {0x000, 0x000, 0x000}, // 16HL,16LH,16HH
    {0x000, 0x000, 0x000}, // 17HL,17LH,17HH
    {0x000, 0x000, 0x000}, // 18HL,18LH,18HH
    {0x000, 0x000, 0x000}, // 19HL,19LH,19HH
    {0x000, 0x000, 0x000}, // 20HL,20LH,20HH
    {0x000, 0x000, 0x000}, // 21HL,21LH,21HH
    {0x000, 0x000, 0x000}, // 22HL,22LH,22HH
    {0x000, 0x000, 0x000}, // 23HL,23LH,23HH
    {0x000, 0x000, 0x000}, // 24HL,24LH,24HH
    {0x000, 0x000, 0x000}, // 25HL,25LH,25HH
    {0x000, 0x000, 0x000}, // 26HL,26LH,26HH
    {0x000, 0x000, 0x000}, // 27HL,27LH,27HH
    {0x000, 0x000, 0x000}, // 28HL,28LH,28HH
    {0x000, 0x000, 0x000}, // 29HL,29LH,29HH
    {0x000, 0x000, 0x000}, // 30HL,30LH,30HH
    {0x000, 0x000, 0x000}, // 31HL,31LH,31HH
    {0x000, 0x000, 0x000}  // 32HL,32LH,32HH
};

// Table 1
static char EEE1_LL[33] = {
    0x000,                // Dummy
    -1,    -3,  -4,  -5,  // 1LL,2LL,3LL,4LL
    -6,    -7,  -8,  -9,  // 5LL,6LL,7LL,8LL
    -10,   -11, -12, -13, // 9LL,10LL,11LL,12LL
    -14,   -15, -16, -17, // 13LL,14LL,15LL,16LL
    -18,   -19, -20, -21, // 17LL,18LL,19LL,20LL
    -22,   -23, -24, -25, // 21LL,22LL,23LL,24LL
    -26,   -27, -28, -29, // 25LL,26LL,27LL,28LL
    -30,   -31, -32, -33  // 29LL,30LL,31LL,32LL
};

static char EEE1_HLLHHH[33][3] = {
    {0, 0, 0},       // Dummy
    {-1, -1, 0},     // 1HL,1LH,1HH
    {-1, -1, 0},     // 2HL,2LH,2HH
    {-3, -3, -2},    // 3HL,3LH,3HH
    {-4, -4, -4},    // 4HL,4LH,4HH
    {-5, -5, -4},    // 5HL,5LH,5HH
    {-6, -6, -5},    // 6HL,6LH,6HH
    {-7, -7, -6},    // 7HL,7LH,7HH
    {-8, -8, -7},    // 8HL,8LH,8HH
    {-9, -9, -8},    // 9HL,9LH,9HH
    {-10, -10, -9},  // 10HL,10LH,10HH
    {-11, -11, -10}, // 11HL,11LH,11HH
    {-12, -12, -11}, // 12HL,12LH,12HH
    {-13, -13, -12}, // 13HL,13LH,13HH
    {-14, -14, -13}, // 14HL,14LH,14HH
    {-15, -15, -14}, // 15HL,15LH,15HH
    {-16, -16, -15}, // 16HL,16LH,16HH
    {-17, -17, -16}, // 17HL,17LH,17HH
    {-18, -18, -17}, // 18HL,18LH,18HH
    {-19, -19, -18}, // 19HL,19LH,19HH
    {-20, -20, -19}, // 20HL,20LH,20HH
    {-21, -21, -20}, // 21HL,21LH,21HH
    {-22, -22, -21}, // 22HL,22LH,22HH
    {-23, -23, -22}, // 23HL,23LH,23HH
    {-24, -24, -23}, // 24HL,24LH,24HH
    {-25, -25, -24}, // 25HL,25LH,25HH
    {-26, -26, -25}, // 26HL,26LH,26HH
    {-27, -27, -26}, // 27HL,27LH,27HH
    {-28, -28, -27}, // 28HL,28LH,28HH
    {-29, -29, -28}, // 29HL,29LH,29HH
    {-30, -30, -29}, // 30HL,30LH,30HH
    {-31, -31, -30}, // 31HL,31LH,31HH
    {-32, -32, -31}  // 32HL,32LH,32HH
};

static ubyte2 MYU1_LL[33] = {
    0x000,                      // Dummy
    0x024, 0x786, 0x735, 0x71E, // 1LL,2LL,3LL,4LL
    0x71E, 0x71E, 0x71E, 0x71E, // 5LL,6LL,7LL,8LL
    0x71E, 0x71E, 0x71E, 0x71E, // 9LL,10LL,11LL,12LL
    0x71E, 0x71E, 0x71E, 0x71E, // 13LL,14LL,15LL,16LL
    0x71E, 0x71E, 0x71E, 0x71E, // 17LL,18LL,19LL,20LL
    0x71E, 0x71E, 0x71E, 0x71E, // 21LL,22LL,23LL,24LL
    0x71E, 0x71E, 0x71E, 0x71E, // 25LL,26LL,27LL,28LL
    0x71E, 0x71E, 0x71E, 0x71E  // 29LL,30LL,31LL,32LL
};

static ubyte2 MYU1_HLLHHH[33][3] = {
    {0x000, 0x000, 0x000}, // Dummy
    {0x7D2, 0x7D2, 0x761}, // 1HL,1LH,1HH
    {0x003, 0x003, 0x045}, // 2HL,2LH,2HH
    {0x74C, 0x74C, 0x764}, // 3HL,3LH,3HH
    {0x700, 0x700, 0x7E2}, // 4HL,4LH,4HH
    {0x700, 0x700, 0x6E2}, // 5HL,5LH,5HH
    {0x700, 0x700, 0x7E2}, // 6HL,6LH,6HH
    {0x700, 0x700, 0x6E2}, // 7HL,7LH,7HH
    {0x700, 0x700, 0x7E2}, // 8HL,8LH,8HH
    {0x700, 0x700, 0x6E2}, // 9HL,9LH,9HH
    {0x700, 0x700, 0x7E2}, // 10HL,10LH,10HH
    {0x700, 0x700, 0x6E2}, // 11HL,11LH,11HH
    {0x700, 0x700, 0x7E2}, // 12HL,12LH,12HH
    {0x700, 0x700, 0x6E2}, // 13HL,13LH,13HH
    {0x700, 0x700, 0x7E2}, // 14HL,14LH,14HH
    {0x700, 0x700, 0x6E2}, // 15HL,15LH,15HH
    {0x700, 0x700, 0x7E2}, // 16HL,16LH,16HH
    {0x700, 0x700, 0x6E2}, // 17HL,17LH,17HH
    {0x700, 0x700, 0x7E2}, // 18HL,18LH,18HH
    {0x700, 0x700, 0x6E2}, // 19HL,19LH,19HH
    {0x700, 0x700, 0x7E2}, // 20HL,20LH,20HH
    {0x700, 0x700, 0x6E2}, // 21HL,21LH,21HH
    {0x700, 0x700, 0x7E2}, // 22HL,22LH,22HH
    {0x700, 0x700, 0x6E2}, // 23HL,23LH,23HH
    {0x700, 0x700, 0x7E2}, // 24HL,24LH,24HH
    {0x700, 0x700, 0x6E2}, // 25HL,25LH,25HH
    {0x700, 0x700, 0x7E2}, // 26HL,26LH,26HH
    {0x700, 0x700, 0x6E2}, // 27HL,27LH,27HH
    {0x700, 0x700, 0x7E2}, // 28HL,28LH,28HH
    {0x700, 0x700, 0x6E2}, // 29HL,29LH,29HH
    {0x700, 0x700, 0x7E2}, // 30HL,30LH,30HH
    {0x700, 0x700, 0x6E2}, // 31HL,31LH,31HH
    {0x700, 0x700, 0x7E2}  // 32HL,32LH,32HH
};

// Table 2
static char EEE2_LL[33] = {
    0x000,                // Dummy
    -1,    -3,  -4,  -5,  // 1LL,2LL,3LL,4LL
    -6,    -7,  -8,  -9,  // 5LL,6LL,7LL,8LL
    -10,   -11, -12, -13, // 9LL,10LL,11LL,12LL
    -14,   -15, -16, -17, // 13LL,14LL,15LL,16LL
    -18,   -19, -20, -21, // 17LL,18LL,19LL,20LL
    -22,   -23, -24, -25, // 21LL,22LL,23LL,24LL
    -26,   -27, -28, -29, // 25LL,26LL,27LL,28LL
    -30,   -31, -32, -33  // 29LL,30LL,31LL,32LL
};

static char EEE2_HLLHHH[33][3] = {
    {0, 0, 0},       // Dummy
    {-1, -1, 0},     // 1HL,1LH,1HH
    {-1, -1, 0},     // 2HL,2LH,2HH
    {-3, -3, -2},    // 3HL,3LH,3HH
    {-4, -4, -4},    // 4HL,4LH,4HH
    {-5, -5, -4},    // 5HL,5LH,5HH
    {-6, -6, -5},    // 6HL,6LH,6HH
    {-7, -7, -6},    // 7HL,7LH,7HH
    {-8, -8, -7},    // 8HL,8LH,8HH
    {-9, -9, -8},    // 9HL,9LH,9HH
    {-10, -10, -9},  // 10HL,10LH,10HH
    {-11, -11, -10}, // 11HL,11LH,11HH
    {-12, -12, -11}, // 12HL,12LH,12HH
    {-13, -13, -12}, // 13HL,13LH,13HH
    {-14, -14, -13}, // 14HL,14LH,14HH
    {-15, -15, -14}, // 15HL,15LH,15HH
    {-16, -16, -15}, // 16HL,16LH,16HH
    {-17, -17, -16}, // 17HL,17LH,17HH
    {-18, -18, -17}, // 18HL,18LH,18HH
    {-19, -19, -18}, // 19HL,19LH,19HH
    {-20, -20, -19}, // 20HL,20LH,20HH
    {-21, -21, -20}, // 21HL,21LH,21HH
    {-22, -22, -21}, // 22HL,22LH,22HH
    {-23, -23, -22}, // 23HL,23LH,23HH
    {-24, -24, -23}, // 24HL,24LH,24HH
    {-25, -25, -24}, // 25HL,25LH,25HH
    {-26, -26, -25}, // 26HL,26LH,26HH
    {-27, -27, -26}, // 27HL,27LH,27HH
    {-28, -28, -27}, // 28HL,28LH,28HH
    {-29, -29, -28}, // 29HL,29LH,29HH
    {-30, -30, -29}, // 30HL,30LH,30HH
    {-31, -31, -30}, // 31HL,31LH,31HH
    {-32, -32, -31}  // 32HL,32LH,32HH
};

static ubyte2 MYU2_LL[33] = {
    0x000,                      // Dummy
    0x024, 0x786, 0x735, 0x71E, // 1LL,2LL,3LL,4LL
    0x71E, 0x71E, 0x71E, 0x71E, // 5LL,6LL,7LL,8LL
    0x71E, 0x71E, 0x71E, 0x71E, // 9LL,10LL,11LL,12LL
    0x71E, 0x71E, 0x71E, 0x71E, // 13LL,14LL,15LL,16LL
    0x71E, 0x71E, 0x71E, 0x71E, // 17LL,18LL,19LL,20LL
    0x71E, 0x71E, 0x71E, 0x71E, // 21LL,22LL,23LL,24LL
    0x71E, 0x71E, 0x71E, 0x71E, // 25LL,26LL,27LL,28LL
    0x71E, 0x71E, 0x71E, 0x71E  // 29LL,30LL,31LL,32LL
};

static ubyte2 MYU2_HLLHHH[33][3] = {
    {0x000, 0x000, 0x000}, // Dummy
    {0x7D2, 0x7D2, 0x761}, // 1HL,1LH,1HH
    {0x003, 0x003, 0x045}, // 2HL,2LH,2HH
    {0x74C, 0x74C, 0x764}, // 3HL,3LH,3HH
    {0x700, 0x700, 0x7E2}, // 4HL,4LH,4HH
    {0x700, 0x700, 0x6E2}, // 5HL,5LH,5HH
    {0x700, 0x700, 0x7E2}, // 6HL,6LH,6HH
    {0x700, 0x700, 0x6E2}, // 7HL,7LH,7HH
    {0x700, 0x700, 0x7E2}, // 8HL,8LH,8HH
    {0x700, 0x700, 0x6E2}, // 9HL,9LH,9HH
    {0x700, 0x700, 0x7E2}, // 10HL,10LH,10HH
    {0x700, 0x700, 0x6E2}, // 11HL,11LH,11HH
    {0x700, 0x700, 0x7E2}, // 12HL,12LH,12HH
    {0x700, 0x700, 0x6E2}, // 13HL,13LH,13HH
    {0x700, 0x700, 0x7E2}, // 14HL,14LH,14HH
    {0x700, 0x700, 0x6E2}, // 15HL,15LH,15HH
    {0x700, 0x700, 0x7E2}, // 16HL,16LH,16HH
    {0x700, 0x700, 0x6E2}, // 17HL,17LH,17HH
    {0x700, 0x700, 0x7E2}, // 18HL,18LH,18HH
    {0x700, 0x700, 0x6E2}, // 19HL,19LH,19HH
    {0x700, 0x700, 0x7E2}, // 20HL,20LH,20HH
    {0x700, 0x700, 0x6E2}, // 21HL,21LH,21HH
    {0x700, 0x700, 0x7E2}, // 22HL,22LH,22HH
    {0x700, 0x700, 0x6E2}, // 23HL,23LH,23HH
    {0x700, 0x700, 0x7E2}, // 24HL,24LH,24HH
    {0x700, 0x700, 0x6E2}, // 25HL,25LH,25HH
    {0x700, 0x700, 0x7E2}, // 26HL,26LH,26HH
    {0x700, 0x700, 0x6E2}, // 27HL,27LH,27HH
    {0x700, 0x700, 0x7E2}, // 28HL,28LH,28HH
    {0x700, 0x700, 0x6E2}, // 29HL,29LH,29HH
    {0x700, 0x700, 0x7E2}, // 30HL,30LH,30HH
    {0x700, 0x700, 0x6E2}, // 31HL,31LH,31HH
    {0x700, 0x700, 0x7E2}  // 32HL,32LH,32HH
};

// Table3
static char EEE3_LL[33] = {
    0x000,                // Dummy
    -1,    -3,  -4,  -5,  // 1LL,2LL,3LL,4LL
    -6,    -7,  -8,  -9,  // 5LL,6LL,7LL,8LL
    -10,   -11, -12, -13, // 9LL,10LL,11LL,12LL
    -14,   -15, -16, -17, // 13LL,14LL,15LL,16LL
    -18,   -19, -20, -21, // 17LL,18LL,19LL,20LL
    -22,   -23, -24, -25, // 21LL,22LL,23LL,24LL
    -26,   -27, -28, -29, // 25LL,26LL,27LL,28LL
    -30,   -31, -32, -33  // 29LL,30LL,31LL,32LL
};

static char EEE3_HLLHHH[33][3] = {
    {0, 0, 0},       // Dummy
    {-1, -1, 0},     // 1HL,1LH,1HH
    {-1, -1, 0},     // 2HL,2LH,2HH
    {-3, -3, -2},    // 3HL,3LH,3HH
    {-4, -4, -4},    // 4HL,4LH,4HH
    {-5, -5, -4},    // 5HL,5LH,5HH
    {-6, -6, -5},    // 6HL,6LH,6HH
    {-7, -7, -6},    // 7HL,7LH,7HH
    {-8, -8, -7},    // 8HL,8LH,8HH
    {-9, -9, -8},    // 9HL,9LH,9HH
    {-10, -10, -9},  // 10HL,10LH,10HH
    {-11, -11, -10}, // 11HL,11LH,11HH
    {-12, -12, -11}, // 12HL,12LH,12HH
    {-13, -13, -12}, // 13HL,13LH,13HH
    {-14, -14, -13}, // 14HL,14LH,14HH
    {-15, -15, -14}, // 15HL,15LH,15HH
    {-16, -16, -15}, // 16HL,16LH,16HH
    {-17, -17, -16}, // 17HL,17LH,17HH
    {-18, -18, -17}, // 18HL,18LH,18HH
    {-19, -19, -18}, // 19HL,19LH,19HH
    {-20, -20, -19}, // 20HL,20LH,20HH
    {-21, -21, -20}, // 21HL,21LH,21HH
    {-22, -22, -21}, // 22HL,22LH,22HH
    {-23, -23, -22}, // 23HL,23LH,23HH
    {-24, -24, -23}, // 24HL,24LH,24HH
    {-25, -25, -24}, // 25HL,25LH,25HH
    {-26, -26, -25}, // 26HL,26LH,26HH
    {-27, -27, -26}, // 27HL,27LH,27HH
    {-28, -28, -27}, // 28HL,28LH,28HH
    {-29, -29, -28}, // 29HL,29LH,29HH
    {-30, -30, -29}, // 30HL,30LH,30HH
    {-31, -31, -30}, // 31HL,31LH,31HH
    {-32, -32, -31}  // 32HL,32LH,32HH
};

static ubyte2 MYU3_LL[33] = {
    0x000,                      // Dummy
    0x024, 0x786, 0x735, 0x71E, // 1LL,2LL,3LL,4LL
    0x71E, 0x71E, 0x71E, 0x71E, // 5LL,6LL,7LL,8LL
    0x71E, 0x71E, 0x71E, 0x71E, // 9LL,10LL,11LL,12LL
    0x71E, 0x71E, 0x71E, 0x71E, // 13LL,14LL,15LL,16LL
    0x71E, 0x71E, 0x71E, 0x71E, // 17LL,18LL,19LL,20LL
    0x71E, 0x71E, 0x71E, 0x71E, // 21LL,22LL,23LL,24LL
    0x71E, 0x71E, 0x71E, 0x71E, // 25LL,26LL,27LL,28LL
    0x71E, 0x71E, 0x71E, 0x71E  // 29LL,30LL,31LL,32LL
};

static ubyte2 MYU3_HLLHHH[33][3] = {
    {0x000, 0x000, 0x000}, // Dummy
    {0x7D2, 0x7D2, 0x761}, // 1HL,1LH,1HH
    {0x003, 0x003, 0x045}, // 2HL,2LH,2HH
    {0x74C, 0x74C, 0x764}, // 3HL,3LH,3HH
    {0x700, 0x700, 0x7E2}, // 4HL,4LH,4HH
    {0x700, 0x700, 0x6E2}, // 5HL,5LH,5HH
    {0x700, 0x700, 0x7E2}, // 6HL,6LH,6HH
    {0x700, 0x700, 0x6E2}, // 7HL,7LH,7HH
    {0x700, 0x700, 0x7E2}, // 8HL,8LH,8HH
    {0x700, 0x700, 0x6E2}, // 9HL,9LH,9HH
    {0x700, 0x700, 0x7E2}, // 10HL,10LH,10HH
    {0x700, 0x700, 0x6E2}, // 11HL,11LH,11HH
    {0x700, 0x700, 0x7E2}, // 12HL,12LH,12HH
    {0x700, 0x700, 0x6E2}, // 13HL,13LH,13HH
    {0x700, 0x700, 0x7E2}, // 14HL,14LH,14HH
    {0x700, 0x700, 0x6E2}, // 15HL,15LH,15HH
    {0x700, 0x700, 0x7E2}, // 16HL,16LH,16HH
    {0x700, 0x700, 0x6E2}, // 17HL,17LH,17HH
    {0x700, 0x700, 0x7E2}, // 18HL,18LH,18HH
    {0x700, 0x700, 0x6E2}, // 19HL,19LH,19HH
    {0x700, 0x700, 0x7E2}, // 20HL,20LH,20HH
    {0x700, 0x700, 0x6E2}, // 21HL,21LH,21HH
    {0x700, 0x700, 0x7E2}, // 22HL,22LH,22HH
    {0x700, 0x700, 0x6E2}, // 23HL,23LH,23HH
    {0x700, 0x700, 0x7E2}, // 24HL,24LH,24HH
    {0x700, 0x700, 0x6E2}, // 25HL,25LH,25HH
    {0x700, 0x700, 0x7E2}, // 26HL,26LH,26HH
    {0x700, 0x700, 0x6E2}, // 27HL,27LH,27HH
    {0x700, 0x700, 0x7E2}, // 28HL,28LH,28HH
    {0x700, 0x700, 0x6E2}, // 29HL,29LH,29HH
    {0x700, 0x700, 0x7E2}, // 30HL,30LH,30HH
    {0x700, 0x700, 0x6E2}, // 31HL,31LH,31HH
    {0x700, 0x700, 0x7E2}  // 32HL,32LH,32HH
};

// Table 4
static char EEE4_LL[33] = {
    0x000,          // Dummy
    1,     1, 1, 1, // 1LL,2LL,3LL,4LL
    1,     1, 1, 1, // 5LL,6LL,7LL,8LL
    1,     1, 1, 1, // 9LL,10LL,11LL,12LL
    1,     1, 1, 1, // 13LL,14LL,15LL,16LL
    1,     1, 1, 1, // 17LL,18LL,19LL,20LL
    1,     1, 1, 1, // 21LL,22LL,23LL,24LL
    1,     1, 1, 1, // 25LL,26LL,27LL,28LL
    1,     1, 1, 1  // 29LL,30LL,31LL,32LL
};

static char EEE4_HLLHHH[33][3] = {
    {0, 0, 0}, // Dummy
    {1, 1, 1}, // 1HL,1LH,1HH
    {1, 1, 1}, // 2HL,2LH,2HH
    {1, 1, 1}, // 3HL,3LH,3HH
    {1, 1, 1}, // 4HL,4LH,4HH
    {1, 1, 1}, // 5HL,5LH,5HH
    {1, 1, 1}, // 6HL,6LH,6HH
    {1, 1, 1}, // 7HL,7LH,7HH
    {1, 1, 1}, // 8HL,8LH,8HH
    {1, 1, 1}, // 9HL,9LH,9HH
    {1, 1, 1}, // 10HL,10LH,10HH
    {1, 1, 1}, // 11HL,11LH,11HH
    {1, 1, 1}, // 12HL,12LH,12HH
    {1, 1, 1}, // 13HL,13LH,13HH
    {1, 1, 1}, // 14HL,14LH,14HH
    {1, 1, 1}, // 15HL,15LH,15HH
    {1, 1, 1}, // 16HL,16LH,16HH
    {1, 1, 1}, // 17HL,17LH,17HH
    {1, 1, 1}, // 18HL,18LH,18HH
    {1, 1, 1}, // 19HL,19LH,19HH
    {1, 1, 1}, // 20HL,20LH,20HH
    {1, 1, 1}, // 21HL,21LH,21HH
    {1, 1, 1}, // 22HL,22LH,22HH
    {1, 1, 1}, // 23HL,23LH,23HH
    {1, 1, 1}, // 24HL,24LH,24HH
    {1, 1, 1}, // 25HL,25LH,25HH
    {1, 1, 1}, // 26HL,26LH,26HH
    {1, 1, 1}, // 27HL,27LH,27HH
    {1, 1, 1}, // 28HL,28LH,28HH
    {1, 1, 1}, // 29HL,29LH,29HH
    {1, 1, 1}, // 30HL,30LH,30HH
    {1, 1, 1}, // 31HL,31LH,31HH
    {1, 1, 1}  // 32HL,32LH,32HH
};

static ubyte2 MYU4_LL[33] = {
    0x000,                      // Dummy
    0x000, 0x000, 0x000, 0x000, // 1LL,2LL,3LL,4LL
    0x000, 0x000, 0x000, 0x000, // 5LL,6LL,7LL,8LL
    0x000, 0x000, 0x000, 0x000, // 9LL,10LL,11LL,12LL
    0x000, 0x000, 0x000, 0x000, // 13LL,14LL,15LL,16LL
    0x000, 0x000, 0x000, 0x000, // 17LL,18LL,19LL,20LL
    0x000, 0x000, 0x000, 0x000, // 21LL,22LL,23LL,24LL
    0x000, 0x000, 0x000, 0x000, // 25LL,26LL,27LL,28LL
    0x000, 0x000, 0x000, 0x000  // 29LL,30LL,31LL,32LL
};

static ubyte2 MYU4_HLLHHH[33][3] = {
    {0x000, 0x000, 0x000}, // Dummy
    {0x000, 0x000, 0x000}, // 1HL,1LH,1HH
    {0x000, 0x000, 0x000}, // 2HL,2LH,2HH
    {0x000, 0x000, 0x000}, // 3HL,3LH,3HH
    {0x000, 0x000, 0x000}, // 4HL,4LH,4HH
    {0x000, 0x000, 0x000}, // 5HL,5LH,5HH
    {0x000, 0x000, 0x000}, // 6HL,6LH,6HH
    {0x000, 0x000, 0x000}, // 7HL,7LH,7HH
    {0x000, 0x000, 0x000}, // 8HL,8LH,8HH
    {0x000, 0x000, 0x000}, // 9HL,9LH,9HH
    {0x000, 0x000, 0x000}, // 10HL,10LH,10HH
    {0x000, 0x000, 0x000}, // 11HL,11LH,11HH
    {0x000, 0x000, 0x000}, // 12HL,12LH,12HH
    {0x000, 0x000, 0x000}, // 13HL,13LH,13HH
    {0x000, 0x000, 0x000}, // 14HL,14LH,14HH
    {0x000, 0x000, 0x000}, // 15HL,15LH,15HH
    {0x000, 0x000, 0x000}, // 16HL,16LH,16HH
    {0x000, 0x000, 0x000}, // 17HL,17LH,17HH
    {0x000, 0x000, 0x000}, // 18HL,18LH,18HH
    {0x000, 0x000, 0x000}, // 19HL,19LH,19HH
    {0x000, 0x000, 0x000}, // 20HL,20LH,20HH
    {0x000, 0x000, 0x000}, // 21HL,21LH,21HH
    {0x000, 0x000, 0x000}, // 22HL,22LH,22HH
    {0x000, 0x000, 0x000}, // 23HL,23LH,23HH
    {0x000, 0x000, 0x000}, // 24HL,24LH,24HH
    {0x000, 0x000, 0x000}, // 25HL,25LH,25HH
    {0x000, 0x000, 0x000}, // 26HL,26LH,26HH
    {0x000, 0x000, 0x000}, // 27HL,27LH,27HH
    {0x000, 0x000, 0x000}, // 28HL,28LH,28HH
    {0x000, 0x000, 0x000}, // 29HL,29LH,29HH
    {0x000, 0x000, 0x000}, // 30HL,30LH,30HH
    {0x000, 0x000, 0x000}, // 31HL,31LH,31HH
    {0x000, 0x000, 0x000}  // 32HL,32LH,32HH
};

// Table 5
static char EEE5_LL[33] = {
    0x000,          // Dummy
    2,     2, 2, 2, // 1LL,2LL,3LL,4LL
    2,     2, 2, 2, // 5LL,6LL,7LL,8LL
    2,     2, 2, 2, // 9LL,10LL,11LL,12LL
    2,     2, 2, 2, // 13LL,14LL,15LL,16LL
    2,     2, 2, 2, // 17LL,18LL,19LL,20LL
    2,     2, 2, 2, // 21LL,22LL,23LL,24LL
    2,     2, 2, 2, // 25LL,26LL,27LL,28LL
    2,     2, 2, 2  // 29LL,30LL,31LL,32LL
};

static char EEE5_HLLHHH[33][3] = {
    {0, 0, 0}, // Dummy
    {2, 2, 2}, // 1HL,1LH,1HH
    {2, 2, 2}, // 2HL,2LH,2HH
    {2, 2, 2}, // 3HL,3LH,3HH
    {2, 2, 2}, // 4HL,4LH,4HH
    {2, 2, 2}, // 5HL,5LH,5HH
    {2, 2, 2}, // 6HL,6LH,6HH
    {2, 2, 2}, // 7HL,7LH,7HH
    {2, 2, 2}, // 8HL,8LH,8HH
    {2, 2, 2}, // 9HL,9LH,9HH
    {2, 2, 2}, // 10HL,10LH,10HH
    {2, 2, 2}, // 11HL,11LH,11HH
    {2, 2, 2}, // 12HL,12LH,12HH
    {2, 2, 2}, // 13HL,13LH,13HH
    {2, 2, 2}, // 14HL,14LH,14HH
    {2, 2, 2}, // 15HL,15LH,15HH
    {2, 2, 2}, // 16HL,16LH,16HH
    {2, 2, 2}, // 17HL,17LH,17HH
    {2, 2, 2}, // 18HL,18LH,18HH
    {2, 2, 2}, // 19HL,19LH,19HH
    {2, 2, 2}, // 20HL,20LH,20HH
    {2, 2, 2}, // 21HL,21LH,21HH
    {2, 2, 2}, // 22HL,22LH,22HH
    {2, 2, 2}, // 23HL,23LH,23HH
    {2, 2, 2}, // 24HL,24LH,24HH
    {2, 2, 2}, // 25HL,25LH,25HH
    {2, 2, 2}, // 26HL,26LH,26HH
    {2, 2, 2}, // 27HL,27LH,27HH
    {2, 2, 2}, // 28HL,28LH,28HH
    {2, 2, 2}, // 29HL,29LH,29HH
    {2, 2, 2}, // 30HL,30LH,30HH
    {2, 2, 2}, // 31HL,31LH,31HH
    {2, 2, 2}  // 32HL,32LH,32HH
};

static ubyte2 MYU5_LL[33] = {
    0x000,                      // Dummy
    0x000, 0x000, 0x000, 0x000, // 1LL,2LL,3LL,4LL
    0x000, 0x000, 0x000, 0x000, // 5LL,6LL,7LL,8LL
    0x000, 0x000, 0x000, 0x000, // 9LL,10LL,11LL,12LL
    0x000, 0x000, 0x000, 0x000, // 13LL,14LL,15LL,16LL
    0x000, 0x000, 0x000, 0x000, // 17LL,18LL,19LL,20LL
    0x000, 0x000, 0x000, 0x000, // 21LL,22LL,23LL,24LL
    0x000, 0x000, 0x000, 0x000, // 25LL,26LL,27LL,28LL
    0x000, 0x000, 0x000, 0x000  // 29LL,30LL,31LL,32LL
};

static ubyte2 MYU5_HLLHHH[33][3] = {
    {0x000, 0x000, 0x000}, // Dummy
    {0x000, 0x000, 0x000}, // 1HL,1LH,1HH
    {0x000, 0x000, 0x000}, // 2HL,2LH,2HH
    {0x000, 0x000, 0x000}, // 3HL,3LH,3HH
    {0x000, 0x000, 0x000}, // 4HL,4LH,4HH
    {0x000, 0x000, 0x000}, // 5HL,5LH,5HH
    {0x000, 0x000, 0x000}, // 6HL,6LH,6HH
    {0x000, 0x000, 0x000}, // 7HL,7LH,7HH
    {0x000, 0x000, 0x000}, // 8HL,8LH,8HH
    {0x000, 0x000, 0x000}, // 9HL,9LH,9HH
    {0x000, 0x000, 0x000}, // 10HL,10LH,10HH
    {0x000, 0x000, 0x000}, // 11HL,11LH,11HH
    {0x000, 0x000, 0x000}, // 12HL,12LH,12HH
    {0x000, 0x000, 0x000}, // 13HL,13LH,13HH
    {0x000, 0x000, 0x000}, // 14HL,14LH,14HH
    {0x000, 0x000, 0x000}, // 15HL,15LH,15HH
    {0x000, 0x000, 0x000}, // 16HL,16LH,16HH
    {0x000, 0x000, 0x000}, // 17HL,17LH,17HH
    {0x000, 0x000, 0x000}, // 18HL,18LH,18HH
    {0x000, 0x000, 0x000}, // 19HL,19LH,19HH
    {0x000, 0x000, 0x000}, // 20HL,20LH,20HH
    {0x000, 0x000, 0x000}, // 21HL,21LH,21HH
    {0x000, 0x000, 0x000}, // 22HL,22LH,22HH
    {0x000, 0x000, 0x000}, // 23HL,23LH,23HH
    {0x000, 0x000, 0x000}, // 24HL,24LH,24HH
    {0x000, 0x000, 0x000}, // 25HL,25LH,25HH
    {0x000, 0x000, 0x000}, // 26HL,26LH,26HH
    {0x000, 0x000, 0x000}, // 27HL,27LH,27HH
    {0x000, 0x000, 0x000}, // 28HL,28LH,28HH
    {0x000, 0x000, 0x000}, // 29HL,29LH,29HH
    {0x000, 0x000, 0x000}, // 30HL,30LH,30HH
    {0x000, 0x000, 0x000}, // 31HL,31LH,31HH
    {0x000, 0x000, 0x000}  // 32HL,32LH,32HH
};

// Table 6
static char EEE6_LL[33] = {
    0x000,          // Dummy
    4,     4, 4, 4, // 1LL,2LL,3LL,4LL
    4,     4, 4, 4, // 5LL,6LL,7LL,8LL
    4,     4, 4, 4, // 9LL,10LL,11LL,12LL
    4,     4, 4, 4, // 13LL,14LL,15LL,16LL
    4,     4, 4, 4, // 17LL,18LL,19LL,20LL
    4,     4, 4, 4, // 21LL,22LL,23LL,24LL
    4,     4, 4, 4, // 25LL,26LL,27LL,28LL
    4,     4, 4, 4  // 29LL,30LL,31LL,32LL
};

static char EEE6_HLLHHH[33][3] = {
    {0, 0, 0}, // Dummy
    {4, 4, 4}, // 1HL,1LH,1HH
    {4, 4, 4}, // 2HL,2LH,2HH
    {4, 4, 4}, // 3HL,3LH,3HH
    {4, 4, 4}, // 4HL,4LH,4HH
    {4, 4, 4}, // 5HL,5LH,5HH
    {4, 4, 4}, // 6HL,6LH,6HH
    {4, 4, 4}, // 7HL,7LH,7HH
    {4, 4, 4}, // 8HL,8LH,8HH
    {4, 4, 4}, // 9HL,9LH,9HH
    {4, 4, 4}, // 10HL,10LH,10HH
    {4, 4, 4}, // 11HL,11LH,11HH
    {4, 4, 4}, // 12HL,12LH,12HH
    {4, 4, 4}, // 13HL,13LH,13HH
    {4, 4, 4}, // 14HL,14LH,14HH
    {4, 4, 4}, // 15HL,15LH,15HH
    {4, 4, 4}, // 16HL,16LH,16HH
    {4, 4, 4}, // 17HL,17LH,17HH
    {4, 4, 4}, // 18HL,18LH,18HH
    {4, 4, 4}, // 19HL,19LH,19HH
    {4, 4, 4}, // 20HL,20LH,20HH
    {4, 4, 4}, // 21HL,21LH,21HH
    {4, 4, 4}, // 22HL,22LH,22HH
    {4, 4, 4}, // 23HL,23LH,23HH
    {4, 4, 4}, // 24HL,24LH,24HH
    {4, 4, 4}, // 25HL,25LH,25HH
    {4, 4, 4}, // 26HL,26LH,26HH
    {4, 4, 4}, // 27HL,27LH,27HH
    {4, 4, 4}, // 28HL,28LH,28HH
    {4, 4, 4}, // 29HL,29LH,29HH
    {4, 4, 4}, // 30HL,30LH,30HH
    {4, 4, 4}, // 31HL,31LH,31HH
    {4, 4, 4}  // 32HL,32LH,32HH
};

static ubyte2 MYU6_LL[33] = {
    0x000,                      // Dummy
    0x000, 0x000, 0x000, 0x000, // 1LL,2LL,3LL,4LL
    0x000, 0x000, 0x000, 0x000, // 5LL,6LL,7LL,8LL
    0x000, 0x000, 0x000, 0x000, // 9LL,10LL,11LL,12LL
    0x000, 0x000, 0x000, 0x000, // 13LL,14LL,15LL,16LL
    0x000, 0x000, 0x000, 0x000, // 17LL,18LL,19LL,20LL
    0x000, 0x000, 0x000, 0x000, // 21LL,22LL,23LL,24LL
    0x000, 0x000, 0x000, 0x000, // 25LL,26LL,27LL,28LL
    0x000, 0x000, 0x000, 0x000  // 29LL,30LL,31LL,32LL
};

static ubyte2 MYU6_HLLHHH[33][3] = {
    {0x000, 0x000, 0x000}, // Dummy
    {0x000, 0x000, 0x000}, // 1HL,1LH,1HH
    {0x000, 0x000, 0x000}, // 2HL,2LH,2HH
    {0x000, 0x000, 0x000}, // 3HL,3LH,3HH
    {0x000, 0x000, 0x000}, // 4HL,4LH,4HH
    {0x000, 0x000, 0x000}, // 5HL,5LH,5HH
    {0x000, 0x000, 0x000}, // 6HL,6LH,6HH
    {0x000, 0x000, 0x000}, // 7HL,7LH,7HH
    {0x000, 0x000, 0x000}, // 8HL,8LH,8HH
    {0x000, 0x000, 0x000}, // 9HL,9LH,9HH
    {0x000, 0x000, 0x000}, // 10HL,10LH,10HH
    {0x000, 0x000, 0x000}, // 11HL,11LH,11HH
    {0x000, 0x000, 0x000}, // 12HL,12LH,12HH
    {0x000, 0x000, 0x000}, // 13HL,13LH,13HH
    {0x000, 0x000, 0x000}, // 14HL,14LH,14HH
    {0x000, 0x000, 0x000}, // 15HL,15LH,15HH
    {0x000, 0x000, 0x000}, // 16HL,16LH,16HH
    {0x000, 0x000, 0x000}, // 17HL,17LH,17HH
    {0x000, 0x000, 0x000}, // 18HL,18LH,18HH
    {0x000, 0x000, 0x000}, // 19HL,19LH,19HH
    {0x000, 0x000, 0x000}, // 20HL,20LH,20HH
    {0x000, 0x000, 0x000}, // 21HL,21LH,21HH
    {0x000, 0x000, 0x000}, // 22HL,22LH,22HH
    {0x000, 0x000, 0x000}, // 23HL,23LH,23HH
    {0x000, 0x000, 0x000}, // 24HL,24LH,24HH
    {0x000, 0x000, 0x000}, // 25HL,25LH,25HH
    {0x000, 0x000, 0x000}, // 26HL,26LH,26HH
    {0x000, 0x000, 0x000}, // 27HL,27LH,27HH
    {0x000, 0x000, 0x000}, // 28HL,28LH,28HH
    {0x000, 0x000, 0x000}, // 29HL,29LH,29HH
    {0x000, 0x000, 0x000}, // 30HL,30LH,30HH
    {0x000, 0x000, 0x000}, // 31HL,31LH,31HH
    {0x000, 0x000, 0x000}  // 32HL,32LH,32HH
};

// Table 7
static char EEE7_LL[33] = {
    0x000,          // Dummy
    8,     8, 8, 8, // 1LL,2LL,3LL,4LL
    8,     8, 8, 8, // 5LL,6LL,7LL,8LL
    8,     8, 8, 8, // 9LL,10LL,11LL,12LL
    8,     8, 8, 8, // 13LL,14LL,15LL,16LL
    8,     8, 8, 8, // 17LL,18LL,19LL,20LL
    8,     8, 8, 8, // 21LL,22LL,23LL,24LL
    8,     8, 8, 8, // 25LL,26LL,27LL,28LL
    8,     8, 8, 8  // 29LL,30LL,31LL,32LL
};

static char EEE7_HLLHHH[33][3] = {
    {0, 0, 0}, // Dummy
    {8, 8, 8}, // 1HL,1LH,1HH
    {8, 8, 8}, // 2HL,2LH,2HH
    {8, 8, 8}, // 3HL,3LH,3HH
    {8, 8, 8}, // 4HL,4LH,4HH
    {8, 8, 8}, // 5HL,5LH,5HH
    {8, 8, 8}, // 6HL,6LH,6HH
    {8, 8, 8}, // 7HL,7LH,7HH
    {8, 8, 8}, // 8HL,8LH,8HH
    {8, 8, 8}, // 9HL,9LH,9HH
    {8, 8, 8}, // 10HL,10LH,10HH
    {8, 8, 8}, // 11HL,11LH,11HH
    {8, 8, 8}, // 12HL,12LH,12HH
    {8, 8, 8}, // 13HL,13LH,13HH
    {8, 8, 8}, // 14HL,14LH,14HH
    {8, 8, 8}, // 15HL,15LH,15HH
    {8, 8, 8}, // 16HL,16LH,16HH
    {8, 8, 8}, // 17HL,17LH,17HH
    {8, 8, 8}, // 18HL,18LH,18HH
    {8, 8, 8}, // 19HL,19LH,19HH
    {8, 8, 8}, // 20HL,20LH,20HH
    {8, 8, 8}, // 21HL,21LH,21HH
    {8, 8, 8}, // 22HL,22LH,22HH
    {8, 8, 8}, // 23HL,23LH,23HH
    {8, 8, 8}, // 24HL,24LH,24HH
    {8, 8, 8}, // 25HL,25LH,25HH
    {8, 8, 8}, // 26HL,26LH,26HH
    {8, 8, 8}, // 27HL,27LH,27HH
    {8, 8, 8}, // 28HL,28LH,28HH
    {8, 8, 8}, // 29HL,29LH,29HH
    {8, 8, 8}, // 30HL,30LH,30HH
    {8, 8, 8}, // 31HL,31LH,31HH
    {8, 8, 8}  // 32HL,32LH,32HH
};

static ubyte2 MYU7_LL[33] = {
    0x000,                      // Dummy
    0x000, 0x000, 0x000, 0x000, // 1LL,2LL,3LL,4LL
    0x000, 0x000, 0x000, 0x000, // 5LL,6LL,7LL,8LL
    0x000, 0x000, 0x000, 0x000, // 9LL,10LL,11LL,12LL
    0x000, 0x000, 0x000, 0x000, // 13LL,14LL,15LL,16LL
    0x000, 0x000, 0x000, 0x000, // 17LL,18LL,19LL,20LL
    0x000, 0x000, 0x000, 0x000, // 21LL,22LL,23LL,24LL
    0x000, 0x000, 0x000, 0x000, // 25LL,26LL,27LL,28LL
    0x000, 0x000, 0x000, 0x000  // 29LL,30LL,31LL,32LL
};

static ubyte2 MYU7_HLLHHH[33][3] = {
    {0x000, 0x000, 0x000}, // Dummy
    {0x000, 0x000, 0x000}, // 1HL,1LH,1HH
    {0x000, 0x000, 0x000}, // 2HL,2LH,2HH
    {0x000, 0x000, 0x000}, // 3HL,3LH,3HH
    {0x000, 0x000, 0x000}, // 4HL,4LH,4HH
    {0x000, 0x000, 0x000}, // 5HL,5LH,5HH
    {0x000, 0x000, 0x000}, // 6HL,6LH,6HH
    {0x000, 0x000, 0x000}, // 7HL,7LH,7HH
    {0x000, 0x000, 0x000}, // 8HL,8LH,8HH
    {0x000, 0x000, 0x000}, // 9HL,9LH,9HH
    {0x000, 0x000, 0x000}, // 10HL,10LH,10HH
    {0x000, 0x000, 0x000}, // 11HL,11LH,11HH
    {0x000, 0x000, 0x000}, // 12HL,12LH,12HH
    {0x000, 0x000, 0x000}, // 13HL,13LH,13HH
    {0x000, 0x000, 0x000}, // 14HL,14LH,14HH
    {0x000, 0x000, 0x000}, // 15HL,15LH,15HH
    {0x000, 0x000, 0x000}, // 16HL,16LH,16HH
    {0x000, 0x000, 0x000}, // 17HL,17LH,17HH
    {0x000, 0x000, 0x000}, // 18HL,18LH,18HH
    {0x000, 0x000, 0x000}, // 19HL,19LH,19HH
    {0x000, 0x000, 0x000}, // 20HL,20LH,20HH
    {0x000, 0x000, 0x000}, // 21HL,21LH,21HH
    {0x000, 0x000, 0x000}, // 22HL,22LH,22HH
    {0x000, 0x000, 0x000}, // 23HL,23LH,23HH
    {0x000, 0x000, 0x000}, // 24HL,24LH,24HH
    {0x000, 0x000, 0x000}, // 25HL,25LH,25HH
    {0x000, 0x000, 0x000}, // 26HL,26LH,26HH
    {0x000, 0x000, 0x000}, // 27HL,27LH,27HH
    {0x000, 0x000, 0x000}, // 28HL,28LH,28HH
    {0x000, 0x000, 0x000}, // 29HL,29LH,29HH
    {0x000, 0x000, 0x000}, // 30HL,30LH,30HH
    {0x000, 0x000, 0x000}, // 31HL,31LH,31HH
    {0x000, 0x000, 0x000}  // 32HL,32LH,32HH
};
//                    compts  Band  e myu
static byte2 Qdata0[3][3][16][2] = {{// QF05
                                     {{-1, 1048},
                                      {-1, 1006},
                                      {-1, 1006},
                                      {-1, 970},
                                      {-2, 1024},
                                      {-2, 1024},
                                      {-2, 1000},
                                      {-3, 1085},
                                      {-3, 1085},
                                      {-3, 1104},
                                      {-4, 1870},
                                      {-4, 1870},
                                      {-4, 1996},
                                      {-7, 889},
                                      {-7, 889},
                                      {-7, 807}},
                                     {{-1, 1048},
                                      {-1, 1710},
                                      {-1, 1710},
                                      {-1, 1666},
                                      {-2, 1732},
                                      {-2, 1732},
                                      {-2, 1703},
                                      {-4, 256},
                                      {-4, 256},
                                      {-4, 270},
                                      {-5, 1309},
                                      {-5, 1309},
                                      {-5, 1417},
                                      {-7, 983},
                                      {-7, 983},
                                      {-7, 899}},
                                     {{-1, 1048},
                                      {-1, 1520},
                                      {-1, 1520},
                                      {-1, 1477},
                                      {-2, 1540},
                                      {-2, 1540},
                                      {-2, 1512},
                                      {-4, 41},
                                      {-4, 41},
                                      {-4, 54},
                                      {-5, 747},
                                      {-5, 747},
                                      {-5, 837},
                                      {-7, 111},
                                      {-7, 111},
                                      {-7, 51}}},
                                    {// QF15
                                     {{0, 16},
                                      {0, 2024},
                                      {0, 2024},
                                      {-1, 1976},
                                      {-2, 0},
                                      {-2, 0},
                                      {-2, 2016},
                                      {-3, 41},
                                      {-3, 41},
                                      {-4, 53},
                                      {-4, 564},
                                      {-4, 564},
                                      {-5, 648},
                                      {-6, 1868},
                                      {-6, 1868},
                                      {-7, 1759}},
                                     {{0, 16},
                                      {-1, 458},
                                      {-1, 458},
                                      {-2, 428},
                                      {-2, 472},
                                      {-2, 472},
                                      {-3, 452},
                                      {-3, 1024},
                                      {-3, 1024},
                                      {-4, 1043},
                                      {-5, 190},
                                      {-5, 190},
                                      {-6, 262},
                                      {-6, 1994},
                                      {-6, 1994},
                                      {-7, 1881}},
                                     {{0, 16},
                                      {-1, 330},
                                      {-1, 330},
                                      {-2, 302},
                                      {-2, 344},
                                      {-2, 344},
                                      {-3, 326},
                                      {-3, 738},
                                      {-3, 738},
                                      {-4, 754},
                                      {-4, 1678},
                                      {-4, 1678},
                                      {-5, 1798},
                                      {-6, 831},
                                      {-6, 831},
                                      {-7, 750}}},
                                    {// QF25
                                     {{1, 429},
                                      {0, 395},
                                      {0, 395},
                                      {-1, 366},
                                      {-1, 409},
                                      {-1, 409},
                                      {-2, 390},
                                      {-2, 459},
                                      {-2, 459},
                                      {-3, 474},
                                      {-3, 1086},
                                      {-3, 1086},
                                      {-4, 1187},
                                      {-6, 302},
                                      {-6, 302},
                                      {-7, 236}},
                                     {{1, 429},
                                      {0, 959},
                                      {0, 959},
                                      {-1, 923},
                                      {-1, 976},
                                      {-1, 976},
                                      {-2, 953},
                                      {-2, 1639},
                                      {-2, 1639},
                                      {-3, 1661},
                                      {-4, 637},
                                      {-4, 637},
                                      {-5, 724},
                                      {-6, 377},
                                      {-6, 377},
                                      {-7, 309}},
                                     {{1, 429},
                                      {0, 806},
                                      {0, 806},
                                      {-1, 772},
                                      {-1, 823},
                                      {-1, 823},
                                      {-2, 800},
                                      {-2, 1295},
                                      {-2, 1295},
                                      {-3, 1315},
                                      {-4, 188},
                                      {-4, 188},
                                      {-5, 260},
                                      {-5, 1407},
                                      {-5, 1407},
                                      {-6, 1310}}}};

byte4 QcdDec(struct Tile_s *t, struct EECod_s *EECod, struct EEQcd_s *EEQcd, byte2 ccc);
void EQcdDestory(struct EQcd_s *EQcd);
// byte4 Transform_fe_To_myue(long double f_e, byte2 *eee, byte4 *myumyu);

#endif