/*
Copyright (c) 2000-2021, ICT-Link
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the HTJ2K project.
*/

#include <stdio.h>
#include <stdlib.h>
#include "ImageUtil.h"
#include "j2k2.h"
#include "ctx_enc.h"
#include "t1codec.h"
#include "MqCodec.h"

#if Cplus
byte4 enc_magpass(uchar *CX_, ubyte2 *flag, struct Image_s *CbData, byte4 width, byte4 height,
                  byte4 row1stepf, byte4 row1stepCb, uchar bittop, byte4 bitmask) {
	ubyte2 *F1;
	byte4 *d_;
	byte4 i, j, jj, temp_height;
	byte4 fj, fi;
	byte4 P;
	byte4 *d2_;

	row1stepf /= 2;
	row1stepCb /= 4;
	d2_ = CbData->data;

	P = 0;
	for (jj = 0, fj = 4; jj < height; jj += 4, fj += 4) {
		temp_height = 4 < (CbData->tby1 - jj) ? 4 : (CbData->tby1 - jj);
		for (i = 0, fi = 1; i < width; i++, fi++) {
			F1 = &flag[fj - 1 + fi * row1stepf];
			d_ = &d2_[jj + i * row1stepCb];
			for (j = 0; j < temp_height; j++) {
				if ((F1[j + 1] & SIG) && (!(F1[j + 1] & VISIT))) {
					CX_[P] = (uchar) ((F1[j + 1] & 6) / 2 + 14);
					CX_[P] |= ((d_[j] >> bittop) & 1) << 7;
					F1[j + 1] |= (VISIT | MAG_FIRST | NEIGHBOR);
					P++;
					d_[j] &= bitmask;
				}
			}
		}
	}
	return P;
}
#endif

struct StreamChain_s *dec_magpass(struct Cblk_s *Cblk, struct Image_s *CbData, char bitpos,
                                  struct mqdec_s *dec, struct StreamChain_s *str, uchar FlagFlag) {
	byte2 *Flag;
	byte2 *F1;
	byte4 i, j, jj, temp_height;
	byte4 fj, fi;
	byte4 col1stepCb, row1stepCb, col1stepf, row1stepf;
	byte4 one;
	uchar CX, D;
	byte4 *Data;

	Flag       = (byte2 *) &Cblk->flags->data[0];
	Data       = (byte4 *) &CbData->data[0];
	col1stepf  = Cblk->flags->col1step;
	row1stepf  = Cblk->flags->row1step;
	row1stepCb = CbData->row1step;
	col1stepCb = CbData->col1step;
	one        = 1 << bitpos;
	for (jj = CbData->tby0, fj = 4; jj < CbData->tby1; jj += 4, fj += 4) {
		temp_height = 4 < (CbData->tby1 - jj) ? 4 : (CbData->tby1 - jj);
		for (i = CbData->tbx0, fi = 1; i < CbData->tbx1; i++, fi++) {
			F1 = &Flag[fj * col1stepf + fi * row1stepf];
			for (j = 0; j < temp_height; j++, ++F1) {
				if ((F1[0] & SIG) && (!(F1[0] & VISIT))) {
					CX = (uchar) ((F1[0] & 6) + 28);
					if (nullptr == (str = MQ_Dec(&D, CX, dec, str, QeIndexTable3))) {
						printf("[dec_magpass]:: MQ_Dec error.\n");
						return nullptr;
					}
					if (D) Data[(jj + j) * col1stepCb + i * row1stepCb] |= one;
					F1[0] |= (VISIT | MAG_FIRST | NEIGHBOR);
				}
			}
		}
	}
	return str;
}

struct StreamChain_s *dec_magpass_vcausal(struct Cblk_s *Cblk, struct Image_s *CbData, char bitpos,
                                          struct mqdec_s *dec, struct StreamChain_s *str, uchar FlagFlag) {
	byte2 *Flag;
	byte2 *F0, *F1, *F2;
	byte4 i, j, jj, temp_height;
	byte4 fj, fi;
	byte4 col1stepCb, row1stepCb, col1stepf, row1stepf;
	byte4 one;
	uchar CX, D;
	byte4 *Data;

	Flag       = (byte2 *) &Cblk->flags->data[0];
	Data       = (byte4 *) &CbData->data[0];
	col1stepf  = Cblk->flags->col1step;
	row1stepf  = Cblk->flags->row1step;
	row1stepCb = CbData->row1step;
	col1stepCb = CbData->col1step;
	one        = 1 << bitpos;
	for (jj = CbData->tby0, fj = 4; jj < CbData->tby1; jj += 4, fj += 4) {
		temp_height = 4 < (CbData->tby1 - jj) ? 4 : (CbData->tby1 - jj);
		for (i = CbData->tbx0, fi = 1; i < CbData->tbx1; i++, fi++) {
			F0 = &Flag[fj * col1stepf + (fi - 1) * row1stepf];
			F1 = &Flag[fj * col1stepf + fi * row1stepf];
			F2 = &Flag[fj * col1stepf + (fi + 1) * row1stepf];
			for (j = 0; j < temp_height - 1; j++, ++F0, ++F1, ++F2) {
				if ((F1[0] & SIG) && (!(F1[0] & VISIT))) {
					CX  = (uchar) ((F1[0] & 6) + 28);
					str = MQ_Dec(&D, CX, dec, str, QeIndexTable3);
					if (D) Data[(jj + j) * col1stepCb + i * row1stepCb] |= one;
					F1[0] |= (VISIT | MAG_FIRST | NEIGHBOR);
				}
			}
			for (; j < temp_height; j++, ++F0, ++F1, ++F2) {
				if ((F1[0] & SIG) && (!(F1[0] & VISIT))) {
					if (!(F1[0] & MAG_FIRST))
						CX = (uchar) ((F1[0] & MAG_FIRST)
						              + (((F0[-1] | F0[0] | F1[-1] | F2[-1] | F2[0]) & SIG) << 1)
						              + 28); // 4colm last coefficient is not able to use NEIGHBOR bit.
						                     // Neightbor without below colom coefficient SIG bit have to be
						                     // used.
					else
						CX = 34;
					str = MQ_Dec(&D, CX, dec, str, QeIndexTable3);
					if (D) Data[(jj + j) * col1stepCb + i * row1stepCb] |= one;
					F1[0] |= (VISIT | MAG_FIRST | NEIGHBOR);
				}
			}
		}
	}
	return str;
}

struct StreamChain_s *dec_magpass_Lazy(struct Cblk_s *Cblk, struct Image_s *CbData, char bitpos,
                                       byte4 counter_limit, struct StreamChain_s *str, uchar FlagFlag) {
	byte2 *Flag;
	byte2 *F1;
	byte4 i, j, jj;
	byte4 fj, fi;
	byte4 temp_height;
	byte4 col1stepCb, row1stepCb, col1stepf, row1stepf;
	byte4 one;
	uchar CX, D;
	byte4 *Data;

	Flag       = (byte2 *) &Cblk->flags->data[0];
	Data       = (byte4 *) &CbData->data[0];
	col1stepf  = Cblk->flags->col1step;
	row1stepf  = Cblk->flags->row1step;
	row1stepCb = CbData->row1step;
	col1stepCb = CbData->col1step;
	one        = 1 << bitpos;
	for (jj = CbData->tby0, fj = 4; jj < CbData->tby1; jj += 4, fj += 4) {
		temp_height = 4 < (CbData->tby1 - jj) ? 4 : (CbData->tby1 - jj);
		for (i = CbData->tbx0, fi = 1; i < CbData->tbx1; i++, fi++) {
			F1 = &Flag[fj * col1stepf + fi * row1stepf];
			for (j = 0; j < temp_height; j++, ++F1) {
				if ((F1[0] & SIG) && (!(F1[0] & VISIT))) {
					CX = (uchar) ((F1[0] & 6) + 28);
					if (nullptr == (str = MQ_Dec_Lazy(&D, counter_limit, str))) {
						printf("[dec_magpass_Lazy]:: MQ_Dec_Lazy error\n");
						return nullptr;
					}
					if (D) Data[(jj + j) * col1stepCb + i * row1stepCb] |= one;
					F1[0] |= (VISIT | MAG_FIRST | NEIGHBOR);
				}
			}
		}
	}

	return str;
}

struct StreamChain_s *dec_magpass_Lazy_vcausal(struct Cblk_s *Cblk, struct Image_s *CbData, char bitpos,
                                               byte4 counter_limit, struct StreamChain_s *str) {
	byte2 *Flag;
	byte2 *F1;
	byte4 i, j, jj;
	byte4 fj, fi;
	byte4 temp_height;
	byte4 col1stepCb, row1stepCb, col1stepf, row1stepf;
	byte4 one;
	uchar CX, D;
	byte4 *Data;

	Flag       = (byte2 *) &Cblk->flags->data[0];
	Data       = (byte4 *) &CbData->data[0];
	col1stepf  = Cblk->flags->col1step;
	row1stepf  = Cblk->flags->row1step;
	row1stepCb = CbData->row1step;
	col1stepCb = CbData->col1step;
	one        = 1 << bitpos;
	for (jj = CbData->tby0, fj = 4; jj < CbData->tby1; jj += 4, fj += 4) {
		temp_height = 4 < (CbData->tby1 - jj) ? 4 : (CbData->tby1 - jj);
		for (i = CbData->tbx0, fi = 1; i < CbData->tbx1; i++, fi++) {
			F1 = &Flag[fj * col1stepf + fi * row1stepf];
			for (j = 0; j < temp_height; j++, ++F1) {
				if ((F1[0] & SIG) && (!(F1[0] & VISIT))) {
					CX = (uchar) ((F1[0] & 6) + 28);
					if (nullptr == (str = MQ_Dec_Lazy(&D, counter_limit, str))) {
						printf("[dec_magpass_Lazy]:: MQ_Dec_Lazy error\n");
						return nullptr;
					}
					if (D) Data[(jj + j) * col1stepCb + i * row1stepCb] |= one;
					F1[0] |= (VISIT | MAG_FIRST | NEIGHBOR);
				}
			}
		}
	}
	return str;
}
