/*
Copyright (c) 2000-2021, ICT-Link
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the HTJ2K project.
*/

#ifndef Icc_h
#define Icc_h
#include "j2k2.h"

#define ICC_DEBUG0 0
#define ICC_DEBUG1 0

#define I1 0x0132
#define I2 0x0259
#define I3 0x0074
#define I4 0x00ac
#define I5 0x0153
#define I6 0x0200
#define I7 0x0200
#define I8 0x01ac
#define I9 0x0053
#define Y1 0x0400
#define Y2 0x0000
#define Y3 0x059b
#define Y4 0x0400
#define Y5 0x0160
#define Y6 0x02db
#define Y7 0x0400
#define Y8 0x0716
#define Y9 0x0000

byte4 RGBtoYUV2000(struct Image_s *Image, struct Image_s *Stream, struct Tile_s *t, struct Siz_s *Siz,
                   ubyte2 ccc, byte4 offset, char metric);
byte4 RGBtoICC(/*struct J2kParam_s *J2kParam,*/ struct Image_s *Image, struct Image_s *Stream,
               struct Tile_s *t, struct Siz_s *Siz, ubyte2 ccc, byte4 offset);
byte4 DcEnc_J2K(struct J2kParam_s *J2kParam, struct Image_s *Image, struct Image_s *Stream,
                struct Tile_s *t, struct Siz_s *Siz, ubyte2 ccc, byte4 offset, char FILTER);
byte4 YUVtoRGB_3(struct Codec_s *Codec, struct Siz_s *siz, struct Image_s *InStream_0,
                 struct Image_s *InStream_1, struct Image_s *InStream_2, struct Image_s *OutStream,
                 char metric);
byte4 YUVtoRGB_4(struct Codec_s *Codec, struct Siz_s *siz, struct Image_s *InStream_0,
                 struct Image_s *InStream_1, struct Image_s *InStream_2, struct Image_s *OutStream);
byte4 ICCDec3(struct Codec_s *Codec, struct Tile_s *Tile, struct Siz_s *siz, struct Image_s *InStream_0,
              struct Image_s *InStream_1, struct Image_s *InStream_2, struct Image_s *OutStream,
              char metric);
byte4 ICCDec4(struct Codec_s *Codec, struct Siz_s *siz, struct Image_s *InStream_0,
              struct Image_s *InStream_1, struct Image_s *InStream_2, struct Image_s *OutStream);
byte4 DcDec3(struct Codec_s *Codec, struct Tile_s *Tile, struct Image_s *InStream, ubyte2 ccc,
             struct Image_s *OutStream);

#endif
