/*
Copyright (c) 2000-2021, ICT-Link
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the HTJ2K project.
*/

#ifndef T1CODEC_H
#define T1CODEC_H

#include "j2k2.h"
#include "asmasm.h"

#define T1_DEBUG11 0
#define T1_DEBUG12 0

#define T1_DEBUG0 0
#define T1_DEBUG01 0
#define T1_DEBUG09 0
#define T1_DEBUG19 0
#define T1_DEBUG7 0
#define CB_BUF_LENGTH 0x8000

static uchar main_header[16] = {0x57, 0x47, 0x31, 0x2d, 0x48, 0x54, 0x4a, 0x32,
                                0x4b, 0x65, 0x76, 0x61, 0x6c, 0x2d, 0x76, 0x31};

/*
static ubyte4 QeIndexTable[47] = {
2164348417,
100807681,
151197697,
201591489,
486868257,
556139041,
2248627713,
235426817,
235489281,
235550721,
285945857,
302785537,
336403457,
354227713,
2383369729,
235951105,
252793089,
269633537,
286472193,
303313921,
320155649,
320219137,
337060865,
353903105,
370744321,
387586049,
404428289,
421270529,
438112769,
454955265,
471796417,
488638913,
505481377,
522323233,
539165761,
556008097,
572850721,
589693249,
606535953,
623378565,
640221257,
657063973,
673906709,
690749449,
707592197,
724369409,
774788609};
*/

static ubyte4 QeIndexTable[47] = {
    2164348417, 100807681, 151197697, 201591489, 486868257, 556139041, 2248627713, 235426817,
    235489281,  235550721, 285945857, 302785537, 336403457, 354227713, 2383369729, 235951105,
    252793089,  269633537, 286472193, 303313921, 320155649, 320219137, 337060865,  353903105,
    370744321,  387586049, 404428289, 421270529, 438112769, 454955265, 471796417,  488638913,
    505481377,  522323233, 539165761, 556008097, 572850721, 589693249, 606535953,  623378565,
    640221257,  657063973, 673906709, 690749449, 707592197, 724369409, 774788609};

static ubyte4 QeIndexTable2[47] = {
    1442939137, 872482306,  402721027, 180423684, 86056197,  35725606,  1442940423, 1409355272,
    1208028681, 939593226,  805376267, 604049932, 469832717, 369169693, 1442942479, 1409355280,
    1359023889, 1208029202, 939594003, 872485396, 805376789, 671159062, 604050455,  570496280,
    469833241,  402724634,  369170459, 335616284, 302062109, 285285150, 180427807,  163650848,
    144776737,  86056738,   71376931,  44114212,  35725861,  21046054,  17900583,   8725800,
    4793897,    2434858,    1386539,   600364,    338477,    76589,     1442917934};

static ubyte4 QeIndexTable3[47] = {
    1442907396, 872486920,  402727948, 180432912, 86078484,  35751064,  1442912540, 1409366048,
    1208039460, 939604008,  805389356, 604063792, 469848116, 369185908, 1442920764, 1409366080,
    1359035460, 1208041544, 939607116, 872499280, 805391444, 671173720, 604065884,  570512480,
    469850212,  402742376,  369188972, 335635568, 302082164, 285305976, 180449404,  163673216,
    144799876,  86080648,   71401612,  44139664,  35752084,  21073048,  17928348,   8754336,
    4823204,    2464936,    1417388,   631984,    370868,    109748,    1442953400};

#if T1_THREAD
typedef struct T1_Thread_s {
	struct Cblk_s *Cblk;
	struct Image_s *flag;
	struct Image_s *CbData;
	struct EECod_s *EECod;
	struct mqenc_s *enc;
	struct Tile_s *t;
	uchar *ctx_table;
	byte4 numCblk;
	byte4 Rb;
	byte4 e;
	byte4 myu;
	char G;
	char ccc;
	char int_num;
	char shift;
} T1_Thread_t;
#endif

struct mqenc_s *Mqenc_Create(int max_contexts);
struct StreamChain_s *mqenc_flush(struct StreamChain_s *str, struct mqenc_s *mqenc,
                                  /*struct Cblk_s *Cblk,*/ byte4 termmode);
void mq_setbits(struct mqenc_s *enc);
void mqenc_init(struct mqenc_s *enc);
void mqindex_init(struct mqenc_s *enc);
// byte4 mqdec_init(struct mqdec_s *dec, ubyte4 y, struct StreamChain_s *s);
struct StreamChain_s *mqdec_init(struct mqdec_s *dec, ubyte4 code_length, struct StreamChain_s *s);
// void	mqdec_init_Lazy(struct mqdec_s *dec, ubyte4 code_length, struct StreamChain_s *s);
void mq_Index_init(struct mqdec_s *dec);
#if 0
byte4 mqdec_bytein(struct mqdec_s *dec, struct StreamChain_s *s );
#else
struct StreamChain_s *mqdec_bytein(struct mqdec_s *dec, struct StreamChain_s *s);
#endif
struct mqdec_s *Mqdec_Create(byte4 max_contexts);

void thread_func(void *targ);
byte4 T1Enc(struct Codec_s *Codec, struct Cblk_s *Cblk, struct EECod_s *EECod, byte4 eee, byte4 Rb,
            byte4 myu, char G, struct mqenc_s *enc, uchar *ctx_table, char shift);
byte4 T1Dec(struct Cblk_s *Cblk, struct Codec_s *Codec, struct EECod_s *EECod, struct EERgn_s *EERgn,
            struct mqdec_s *dec, uchar *ctx_table, struct StreamChain_s *s, ubyte2 ccc);
byte4 T1Enc_HTJ2K(struct J2kParam_s *J2kParam, struct Codec_s *Codec, struct Cblk_s *Cblk,
                  struct EECod_s *EECod, byte4 eee, byte4 Rb, byte4 myu, char G,
                  /*struct mqenc_s *enc,*/ char shift);
byte4 T1Dec_HTJ2K(struct Cblk_s *Cblk, struct Codec_s *Codec, struct EECod_s *EECod, struct EERgn_s *EERgn,
                  struct StreamChain_s *s, ubyte2 ccc);

struct StreamChain_s *MQ_Dec_Lazy(uchar *D, byte4 counter_limit, struct StreamChain_s *s);

struct StreamChain_s *MQ_Dec(uchar *D, char CX, struct mqdec_s *dec, struct StreamChain_s *s,
                             ubyte4 *QeIndexTable_);
uchar MQ_Dec2(char CX, struct mqdec_s *dec, struct StreamChain_s *s, ubyte4 *QeIndexTable_);
byte4 enc_sigpass_Lazy(uchar *CX_, byte2 *flag, byte2 *d2_, uchar *SIG_TABLE, uchar *SIGN3, byte8 *bitpos_,
                       char bittop, byte4 width, byte4 height1, byte4 height2, byte4 row1stepf1,
                       byte4 row1stepCb1, byte4 bitmask);
struct StreamChain_s *dec_sigpass(struct Cblk_s *Cblk, struct Image_s *CbData, char bitpos,
                                  uchar *ctx_table, struct mqdec_s *dec, struct StreamChain_s *str,
                                  uchar FlagFlag);
void dec_inVisit(struct Cblk_s *Cblk, struct Image_s *CbData);
struct StreamChain_s *dec_sigpass_vcausal(struct Cblk_s *Cblk, struct Image_s *CbData, char bitpos,
                                          uchar *ctx_table, struct mqdec_s *dec, struct StreamChain_s *str,
                                          uchar FlagFlag);
struct StreamChain_s *dec_sigpass_Lazy(struct Cblk_s *Cblk, struct Image_s *CbData, char bitpos,
                                       byte4 counter_limit, struct StreamChain_s *s, uchar FlagFlag);
struct StreamChain_s *dec_sigpass_Lazy_vcausal(struct Cblk_s *Cblk, struct Image_s *CbData, char bitpos,
                                               /*uchar *ctx_table,*/ byte4 counter_limit,
                                               struct StreamChain_s *s);
struct StreamChain_s *dec_clnpass(struct Cblk_s *Cblk, struct Image_s *CbData, char bitpos,
                                  uchar *ctx_table, struct mqdec_s *dec, struct StreamChain_s *s,
                                  uchar SegmentSymbol, uchar FlagFlag);
struct StreamChain_s *dec_clnpass_vcausal(struct Cblk_s *Cblk, struct Image_s *CbData, char bitpos,
                                          uchar *ctx_table, struct mqdec_s *dec, struct StreamChain_s *s,
                                          uchar SegmentSymbol, uchar FlagFlag);
struct StreamChain_s *dec_magpass(struct Cblk_s *Cblk, struct Image_s *CbData, char bitpos,
                                  struct mqdec_s *dec, struct StreamChain_s *s, uchar FlagFlag);
struct StreamChain_s *dec_magpass_vcausal(struct Cblk_s *Cblk, struct Image_s *CbData, char bitpos,
                                          struct mqdec_s *dec, struct StreamChain_s *s, uchar FlagFlag);
struct StreamChain_s *dec_magpass_Lazy(struct Cblk_s *Cblk, struct Image_s *CbData, char bitpos,
                                       byte4 counter_limit, struct StreamChain_s *s, uchar FlagFlag);
struct StreamChain_s *dec_magpass_Lazy_vcausal(struct Cblk_s *Cblk, struct Image_s *CbData, char bitpos,
                                               byte4 counter_limit, struct StreamChain_s *str);
void DistortionCal(double *Dis, byte4 *d2_, byte4 width, byte4 height, byte4 col1stepCb, byte4 row1stepCb);
void DecWaveIL53_4(byte4 *Data_, byte4 col1step, struct Image_s *CbData);

#if Cplus
byte4 BitTopSearch(struct Cblk_s *Cblk);
struct StreamChain_s *mqenc_byteout(struct StreamChain_s *str, struct mqenc_s *enc);
// uchar MQ_Dec(char CX, struct mqdec_s *dec, struct StreamChain_s *s, ubyte4 *QeIndexTable_);
struct StreamChain_s *MQ_Enc(struct StreamChain_s *str, struct mqenc_s *enc, ubyte4 *QeIndexTable_,
                             uchar *CX_, byte4 Length);
byte4 enc_clnpass(uchar *CX_, ubyte2 *flag, struct Image_s *CbData, uchar *SIG_TABLE, uchar *SIGN3,
                  byte4 width, byte4 height1, /*byte4 height2,*/ byte4 row1stepf, byte4 row1stepCb,
                  uchar bittop, byte4 bitmask);
byte4 enc_sigpass(uchar *CX_, ubyte2 *flag, struct Image_s *CbData, uchar *SIG_TABLE,
                  /*uchar *SIGN3,*/ byte4 width, byte4 height1, byte4 row1stepf, byte4 row1stepCb,
                  uchar bittop, byte4 bitmask);
// byte4 enc_sigpass_Lazy(uchar *CX_, byte2 *flag, byte2 *d2_, uchar *SIG_TABLE, uchar *SIGN3, byte8
// *bitpos_, char bittop, byte4 width, byte4 height1, byte4 height2, byte4 row1stepf1, byte4 row1stepCb1,
// byte4 bitmask);
byte4 enc_magpass(uchar *CX_, ubyte2 *flag, struct Image_s *CbData, byte4 width, byte4 height1,
                  byte4 row1stepf, byte4 row1stepCb, uchar bittop, byte4 bitmask);
// byte4 dec_clnpass(struct Cblk_s *Cblk, struct Image_s *CbData, char bitpos, uchar *ctx_table, struct
// mqdec_s *dec, struct StreamChain_s *s); byte4 dec_magpass(struct Cblk_s *Cblk, struct Image_s *CbData,
// char bitpos, struct mqdec_s *dec, struct StreamChain_s *s); byte4 dec_magpass_Lazy(struct Cblk_s *Cblk,
// struct Image_s *CbData, char bitpos, struct mqdec_s *dec, struct StreamChain_s *s); void
// DistortionCal(double *Dis, byte4 *d2_, byte4 width, byte4 height, byte4 col1stepCb, byte4 row1stepCb);
#else
#	ifdef __cplusplus
extern "C" {
byte4 BitTopSearch(struct Cblk_s *Cblk);
struct StreamChain_s *mqenc_byteout(struct StreamChain_s *str, struct mqenc_s *enc);
struct StreamChain_s *MQ_Enc(struct StreamChain_s *str, struct mqenc_s *enc, ubyte4 *QeIndexTable_,
                             uchar *CX_, byte4 Length);
inline extern byte4 enc_clnpass(uchar *CX_, ubyte2 *flag, struct Image_s *CbData, uchar *SIG_TABLE,
                                uchar *SIGN3, byte4 width, byte4 height1,
                                /*byte4 height2,*/ byte4 row1stepf, byte4 row1stepCb, uchar bittop,
                                byte4 bitmask);
inline extern byte4 enc_sigpass(uchar *CX_, ubyte2 *flag, struct Image_s *CbData, uchar *SIG_TABLE,
                                /*uchar *SIGN3,*/ byte4 width, byte4 height1, byte4 row1stepf,
                                byte4 row1stepCb, uchar bittop, byte4 bitmask);
byte4 enc_magpass(uchar *CX_, ubyte2 *flag, struct Image_s *CbData, byte4 width, byte4 height1,
                  byte4 row1stepf, byte4 row1stepCb, uchar bittop, byte4 bitmask);
// inline extern byte4 dec_magpass(ubyte2 one, struct mqdec_s *dec, struct StreamChain_s *s, byte2 *f_,
// byte4 row1stepf, byte4 *d_, byte4 row1stepm, byte2 width, byte2 height2, ubyte4 *QeIndexTable_, char
// bittop); inline extern byte4 dec_clnpass(ubyte2 one, struct mqdec_s *dec, struct StreamChain_s *s, byte2
// *f_, byte4 row1stepf, byte4 *d_, byte4 row1stepm, byte2 width, byte2 height2, ubyte4 *QeIndexTable_, uchar
// *SIG_TABLE, uchar *SIGN3); inline extern void DistortionCal(double *Dis, byte4 *d2_, byte4 width, byte4
// height, byte4 col1stepCb, byte4 row1stepCb);
}
#	endif
#endif

#endif
