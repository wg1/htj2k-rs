/*
Copyright (c) 2000-2021, ICT-Link
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the HTJ2K project.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "j2k2.h"
#include "ImageUtil.h"
#include "wavelet_codec.h"
#include "Icc.h"

byte4 RGBtoYUV2000(struct Image_s *Image, struct Image_s *Stream, struct Tile_s *t, struct Siz_s *Siz,
                   ubyte2 ccc, byte4 offset, char metric) {
	byte4 n, nn;
	byte4 width, height, x0, x1, y0, y1;
	byte4 *D, *D_e;
	byte4 i;

	if (t->tx1 < Siz->XOsiz) {
		x0    = 0;
		x1    = 0;
		width = 0;
	} else if (t->tx0 < Siz->XOsiz) {
		x0    = 0;
		x1    = t->tx1 - Siz->XOsiz;
		width = x1 - x0;
	} else if (t->tx1 < (Siz->XOsiz + Siz->Xsiz)) {
		x0    = t->tx0 - Siz->XOsiz;
		x1    = t->tx1 - Siz->XOsiz;
		width = x1 - x0;
	} else {
		x0    = t->tx0 - Siz->XOsiz;
		x1    = Stream->width;
		width = x1 - x0;
	}

	if (t->ty1 < Siz->YOsiz) {
		y0     = 0;
		y1     = 0;
		height = 0;
	} else if (t->ty0 < Siz->YOsiz) {
		y0     = 0;
		y1     = t->ty1 - Siz->YOsiz;
		height = y1 - y0;
	} else if (t->ty1 < (Siz->YOsiz + Siz->Ysiz)) {
		y0     = t->ty0 - Siz->YOsiz;
		y1     = t->ty1 - Siz->YOsiz;
		height = y1 - y0;
	} else {
		y0     = t->ty0 - Siz->YOsiz;
		y1     = Stream->height;
		height = y1 - y0;
	}

	Image[ccc].tbx0   = 0;
	Image[ccc].tbx1   = width;
	Image[ccc].tby0   = 0;
	Image[ccc].tby1   = height;
	Image[ccc].width  = width;
	Image[ccc].height = height;

	if (Stream->type == CHAR) {
		uchar *d_, *d_TS;
		d_TS = (uchar *) Stream->data;
		if (ccc == 0) {
			for (n = y0, nn = 0; n < y1; n++, nn++) {
				d_  = &d_TS[n * Stream->col1step + x0 * Stream->row1step];
				D   = &Image[ccc].data[nn * Image[0].col1step + Image[0].tbx0];
				D_e = &Image[ccc].data[nn * Image[0].col1step + Image[0].tbx1];
				for (i = 0; D != D_e; d_ += Stream->row1step, ++D, i++) {
					*D = (signed) (d_[0] - offset) + (signed) (d_[1] - offset) * 2
					     + (signed) (d_[2] - offset);
					*D = floor2((*D), 4);
				}
			}
		} else if (((ccc == 1) && (metric == METRIC_RGB)) || ((ccc == 2) && (metric == METRIC_BGR))) {
			for (n = y0, nn = 0; n < y1; n++, nn++) {
				d_  = &d_TS[n * Stream->col1step + x0 * Stream->row1step];
				D   = &Image[ccc].data[nn * Image[ccc].col1step];
				D_e = &Image[ccc].data[nn * Image[ccc].col1step + Image[ccc].tbx1];
				for (i = 0; D != D_e; d_ += Stream->row1step, ++D, i++)
					*D = (signed) (d_[2] - d_[1]); //	U=B-G (when RGB:d_[2]-d_[1], when BGR:d_[0]-d_[1])
			}
		} else {
			for (n = y0, nn = 0; n < y1; n++, nn++) {
				d_  = &d_TS[n * Stream->col1step + x0 * Stream->row1step];
				D   = &Image[ccc].data[nn * Image[ccc].col1step];
				D_e = &Image[ccc].data[nn * Image[ccc].col1step + Image[ccc].tbx1];
				for (i = 0; D != D_e; d_ += Stream->row1step, ++D, i++)
					*D = (signed) (d_[0] - d_[1]); //	V=B-G (when RGB:d_[0]-d_[1], when BGR:d_[2]-d_[1])
			}
		}
	} else if (Stream->type == BYTE2) {
		ubyte2 *d_, *d_TS;
		d_TS = (ubyte2 *) Stream->data;
		if (ccc == 0) {
			offset = offset << 2;
			for (n = y0, nn = 0; n < y1; n++, nn++) {
				d_  = &d_TS[n * Stream->col1step + x0 * Stream->row1step];
				D   = &Image[ccc].data[nn * Image->col1step + Image->tbx0];
				D_e = &Image[ccc].data[nn * Image->col1step + Image->tbx1];
				for (i = 0; D != D_e; d_ += Stream->row1step, ++D, i++) {
					*D = (byte4) d_[0] + (byte4) (d_[1] << 1) + (byte4) d_[2] - offset; //	Y=(R+2*G+B)/4
					*D = floor2((*D), 4);
				}
			}
		} else if (((ccc == 1) && (metric == METRIC_RGB)) || ((ccc == 2) && (metric == METRIC_BGR))) {
			for (n = y0, nn = 0; n < y1; n++, nn++) {
				d_  = &d_TS[n * Stream->col1step + x0 * Stream->row1step];
				D   = &Image[ccc].data[nn * Image->col1step];
				D_e = &Image[ccc].data[nn * Image->col1step + Image->tbx1];
				for (; D != D_e; d_ += Stream->row1step, ++D)
					*D = (byte4) (d_[2] - d_[1]); //	U=B-G(when RGB)	V=R-G(when BGR)
			}
		} else {
			for (n = y0, nn = 0; n < y1; n++, nn++) {
				d_  = &d_TS[n * Stream->col1step + x0 * Stream->row1step];
				D   = &Image[ccc].data[nn * Image->col1step];
				D_e = &Image[ccc].data[nn * Image->col1step + Image->tbx1];
				for (; D != D_e; d_ += Stream->row1step, ++D)
					*D = (byte4) (d_[0] - d_[1]); //	V=R-G(when RGB)	U=B-G(when BGR)
			}
		}
	} else if (Stream->type == BYTE4) {
		byte4 *d_, *d_TS;
		d_TS = (byte4 *) Stream->data;
		if (ccc == 0) {
			offset = offset << 2;
			for (n = y0, nn = 0; n < y1; n++, nn++) {
				d_  = &d_TS[n * Stream->col1step + x0 * Stream->row1step];
				D   = &Image[ccc].data[nn * Image->col1step + Image->tbx0];
				D_e = &Image[ccc].data[nn * Image->col1step + Image->tbx1];
				for (; D != D_e; d_ += Stream->row1step, ++D) {
					*D = d_[0] + (d_[1] << 1) + d_[2] - offset; //	Y=(R+2*G+B)/4
					*D = floor2((*D), 4);
				}
			}
		} else if (((ccc == 1) && (metric == METRIC_RGB)) || ((ccc == 2) && (metric == METRIC_BGR))) {
			for (n = y0, nn = 0; n < y1; n++, nn++) {
				d_  = &d_TS[n * Stream->col1step + x0 * Stream->row1step];
				D   = &Image[ccc].data[nn * Image->col1step];
				D_e = &Image[ccc].data[nn * Image->col1step + Image->tbx1];
				for (; D != D_e; d_ += Stream->row1step, ++D)
					*D = (d_[2] - d_[1]); //	U=B-G(when RGB)	V=R-G(when BGR)
			}
		} else {
			for (n = y0, nn = 0; n < y1; n++, nn++) {
				d_  = &d_TS[n * Stream->col1step + x0 * Stream->row1step];
				D   = &Image[ccc].data[nn * Image->col1step];
				D_e = &Image[ccc].data[nn * Image->col1step + Image->tbx1];
				for (; D != D_e; d_ += Stream->row1step, ++D)
					*D = (d_[0] - d_[1]); //	V=R-G(when RGB)	U=B-G(when BGR)
			}
		}
	}
	return EXIT_SUCCESS;
}

byte4 YUVtoRGB_3(struct Codec_s *Codec, struct Siz_s *siz, struct Image_s *InStream_0,
                 struct Image_s *InStream_1, struct Image_s *InStream_2, struct Image_s *OutStream,
                 char metric) {
	byte4 offsetY = 0, offsetU = 0, offsetV = 0;
	byte4 widthS, heightS;
	byte4 col1step1, row1step1;
	byte4 col1stepY, col1stepU, col1stepV;
	byte4 *Y_TS, *Y_, *U_TS, *U_, *V_TS, *V_;
	byte4 tempA;
	byte4 i, j;
	byte4 tcx0, tcy0;
	byte4 D0, D1, D2;

	if (!(siz->Ssiz[0] & 0x80)) offsetY = 1 << (siz->Ssiz[0]);
	if (!(siz->Ssiz[1] & 0x80)) offsetU = 1 << (siz->Ssiz[1]);
	if (!(siz->Ssiz[2] & 0x80)) offsetV = 1 << (siz->Ssiz[2]);
	col1step1 = OutStream->col1step;
	row1step1 = OutStream->row1step;
	col1stepY = InStream_0->col1step;
	col1stepU = InStream_1->col1step;
	col1stepV = InStream_2->col1step;
	tcx0      = InStream_0->tbx0 - Codec->Siz->XOsiz;
	tcy0      = InStream_0->tby0 - Codec->Siz->YOsiz;
	widthS    = InStream_0->tbx1 - InStream_0->tbx0;
	heightS   = InStream_0->tby1 - InStream_0->tby0;
	if (OutStream->type == CHAR) {
		uchar *D_, *D_TS;
		D_TS = (uchar *) &OutStream->data[0];
		D_TS = &D_TS[tcy0 * col1step1 + tcx0 * row1step1];
		Y_TS = (byte4 *) &InStream_0->data[0];
		U_TS = (byte4 *) &InStream_1->data[0];
		V_TS = (byte4 *) &InStream_2->data[0];
		if (metric == METRIC_RGB) {
			for (j = 0; j < heightS;
			     j++, Y_TS += col1stepY, U_TS += col1stepU, V_TS += col1stepV, D_TS += col1step1) {
				D_ = D_TS;
				Y_ = Y_TS;
				U_ = U_TS;
				V_ = V_TS;
				for (i = 0; i < widthS; i++, ++Y_, ++U_, ++V_, D_ += row1step1) {
					if ((j == 32) && (i == 29)) i = i;
					tempA = (*Y_) - (((*U_) + (*V_)) >> 2);
					D0    = ((*V_) + tempA + offsetY);
					if (D0 > 255) D0 = 255;
					if (D0 < 0) D0 = 0;
					D1 = (tempA + offsetU);
					if (D1 > 255) D1 = 255;
					if (D1 < 0) D1 = 0;
					D2 = ((*U_) + tempA + offsetV);
					if (D2 > 255) D2 = 255;
					if (D2 < 0) D2 = 0;
					D_[0] = (uchar) D0;
					D_[1] = (uchar) D1;
					D_[2] = (uchar) D2;
					if (D0 == 0xBF && D1 == 0xc1 && D2 == 0xc1) i = i;
					if (D0 == 0xC1 && D1 == 0xC1 && D2 == 0xBF) i = i;
					if (D0 == 0xC1 && D1 == 0xBF && D2 == 0xC1) i = i;
				}
			}
		} else if (metric == METRIC_BGR) {
			for (j = 0; j < heightS;
			     j++, Y_TS += col1stepY, U_TS += col1stepU, V_TS += col1stepV, D_TS += col1step1) {
				D_ = D_TS;
				Y_ = Y_TS;
				U_ = U_TS;
				V_ = V_TS;
				for (i = 0; i < widthS; i++, ++Y_, ++U_, ++V_, D_ += row1step1) {
					tempA = (*Y_) - (((*U_) + (*V_)) >> 2);
					D_[2] = (uchar) ((*V_) + tempA + offsetV);
					D_[1] = (uchar) (tempA + offsetU);
					D_[0] = (uchar) ((*U_) + tempA + offsetY);
				}
			}
		}
	} else if (OutStream->type == BYTE2) {
		ubyte2 *D_, *D_TS;
		D_TS = (ubyte2 *) &OutStream->data[0];
		D_TS = &D_TS[tcy0 * col1step1 + tcx0 * row1step1];
		Y_TS = (byte4 *) &InStream_0->data[0];
		U_TS = (byte4 *) &InStream_1->data[0];
		V_TS = (byte4 *) &InStream_2->data[0];
		if (metric == METRIC_RGB) {
			for (j = 0; j < heightS;
			     j++, Y_TS += col1stepY, U_TS += col1stepU, V_TS += col1stepV, D_TS += col1step1) {
				D_ = D_TS;
				Y_ = Y_TS;
				U_ = U_TS;
				V_ = V_TS;
				for (i = 0; i < widthS; i++, ++Y_, ++U_, ++V_, D_ += row1step1) {
					tempA = (*Y_) - (((*U_) + (*V_)) >> 2);
					D_[0] = (ubyte2) ((*V_) + tempA + offsetY);
					D_[1] = (ubyte2) (tempA + offsetU);
					D_[2] = (ubyte2) ((*U_) + tempA + offsetV);
				}
			}
		} else if (metric == METRIC_BGR) {
			for (j = 0; j < heightS;
			     j++, Y_TS += col1stepY, U_TS += col1stepU, V_TS += col1stepV, D_TS += col1step1) {
				D_ = D_TS;
				Y_ = Y_TS;
				U_ = U_TS;
				V_ = V_TS;
				for (i = 0; i < widthS; i++, ++Y_, ++U_, ++V_, D_ += row1step1) {
					tempA = (*Y_) - (((*U_) + (*V_)) >> 2);
					D_[2] = (ubyte2) ((*V_) + tempA + offsetV);
					D_[1] = (ubyte2) (tempA + offsetU);
					D_[0] = (ubyte2) ((*U_) + tempA + offsetY);
				}
			}
		}
	}
	return EXIT_SUCCESS;
}

byte4 YUVtoRGB_4(struct Codec_s *Codec, struct Siz_s *siz, struct Image_s *InStream_0,
                 struct Image_s *InStream_1, struct Image_s *InStream_2, struct Image_s *OutStream) {
	byte4 offsetY = 0, offsetU = 0, offsetV = 0;
	byte4 widthS, heightS;
	byte4 col1step1, row1step1;
	byte4 col1stepY, col1stepU, col1stepV;
	byte4 *Y_TS, *Y_, *U_TS, *U_, *V_TS, *V_;
	byte4 tempA;
	byte4 i, j;
	byte4 tcx0, tcy0;
	byte4 D0, D1, D2;
	byte4 Top_Limit_0, Top_Limit_1, Top_Limit_2;
	byte4 numData;
	ubyte2 ccc;

	numData = (OutStream->tby1 - OutStream->tby0) * OutStream->col1step;

	if (!(siz->Ssiz[0] & 0x80)) offsetY = 1 << (siz->Ssiz[0]);
	if (!(siz->Ssiz[1] & 0x80)) offsetU = 1 << (siz->Ssiz[1]);
	if (!(siz->Ssiz[2] & 0x80)) offsetV = 1 << (siz->Ssiz[2]);
	Top_Limit_0 = (1 << (siz->Ssiz[0] + 1)) - 1;
	Top_Limit_1 = (1 << (siz->Ssiz[1] + 1)) - 1;
	Top_Limit_2 = (1 << (siz->Ssiz[2] + 1)) - 1;
	col1step1   = OutStream->col1step;
	row1step1   = OutStream->row1step;
	col1stepY   = InStream_0->col1step;
	col1stepU   = InStream_1->col1step;
	col1stepV   = InStream_2->col1step;
	tcx0        = InStream_0->tbx0 - Codec->Siz->XOsiz;
	tcy0        = InStream_0->tby0 - Codec->Siz->YOsiz;
	widthS      = InStream_0->tbx1 - InStream_0->tbx0;
	heightS     = InStream_0->tby1 - InStream_0->tby0;
	if (OutStream->type == CHAR) {
		uchar *D_TS, *D0_, *D0_TS, *D1_, *D1_TS, *D2_, *D2_TS;
		D_TS  = (uchar *) &OutStream->data[0];
		D_TS  = &D_TS[tcy0 * col1step1 + tcx0 * row1step1];
		ccc   = 0;
		D0_TS = &D_TS[ccc * numData];
		ccc   = 1;
		D1_TS = &D_TS[ccc * numData];
		ccc   = 2;
		D2_TS = &D_TS[ccc * numData];
		Y_TS  = (byte4 *) &InStream_0->data[0]; //&Tile->Cmpt[0].data->data[0];
		U_TS  = (byte4 *) &InStream_1->data[0]; //&Tile->Cmpt[1].data->data[0];
		V_TS  = (byte4 *) &InStream_2->data[0]; //&Tile->Cmpt[2].data->data[0];
		for (j = 0; j < heightS; j++, Y_TS += col1stepY, U_TS += col1stepU, V_TS += col1stepV,
		    D0_TS += col1step1, D1_TS += col1step1, D2_TS += col1step1) {
			D0_ = D0_TS;
			D1_ = D1_TS;
			D2_ = D2_TS;
			Y_  = Y_TS;
			U_  = U_TS;
			V_  = V_TS;
			for (i = 0; i < widthS;
			     i++, ++Y_, ++U_, ++V_, D0_ += row1step1, D1_ += row1step1, D2_ += row1step1) {
				tempA = (*Y_) - (((*U_) + (*V_)) >> 2);
				D0    = ((*V_) + tempA + offsetY);
				if (D0 > 255) D0 = 255;
				if (D0 < 0) D0 = 0;
				D1 = (tempA + offsetU);
				if (D1 > 255) D1 = 255;
				if (D1 < 0) D1 = 0;
				D2 = ((*U_) + tempA + offsetV);
				if (D2 > 255) D2 = 255;
				if (D2 < 0) D2 = 0;
				D0_[0] = (uchar) D0;
				D1_[0] = (uchar) D1;
				D2_[0] = (uchar) D2;
			}
		}
	} else if (OutStream->type == BYTE2) {
		ubyte2 *D_TS, *D0_, *D0_TS, *D1_, *D1_TS, *D2_, *D2_TS;
		D_TS  = (ubyte2 *) &OutStream->data[0];
		D_TS  = &D_TS[tcy0 * col1step1 + tcx0 * row1step1];
		ccc   = 0;
		D0_TS = &D_TS[ccc * heightS * col1step1];
		ccc   = 1;
		D1_TS = &D_TS[ccc * heightS * col1step1];
		ccc   = 2;
		D2_TS = &D_TS[ccc * heightS * col1step1];
		Y_TS  = (byte4 *) &InStream_0->data[0]; //&Tile->Cmpt[0].data->data[0];
		U_TS  = (byte4 *) &InStream_1->data[0]; //&Tile->Cmpt[1].data->data[0];
		V_TS  = (byte4 *) &InStream_2->data[0]; //&Tile->Cmpt[2].data->data[0];
		for (j = 0; j < heightS; j++, Y_TS += col1stepY, U_TS += col1stepU, V_TS += col1stepV,
		    D0_TS += col1step1, D1_TS += col1step1, D2_TS += col1step1) {
			D0_ = D0_TS;
			D1_ = D1_TS;
			D2_ = D2_TS;
			Y_  = Y_TS;
			U_  = U_TS;
			V_  = V_TS;
			for (i = 0; i < widthS;
			     i++, ++Y_, ++U_, ++V_, D0_ += row1step1, D1_ += row1step1, D2_ += row1step1) {
				tempA = (*Y_) - (((*U_) + (*V_)) >> 2);
				D0    = ((*V_) + tempA + offsetY);
				if (D0 > Top_Limit_0) D0 = Top_Limit_0;
				if (D0 < 0) D0 = 0;
				D1 = (tempA + offsetU);
				if (D1 > Top_Limit_1) D1 = Top_Limit_1;
				if (D1 < 0) D1 = 0;
				D2 = ((*U_) + tempA + offsetV);
				if (D2 > Top_Limit_2) D2 = Top_Limit_2;
				if (D2 < 0) D2 = 0;
				D0_[0] = (ubyte2) D0;
				D1_[0] = (ubyte2) D1;
				D2_[0] = (ubyte2) D2;
			}
		}
	}
	return EXIT_SUCCESS;
}

byte4 RGBtoICC(/*struct J2kParam_s *J2kParam,*/ struct Image_s *Image, struct Image_s *Stream,
               struct Tile_s *t, struct Siz_s *Siz, ubyte2 ccc, byte4 offset) {
	byte4 n, nn;
	byte4 width, height, x0, x1, y0, y1;
	byte4 i;
	//	byte4	one;

	if (t->tx1 < Siz->XOsiz) {
		x0    = 0;
		x1    = 0;
		width = 0;
	} else if (t->tx0 < Siz->XOsiz) {
		x0    = 0;
		x1    = t->tx1 - Siz->XOsiz;
		width = x1 - x0;
	} else if (t->tx1 < (Siz->XOsiz + Siz->Xsiz)) {
		x0    = t->tx0 - Siz->XOsiz;
		x1    = t->tx1 - Siz->XOsiz;
		width = x1 - x0;
	} else {
		x0    = t->tx0 - Siz->XOsiz;
		x1    = Stream->width;
		width = x1 - x0;
	}

	if (t->ty1 < Siz->YOsiz) {
		y0     = 0;
		y1     = 0;
		height = 0;
	} else if (t->ty0 < Siz->YOsiz) {
		y0     = 0;
		y1     = t->ty1 - Siz->YOsiz;
		height = y1 - y0;
	} else if (t->ty1 < (Siz->YOsiz + Siz->Ysiz)) {
		y0     = t->ty0 - Siz->YOsiz;
		y1     = t->ty1 - Siz->YOsiz;
		height = y1 - y0;
	} else {
		y0     = t->ty0 - Siz->YOsiz;
		y1     = Stream->height;
		height = y1 - y0;
	}

	Image[ccc].tbx0   = 0;
	Image[ccc].tbx1   = width;
	Image[ccc].tby0   = 0;
	Image[ccc].tby1   = height;
	Image[ccc].width  = width;
	Image[ccc].height = height;

	if ((Stream->type == CHAR) /*&& (!J2kParam->HW)*/) {
		float *D, *D_e;
		uchar *d_, *d_TS;
		d_TS = (uchar *) Stream->data;
		if (ccc == 0) {
			for (n = y0, nn = 0; n < y1; n++, nn++) {
				d_  = &d_TS[n * Stream->col1step + x0 * Stream->row1step];
				D   = (float *) &Image[ccc].data[nn * Image[0].col1step + Image[0].tbx0];
				D_e = (float *) &Image[ccc].data[nn * Image[0].col1step + Image[0].tbx1];
				for (i = 0; D != D_e; d_ += Stream->row1step, ++D, i++) {
					*D = (float) ((d_[0] - offset) * 0.299 + (d_[1] - offset) * 0.587
					              + (d_[2] - offset) * 0.114);
				}
			}
		} else if (ccc == 1) {
			for (n = y0, nn = 0; n < y1; n++, nn++) {
				d_  = &d_TS[n * Stream->col1step + x0 * Stream->row1step];
				D   = (float *) &Image[ccc].data[nn * Image[ccc].col1step];
				D_e = (float *) &Image[ccc].data[nn * Image[ccc].col1step + Image[ccc].tbx1];
				for (i = 0; D != D_e; d_ += Stream->row1step, ++D, i++)
					*D = (float) (-(d_[0] - offset) * 0.16875 - (d_[1] - offset) * 0.33126
					              + (d_[2] - offset) * 0.5);
			}
		} else {
			for (n = y0, nn = 0; n < y1; n++, nn++) {
				d_  = &d_TS[n * Stream->col1step + x0 * Stream->row1step];
				D   = (float *) &Image[ccc].data[nn * Image[ccc].col1step];
				D_e = (float *) &Image[ccc].data[nn * Image[ccc].col1step + Image[ccc].tbx1];
				for (i = 0; D != D_e; d_ += Stream->row1step, ++D, i++)
					*D = (float) ((d_[0] - offset) * 0.5 - (d_[1] - offset) * 0.41869
					              - (d_[2] - offset) * 0.08131);
			}
		}
	}
	//	else if( (Stream->type==CHAR) /*&& (J2kParam->HW)*/ ){
	//		byte4 X0, X1, X2, X3, X4, X5, X6, X7, X8;
	// 0.299   => 0x4c8b/0x10000
	// 0.587   => 0x9646/0x10000
	// 0.114   => 0x1d2f/0x10000
	// 0.16875 => 0x2b33/0x10000
	// 0.33126 => 0x54cd/0x10000
	// 0.5     => 0x8000/0x10000
	// 0.41869 => 0x6b30/0x10000
	// 0.08131 => 0x14d1/0x10000
	//		byte4	*D,*D_e;
	//		uchar	*d_, *d_TS;
	//		X0 = 0x4c8b;	X1 = 0x9646;	X2 = 0x1d2f;
	//		X3 = 0x2b33;	X4 = 0x54cd;	X5 = 0x8000;
	//		X6 = 0x8000;	X7 = 0x6b30;	X8 = 0x14d1;
	//
	//		one = 1<<( 16-J2kParam->HW_DecimalPoint );
	//		d_TS = (uchar *)Stream->data;
	//		if(ccc==0){
	//			for(n=y0, nn=0 ; n<y1 ; n++, nn++){
	//				d_= &d_TS[n*Stream->col1step + x0*Stream->row1step];
	//				D   = (byte4 *)&Image[ccc].data[nn*Image[0].col1step + Image[0].tbx0];
	//				D_e = (byte4 *)&Image[ccc].data[nn*Image[0].col1step + Image[0].tbx1];
	//				for( i=0 ; D!=D_e ; d_+=Stream->row1step, ++D, i++){
	//					*D =(byte4)( ( (d_[0]-offset)*X0 + (d_[1]-offset)*X1 + (d_[2]-offset)*X2 )/one );
	//				}
	//			}
	//		}
	//		else if( ccc==1 ){
	//			for(n=y0, nn=0 ; n<y1 ; n++, nn++){
	//				d_= &d_TS[n*Stream->col1step + x0*Stream->row1step];
	//				D = (byte4 *)&Image[ccc].data[nn*Image[ccc].col1step];
	//				D_e = (byte4 *)&Image[ccc].data[nn*Image[ccc].col1step + Image[ccc].tbx1];
	//				for( i=0 ; D!=D_e ;d_+=Stream->row1step, ++D, i++)
	//					*D =(byte4)( (-(d_[0]-offset)*X3 - (d_[1]-offset)*X4 + (d_[2]-offset)*X5 )/one );
	//			}
	//		}
	//		else {
	//			for(n=y0, nn=0 ; n<y1 ; n++, nn++){
	//				d_= &d_TS[n*Stream->col1step + x0*Stream->row1step];
	//				D = (byte4 *)&Image[ccc].data[nn*Image[ccc].col1step];
	//				D_e = (byte4 *)&Image[ccc].data[nn*Image[ccc].col1step + Image[ccc].tbx1];
	//				for( i=0 ; D!=D_e ; d_+=Stream->row1step, ++D, i++)
	//					*D =(byte4)( ( (d_[0]-offset)*X6 - (d_[1]-offset)*X7 - (d_[2]-offset)*X8 )/one );
	//			}
	//		}
	//	}
	return EXIT_SUCCESS;
}

byte4 ICCDec3(struct Codec_s *Codec, struct Tile_s *t, struct Siz_s *siz, struct Image_s *InStream_0,
              struct Image_s *InStream_1, struct Image_s *InStream_2, struct Image_s *OutStream,
              char metric) {
	float offsetY, offsetU, offsetV;
	byte4 width, height;
	byte4 col1step, row1step;
	float *Ys, *Us, *Vs;
	float data2;

	if (!(Codec->Siz->Ssiz[0] & 0x80))
		offsetY = (float) (1 << (Codec->Siz->Ssiz[0] & 0x7f));
	else
		offsetY = (float) 0;
	if (!(Codec->Siz->Ssiz[1] & 0x80))
		offsetU = (float) (1 << (Codec->Siz->Ssiz[1] & 0x7f));
	else
		offsetU = (float) 0;
	if (!(Codec->Siz->Ssiz[2] & 0x80))
		offsetV = (float) (1 << (Codec->Siz->Ssiz[2] & 0x7f));
	else
		offsetV = (float) 0;

	col1step = OutStream->col1step;
	row1step = OutStream->row1step;
	width    = InStream_0->tbx1 - InStream_0->tbx0; // t->Cmpt[0].tcx1-t->Cmpt[0].tcx0;
	height   = InStream_0->tby1 - InStream_0->tby0; // t->Cmpt[0].tcy1-t->Cmpt[0].tcy0;
	byte4 data;
	byte4 j, jj;
	float *Ye;
	if (OutStream->type == CHAR) {
		uchar *D_, *D_TS;
		D_TS = (uchar *) OutStream->data;
		if (metric == METRIC_RGB) {
			for (j = 0, jj = t->ty0 - siz->YOsiz; j < height; j++, jj++) {
				Ys =
				    (float *) &InStream_0->data
				        [j * InStream_0->col1step]; //(float
				                                    //*)&t->Cmpt[0].data->data[j*t->Cmpt[0].data->col1step];
				Ye = (float *) &InStream_0
				         ->data[j * InStream_0->col1step
				                + width]; //(float
				                          //*)&t->Cmpt[0].data->data[j*t->Cmpt[0].data->col1step+width];
				Us =
				    (float *) &InStream_1->data
				        [j * InStream_1->col1step]; //(float
				                                    //*)&t->Cmpt[1].data->data[j*t->Cmpt[1].data->col1step];
				Vs =
				    (float *) &InStream_2->data
				        [j * InStream_2->col1step]; //(float
				                                    //*)&t->Cmpt[2].data->data[j*t->Cmpt[2].data->col1step];
				D_ = &D_TS[col1step * jj + (t->tx0 - siz->XOsiz) * row1step];
				for (; Ys != Ye; ++Ys, ++Us, ++Vs, D_ += row1step) {
					data2 = (*Ys) + (*Us) * (float) 1.77200 + offsetV;
					data  = (byte4) data2;
					if (data < 0) data = 0;
					if (data > 255) data = 255;
					D_[2] = (uchar) data; //(data<0 ? 0:data)<255 ? (data<0 ? 0:data) : 255;
					data2 = (*Ys) - (*Us) * (float) 0.34413 - (*Vs) * (float) 0.71414 + offsetU;
					data  = (byte4) data2;
					if (data < 0) data = 0;
					if (data > 255) data = 255;
					D_[1] = (uchar) data; //(data<0 ? 0:data)<255 ? (data<0 ? 0:data) : 255;
					data2 = (*Ys) + (*Vs) * (float) 1.40200 + offsetY;
					data  = (byte4) data2;
					if (data < 0) data = 0;
					if (data > 255) data = 255;
					D_[0] = (uchar) data; //(data<0 ? 0:data)<255 ? (data<0 ? 0:data) : 255;
				}
			}
		} else if (metric == METRIC_BGR) {
			for (j = 0, jj = t->ty0 - siz->YOsiz; j < height; j++, jj++) {
				//				Ys=(float *)&t->Cmpt[0].data->data[j*t->Cmpt[0].data->col1step];
				//				Ye=(float *)&t->Cmpt[0].data->data[j*t->Cmpt[0].data->col1step+width];
				//				Us=(float *)&t->Cmpt[1].data->data[j*t->Cmpt[1].data->col1step];
				//				Vs=(float *)&t->Cmpt[2].data->data[j*t->Cmpt[2].data->col1step];
				Ys =
				    (float *) &InStream_0->data
				        [j * InStream_0->col1step]; //(float
				                                    //*)&t->Cmpt[0].data->data[j*t->Cmpt[0].data->col1step];
				Ye = (float *) &InStream_0
				         ->data[j * InStream_0->col1step
				                + width]; //(float
				                          //*)&t->Cmpt[0].data->data[j*t->Cmpt[0].data->col1step+width];
				Us =
				    (float *) &InStream_1->data
				        [j * InStream_1->col1step]; //(float
				                                    //*)&t->Cmpt[1].data->data[j*t->Cmpt[1].data->col1step];
				Vs =
				    (float *) &InStream_2->data
				        [j * InStream_2->col1step]; //(float
				                                    //*)&t->Cmpt[2].data->data[j*t->Cmpt[2].data->col1step];
				D_ = &D_TS[col1step * jj + (t->tx0 - siz->XOsiz) * row1step];
				for (; Ys != Ye; ++Ys, ++Us, ++Vs, D_ += row1step) {
					data  = (byte4) ((*Ys) + (*Us) * 1.77200 + offsetY);
					D_[0] = (uchar) ((data < 0 ? 0 : data) < 255 ? (data < 0 ? 0 : data) : 255);
					data  = (byte4) ((*Ys) - (*Us) * 0.34413 - (*Vs) * 0.71414 + offsetU);
					D_[1] = (uchar) ((data < 0 ? 0 : data) < 255 ? (data < 0 ? 0 : data) : 255);
					data  = (byte4) ((*Ys) + (*Vs) * 1.40200 + offsetV);
					D_[2] = (uchar) ((data < 0 ? 0 : data) < 255 ? (data < 0 ? 0 : data) : 255);
				}
			}
		}
	} else if (OutStream->type == BYTE2) {
		ubyte2 *D_, *D_TS;
		D_TS = (ubyte2 *) OutStream->data;
		if (metric == METRIC_BGR) {
			for (j = 0, jj = t->ty0 - siz->YOsiz; j < height; j++, jj++) {
				//				Ys=(float *)&t->Cmpt[0].data->data[j*t->Cmpt[0].data->col1step];
				//				Ye=(float *)&t->Cmpt[0].data->data[j*t->Cmpt[0].data->col1step+width];
				//				Us=(float *)&t->Cmpt[1].data->data[j*t->Cmpt[1].data->col1step];
				//				Vs=(float *)&t->Cmpt[2].data->data[j*t->Cmpt[2].data->col1step];
				Ys =
				    (float *) &InStream_0->data
				        [j * InStream_0->col1step]; //(float
				                                    //*)&t->Cmpt[0].data->data[j*t->Cmpt[0].data->col1step];
				Ye = (float *) &InStream_0
				         ->data[j * InStream_0->col1step
				                + width]; //(float
				                          //*)&t->Cmpt[0].data->data[j*t->Cmpt[0].data->col1step+width];
				Us =
				    (float *) &InStream_1->data
				        [j * InStream_1->col1step]; //(float
				                                    //*)&t->Cmpt[1].data->data[j*t->Cmpt[1].data->col1step];
				Vs =
				    (float *) &InStream_2->data
				        [j * InStream_2->col1step]; //(float
				                                    //*)&t->Cmpt[2].data->data[j*t->Cmpt[2].data->col1step];
				D_ = &D_TS[col1step * jj + (t->tx0 - siz->XOsiz) * row1step];
				for (; Ys != Ye; ++Ys, ++Us, ++Vs, D_ += row1step) {
					data2 = (float) (*Ys + *Us * 1.77200 + offsetY);
					data  = (byte4) ((data2 < 0.0 ? 0.0 : data2) < 65535.0 ? (data2 < 0.0 ? 0.0 : data2)
					                                                       : 65535.0);
					D_[0] = (ubyte2) data;
					data2 = (float) (*Ys - *Us * 0.34413 - *Vs * 0.71414 + offsetU);
					data  = (byte4) ((data2 < 0.0 ? 0.0 : data2) < 65535.0 ? (data2 < 0.0 ? 0.0 : data2)
					                                                       : 65535.0);
					D_[1] = (ubyte2) data;
					data2 = (float) (*Ys + *Vs * 1.40200 + offsetV);
					data  = (byte4) ((data2 < 0.0 ? 0.0 : data2) < 65535.0 ? (data2 < 0.0 ? 0.0 : data2)
					                                                       : 65535.0);
					D_[2] = (ubyte2) data;
				}
			}
		} else if (metric == METRIC_RGB) {
			for (j = 0, jj = t->ty0 - siz->YOsiz; j < height; j++, jj++) {
				//				Ys=(float *)&t->Cmpt[0].data->data[j*t->Cmpt[0].data->col1step];
				//				Ye=(float *)&t->Cmpt[0].data->data[j*t->Cmpt[0].data->col1step+width];
				//				Us=(float *)&t->Cmpt[1].data->data[j*t->Cmpt[1].data->col1step];
				//				Vs=(float *)&t->Cmpt[2].data->data[j*t->Cmpt[2].data->col1step];
				Ys =
				    (float *) &InStream_0->data
				        [j * InStream_0->col1step]; //(float
				                                    //*)&t->Cmpt[0].data->data[j*t->Cmpt[0].data->col1step];
				Ye = (float *) &InStream_0
				         ->data[j * InStream_0->col1step
				                + width]; //(float
				                          //*)&t->Cmpt[0].data->data[j*t->Cmpt[0].data->col1step+width];
				Us =
				    (float *) &InStream_1->data
				        [j * InStream_1->col1step]; //(float
				                                    //*)&t->Cmpt[1].data->data[j*t->Cmpt[1].data->col1step];
				Vs =
				    (float *) &InStream_2->data
				        [j * InStream_2->col1step]; //(float
				                                    //*)&t->Cmpt[2].data->data[j*t->Cmpt[2].data->col1step];
				D_ = &D_TS[col1step * jj + (t->tx0 - siz->XOsiz) * row1step];
				for (; Ys != Ye; ++Ys, ++Us, ++Vs, D_ += row1step) {
					data2 = (float) (*Ys + *Us * 1.77200 + offsetV);
					data  = (byte4) ((data2 < 0.0 ? 0.0 : data2) < 65535.0 ? (data2 < 0.0 ? 0.0 : data2)
					                                                       : 65535.0);
					D_[2] = (ubyte2) data;
					data2 = (float) (*Ys - *Us * 0.34413 - *Vs * 0.71414 + offsetU);
					data  = (byte4) ((data2 < 0.0 ? 0.0 : data2) < 65535.0 ? (data2 < 0.0 ? 0.0 : data2)
					                                                       : 65535.0);
					D_[1] = (ubyte2) data;
					data2 = (float) (*Ys + *Vs * 1.40200 + offsetY);
					data  = (byte4) ((data2 < 0.0 ? 0.0 : data2) < 65535.0 ? (data2 < 0.0 ? 0.0 : data2)
					                                                       : 65535.0);
					D_[0] = (ubyte2) data;
				}
			}
		}
	}
	return EXIT_SUCCESS;
}

byte4 ICCDec4(struct Codec_s *Codec, struct Siz_s *siz, struct Image_s *InStream_0,
              struct Image_s *InStream_1, struct Image_s *InStream_2, struct Image_s *OutStream) {
	byte4 offsetY = 0, offsetU = 0, offsetV = 0;
	byte4 widthS, heightS;
	byte4 col1step1, row1step1;
	byte4 col1stepY, col1stepU, col1stepV;
	float *Y_TS, *Y_, *U_TS, *U_, *V_TS, *V_;
	byte4 i, j;
	byte4 tcx0, tcy0;
	byte4 Top_Limit_0, Top_Limit_1, Top_Limit_2;
	byte4 Bottom_Limit_0, Bottom_Limit_1, Bottom_Limit_2;
	float dataf;
	byte4 data;
	ubyte2 ccc;
	byte4 numData;
	numData = (OutStream->tby1 - OutStream->tby0) * OutStream->col1step;

	if (!(siz->Ssiz[0] & 0x80)) {
		offsetY        = (1 << (siz->Ssiz[0]));
		Top_Limit_0    = ((1 << (siz->Ssiz[0] + 1)) - 1);
		Bottom_Limit_0 = 0;
	} else {
		offsetY        = 0;
		Top_Limit_0    = ((1 << (siz->Ssiz[0])) - 1);
		Bottom_Limit_0 = -1 * ((1 << (siz->Ssiz[0])));
	}
	if (!(siz->Ssiz[1] & 0x80)) {
		offsetU        = (1 << (siz->Ssiz[1]));
		Top_Limit_1    = ((1 << (siz->Ssiz[1] + 1)) - 1);
		Bottom_Limit_1 = 0;
	} else {
		offsetU        = 0;
		Top_Limit_1    = ((1 << (siz->Ssiz[1])) - 1);
		Bottom_Limit_1 = -1 * ((1 << (siz->Ssiz[1])));
	}
	if (!(siz->Ssiz[2] & 0x80)) {
		offsetV        = (1 << (siz->Ssiz[2]));
		Top_Limit_2    = ((1 << (siz->Ssiz[2] + 1)) - 1);
		Bottom_Limit_2 = 0;
	} else {
		offsetV        = 0;
		Top_Limit_2    = ((1 << (siz->Ssiz[2])) - 1);
		Bottom_Limit_2 = -1 * ((1 << (siz->Ssiz[2])));
	}
	col1step1 = OutStream->col1step;
	row1step1 = OutStream->row1step;
	col1stepY = InStream_0->col1step;
	col1stepU = InStream_1->col1step;
	col1stepV = InStream_2->col1step;
	tcx0      = InStream_0->tbx0 - Codec->Siz->XOsiz;
	tcy0      = InStream_0->tby0 - Codec->Siz->YOsiz;
	widthS    = InStream_0->tbx1 - InStream_0->tbx0;
	heightS   = InStream_0->tby1 - InStream_0->tby0;
	if (OutStream->type == CHAR) {
		uchar *D_TS, *D0_, *D0_TS, *D1_, *D1_TS, *D2_, *D2_TS;
		D_TS  = (uchar *) &OutStream->data[0];
		ccc   = 0;
		D0_TS = &D_TS[ccc * numData + (tcy0 * col1step1 + tcx0 * row1step1)];
		ccc   = 1;
		D1_TS = &D_TS[ccc * numData + (tcy0 * col1step1 + tcx0 * row1step1)];
		ccc   = 2;
		D2_TS = &D_TS[ccc * numData + (tcy0 * col1step1 + tcx0 * row1step1)];
		Y_TS  = (float *) &InStream_0->data[0]; //&Tile->Cmpt[0].data->data[0];
		U_TS  = (float *) &InStream_1->data[0]; //&Tile->Cmpt[1].data->data[0];
		V_TS  = (float *) &InStream_2->data[0]; //&Tile->Cmpt[2].data->data[0];
		for (j = 0; j < heightS; j++, Y_TS += col1stepY, U_TS += col1stepU, V_TS += col1stepV,
		    D0_TS += col1step1, D1_TS += col1step1, D2_TS += col1step1) {
			D0_ = D0_TS;
			D1_ = D1_TS;
			D2_ = D2_TS;
			Y_  = Y_TS;
			U_  = U_TS;
			V_  = V_TS;
			for (i = 0; i < widthS;
			     i++, ++Y_, ++U_, ++V_, D0_ += row1step1, D1_ += row1step1, D2_ += row1step1) {
				dataf = (*Y_) + (*U_) * (float) 1.77200; //
				data  = (byte4) dataf;
				data += offsetV;
				if (data < Bottom_Limit_2) data = Bottom_Limit_2;
				if (data > Top_Limit_2) data = Top_Limit_2;
				D2_[0] = (uchar) data;
				dataf  = (*Y_) - (*U_) * (float) 0.34413 - (*V_) * (float) 0.71414; // + offsetU;
				data   = (byte4) dataf;
				data += offsetU;
				if (data < Bottom_Limit_1) data = Bottom_Limit_1;
				if (data > Top_Limit_1) data = Top_Limit_1;
				D1_[0] = (uchar) data;                    //(data<0 ? 0:data)<255 ? (data<0 ? 0:data) : 255;
				dataf  = (*Y_) + (*V_) * (float) 1.40200; //                + offsetY;
				data   = (byte4) dataf;
				data += offsetY;
				if (data < Bottom_Limit_0) data = Bottom_Limit_0;
				if (data > Top_Limit_0) data = Top_Limit_0;
				D0_[0] = (uchar) data; //(data<0 ? 0:data)<255 ? (data<0 ? 0:data) : 255;
			}
		}
	} else if (OutStream->type == BYTE2) {
		ubyte2 *D_TS, *D0_, *D0_TS, *D1_, *D1_TS, *D2_, *D2_TS;
		D_TS  = (ubyte2 *) &OutStream->data[0];
		ccc   = 0;
		D0_TS = &D_TS[ccc * numData + (tcy0 * col1step1 + tcx0 * row1step1)];
		ccc   = 1;
		D1_TS = &D_TS[ccc * numData + (tcy0 * col1step1 + tcx0 * row1step1)];
		ccc   = 2;
		D2_TS = &D_TS[ccc * numData + (tcy0 * col1step1 + tcx0 * row1step1)];
		Y_TS  = (float *) &InStream_0->data[0]; //&Tile->Cmpt[0].data->data[0];
		U_TS  = (float *) &InStream_1->data[0]; //&Tile->Cmpt[1].data->data[0];
		V_TS  = (float *) &InStream_2->data[0]; //&Tile->Cmpt[2].data->data[0];
		for (j = 0; j < heightS; j++, Y_TS += col1stepY, U_TS += col1stepU, V_TS += col1stepV,
		    D0_TS += col1step1, D1_TS += col1step1, D2_TS += col1step1) {
			D0_ = D0_TS;
			D1_ = D1_TS;
			D2_ = D2_TS;
			Y_  = Y_TS;
			U_  = U_TS;
			V_  = V_TS;
			for (i = 0; i < widthS;
			     i++, ++Y_, ++U_, ++V_, D0_ += row1step1, D1_ += row1step1, D2_ += row1step1) {
				dataf = (*Y_) + (*U_) * (float) 1.77200; //
				data  = (byte4) dataf;
				data += offsetV;
				if (data < Bottom_Limit_2) data = Bottom_Limit_2;
				if (data > Top_Limit_2) data = Top_Limit_2;
				D2_[0] = (ubyte2) data;
				dataf  = (*Y_) - (*U_) * (float) 0.34413 - (*V_) * (float) 0.71414; // + offsetU;
				data   = (byte4) dataf;
				data += offsetU;
				if (data < Bottom_Limit_1) data = Bottom_Limit_1;
				if (data > Top_Limit_1) data = Top_Limit_1;
				D1_[0] = (ubyte2) data;                   //(data<0 ? 0:data)<255 ? (data<0 ? 0:data) : 255;
				dataf  = (*Y_) + (*V_) * (float) 1.40200; //                + offsetY;
				data   = (byte4) dataf;
				data += offsetY;
				if (data < Bottom_Limit_0) data = Bottom_Limit_0;
				if (data > Top_Limit_0) data = Top_Limit_0;
				D0_[0] = (ubyte2) data; //(data<0 ? 0:data)<255 ? (data<0 ? 0:data) : 255;
			}
		}
	}
	return EXIT_SUCCESS;
}

byte4 DcEnc_J2K(struct J2kParam_s *J2kParam, struct Image_s *Image, struct Image_s *Stream,
                struct Tile_s *t, struct Siz_s *Siz, ubyte2 ccc, byte4 offset, char FILTER) {
	byte4 width, height, x0, x1, y0, y1;
	byte4 i, j;
	FILE *fp;

	if (J2kParam->debugFlag) {
		char debugFname[256];
		strcpy(debugFname, J2kParam->debugFname);
		strcat(debugFname, "_DCout.csv");
		fp = fopen(debugFname, "a+");
	}

	if (t->tx1 < Siz->XOsiz) {
		x0    = 0;
		x1    = 0;
		width = 0;
	} else if (t->tx0 < Siz->XOsiz) {
		x0    = 0;
		x1    = t->tx1 - Siz->XOsiz;
		width = x1 - x0;
	} else if (t->tx1 < (Siz->XOsiz + Siz->Xsiz)) {
		x0    = t->tx0 - Siz->XOsiz;
		x1    = t->tx1 - Siz->XOsiz;
		width = x1 - x0;
	} else {
		x0    = t->tx0 - Siz->XOsiz;
		x1    = Stream->width;
		width = x1 - x0;
	}

	if (t->ty1 < Siz->YOsiz) {
		y0     = 0;
		y1     = 0;
		height = 0;
	} else if (t->ty0 < Siz->YOsiz) {
		y0     = 0;
		y1     = t->ty1 - Siz->YOsiz;
		height = y1 - y0;
	} else if (t->ty1 < (Siz->YOsiz + Siz->Ysiz)) {
		y0     = t->ty0 - Siz->YOsiz;
		y1     = t->ty1 - Siz->YOsiz;
		height = y1 - y0;
	} else {
		y0     = t->ty0 - Siz->YOsiz;
		y1     = Stream->height;
		height = y1 - y0;
	}

	Image[ccc].tbx0   = 0;
	Image[ccc].tbx1   = width;
	Image[ccc].tby0   = 0;
	Image[ccc].tby1   = height;
	Image[ccc].width  = width;
	Image[ccc].height = height;

	if (Stream->type == CHAR) {
		if ((FILTER == FILTER53) /* && (!J2kParam->HW)*/) {
			uchar *d_, *d_TS;
			byte4 *D_TS, *D_;
			d_TS = (uchar *) Stream->data;
			D_TS = (byte4 *) &Image[ccc].data[y0 * Image[ccc].col1step + Image[ccc].tbx0];
			for (j = y0; j < y1; j++, D_TS += Image[ccc].col1step) {
				d_ = &d_TS[j * Stream->col1step + x0 * Stream->row1step];
				D_ = D_TS;
				for (i = Image[ccc].tbx0; i < Image[ccc].tbx1; i++, d_ += Stream->row1step, ++D_) {
					*D_ = (byte4) (d_[ccc] - offset);
					if (J2kParam->debugFlag) fprintf(fp, "%d,", (*D_));
				}
				if (J2kParam->debugFlag) fprintf(fp, "\n");
			}
		}
		//		else if( (FILTER==FILTER53) && (J2kParam->HW) ){
		//			uchar	*d_, *d_TS;
		//			byte4	*D_TS, *D_;
		//			d_TS = (uchar *)Stream->data;
		//			D_TS = (byte4 *)&Image[ccc].data[y0*Image[ccc].col1step + Image[ccc].tbx0];
		//			for( j=y0 ; j<y1 ; j++, D_TS+=Image[ccc].col1step ){
		//				d_ = &d_TS[j*Stream->col1step + x0*Stream->row1step];
		//				D_ = D_TS;
		//				for( i=Image[ccc].tbx0 ; i<Image[ccc].tbx1 ; i++, d_+=Stream->row1step, ++D_ ){
		//					*D_ = (byte4)( d_[ccc]-offset );
		//					if( J2kParam->debugFlag )
		//						fprintf( fp, "%d,", (*D_) );
		//				}
		//				if( J2kParam->debugFlag )
		//					fprintf( fp, "\n" );
		//			}
		//		}
		else if ((FILTER == FILTER97) /* && (!J2kParam->HW)*/) {
			uchar *d_, *d_TS;
			float *f_TS, *f_;
			d_TS = (uchar *) Stream->data;
			f_TS = (float *) &Image[ccc].data[y0 * Image[ccc].col1step + Image[ccc].tbx0];
			for (j = y0; j < y1; j++, f_TS += Image[ccc].col1step) {
				d_ = &d_TS[j * Stream->col1step + x0 * Stream->row1step];
				f_ = f_TS;
				for (i = Image[ccc].tbx0; i < Image[ccc].tbx1; i++, d_ += Stream->row1step, ++f_) {
					*f_ = (float) (d_[ccc] - offset);
					if (J2kParam->debugFlag) fprintf(fp, "%f,", (*f_));
				}
				if (J2kParam->debugFlag) fprintf(fp, "\n");
			}
		}
		//		else{	//( (FILTER==FILTER97) && (J2kParam->HW) )
		//			uchar	*d_, *d_TS;
		//			byte4	*D_TS, *D_;
		//			d_TS = (uchar *)Stream->data;
		//			D_TS = (byte4 *)&Image[ccc].data[y0*Image[ccc].col1step + Image[ccc].tbx0];
		//			for( j=y0 ; j<y1 ; j++, D_TS+=Image[ccc].col1step ){
		//				d_= &d_TS[j*Stream->col1step + x0*Stream->row1step];
		//				D_   = D_TS;
		//				for( i=Image[ccc].tbx0 ; i<Image[ccc].tbx1 ; i++, d_+=Stream->row1step, ++D_ ){
		//					*D_ = (byte4)(( d_[ccc]-offset )<<J2kParam->HW_DecimalPoint);
		//					if( J2kParam->debugFlag )
		//						fprintf( fp, "%d,", (*D_) );
		//				}
		//				if( J2kParam->debugFlag )
		//					fprintf( fp, "\n" );
		//			}
		//		}
	} else if (Stream->type == BYTE2) {
		if ((FILTER == FILTER53) /* && (!J2kParam->HW)*/) {
			ubyte2 *d_, *d_TS;
			byte4 *D_TS, *D_;
			d_TS = (ubyte2 *) Stream->data;
			D_TS = (byte4 *) &Image[ccc].data[y0 * Image[ccc].col1step + Image[ccc].tbx0];
			for (j = y0; j < y1; j++, D_TS += Image[ccc].col1step) {
				d_ = &d_TS[j * Stream->col1step + x0 * Stream->row1step];
				D_ = D_TS;
				for (i = Image[ccc].tbx0; i < Image[ccc].tbx1; i++, d_ += Stream->row1step, ++D_) {
					*D_ = (byte4) (d_[ccc] - offset);
					if (J2kParam->debugFlag) fprintf(fp, "%d,", (*D_));
				}
				if (J2kParam->debugFlag) fprintf(fp, "\n");
			}
		}
		//		else if( (FILTER==FILTER53) && (J2kParam->HW) ){
		//			ubyte2	*d_, *d_TS;
		//			byte4	*D_TS, *D_;
		//			d_TS = (ubyte2 *)Stream->data;
		//			D_TS = (byte4 *)&Image[ccc].data[y0*Image[ccc].col1step + Image[ccc].tbx0];
		//			for( j=y0 ; j<y1 ; j++, D_TS+=Image[ccc].col1step ){
		//				d_ = &d_TS[j*Stream->col1step + x0*Stream->row1step];
		//				D_ = D_TS;
		//				for( i=Image[ccc].tbx0 ; i<Image[ccc].tbx1 ; i++, d_+=Stream->row1step, ++D_ ){
		//					*D_ = (byte4)( d_[ccc]-offset );
		//					if( J2kParam->debugFlag )
		//						fprintf( fp, "%d,", (*D_) );
		//				}
		//				if( J2kParam->debugFlag )
		//					fprintf( fp, "\n" );
		//			}
		//		}
		else if ((FILTER == FILTER97) /* && (!J2kParam->HW)*/) {
			ubyte2 *d_, *d_TS;
			float *f_TS, *f_;
			d_TS = (ubyte2 *) Stream->data;
			f_TS = (float *) &Image[ccc].data[y0 * Image[ccc].col1step + Image[ccc].tbx0];
			for (j = y0; j < y1; j++, f_TS += Image[ccc].col1step) {
				d_ = &d_TS[j * Stream->col1step + x0 * Stream->row1step];
				f_ = f_TS;
				for (i = Image[ccc].tbx0; i < Image[ccc].tbx1; i++, d_ += Stream->row1step, ++f_) {
					*f_ = (float) (d_[ccc] - offset);
					if (J2kParam->debugFlag) fprintf(fp, "%f,", (*f_));
				}
				if (J2kParam->debugFlag) fprintf(fp, "\n");
			}
		}
		//		else{	//( (FILTER==FILTER97) && (J2kParam->HW) )
		//			ubyte2	*d_, *d_TS;
		//			byte4	*D_TS, *D_;
		//			d_TS = (ubyte2 *)Stream->data;
		//			D_TS = (byte4 *)&Image[ccc].data[y0*Image[ccc].col1step + Image[ccc].tbx0];
		//			for( j=y0 ; j<y1 ; j++, D_TS+=Image[ccc].col1step ){
		//				d_= &d_TS[j*Stream->col1step + x0*Stream->row1step];
		//				D_   = D_TS;
		//				for( i=Image[ccc].tbx0 ; i<Image[ccc].tbx1 ; i++, d_+=Stream->row1step, ++D_ ){
		//					*D_ = (byte4)(( d_[ccc]-offset )<<J2kParam->HW_DecimalPoint);
		//					if( J2kParam->debugFlag )
		//						fprintf( fp, "%d,", (*D_) );
		//				}
		//				if( J2kParam->debugFlag )
		//					fprintf( fp, "\n" );
		//			}
		//		}
	} else if (Stream->type == BYTE4) {
		if (FILTER == FILTER53) {
			byte4 *d_, *d_TS;
			byte4 *D_TS, *D_;
			d_TS = (byte4 *) Stream->data;
			D_TS = (byte4 *) &Image[ccc].data[y0 * Image[ccc].col1step + Image[ccc].tbx0];
			for (j = y0; j < y1; j++, D_TS += Image[ccc].col1step) {
				d_ = &d_TS[j * Stream->col1step + x0 * Stream->row1step];
				D_ = D_TS;
				for (i = Image[ccc].tbx0; i < Image[ccc].tbx1; i++, d_ += Stream->row1step, ++D_) {
					*D_ = (byte4) ((d_[ccc] - offset) * 64);
					if (J2kParam->debugFlag) fprintf(fp, "%d,", (*D_));
				}
				if (J2kParam->debugFlag) fprintf(fp, "\n");
			}
		} else {
			byte4 *d_, *d_TS;
			float *f_TS, *f_;
			d_TS = (byte4 *) Stream->data;
			f_TS = (float *) &Image[ccc].data[y0 * Image[ccc].col1step + Image[ccc].tbx0];
			for (j = y0; j < y1; j++, f_TS += Image[ccc].col1step) {
				d_ = &d_TS[j * Stream->col1step + x0 * Stream->row1step];
				f_ = f_TS;
				for (i = Image[ccc].tbx0; i < Image[ccc].tbx1; i++, d_ += Stream->row1step, ++f_) {
					*f_ = (float) (d_[ccc] - offset);
					if (J2kParam->debugFlag) fprintf(fp, "%f,", (*f_));
				}
				if (J2kParam->debugFlag) fprintf(fp, "\n");
			}
		}
	}
	if (J2kParam->debugFlag) {
		fprintf(fp, "\n\n");
		fclose(fp);
	}
	return EXIT_SUCCESS;
}

byte4 DcDec3(struct Codec_s *Codec, struct Tile_s *Tile, struct Image_s *InStream, ubyte2 ccc,
             struct Image_s *OutStream) {
	byte4 i, j, k;
	byte4 widthS, heightS, widthD, heightD;
	byte4 offset;
	byte4 upper_Limit, Bottom_Limit;
	byte4 tx0, tx1, ty0, ty1;
	byte4 tcx0, tcx1, tcy0, tcy1;
	byte4 col1step0, col1step1, row1step0, row1step1;
	uchar FILTER;

	col1step1 = OutStream->col1step;
	row1step1 = OutStream->row1step;
	widthD    = OutStream->tbx1 - OutStream->tbx0;
	heightD   = OutStream->tby1 - OutStream->tby0;
	tx0       = Codec->Siz->XOsiz;
	tx1       = Codec->Siz->Xsiz;
	ty0       = Codec->Siz->YOsiz;
	ty1       = Codec->Siz->Ysiz;

	if (Codec->Siz->Ssiz[ccc] & 0x80) {
		offset       = 0;
		upper_Limit  = (Codec->Siz->Ssiz[ccc] & 0x7f);
		upper_Limit  = (1 << upper_Limit) - 1;
		Bottom_Limit = -1 * (1 << (Codec->Siz->Ssiz[ccc] & 0x7f));
	} else {
		offset       = 1 << (Codec->Siz->Ssiz[ccc] & 0x7f);
		upper_Limit  = (Codec->Siz->Ssiz[ccc] & 0x7f) + 1;
		upper_Limit  = (1 << upper_Limit) - 1;
		Bottom_Limit = 0;
	}
	FILTER    = Codec->ECod->EECod[ccc * Codec->ECod->numTiles + Tile->T].Filter;
	col1step0 = InStream->col1step;
	row1step0 = InStream->row1step;
	tcx0      = InStream->tbx0 - ceil2(tx0, Codec->Siz->XRsiz[ccc]);
	tcx1      = InStream->tbx1 - ceil2(tx0, Codec->Siz->XRsiz[ccc]);
	tcy0      = InStream->tby0 - ceil2(ty0, Codec->Siz->YRsiz[ccc]);
	tcy1      = InStream->tby1 - ceil2(ty0, Codec->Siz->YRsiz[ccc]);
	widthS    = tcx1 - tcx0;
	heightS   = tcy1 - tcy0;
	if ((OutStream->type == BIT1) && (FILTER == FILTER53)) {
		uchar *D_TS, *D_;
		byte4 *S_TS, *S_;
		byte4 data;
		D_TS = (uchar *) &OutStream->data[0];
		D_TS = &D_TS[ccc * heightD * col1step1 + (tcy0 * col1step1 + tcx0 * row1step1)];
		S_TS = (byte4 *) &InStream->data[0];
		for (j = 0; j < heightS; j++, S_TS += col1step0, D_TS += col1step1) {
			S_ = S_TS;
			D_ = D_TS;
			for (k = 0x80, i = 0, data = 0; i < widthS; i++, S_ += row1step0, k >>= 1) {
				data |= ((*S_) ? k : 0);
				if (k == 1) {
					*D_ = (uchar) data;
					++D_;
					data = 0;
					k    = 0x100;
				}
			}
		}
	} else if ((OutStream->type == CHAR) && (FILTER == FILTER53)) {
		if (Codec->Siz->Ssiz[ccc] & 0x80) {
			char *D_TS, *D_;
			byte4 *S_TS, *S_;
			byte4 data;
			D_TS = (char *) &OutStream->data[0];
			D_TS = &D_TS[ccc * heightD * col1step1 + (tcy0 * col1step1 + tcx0 * row1step1)];
			S_TS = (byte4 *) &InStream->data[0];
			for (j = 0; j < heightS; j++, S_TS += col1step0, D_TS += col1step1) {
				S_ = S_TS;
				D_ = D_TS;
				for (i = 0; i < widthS; i++, D_ += row1step1, S_ += row1step0) {
					data = *S_;
					if (data < Bottom_Limit) data = Bottom_Limit;
					if (data > upper_Limit) data = upper_Limit;
					*D_ = (char) data;
				}
			}
		} else {
			uchar *D_TS, *D_;
			byte4 *S_TS, *S_;
			byte4 data;
			D_TS = (uchar *) &OutStream->data[0];
			D_TS = &D_TS[ccc * heightD * col1step1 + (tcy0 * col1step1 + tcx0 * row1step1)];
			S_TS = (byte4 *) &InStream->data[0];
			for (j = 0; j < heightS; j++, S_TS += col1step0, D_TS += col1step1) {
				S_ = S_TS;
				D_ = D_TS;
				for (i = 0; i < widthS; i++, D_ += row1step1, S_ += row1step0) {
					data = *S_ + offset;
					if (data < Bottom_Limit) data = Bottom_Limit;
					if (data > upper_Limit) data = upper_Limit;
					*D_ = (uchar) data;
				}
			}
		}
	} else if ((OutStream->type == CHAR) && (FILTER == FILTER97)) {
		if (Codec->Siz->Ssiz[ccc] & 0x80) {
			char *D_TS, *D_;
			float *S_TS, *S_;
			byte4 data;
			D_TS = (char *) &OutStream->data[0];
			D_TS = &D_TS[ccc * heightD * col1step1 + tcy0 * col1step1 + tcx0 * row1step1];
			S_TS = (float *) &InStream->data[0];
			for (j = 0; j < heightS; j++, S_TS += col1step0, D_TS += col1step1) {
				S_ = S_TS;
				D_ = D_TS;
				for (i = 0; i < widthS; i++, D_ += row1step1, S_ += row1step0) {
					if (*S_ > 0) *S_ = *S_ + (float) 0.5;
					if (*S_ < 0) *S_ = *S_ - (float) 0.5;
					data = (byte4) *S_;
					if (data < Bottom_Limit) data = Bottom_Limit;
					if (data > upper_Limit) data = upper_Limit;
					*D_ = (char) data;
				}
			}
		} else {
			uchar *D_TS, *D_;
			float *S_TS, *S_;
			byte4 data;
			D_TS = (uchar *) &OutStream->data[0];
			D_TS = &D_TS[ccc * heightD * col1step1 + tcy0 * col1step1 + tcx0 * row1step1];
			S_TS = (float *) &InStream->data[0];
			for (j = 0; j < heightS; j++, S_TS += col1step0, D_TS += col1step1) {
				S_ = S_TS;
				D_ = D_TS;
				for (i = 0; i < widthS; i++, D_ += row1step1, S_ += row1step0) {
					if (*S_ > 0) *S_ = *S_ + (float) 0.5;
					if (*S_ < 0) *S_ = *S_ - (float) 0.5;
					data = (byte4) (*S_);
					data += offset;
					if (data < Bottom_Limit) data = Bottom_Limit;
					if (data > upper_Limit) data = upper_Limit;
					*D_ = (uchar) data; //(uchar)((data<0.0? 0:data)>255.0? 255:data);
				}
			}
		}
	}
	if ((OutStream->type == BYTE2) && (FILTER == FILTER53)) {
		if (Codec->Siz->Ssiz[ccc] & 0x80) {
			byte2 *D_TS, *D_;
			byte4 *S_TS, *S_;
			byte4 data;
			D_TS = (byte2 *) &OutStream->data[0];
			D_TS = &D_TS[ccc * heightD * col1step1 + tcy0 * col1step1 + tcx0 * row1step1];
			S_TS = (byte4 *) &InStream->data[0];
			for (j = 0; j < heightS; j++, S_TS += col1step0, D_TS += col1step1) {
				S_ = S_TS;
				D_ = D_TS;
				for (i = 0; i < widthS; i++, D_ += row1step1, S_ += row1step0) {
					data = (byte4) *S_;
					if (data < Bottom_Limit) data = Bottom_Limit;
					if (data > upper_Limit) data = upper_Limit;
					*D_ = (byte2) data;
				}
			}
		} else {
			ubyte2 *D_TS, *D_;
			byte4 *S_TS, *S_;
			byte4 data;
			D_TS = (ubyte2 *) &OutStream->data[0];
			D_TS = &D_TS[ccc * heightD * col1step1 + tcy0 * col1step1 + tcx0 * row1step1];
			S_TS = (byte4 *) &InStream->data[0];
			for (j = 0; j < heightS; j++, S_TS += col1step0, D_TS += col1step1) {
				S_ = S_TS;
				D_ = D_TS;
				for (i = 0; i < widthS; i++, D_ += row1step1, S_ += row1step0) {
					data = (byte4) (*S_);
					data += offset;
					if (data < Bottom_Limit) data = Bottom_Limit;
					if (data > upper_Limit) data = upper_Limit;
					*D_ = (ubyte2) data;
				}
			}
		}
	} else if ((OutStream->type == BYTE2) && (FILTER == FILTER97)) {
		if (Codec->Siz->Ssiz[ccc] & 0x80) {
			byte2 *D_TS, *D_;
			float *S_TS, *S_;
			byte4 data;
			D_TS = (byte2 *) &OutStream->data[0];
			D_TS = &D_TS[ccc * heightD * col1step1 + tcy0 * col1step1 + tcx0 * row1step1];
			S_TS = (float *) &InStream->data[0];
			for (j = 0; j < heightS; j++, S_TS += col1step0, D_TS += col1step1) {
				S_ = S_TS;
				D_ = D_TS;
				for (i = 0; i < widthS; i++, D_ += row1step1, S_ += row1step0) {
					if (*S_ > 0) *S_ = *S_ + (float) 0.5;
					if (*S_ < 0) *S_ = *S_ - (float) 0.5;
					data = (byte4) *S_;
					if (data < Bottom_Limit) data = Bottom_Limit;
					if (data > upper_Limit) data = upper_Limit;
					*D_ = (byte2) data;
				}
			}
		} else {
			ubyte2 *D_TS, *D_;
			float *S_TS, *S_;
			byte4 data;
			D_TS = (ubyte2 *) &OutStream->data[0];
			D_TS = &D_TS[ccc * heightD * col1step1 + tcy0 * col1step1 + tcx0 * row1step1];
			S_TS = (float *) &InStream->data[0];
			for (j = 0; j < heightS; j++, S_TS += col1step0, D_TS += col1step1) {
				S_ = S_TS;
				D_ = D_TS;
				for (i = 0; i < widthS; i++, D_ += row1step1, S_ += row1step0) {
					if (*S_ > 0) *S_ = *S_ + (float) 0.5;
					if (*S_ < 0) *S_ = *S_ - (float) 0.5;
					data = (byte4) (*S_);
					data += offset;
					if (data < Bottom_Limit) data = Bottom_Limit;
					if (data > upper_Limit) data = upper_Limit;
					*D_ = (ubyte2) data;
				}
			}
		}
	}
	return EXIT_SUCCESS;
}
