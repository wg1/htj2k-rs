/*
Copyright (c) 2000-2021, ICT-Link
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the HTJ2K project.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "asmasm.h"
#include "j2k2.h"
#include "ImageUtil.h"
#include "wavelet_codec.h"
#include "Icc.h"
#include "t2codec.h"
#include "annexA.h"
#include "annexB.h"
#include "t1codec.h"
#include "annexE.h"
#include "annexH.h"
#include "MqCodec.h"
#include "RateControl.h"
#include "vlc_codec.h"
#include "MelCodec.h"

//#define	MAX_THREAD_NUM	2
// HANDLE	sem;

struct J2kParam_s *J2kParamCreate(void) {
	struct J2kParam_s *J2kParam;
	if (nullptr == (J2kParam = new struct J2kParam_s)) {
		printf("[J2kParamCreate] struct J2kParam_s* malloc error\n");
		return nullptr;
	}

	J2kParam->CbBypass        = CB_BYPASS_;
	J2kParam->CbH             = CB_W_;
	J2kParam->CbW             = CB_H_;
	J2kParam->CbPreterm       = CB_PTERM_;
	J2kParam->CbReset         = CB_RESET_;
	J2kParam->CbSegmentSymbol = CB_SEGMENTSYMBOL_;
	J2kParam->CbTerm          = CB_TERM_;
	J2kParam->CbVcausal       = CB_VCAUSAL_;
	J2kParam->DecompLev       = DECOMPLEV_;
	J2kParam->EphOn           = EPH_ON_;
	J2kParam->Filter          = FILTER_;
	J2kParam->MC              = MC_;
	J2kParam->MC_OFF          = 0;
	J2kParam->Sign_OFF        = 0;
	J2kParam->numLayer        = NUMLAYER_;
	J2kParam->ProgOrder       = PROGORDER_;
	J2kParam->SopOn           = SOP_ON_;
	J2kParam->Sqcd            = S_QCD_;
	J2kParam->Tile_x          = X_TSIZE_;
	J2kParam->Tile_y          = Y_TSIZE_;
	J2kParam->Tile_OffsetX    = X_TOFFSET_;
	J2kParam->Tile_OffsetY    = Y_TOFFSET_;
	J2kParam->OffsetX         = X_OFFSET_;
	J2kParam->OffsetY         = Y_OFFSET_;
	J2kParam->RateControl     = RATECONTROL_;
	J2kParam->rate            = (float) RATE_;
	J2kParam->byteLimit       = BYTELIMIT_;
	J2kParam->ROI             = 0;
	J2kParam->metric          = METRIC_RGB;
	J2kParam->Q_select        = 0;
	J2kParam->Q_value0        = 0;
	J2kParam->Q_value1        = 0;
	if (nullptr == (J2kParam->PrcH = new uchar[33])) {
		printf("[J2kParamCreate] struct J2kParam->PrcH malloc error\n");
		return nullptr;
	}
	memset(J2kParam->PrcH, 0xf, sizeof(uchar) * 33);

	if (nullptr == (J2kParam->PrcW = new uchar[33])) {
		printf("[J2kParamCreate] struct J2kParam->PrcW malloc error\n");
		return nullptr;
	}
	memset(J2kParam->PrcW, 0xf, sizeof(uchar) * 33);

	if (nullptr == (J2kParam->fileForm = new char[8])) {
		printf("[J2kParamCreate] struct J2kParam->fileForm malloc error\n");
		return nullptr;
	}
	memset(J2kParam->fileForm, 0, sizeof(char) * 8);

	if (nullptr == (J2kParam->fname = new char[256])) {
		printf("[J2kParamCreate] struct J2kParam->fname malloc error\n");
		return nullptr;
	}
	memset(J2kParam->fname, 0, sizeof(char) * 256);

	J2kParam->Roi_fname     = nullptr;
	J2kParam->HTJ2K         = 0;
	J2kParam->DEC_DecompLev = -1;
	J2kParam->debugFlag     = 0;
	//	J2kParam->HW=0;
	//	J2kParam->HW_DecimalPoint=0;
	//	J2kParam->HW_DisCard=0;
	//	J2kParam->HT_ccc=0;
	//	J2kParam->HT_rrr=0;
	//	J2kParam->HT_bbb=0;
	//	J2kParam->HT_kkk=0;
	//	J2kParam->HT_ALPHA_TEST=0;
	//	J2kParam->HT_BETA_TEST=0;
	//	J2kParam->HT_GAMMA_TEST=0;
	//	J2kParam->HT_DELTA_TEST=0;
	//	J2kParam->HT_KAPPA0_TEST=0;
	//	J2kParam->HT_KAPPA1_TEST=0;
	//	J2kParam->HT_ALPHA_TEST_ON=0;
	//	J2kParam->HT_BETA_TEST_ON=0;
	//	J2kParam->HT_GAMMA_TEST_ON=0;
	//	J2kParam->HT_DELTA_TEST_ON=0;
	//	J2kParam->HT_KAPPA0_TEST_ON=0;
	//	J2kParam->HT_KAPPA1_TEST_ON=0;
	//	J2kParam->HT_CleanUP_ALL_TEST_ON=0;
	//	J2kParam->HT_CleanUP_ALL_TEST=0;
	//	J2kParam->HT_CleanUP_CX0_TEST_ON=0;
	//	J2kParam->HT_CleanUP_CX0_TEST=0;
	//	J2kParam->HT_CleanUP_CX1_TEST_ON=0;
	//	J2kParam->HT_CleanUP_CX1_TEST=0;
	//	J2kParam->HT_WAVE_TEST=0;
	//	J2kParam->HT_DelayAlpha = 0;
	//	J2kParam->HT_DelayBeta = 0;
	//	J2kParam->HT_DelayGamma = 0;
	//	J2kParam->HT_DelayDelta = 0;
	//	J2kParam->HT_DelayKappa0 = 0;
	//	J2kParam->HT_DelayKappa1 = 0;
	//	J2kParam->HT_TG = 0;
	//	J2kParam->HT_Width_Margin = 0;
	//	J2kParam->HT_Height_Margin = 0;

	return J2kParam;
}

byte4 EncMainBody(struct J2kParam_s *J2kParam, struct Tile_s *t, struct EECod_s *EECod,
                  struct EEQcd_s *EEQcd, struct Codec_s *Codec, ubyte2 ccc, uchar shift) {
	byte4 kkk;
	struct mqenc_s *enc = nullptr;
	struct EPrc_s *EPrc;
	byte4 ppp;
	uchar *ctx_table;
	uchar rrr, bbb, numBands, BAND;

	if (nullptr == (enc = Mqenc_Create(MAX_CONTEXTS))) {
		printf("[EncMainBody]:: Mqenc_Create error.\n");
		return EXIT_FAILURE;
	}

	for (rrr = 0; rrr <= EECod->DecompLev; rrr++) {
		for (ppp = 0; ppp < t->Cmpt[ccc].EEPrc[rrr].numPrc; ppp++) {
			EPrc     = &(t->Cmpt[ccc].EEPrc[rrr].EPrc[ppp]);
			numBands = t->Cmpt[ccc].EEPrc[rrr].numBand;
			for (bbb = 0; bbb < numBands; bbb++) {
				if (t->Cmpt[ccc].EEPrc[rrr].numBand == 1) {
					ctx_table = lhll_table;
					BAND      = 0;
				} else {
					switch (bbb) {
						case 0:
							ctx_table = hl_table;
							BAND      = (char) (rrr * 3 - 2);
							break;
						case 1:
							ctx_table = lhll_table;
							BAND      = (char) (rrr * 3 - 1);
							break;
						case 2:
							ctx_table = hh_table;
							BAND      = (char) (rrr * 3);
							break;
						default:
							printf("[EncMainBody]:: Band number error bbb=%d\n", bbb);
							return EXIT_FAILURE;
					}
				}
				// Normal JPEG 2000
				if (!J2kParam->HTJ2K) {
					for (kkk = 0; kkk < EPrc->Prc[bbb].numCblk; kkk++) {
						if (EXIT_SUCCESS
						    != T1Enc(Codec, &(EPrc->Prc[bbb].Cblk[kkk]), EECod, EEQcd->e[BAND],
						             EEQcd->Rb[BAND], EEQcd->myu[BAND], EEQcd->G, enc, ctx_table, shift)) {
							printf("[EncMainBody] :: T1Enc error in (rrr=%d bbb=%d kkk=%d)\n", rrr, bbb,
							       kkk);
							return EXIT_FAILURE;
						}
					}
				}
				// HT JPEG 2000
				else {
					for (kkk = 0; kkk < EPrc->Prc[bbb].numCblk; kkk++) {
						if (EXIT_SUCCESS
						    != T1Enc_HTJ2K(J2kParam, Codec, &(EPrc->Prc[bbb].Cblk[kkk]), EECod,
						                   EEQcd->e[BAND], EEQcd->Rb[BAND], EEQcd->myu[BAND], EEQcd->G,
						                   /*enc,*/ shift)) {
							printf("[EncMainBody] :: T1Enc_HTJ2K error in (rrr=%d bbb=%d kkk=%d)\n", rrr,
							       bbb, kkk);
							return EXIT_FAILURE;
						}
					}
				}
			}
		}
	}
	delete[] enc->ind;
	delete[] enc->cx;
	delete enc;
	return EXIT_SUCCESS;
}

byte4 TileEnc(struct J2kParam_s *J2kParam, struct wavelet_s *Wave, struct Image_s *Image,
              struct Image_s *Stream, struct Codec_s *Codec, byte4 index, struct ERgn_s *ERgn) {
	struct EECod_s *EECod;
	struct EEQcd_s *EEQcd;
	struct Tile_s *Tile;
	ubyte2 ccc;
	byte4 offset, offset_ROI;
	uchar NL;
	uchar shift = 0;

	NL   = Wave->NL;
	Tile = &(Codec->Tile[index]);
	for (ccc = 0; ccc < Codec->Siz->Csiz; ccc++) {
		EECod  = &(Codec->ECod->EECod[ccc * Codec->numTiles + Tile->T]);
		EEQcd  = &(Codec->EQcd->EEQcd[ccc * Codec->numTiles + Tile->T]);
		offset = 1 << Codec->Siz->Ssiz[ccc];
		if ((Codec->Siz->Csiz == 3) && (EECod->MC)) {
			if (EECod->Filter)
				RGBtoYUV2000(Image, Stream, Tile, Codec->Siz, ccc, offset, J2kParam->metric);
			else
				RGBtoICC(Image, Stream, Tile, Codec->Siz, ccc, offset);
		} else
			DcEnc_J2K(J2kParam, Image, Stream, Tile, Codec->Siz, ccc, offset, EECod->Filter);

		Tile->Cmpt[ccc].data = (struct Image_s *) (&Image[ccc]);
		Tile->Cmpt[ccc].wave = &Wave[ccc];
		if (EXIT_SUCCESS != WaveEnc(J2kParam, /*ccc,*/ Tile->Cmpt[ccc].wave, EECod, EEQcd)) {
			printf("[TileEnc] :: WaveEnc(ccc=%d)error\n", ccc);
			return EXIT_FAILURE;
		}

		if (J2kParam->ROI) {
			offset_ROI = 1;
			ImageCreate(&(ERgn->EERgn[ccc].Wave_Roi->ll_d[NL]), Image[ccc].width, Image[ccc].height,
			            Image[ccc].tbx0, Image[ccc].tbx1, Image[ccc].tby0, Image[ccc].tby1, BYTE4);
			DcEnc_J2K(J2kParam, &ERgn->EERgn[ccc].Wave_Roi->ll_d[NL], ERgn->EERgn[ccc].Image_Roi, Tile,
			          Codec->Siz, ccc, offset_ROI, EECod->Filter);
			Wave_ROI(ERgn->EERgn[ccc].Wave_Roi);
			Roi_S_ValueCal(EECod, EEQcd, ERgn, ccc);
			if (EXIT_SUCCESS != Roi_S_ShiftUP(Tile, ERgn, ccc)) {
				printf("[TileEnc] :: Roi_S_ShiftUP(ccc=%d) error.\n", ccc);
				return EXIT_FAILURE;
			}
		}

		if (EXIT_SUCCESS != PrcAdrCal(Tile, Tile->Cmpt[ccc].wave, ccc, EECod)) {
			printf("[TileEnc] PrcAdrCal(ccc=%d)error\n", ccc);
			return EXIT_FAILURE;
		}

		if (EXIT_SUCCESS != CblkAdrCal(Codec, Tile->Cmpt[ccc].EEPrc, EECod, EEQcd)) {
			printf("[TileEnc] CblkAdrCal(ccc=%d)error\n", ccc);
			return EXIT_FAILURE;
		}

		if (ERgn == nullptr)
			shift = 0;
		else
			shift = ERgn->EERgn[ccc].SPrgn;

		if (EXIT_SUCCESS != EncMainBody(J2kParam, Tile, EECod, EEQcd, Codec, ccc, shift)) {
			printf("[TileEnc]EncMainBody(ccc=%d) error\n", ccc);
			return EXIT_FAILURE;
		}
	}
	//	printf( "[TileEnc] completet\n");
	return EXIT_SUCCESS;
}

byte4 J2kEnc(struct Image_s *Stream, struct Codec_s *Codec, struct StreamChain_s *str,
             struct J2kParam_s *J2kParam) {
	struct EQcd_s *EQcd       = nullptr;
	struct Siz_s *Siz         = nullptr;
	struct ECod_s *ECod       = nullptr;
	struct EECod_s *EECod     = nullptr;
	struct ERgn_s *ERgn       = nullptr;
	struct Tile_s *Tile       = nullptr;
	struct Image_s *Image_Roi = nullptr;
	byte2 ttt;
	ubyte2 ccc;
	uchar rrr;
	byte4 ppp;
	struct Image_s *Image  = nullptr;
	struct wavelet_s *Wave = nullptr, *Wave_Roi = nullptr;
	struct mqenc_s *enc = nullptr;
	byte4 x1, y1;
	byte4 byteLimit, MarkerByte, PIbyte;
	byte4 CCsiz;
	byte4 numPackets, numberOfPackets, maxDecompLev, maxnumPasses, maxnumPrc;
	byte2 *Cppp    = nullptr;
	uchar *flagD   = nullptr;
	uchar FlagFlag = 0;
	uchar NL;
	Siz  = Codec->Siz;
	ECod = Codec->ECod;
	EQcd = Codec->EQcd;

	x1 = Siz->XTsiz;
	y1 = Siz->YTsiz;
	if (!x1) x1 = 1;
	if (!y1) y1 = 1;

	if (nullptr == (Image = new struct Image_s[Codec->Siz->Csiz])) {
		printf("[J2kEnc]:: Image malloc Error\n");
		return EXIT_FAILURE;
	}
	for (ccc = 0; ccc < Codec->Siz->Csiz; ccc++) {
		x1 = (Codec->Siz->Xdim < Codec->Siz->XTsiz) ? Codec->Siz->Xdim : Codec->Siz->XTsiz;
		y1 = (Codec->Siz->Ydim < Codec->Siz->YTsiz) ? Codec->Siz->Ydim : Codec->Siz->YTsiz;
		x1 = ceil2(x1, Codec->Siz->XRsiz[ccc]);
		y1 = ceil2(y1, Codec->Siz->YRsiz[ccc]);
		if (nullptr == ImageCreate(&Image[ccc], x1, y1, 0, x1, 0, y1, BYTE4)) {
			printf("[J2kEnc]:: ImageCreate Error (ccc=%d)\n", ccc);
			return EXIT_FAILURE;
		}
	}

	for (ttt = 0; ttt < Codec->numTiles; ttt++) {
		Tile  = &Codec->Tile[ttt];
		EECod = &ECod->EECod[ttt];
		if (EXIT_FAILURE == (TileAdrCal(Tile, Siz, ttt))) {
			printf("[J2kEnc]:: TileAdrCal Error (ttt=%d)\n", ttt);
			return EXIT_FAILURE;
		}
		if (nullptr == (Wave = new struct wavelet_s[Tile->numCompts])) {
			printf("[J2kEnc]:: wavelet_s malloc error. ttt=%d Tile->numCompts=%d\n", ttt, Tile->numCompts);
			return EXIT_FAILURE;
		}
		if (J2kParam->ROI) {
			ERgn            = new struct ERgn_s;
			ERgn->numCompts = Codec->Siz->Csiz;
			ERgn->EERgn     = new struct EERgn_s[Codec->Siz->Csiz];
			for (ccc = 0; ccc < Codec->Siz->Csiz; ccc++) {
				if (Codec->Siz->Csiz < 256)
					ERgn->EERgn[ccc].Lrgn = 5;
				else
					ERgn->EERgn[ccc].Lrgn = 6;
				ERgn->EERgn[ccc].SPrgn = 0;
				ERgn->EERgn[ccc].Srgn  = 0;
				ERgn->EERgn[ccc].Crgn  = ccc;
			}
			if (nullptr == (Image_Roi = (struct Image_s *) LoadBmp(J2kParam->Roi_fname))) {
				printf("[J2kEnc]:: LoadBmp error. Image_Roi create error\n");
				return EXIT_FAILURE;
			}

			if (nullptr == (Wave_Roi = new struct wavelet_s)) {
				printf("[J2kEnc]:: LoadBmp error. Wave_Roi create error\n");
				return EXIT_FAILURE;
			}

			NL = Codec->ECod->EECod[ttt].DecompLev;
			Wavelet_Create(Wave_Roi, (Tile->Cmpt[0].tcx1 - Tile->Cmpt[0].tcx0),
			               (Tile->Cmpt[0].tcy1 - Tile->Cmpt[0].tcy0), NL);
			WaveAdrCal(Wave_Roi, Image_Roi, EECod, FlagFlag);
			for (ccc = 0; ccc < Codec->Siz->Csiz; ccc++) {
				ERgn->EERgn[ccc].Image_Roi = Image_Roi;
				ERgn->EERgn[ccc].Wave_Roi  = Wave_Roi;
			}
		}
		for (ccc = 0; ccc < Codec->Siz->Csiz; ccc++) {
			NL = Codec->ECod->EECod[ccc * Codec->ECod->numTiles + ttt].DecompLev;
			Wavelet_Create(&Wave[ccc], (Tile->Cmpt[ccc].tcx1 - Tile->Cmpt[ccc].tcx0),
			               (Tile->Cmpt[ccc].tcy1 - Tile->Cmpt[ccc].tcy0), NL);
			WaveAdrCal(&Wave[ccc], &Image[ccc], EECod, FlagFlag);
			Wave[ccc].ll_d[Wave->NL].data = Image[ccc].data;
			Wave[ccc].ll_d[Wave->NL].type = Image[ccc].type;
		}
		if (EXIT_SUCCESS != TileEnc(J2kParam, Wave, Image, Stream, Codec, ttt, ERgn)) {
			printf("[J2kEnc]::TileEnc(ttt=%d) error \n", ttt);
			return EXIT_FAILURE;
		}

		if (EXIT_FAILURE == (numPackets = PacketCreate(Tile, ECod, 1))) {
			printf("[J2kEnc]::PacketCreate error. \n");
			return EXIT_FAILURE;
		}
		maxDecompLev = 0;
		maxnumPasses = 0;
		for (ccc = 0; ccc < Codec->Siz->Csiz; ccc++) {
			maxDecompLev =
			    (maxDecompLev > Codec->ECod->EECod[ccc * Codec->ECod->numTiles + Tile->T].DecompLev)
			        ? maxDecompLev
			        : Codec->ECod->EECod[ccc * Codec->ECod->numTiles + Tile->T].DecompLev;
			maxnumPasses =
			    (maxnumPasses > Codec->ECod->EECod[ccc * Codec->ECod->numTiles + Tile->T].numLayer)
			        ? maxnumPasses
			        : Codec->ECod->EECod[ccc * Codec->ECod->numTiles + Tile->T].numLayer;
		}
		maxDecompLev++;
		maxnumPrc = 0;
		for (ccc = 0; ccc < Codec->Siz->Csiz; ccc++) {
			for (rrr = 0; rrr <= Codec->ECod->EECod[ccc * Codec->ECod->numTiles + Tile->T].DecompLev;
			     rrr++) {
				maxnumPrc = (maxnumPrc > Tile->Cmpt[ccc].EEPrc[rrr].numPrc)
				                ? maxnumPrc
				                : Tile->Cmpt[ccc].EEPrc[rrr].numPrc;
			}
		}

		if (nullptr == (Cppp = new byte2[Codec->Siz->Csiz * maxDecompLev])) {
			printf("[J2kEnc]:: Cppp malloc error.\n");
			return EXIT_FAILURE;
		}
		memset(Cppp, 0, sizeof(byte2) * Codec->Siz->Csiz * maxDecompLev);
		numberOfPackets = Codec->Siz->Csiz * maxDecompLev * maxnumPasses * maxnumPrc;
		if (nullptr == (flagD = new uchar[numberOfPackets])) {
			printf("[J2kEnc]:: flagD malloc error. numberOfPackets=%d\n", numberOfPackets);
			return EXIT_FAILURE;
		}
		memset(flagD, 0, sizeof(uchar) * numberOfPackets);
		Tile->PacketsCount = 0;

		if (EXIT_FAILURE == ProgCnt(Codec, Tile, 0, Wave, ECod, Cppp, flagD, 0)) {
			printf("[J2kEnc]::ProgCnt error. (ttt=%d)\n", ttt);
			return EXIT_FAILURE;
		}
		TagtreeCreate2(Tile, 0, numPackets);
		//		printf( "[J2kEnc] TagtreeCreate2 complete!\n" );
	}

	if (J2kParam->RateControl == RATE_CONTROL_RateValue) {
		byteLimit = 0;
		CCsiz     = 0;
		for (ccc = 0; ccc < Siz->Csiz; ccc++) {
			byteLimit += Siz->Xsiz * Siz->Ysiz * (Siz->Ssiz[ccc] + 1);
		}
		byteLimit = (byte4) ((float) byteLimit * J2kParam->rate);
		byteLimit /= 8;
		MarkerByte = MarkerSizeCar(Codec->Siz, Codec->ECod, Codec->EQcd);
		PIbyte     = PIbyteCar(Codec->Siz, Codec->ECod);
		Truncate2(Codec, byteLimit, MarkerByte, PIbyte);
	} else if (J2kParam->RateControl == RATE_CONTROL_ByteLimit) {
		MarkerByte = MarkerSizeCar(Codec->Siz, Codec->ECod, Codec->EQcd);
		PIbyte     = PIbyteCar(Codec->Siz, Codec->ECod);
		Truncate2(Codec, J2kParam->byteLimit, MarkerByte, PIbyte);
	}

	//	printf( "[J2kEnc] PacketInfoEnc Start\n" );
	for (ttt = 0; ttt < Codec->numTiles; ttt++) {
		Tile = &(Codec->Tile[ttt]);
		for (ppp = 0; ppp < Tile->numPackets; ppp++) {
			if (EXIT_FAILURE == PacketInfoEnc(&(Tile->Packet[ppp]))) {
				printf("[J2kEnc] :: PacketInfoEnc error. Tile Number=%d. Packet Number=%d.\n", ttt, ppp);
				return EXIT_FAILURE;
			}
		}
	}
	//	printf( "[J2kEnc] PacketInfoEnc complete!\n" );

	//	printf( "[J2kEnc] MainHeaderWrite Start\n" );
	str = MainHeaderWrite(ECod, EQcd, ERgn, J2kParam->ROI, Siz, str);
	//	printf( "[J2kEnc] MainHeaderWrite complete!\n" );

	//	printf( "[J2kEnc] TileMarkerWrite start\n" );
	Siz  = Codec->Siz;
	ECod = Codec->ECod;
	EQcd = Codec->EQcd;
	for (ttt = 0; ttt < Codec->numTiles; ttt++) {
		Tile = &(Codec->Tile[ttt]);
		//		printf( "Tile=%x\n", Tile );
		str = TileMarkerWrite(Siz, ECod, EQcd, Tile, str);
	}
	//	printf( "[J2kEnc] TileMarkerWrite complete!\n" );

	str = EocMarWrite(str);
	//	printf( "[J2kEnc] EocWrite complete!\n" );

	return EXIT_SUCCESS;
}

byte4 J2K_EncMain(struct Image_s *InputImage, struct StreamChain_s *str, struct J2kParam_s *J2kParam) {
	struct Codec_s *Codec;
	byte4 fwidth, fheight;
	byte4 Cbwidth, Cbheight;
	ubyte2 numCmpts;

	numCmpts = (ubyte2) InputImage->row1step;
	if (nullptr == (Codec = CreateCodec(J2kParam))) {
		printf("[J2K_EncMain]:: struct Codec_s* malloc error\n");
		return EXIT_FAILURE;
	}

	if (J2kParam->Tile_x == 0) J2kParam->Tile_x = InputImage->width;  // bmp->BmpInfo->xpixel;
	if (J2kParam->Tile_y == 0) J2kParam->Tile_y = InputImage->height; // bmp->BmpInfo->ypixel;

	// Siz
	if (nullptr == (Codec->Siz = SizEnc(InputImage->width, InputImage->height, numCmpts, J2kParam))) {
		printf("[J2K_EncMain]::SizEnc Codec->Siz error\n");
		return EXIT_FAILURE;
	}
	if (EXIT_FAILURE == SizSsiz(Codec->Siz, numCmpts, InputImage)) {
		printf("[J2K_EncMain]::SizSsiz error. This codec is only for under 16bits Image data. Image data "
		       "max value is %d\n",
		       InputImage->MaxValue);
		return EXIT_FAILURE;
	}

	// ECod
	if (nullptr == (Codec->ECod = ECodCreate(Codec->Siz, J2kParam))) {
		printf("[J2K_EncMain]::ECodCreate Codec->ECod error\n");
		return EXIT_FAILURE;
	}

	// EQcd
	if (nullptr == (Codec->EQcd = EQCreate(Codec->ECod, Codec->Siz, ENC, J2kParam))) {
		printf("[J2K_EncMain]::EQCreate Codec->EQcd error\n");
		return EXIT_FAILURE;
	}

	Codec->XTSiz    = Codec->Siz->XTsiz;
	Codec->YTSiz    = Codec->Siz->YTsiz;
	Codec->numTiles = Codec->Siz->numTiles;

	if (nullptr == (Codec->Tile = TileCreate(Codec, 1))) {
		printf("[J2K_EncMain]::TileCreate error.\n");
		return EXIT_FAILURE;
	}

	Cbwidth  = 4 << Codec->ECod->EECod[0].CbW;
	Cbheight = 4 << Codec->ECod->EECod[0].CbH;
	fwidth   = Cbwidth + 2;
	fheight  = Cbheight + 8;
	if (!J2kParam->HTJ2K) {
		if (nullptr
		    == (Codec->CbData = ImageCreate(nullptr, Cbheight, Cbwidth, 0, Cbheight, 0, Cbwidth, BYTE4))) {
			printf("[J2K_EncMain]:: ImageCreate Codec->CbData malloc error\n");
			return EXIT_FAILURE;
		}
		Codec->CbData->row1step = Codec->CbData->col1step;
		Codec->CbData->col1step = 1;
		if (nullptr
		    == (Codec->flag = ImageCreate(nullptr, fheight, fwidth, 0, fheight, 0, fwidth, BYTE2))) {
			printf("[J2K_EncMain]:: ImageCreate Codec->flag malloc error\n");
			return EXIT_FAILURE;
		}
		Codec->flag->row1step = Codec->flag->col1step;
		Codec->flag->col1step = 1;
		Codec->Image_mex      = nullptr;
		Codec->Image_msbits   = nullptr;
		Codec->Image_qinf     = nullptr;
		Codec->Image_qmex     = nullptr;
		Codec->Image_sigma    = nullptr;
		Codec->codecf         = nullptr;
		Codec->codecR         = nullptr;
		Codec->codecM         = nullptr;
		Codec->Image_epciron1 = nullptr;
		Codec->Image_epcironK = nullptr;
	} else {
		if (nullptr
		    == (Codec->CbData = ImageCreate(nullptr, Cbwidth * 2, Cbheight / 2, 0, Cbwidth * 2, 0,
		                                    Cbheight / 2, BYTE4))) {
			printf("[J2K_EncMain]:: Codec->CbData ImageCreate Error. \n");
			return EXIT_FAILURE;
		}
		Codec->flag = nullptr;
		if (nullptr
		    == (Codec->Image_qinf = ImageCreate(nullptr, ((Cbwidth + 1) / 2), ((Cbheight + 1) / 2), 0,
		                                        ((Cbwidth + 1) / 2), 0, ((Cbheight + 1) / 2), BYTE2))) {
			printf("[J2K_EncMain]:: Codec->Image_qinf ImageCreate Error. \n");
			return EXIT_FAILURE;
		}

		if (nullptr
		    == (Codec->Image_qmex = ImageCreate(nullptr, ((Cbwidth + 1) / 2), ((Cbheight + 1) / 2), 0,
		                                        ((Cbwidth + 1) / 2), 0, ((Cbheight + 1) / 2), CHAR))) {
			printf("[J2K_EncMain]::  Codec->Image_qmex ImageCreate Error. \n");
			return EXIT_FAILURE;
		}
		if (nullptr
		    == (Codec->Image_msbits = ImageCreate(nullptr, ((Cbwidth + 1) / 2), ((Cbheight + 1) / 2), 0,
		                                          ((Cbwidth + 1) / 2), 0, ((Cbheight + 1) / 2), CHAR))) {
			printf("[J2K_EncMain]::  Codec->Image_msbits ImageCreate Error. \n");
			return EXIT_FAILURE;
		}
		if (nullptr
		    == (Codec->Image_mex = ImageCreate(nullptr, Cbwidth * 2 + 16, (Cbheight + 1) / 2 + 2, 0,
		                                       Cbwidth * 2 + 16, 0, (Cbheight + 1) / 2 + 2, CHAR))) {
			printf("[J2K_EncMain]::  Codec->Image_mex ImageCreate Error. \n");
			return EXIT_FAILURE;
		}
		if (nullptr
		    == (Codec->Image_sigma = ImageCreate(nullptr, (Cbwidth + 3) / 4, (Cbheight + 1) / 2, 0,
		                                         (Cbwidth + 3) / 4, 0, (Cbheight + 1) / 2, BYTE2))) {
			printf("[J2K_EncMain]::  Codec->Image_sigma ImageCreate Error. \n");
			return EXIT_FAILURE;
		}
		if (nullptr
		    == (Codec->Image_epcironK = ImageCreate(nullptr, ((Cbwidth + 1) / 2), ((Cbheight + 1) / 2), 0,
		                                            ((Cbwidth + 1) / 2), 0, ((Cbheight + 1) / 2), CHAR))) {
			printf("[J2K_EncMain]::  Codec->Image_epcironK ImageCreate Error. \n");
			return EXIT_FAILURE;
		}
		if (nullptr
		    == (Codec->Image_epciron1 = ImageCreate(nullptr, ((Cbwidth + 1) / 2), ((Cbheight + 1) / 2), 0,
		                                            ((Cbwidth + 1) / 2), 0, ((Cbheight + 1) / 2), CHAR))) {
			printf("[J2K_EncMain]::  Codec->Image_epciron1 ImageCreate Error. \n");
			return EXIT_FAILURE;
		}
		if (nullptr
		    == (Codec->Image_refine = ImageCreate(nullptr, ceil2(Cbwidth, 2), ceil2(Cbheight, 2), 0,
		                                          ceil2(Cbwidth, 2), 0, ceil2(Cbheight, 2), CHAR))) {
			printf("[J2K_EncMain]::  Codec->Image_refine ImageCreate Error. \n");
			return EXIT_FAILURE;
		}
		if (nullptr == (Codec->codecf = new struct fRawCodec_s)) {
			printf("[J2K_EncMain]::  Codec->codecf malloc error. \n");
			return EXIT_FAILURE;
		}
		if (nullptr == (Codec->codecR = RRawCodecCreate())) {
			printf("[J2K_EncMain]::  Codec->codecR malloc error. \n");
			return EXIT_FAILURE;
		}
		if (nullptr == (Codec->codecM = MelCodecCreate())) {
			printf("[J2K_EncMain]::  Codec->codecM malloc error. \n");
			return EXIT_FAILURE;
		}
	}

	if (EXIT_SUCCESS != J2kEnc(InputImage, Codec, str, J2kParam)) {
		printf("[J2K_EncMain::J2kEnc] error\n");
		return EXIT_FAILURE;
	}
	// malloc free
	if (J2kParam->HTJ2K) {
		//		printf( "[J2K_EncMain] Destroy start!\n" );
		MelCodecDestroy(Codec->codecM);
		//		printf( "[J2K_EncMain] MelCodecDestroy complete!\n" );
		RRawCodecDestroy(Codec->codecR);
		//		printf( "[J2K_EncMain] RRawCodecDestroy complete!\n" );
		delete Codec->codecf;
		//		printf( "[J2K_EncMain] Codecf Destroy complete!\n" );
		ImageDestory(Codec->Image_refine);
		//		printf( "[J2K_EncMain]Codec->Image_refine Destroy complete!\n" );
		ImageDestory(Codec->Image_sigma);
		//		printf( "[J2K_EncMain]Codec->Image_sigma Destroy complete!\n" );
		ImageDestory(Codec->Image_qmex);
		//		printf( "[J2K_EncMain]Codec->Image_qmex Destroy complete!\n" );
		ImageDestory(Codec->Image_qinf);
		//		printf( "[J2K_EncMain]Codec->Image_qinf Destroy complete!\n" );
		ImageDestory(Codec->Image_msbits);
		//		printf( "[J2K_EncMain]Codec->Image_msbits Destroy complete!\n" );
		ImageDestory(Codec->Image_mex);
		//		printf( "[J2K_EncMain]Codec->Image_mex Destroy complete!\n" );
		ImageDestory(Codec->CbData);
		//		printf( "[J2K_EncMain]Codec->CbData Destroy complete!\n" );
	} else {
		ImageDestory(Codec->flag);
		ImageDestory(Codec->CbData);
	}
	//	printf("[J2K_EncMain] complete!\n" );
	return EXIT_SUCCESS;
}

//[Func 34]
struct StreamChain_s *MainHeaderWrite(struct ECod_s *ECod, struct EQcd_s *EQcd, struct ERgn_s *ERgn,
                                      char onROI, struct Siz_s *Siz, struct StreamChain_s *s) {
	ubyte2 ccc;

	if (nullptr == (s = Stream2ByteWrite(s, SOC, BUF_LENGTH, TT_BIG_ENDIAN))) {
		printf("[MainHeaderWrite]:: SOC write error.\n");
		return nullptr;
	}
	//	printf( "1\n");
	if (nullptr == (s = SizMarWrite(s, Siz))) {
		printf("[MainHeaderWrite]:: SizMarWrite error.\n");
		return nullptr;
	}
	//	printf( "2\n");

	if (nullptr == (s = CodMarWrite(s, ECod, NULL))) {
		printf("[MainHeaderWrite]:: CodMarWrite error.\n");
		return nullptr;
	}
	//	printf( "3\n");

	for (ccc = 1; ccc < ECod->numCompts; ccc++) {
		if (nullptr == (s = CocMarWrite(s, ECod, ccc, NULL))) {
			printf("[MainHeaderWrite]:: CocMarWrite error.ccc=%d\n", ccc);
			return nullptr;
		}
	}
	//	printf( "4\n");
	if (nullptr == (s = QcdMarWrite(s, ECod, EQcd, NULL))) {
		printf("[MainHeaderWrite]:: QcdMarWrite error\n");
		return nullptr;
	}
	//	printf( "5\n");
	for (ccc = 1; ccc < ECod->numCompts; ccc++) {
		if (nullptr == (s = QccMarWrite(s, ECod, EQcd, NULL, ccc))) {
			printf("[MainHeaderWrite]:: QccMarWrite error.ccc=%d\n", ccc);
			return nullptr;
		}
	}
	//	printf( "6\n");
	//	printf( "onROI=%d\n", onROI );
	if (onROI) {
		for (ccc = 0; ccc < ECod->numCompts; ccc++) {
			if (nullptr == (s = RgnMarWrite(s, ERgn, ccc))) {
				printf("[MainHeaderWrite]:: RgnMarWrite error.ccc=%d\n", ccc);
				return nullptr;
			}
		}
	}
	//	printf("7\n" );

	return s;
}

byte4 PsotCal2(struct Tile_s *t, struct EECod_s *EECod, byte4 TileMarkerByte) {
	byte4 Psot = 0;
	struct Packet_s *Ps, *Pe;
	struct Prc_s *Prcs, *Prce;
	struct Cblk_s *Cbs, *Cbe;

	Ps = &t->Packet[0];
	Pe = &t->Packet[t->numPackets];
	for (; Ps != Pe; ++Ps) {
		if (EECod->SopOn) Psot += 6;
		Psot += StreamChainCounter(Ps->s);
		if (EECod->EphOn) Psot += 2;
		Prcs = &Ps->EPrc->Prc[0];
		Prce = &Ps->EPrc->Prc[Ps->numband];
		for (; Prcs != Prce; ++Prcs) {
			Cbs = &Prcs->Cblk[0];
			Cbe = &Prcs->Cblk[Prcs->numCblk];
			for (; Cbs != Cbe; ++Cbs) {
				Psot += Cbs->length;
			}
		}
	}
	Psot += TileMarkerByte;
	return Psot;
}

struct StreamChain_s *T1T2DataWrite(struct Tile_s *t, struct StreamChain_s *s, struct EECod_s *EECod) {
	struct Packet_s *Ps, *Pe;
	struct Prc_s *Prcs, *Prce;
	struct Cblk_s *Cbs, *Cbe;
	byte4 index;

	//	printf("[T1T2DataWrite] start\n" );

	Ps = &t->Packet[0];
	Pe = &t->Packet[t->numPackets];

	for (index = 0; Ps != Pe; ++Ps, index++) {
		if (EECod->SopOn) s = SopMarWrite(s, index);

		s = PacketInfoBind2FileStream(s, Ps->s);

		if (EECod->EphOn) s = Stream2ByteWrite(s, EPH, 2, TT_BIG_ENDIAN);

		Prcs = &Ps->EPrc->Prc[0];
		Prce = &Ps->EPrc->Prc[Ps->numband];
		for (; Prcs != Prce; ++Prcs) {
			Cbs = &Prcs->Cblk[0];
			Cbe = &Prcs->Cblk[Prcs->numCblk];
			for (; Cbs != Cbe; ++Cbs) {
				if (Cbs->NoStream == 0) {
					Cbs->s = StreamChainTruncate(Cbs->s, Cbs->length);
					s      = StreamChainBind(s, Cbs->s);
				}
			}
		}
	}
	//	printf("[T1T2DataWrite] complete!\n" );

	return s;
}

//[Func 20]
byte4 TileAdrCal(struct Tile_s *Tile, struct Siz_s *siz, byte4 index) {
	byte4 index_x, index_y, xnumTile;
	ubyte2 ccc;

	xnumTile = siz->numXTiles; // ceil2((siz->Xsiz+siz->XOsiz-siz->XTOsiz), siz->XTsiz);
	if (xnumTile <= 0) {
		printf("[TileAdrCal]:: xnumTile is needed positive value. xnumTile=%d ynumTile=%d\n",
		       siz->numXTiles, siz->numYTiles);
		return EXIT_FAILURE;
	}
	index_y = index / xnumTile;
	index_x = index % xnumTile;

	Tile->T = index;

	Tile->tx0 = siz->XTOsiz + index_x * siz->XTsiz;
	Tile->tx1 = Tile->tx0 + siz->XTsiz;
	if (Tile->tx0 < siz->XOsiz) Tile->tx0 = siz->XOsiz;

	if (Tile->tx1 > siz->Xsiz) Tile->tx1 = siz->Xsiz;

	Tile->ty0 = siz->YTOsiz + index_y * siz->YTsiz;
	Tile->ty1 = Tile->ty0 + siz->YTsiz;
	if (Tile->ty0 < siz->YOsiz) Tile->ty0 = siz->YOsiz;

	if (Tile->ty1 > siz->Ysiz) Tile->ty1 = siz->Ysiz;

	Tile->TPsot[0]  = 0;
	Tile->numCompts = siz->Csiz;
	Tile->TNsot     = 0;
	for (ccc = 0; ccc < Tile->numCompts; ccc++) {
		Tile->Cmpt[ccc].C         = ccc;
		Tile->Cmpt[ccc].firstTile = 0;
		Tile->Cmpt[ccc].tcx0      = ceil2(Tile->tx0, siz->XRsiz[ccc]);
		Tile->Cmpt[ccc].tcx1      = ceil2(Tile->tx1, siz->XRsiz[ccc]);
		Tile->Cmpt[ccc].tcy0      = ceil2(Tile->ty0, siz->YRsiz[ccc]);
		Tile->Cmpt[ccc].tcy1      = ceil2(Tile->ty1, siz->YRsiz[ccc]);
	}
	return EXIT_SUCCESS;
}

byte4 MarkerSizeCar(struct Siz_s *Siz, struct ECod_s *ECod, struct EQcd_s *EQcd) {
	byte4 mainSoc_num, mainSiz_num, mainQcd_num, mainQcc_num, mainCod_num, mainCoc_num,
	    tileQcd_num = 0, tileQcc_num = 0, Sop_num = 0, Eph_num = 0, Sot_num, Sod_num;
	byte4 byte_length = 0;
	byte4 ccc, ttt, bbb, numBands;
	char flag = 0;
	char *e;
	ubyte2 *myu;

	mainSoc_num = 1;
	mainSiz_num = 1;
	mainQcd_num = 1;
	mainQcc_num = Siz->Csiz - 1;
	mainCod_num = 1;
	mainCoc_num = Siz->Csiz - 1;

	Sot_num = Siz->numTiles;
	Sod_num = Siz->numTiles;

	for (ttt = 0; ttt < (signed) Siz->numTiles; ttt++) {
		for (ccc = 0; ccc < Siz->Csiz; ccc++) {
			Sop_num += ECod->EECod[ccc * Siz->numTiles + ttt].DecompLev + 1;
		}
	}
	Eph_num = Sop_num;

	e   = EQcd->EEQcd[0].e;
	myu = EQcd->EEQcd[0].myu;
	for (ttt = 1; ttt < (signed) Siz->numTiles; ttt++) {
		numBands = (ECod->EECod[ttt].DecompLev * 3 + 1);
		for (bbb = 0, flag = 0; bbb < numBands; bbb++) {
			if ((EQcd->EEQcd[ttt].e[bbb] != e[bbb]) || (EQcd->EEQcd[ttt].myu[bbb] != myu[bbb])) {
				flag = 1;
			}
		}
		if (flag) tileQcd_num++;
	}

	for (ttt = 0; ttt < (signed) Siz->numTiles; ttt++) {
		e   = EQcd->EEQcd[ttt].e;
		myu = EQcd->EEQcd[ttt].myu;
		for (ccc = 1, flag = 0; ccc < Siz->Csiz; ccc++) {
			numBands = (ECod->EECod[ccc * Siz->numTiles + ttt].DecompLev * 3 + 1);
			for (bbb = 0; bbb < numBands; bbb++) {
				if ((EQcd->EEQcd[ccc * Siz->numTiles + ttt].e[bbb] != e[bbb])
				    || (EQcd->EEQcd[ccc * Siz->numTiles + ttt].myu[bbb] != myu[bbb])) {
					flag = 1;
				}
			}
			if (flag) tileQcc_num++;
		}
	}

	byte_length = byte_length + mainSoc_num * 2;
	byte_length = byte_length + mainSiz_num * (40 + Siz->Csiz * 3);
	byte_length = byte_length + mainCod_num * 14;
	byte_length = byte_length + mainCoc_num * 15;
	byte_length = byte_length + mainQcd_num * ((5 + 6 * ECod->EECod[0].DecompLev) + 2);
	byte_length = byte_length + mainQcc_num * ((6 + 6 * ECod->EECod[0].DecompLev) + 2);
	byte_length = byte_length + Sot_num * 12;
	byte_length = byte_length + Sod_num * 2;
	if (ECod->EECod[0].SopOn) byte_length = byte_length + (Sop_num * 6);
	if (ECod->EECod[0].EphOn) byte_length = byte_length + Eph_num * 2;

	byte_length = byte_length + 2; // EOC

	return byte_length;
}

byte4 PIbyteCar(struct Siz_s *Siz, struct ECod_s *ECod) {
	byte4 PacketInfo_num;
	byte4 PIbyte_length = 0;
	byte4 ccc, ttt;

	PacketInfo_num = 0;
	for (ttt = 0; ttt < (signed) Siz->numTiles; ttt++) {
		for (ccc = 0; ccc < Siz->Csiz; ccc++) {
			PacketInfo_num += ECod->EECod[ccc * Siz->numTiles + ttt].DecompLev + 1;
		}
	}

	PIbyte_length = PacketInfo_num * 5;

	return PIbyte_length;
}

struct StreamChain_s *TileMarkerWrite(struct Siz_s *Siz, struct ECod_s *ECod, struct EQcd_s *EQcd,
                                      struct Tile_s *Tile, struct StreamChain_s *s) {
	byte2 ccc;
	byte4 Psot;
	byte4 TileMarkerByte;

	//	printf("TileMarkerWrite 1\n" );
	TileMarkerByte = 0;
	ccc            = 0;
	if (EXIT_SUCCESS != CodCompare(&ECod->EECod[0], &ECod->EECod[ccc * ECod->numTiles + Tile->T]))
		TileMarkerByte += CodMarByteCalculate(ECod, Tile->T);

	//	printf("TileMarkerWrite 2\n" );
	ccc = 0;
	if (EXIT_SUCCESS
	    != QcdCompare(&EQcd->EEQcd[0], &EQcd->EEQcd[ccc * ECod->numTiles + Tile->T],
	                  &ECod->EECod[ccc * ECod->numTiles + Tile->T]))
		TileMarkerByte += QcdMarByteCalculate(ECod, EQcd, Tile->T);

	//	printf("TileMarkerWrite 3\n" );
	for (ccc = 1; ccc < Siz->Csiz; ccc++) {
		if (EXIT_SUCCESS != CodCompare(&ECod->EECod[0], &ECod->EECod[ccc * ECod->numTiles + Tile->T])) {
			TileMarkerByte += CocMarByteCalculate(ECod, Siz, Tile->T, ccc);
		}
	}
	//	printf("TileMarkerWrite 4\n" );
	for (ccc = 1; ccc < Siz->Csiz; ccc++)
		TileMarkerByte += QccMarByteCalculate(ECod, EQcd, Tile->T, ccc);

	//	printf("TileMarkerWrite 5\n" );
	Psot = PsotCal2(Tile, &ECod->EECod[Tile->T], TileMarkerByte);
	s    = SotMarWrite(s, Tile->T, (Psot + 14), Tile->TPsot[0], 1);

	//	printf("TileMarkerWrite 6\n" );
	ccc = 0;
	if (EXIT_SUCCESS != CodCompare(&ECod->EECod[0], &ECod->EECod[ccc * ECod->numTiles + Tile->T]))
		s = CodMarWrite(s, ECod, Tile->T);

	//	printf("TileMarkerWrite 7\n" );
	for (ccc = 1; ccc < Siz->Csiz; ccc++) {
		if (EXIT_SUCCESS != CodCompare(&ECod->EECod[0], &ECod->EECod[ccc * ECod->numTiles + Tile->T]))
			s = CocMarWrite(s, ECod, ccc, Tile->T);
	}

	//	printf("TileMarkerWrite 8\n" );
	ccc = 0;
	if (EXIT_SUCCESS
	    != QcdCompare(&EQcd->EEQcd[0], &EQcd->EEQcd[ccc * ECod->numTiles + Tile->T],
	                  &ECod->EECod[ccc * ECod->numTiles + Tile->T]))
		s = QcdMarWrite(s, ECod, EQcd, Tile->T);

	//	printf("TileMarkerWrite 9\n" );
	for (ccc = 1; ccc < Siz->Csiz; ccc++)
		s = QccMarWrite(s, ECod, EQcd, Tile->T, ccc);

	//	printf("TileMarkerWrite 10\n" );
	s = Stream2ByteWrite(s, SOD, BUF_LENGTH, TT_BIG_ENDIAN);

	//	printf("TileMarkerWrite 11 Tile=%x EECod=%x\n", Tile,  (&ECod->EECod[Tile->T]) );
	s = T1T2DataWrite(Tile, s, &ECod->EECod[Tile->T]);
	//	printf("TileMarkerWrite 12\n" );

	return s;
}
