/*
Copyright (c) 2000-2021, ICT-Link
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the HTJ2K project.
*/

#include <stdio.h>
#include <stdlib.h>
#include "j2k2.h"
#include "ctx_enc.h"
#include "t1codec.h"
#include "MqCodec.h"

#if Cplus
byte4 enc_clnpass(uchar *CX_, ubyte2 *flag, struct Image_s *CbData, uchar *SIG_TABLE, uchar *SIGN3,
                  byte4 width, byte4 height, byte4 row1stepf, byte4 row1stepCb, uchar bittop,
                  byte4 bitmask) {
	ubyte2 *F0, *F1, *F2;
	byte4 *d2_, *d_;
	byte4 i, j, jj, temp_height;
	byte4 fj, fi;
	char insig_run;
	char runlength;
	uchar SignCX;
	uchar S;
	char d0, d1, d2, d3;
	char D;
	char negneg;
	byte4 P;
	byte4 one;

	d2_ = (byte4 *) CbData->data;

	row1stepf /= 2;
	row1stepCb /= 4;
	one = 1 << bittop;
	P   = 0;
	for (jj = 0, fj = 4; jj < height; jj += 4, fj += 4) {
		temp_height = 4 < (CbData->tby1 - jj) ? 4 : (CbData->tby1 - jj);
		for (i = 0, fi = 1; i < width; i++, fi++) {
			F0 = &flag[fj - 1 + (fi - 1) * row1stepf];
			F1 = &flag[fj - 1 + fi * row1stepf];
			F2 = &flag[fj - 1 + (fi + 1) * row1stepf];
			d_ = &d2_[jj + i * row1stepCb];
			insig_run =
			    (char) ((F1[1] & SIG) | (F1[2] & SIG) | (F1[3] & SIG) | (F1[4] & SIG) | (F1[1] & NEIGHBOR)
			            | (F1[2] & NEIGHBOR) | (F1[3] & NEIGHBOR) | (F1[4] & NEIGHBOR));
			if ((!insig_run) && (temp_height >= 4)) {
				d3        = (char) ((d_[0] & one) >> bittop);
				d2        = (char) ((d_[1] & one) >> bittop);
				d1        = (char) ((d_[2] & one) >> bittop);
				d0        = (char) ((d_[3] & one) >> bittop);
				S         = (uchar) ((d3 << 3) | (d2 << 2) | (d1 << 1) | d0);
				runlength = run_table[S];
				if (runlength != 4) {
					CX_[P] = (uchar) (CTXRUN | 0x80);
					P++;
					CX_[P] = (uchar) (CTXUNI | (((runlength & 0x02) >> 1) << 7));
					P++;
					CX_[P] = (uchar) (CTXUNI | ((runlength & 0x01) << 7));
					P++;
					S      = (uchar) ((F1[runlength + 1] >> 4) & 0xff);
					SignCX = SIGN3[S];
					CX_[P] = (uchar) (SignCX & 0x0f);
					if (d_[runlength] & NEGATIVE_BIT) {
						negneg = 1;
						CX_[P] |= (uchar) ((SignCX & 0x80) ^ 0x80);
						P++;
					} else {
						negneg = 0;
						CX_[P] |= ((SignCX & 0x80) ^ 0x00);
						P++;
					}
					F0[runlength] |= 0x4002;
					F1[runlength] |= (0x0802 | (negneg * 0x80));
					F2[runlength] |= 0x8002;
					F0[runlength + 1] |= (0x0202 | (negneg * 0x20));
					F1[runlength + 1] |= 0x0001;
					F2[runlength + 1] |= (0x0102 | (negneg * 0x10));
					F0[runlength + 2] |= 0x2002;
					F1[runlength + 2] |= (0x0402 | (negneg * 0x40));
					F2[runlength + 2] |= 0x1002;
					d_[runlength] &= bitmask;
				} else {
					CX_[P] = CTXRUN;
					P++;
				}
				j = runlength + 1;
			} else
				j = 0;
			for (; j < temp_height; j++) {
				if (!(F1[j + 1] & VISIT)) {
					S      = (uchar) ((F1[j + 1] >> 8));
					D      = (char) ((d_[j] & one) >> bittop);
					CX_[P] = (uchar) (SIG_TABLE[S] | (D << 7));
					P++;
					if (D) {
						S      = (uchar) ((F1[j + 1] >> 4) & 0xff);
						SignCX = SIGN[S];
						if (d_[j] & NEGATIVE_BIT) {
							negneg = 1;
							CX_[P] = (uchar) ((SignCX & 0x0f) | ((SignCX & 0x80) ^ 0x80));
							P++;
						} else {
							negneg = 0;
							CX_[P] = (uchar) ((SignCX & 0x0f) | ((SignCX & 0x80) ^ 0x00));
							P++;
						}
						F0[j] |= 0x4002;
						F1[j] |= (0x0802 | (negneg * 0x80));
						F2[j] |= 0x8002;
						F0[j + 1] |= (0x0202 | (negneg * 0x20));
						F1[j + 1] |= SIG;
						F2[j + 1] |= (0x0102 | (negneg * 0x10));
						F0[j + 2] |= 0x2002;
						F1[j + 2] |= (0x0402 | (negneg * 0x40));
						F2[j + 2] |= 0x1002;
						d_[j] &= bitmask;
					}
				}
			}
		}
	}
#	if 0
	byte4	ddd;
	printf( "[CLN] %d\n", bittop );
	for( ddd=0 ; ddd<P ; ddd++ )
		printf( "%d\n", CX_[ddd] );
#	endif
	return P;
}

struct StreamChain_s *dec_clnpass(struct Cblk_s *Cblk, struct Image_s *CbData, char bitpos,
                                  uchar *ctx_table, struct mqdec_s *dec, struct StreamChain_s *s,
                                  uchar SegmentSymbol, uchar FlagFlag) {
	ubyte2 *Flag;
	byte4 *Data;
	ubyte2 *F0, *F1, *F2;
	byte4 j, ii, jj, temp_height;
	byte4 fj, fi;
	byte4 col1stepf, row1stepf;
	byte4 col1stepCb, row1stepCb;
	char insig_run;
	char runlength;
	uchar SignCX;
	uchar CX;
	uchar S;
	byte4 *d_;
	uchar D, DD;
	char negneg;
	byte4 one;

	col1stepf  = Cblk->flags->col1step;
	row1stepf  = Cblk->flags->row1step;
	col1stepCb = CbData->col1step;
	row1stepCb = CbData->row1step;
	one        = 1 << bitpos;

	Data = (byte4 *) &CbData->data[0];
	Flag = (ubyte2 *) &Cblk->flags->data[0];
	for (jj = CbData->tby0, fj = 4; jj < CbData->tby1; jj += 4, fj += 4) {
		temp_height = 4 < (CbData->tby1 - jj) ? 4 : (CbData->tby1 - jj);
		for (ii = CbData->tbx0, fi = 1; ii < CbData->tbx1; ii++, fi++) {
			F0 = &Flag[(fj - 1) * col1stepf + (fi - 1) * row1stepf]; // left
			F1 = &Flag[(fj - 1) * col1stepf + fi * row1stepf];       // myself
			F2 = &Flag[(fj - 1) * col1stepf + (fi + 1) * row1stepf]; // right
			d_ = &Data[jj * col1stepCb + ii * row1stepCb];
			insig_run =
			    (char) ((F1[1] & SIG) | (F1[2] & SIG) | (F1[3] & SIG) | (F1[4] & SIG) | (F1[1] & NEIGHBOR)
			            | (F1[2] & NEIGHBOR) | (F1[3] & NEIGHBOR) | (F1[4] & NEIGHBOR));
			if ((!insig_run) && (CbData->tby1 - jj >= 4)) {
				CX = CTXRUN2;
				s  = MQ_Dec(&D, CX, dec, s, QeIndexTable3);
				if (D) {
					CX        = CTXUNI2;
					s         = MQ_Dec(&D, CX, dec, s, QeIndexTable3);
					runlength = (char) (D * 2);
					CX        = CTXUNI2;
					s         = MQ_Dec(&D, CX, dec, s, QeIndexTable3);
					runlength |= D;
					F0 += runlength * col1stepf;
					F1 += runlength * col1stepf;
					F2 += runlength * col1stepf;
					d_ += runlength * col1stepCb;
					*d_ |= one;
					S      = (uchar) ((F1[1] >> 4) & 0xff);
					SignCX = SIGN2[S];
					CX     = (uchar) (SignCX & 0xfe);
					s      = MQ_Dec(&D, CX, dec, s, QeIndexTable3);
					if (D ^ (SignCX & 1)) {
						*d_ |= NEGATIVE_BIT;
						negneg = 1;
					} else
						negneg = 0;
					F0[0] |= 0x4002;
					F1[0] |= (0x0802 | (negneg * 0x80));
					F2[0] |= 0x8002;
					F0[1] |= (0x0202 | (negneg * 0x20));
					F1[1] |= 0x0001;
					F2[1] |= (0x0102 | (negneg * 0x10));
					F0[2] |= 0x2002;
					F1[2] |= (0x0402 | (negneg * 0x40));
					F2[2] |= 0x1002;
					++F0;
					++F1;
					++F2;
					d_ += col1stepCb;
					runlength++;
					j = runlength;
				} else {
					j = 4;
				}
			} else {
				j = 0;
			}
			for (; j < temp_height; j++, ++F0, ++F1, ++F2, d_ += col1stepCb) {
				if (!(F1[1] & VISIT)) {
					S  = (uchar) ((F1[1] >> 8));
					CX = ctx_table[S];
					s  = MQ_Dec(&D, CX, dec, s, QeIndexTable3);
					if (D) {
						*d_ |= one;
						S      = (uchar) ((F1[1] >> 4) & 0xff);
						SignCX = SIGN2[S];
						CX     = (uchar) (SignCX & 0xfe);
						s      = MQ_Dec(&D, CX, dec, s, QeIndexTable3);
						if (D ^ (SignCX & 1)) {
							*d_ |= NEGATIVE_BIT;
							negneg = 1;
						} else
							negneg = 0;
						F0[0] |= 0x4002;
						F1[0] |= (0x0802 | (negneg * 0x80));
						F2[0] |= 0x8002;
						F0[1] |= (0x0202 | (negneg * 0x20));
						F1[1] |= SIG;
						F2[1] |= (0x0102 | (negneg * 0x10));
						F0[2] |= 0x2002;
						F1[2] |= (0x0402 | (negneg * 0x40));
						F2[2] |= 0x1002;
					}
				}
			}
		}
	}
	if (SegmentSymbol) {
		CX = CTXUNI2;
		s  = MQ_Dec(&D, CX, dec, s, QeIndexTable3);
		DD = D;
		DD <<= 1;
		s = MQ_Dec(&D, CX, dec, s, QeIndexTable3);
		DD |= D;
		DD <<= 1;
		s = MQ_Dec(&D, CX, dec, s, QeIndexTable3);
		DD |= D;
		DD <<= 1;
		s = MQ_Dec(&D, CX, dec, s, QeIndexTable3);
		DD |= D;
		if (DD != 0xa) {
			printf("[dec_clnpass_vcausal]:: SegmentSymbol Error Symbol=%d\n", DD);
			return nullptr;
		}
	}
	return s;
}

struct StreamChain_s *dec_clnpass_vcausal(struct Cblk_s *Cblk, struct Image_s *CbData, char bitpos,
                                          uchar *ctx_table, struct mqdec_s *dec, struct StreamChain_s *s,
                                          uchar SegmentSymbol, uchar FlagFlag) {
	ubyte2 *Flag;
	byte4 *Data;
	ubyte2 *F0, *F1, *F2;
	byte4 j, ii, jj, temp_height;
	byte4 fj, fi;
	byte4 col1stepf, row1stepf;
	byte4 col1stepCb, row1stepCb;
	char insig_run;
	char runlength;
	uchar SignCX;
	uchar CX;
	uchar S;
	byte4 *d_;
	uchar D, DD;
	char negneg;
	byte4 one;

	col1stepf  = Cblk->flags->col1step;
	row1stepf  = Cblk->flags->row1step;
	col1stepCb = CbData->col1step;
	row1stepCb = CbData->row1step;
	one        = 1 << bitpos;

	Data = (byte4 *) &CbData->data[0];
	Flag = (ubyte2 *) &Cblk->flags->data[0];
	for (jj = CbData->tby0, fj = 4; jj < CbData->tby1; jj += 4, fj += 4) {
		temp_height = 4 < (CbData->tby1 - jj) ? 4 : (CbData->tby1 - jj);
		for (ii = CbData->tbx0, fi = 1; ii < CbData->tbx1; ii++, fi++) {
			F0 = &Flag[(fj - 1) * col1stepf + (fi - 1) * row1stepf]; // left
			F1 = &Flag[(fj - 1) * col1stepf + fi * row1stepf];       // myself
			F2 = &Flag[(fj - 1) * col1stepf + (fi + 1) * row1stepf]; // right
			d_ = &Data[jj * col1stepCb + ii * row1stepCb];

			insig_run = (char) ((F1[1] & SIG) | (F1[2] & SIG) | (F1[3] & SIG) | (F1[4] & SIG)
			                    | (F1[1] & NEIGHBOR) | (F1[2] & NEIGHBOR) | (F1[3] & NEIGHBOR)
			                    | (F1[4] & VISIT) | (F0[4] & SIG) | (F2[4] & SIG));
			if ((!insig_run) && (CbData->tby1 - jj >= 4)) {
				CX = CTXRUN2;
				s  = MQ_Dec(&D, CX, dec, s, QeIndexTable3);
				if (D) {
					CX        = CTXUNI2;
					s         = MQ_Dec(&D, CX, dec, s, QeIndexTable3);
					runlength = (char) (D * 2);
					CX        = CTXUNI2;
					s         = MQ_Dec(&D, CX, dec, s, QeIndexTable3);
					runlength |= D;
					F0 += runlength * col1stepf;
					F1 += runlength * col1stepf;
					F2 += runlength * col1stepf;
					d_ += runlength * col1stepCb;
					*d_ |= one;
					if (runlength == 3)
						S = (uchar) (((F1[1] & 0x377f) >> 4) & 0xff);
					else
						S = (uchar) ((F1[1] >> 4) & 0xff);
					SignCX = SIGN2[S];
					CX     = (uchar) (SignCX & 0xfe);
					s      = MQ_Dec(&D, CX, dec, s, QeIndexTable3);
					if (D ^ (SignCX & 1)) {
						*d_ |= NEGATIVE_BIT;
						negneg = 1;
					} else
						negneg = 0;
					F0[0] |= 0x4002;
					F1[0] |= (0x0802 | (negneg * 0x80));
					F2[0] |= 0x8002;
					F0[1] |= (0x0202 | (negneg * 0x20));
					F1[1] |= SIG;
					F2[1] |= (0x0102 | (negneg * 0x10));
					F0[2] |= 0x2002;
					F1[2] |= (0x0402 | (negneg * 0x40));
					F2[2] |= 0x1002;
					++F0;
					++F1;
					++F2;
					d_ += col1stepCb;
					runlength++;
					j = runlength;
				} else {
					j = 4;
				}
			} else {
				j = 0;
			}
			for (; j < temp_height - 1; j++, ++F0, ++F1, ++F2, d_ += col1stepCb) {
				if (!(F1[1] & VISIT)) {
					S  = (uchar) ((F1[1] >> 8));
					CX = ctx_table[S];
					s  = MQ_Dec(&D, CX, dec, s, QeIndexTable3);
					if (D) {
						*d_ |= one;
						S      = (uchar) ((F1[1] >> 4) & 0xff);
						SignCX = SIGN2[S];
						CX     = (uchar) (SignCX & 0xfe);
						s      = MQ_Dec(&D, CX, dec, s, QeIndexTable3);
						if (D ^ (SignCX & 1)) {
							*d_ |= NEGATIVE_BIT;
							negneg = 1;
						} else
							negneg = 0;
						F0[0] |= 0x4002;
						F1[0] |= (0x0802 | (negneg * 0x80));
						F2[0] |= 0x8002;
						F0[1] |= (0x0202 | (negneg * 0x20));
						F1[1] |= SIG;
						F2[1] |= (0x0102 | (negneg * 0x10));
						F0[2] |= 0x2002;
						F1[2] |= (0x0402 | (negneg * 0x40));
						F2[2] |= 0x1002;
					}
				}
			}
			for (; j < temp_height; j++, ++F0, ++F1, ++F2, d_ += col1stepCb) {
				if (!(F1[1] & VISIT)) {
					S  = (uchar) (((F1[1] & 0x377f) >> 8));
					CX = ctx_table[S];
					s  = MQ_Dec(&D, CX, dec, s, QeIndexTable3);
					if (D) {
						*d_ |= one;
						S      = (uchar) ((((F1[1] & 0x377f) >> 4) & 0xff));
						SignCX = SIGN2[S];
						CX     = (uchar) (SignCX & 0xfe);
						s      = MQ_Dec(&D, CX, dec, s, QeIndexTable3);
						if (D ^ (SignCX & 1)) {
							*d_ |= NEGATIVE_BIT;
							negneg = 1;
						} else
							negneg = 0;
						F0[0] |= 0x4002;
						F1[0] |= (0x0802 | (negneg * 0x80));
						F2[0] |= 0x8002;
						F0[1] |= (0x0202 | (negneg * 0x20));
						F1[1] |= SIG;
						F2[1] |= (0x0102 | (negneg * 0x10));
						F0[2] |= 0x2002;
						F1[2] |= (0x0402 | (negneg * 0x40));
						F2[2] |= 0x1002;
					}
				}
			}
		}
	}
	if (SegmentSymbol) {
		CX = CTXUNI2;
		s  = MQ_Dec(&D, CX, dec, s, QeIndexTable3);
		DD = D;
		DD <<= 1;
		s = MQ_Dec(&D, CX, dec, s, QeIndexTable3);
		DD |= D;
		DD <<= 1;
		s = MQ_Dec(&D, CX, dec, s, QeIndexTable3);
		DD |= D;
		DD <<= 1;
		s = MQ_Dec(&D, CX, dec, s, QeIndexTable3);
		DD |= D;
		if (DD != 0xa) {
			printf("[dec_clnpass_vcausal]:: SegmentSymbol Error Symbol=%d\n", DD);
			return nullptr;
		}
	}

	return s;
}

#endif