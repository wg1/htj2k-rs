/*
Copyright (c) 2000-2021, ICT-Link
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the HTJ2K project.
*/

#ifndef T2CODEC_H
#define T2CODEC_H

#define FIXED 2
#define YET 1
#define UNHANDLE 0
//#define UBYTE4MAX 0xffffffff
#define CHARMAX 0x7f
#define HT_Mode 0x90
#define PACKETINFOLENGTH 16384
#define MAX_SEGMENT 100
//#define T2_DEBUG 0

byte4 PacketInfoEnc(struct Packet_s *P);
struct Packet_s *PacketInfoControl(struct J2kParam_s *J2kParam, struct Codec_s *Codec, struct Tile_s *Tile,
                                   struct Packet_s *Ps, struct StreamChain_s *strPI, byte4 PI_Saddr,
                                   struct StreamChain_s *str, byte4 Tile_Saddr,
                                   byte4 Tile_Eaddr /*, uchar FlagFlag*/);
struct Packet_s *PacketInfoDec(struct Packet_s *Ps, struct StreamChain_s *strPI,
                               struct StreamChain_s *strPC, struct EECod_s *EECod, uchar ppm_flag);
struct Tagtree_s *TagtreeCreate1(byte4 numCblkX, byte4 numCblkY);
byte4 TagtreeCreate0(struct Tile_s *t, struct ECod_s *ECod);
void TagtreeDestory(struct Tagtree_s *tag);
struct StreamChain_s *TagTreeEnc(/*struct Tagtree_s *tree,*/ struct TagtreeNode_s *leaf,
                                 /*ubyte4*/ char threshold, struct StreamChain_s *s);
struct StreamChain_s *NumberOfPassesEnc(byte2 n, struct StreamChain_s *s);
byte4 TagTreeNodeSetValue(struct TagtreeNode_s *leaf, /*ubyte4*/ char value);
byte4 TagTreeNodeInit(struct Tagtree_s *leaf);
struct StreamChain_s *LblockChange(byte4 n, struct StreamChain_s *s);
uchar NumberOfPassesDec(struct StreamChain_s *s);
byte4 TagTreeDec(struct TagtreeNode_s *leaf, char threshold, struct StreamChain_s *s);
byte4 LblockChangeDec(struct StreamChain_s *s);
struct StreamChain_s *PacketInfoBind2FileStream(struct StreamChain_s *s, struct StreamChain_s *t);

#endif