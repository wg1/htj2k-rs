/*
Copyright (c) 2000-2021, ICT-Link
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the HTJ2K project.
*/

#include <stdio.h>
#include <stdlib.h>
#include "ImageUtil.h"
#include "j2k2.h"
#include "ctx_enc.h"
#include "t1codec.h"
#include "MqCodec.h"

#if Cplus
byte4 enc_sigpass(uchar *CX_, ubyte2 *flag, struct Image_s *CbData, uchar *SIG_TABLE, byte4 width,
                  byte4 height, byte4 row1stepf, byte4 row1stepCb, uchar bittop, byte4 bitmask) {
	ubyte2 *F0, *F1, *F2;
	byte4 *d_, *d2_;
	byte4 i, j, jj, temp_height;
	byte4 fj, fi;
	byte4 one;
	uchar SignCX;
	uchar S;
	char D;
	char negneg;
	byte4 P;

	d2_ = CbData->data;
	row1stepf /= 2;  // row1stepf1/2;
	row1stepCb /= 4; // row1stepCb1/2;
	one = 1 << bittop;
	P   = 0;
	for (jj = 0, fj = 4; jj < height; jj += 4, fj += 4) {
		temp_height = 4 < (CbData->tby1 - jj) ? 4 : (CbData->tby1 - jj);
		for (i = 0, fi = 1; i < width; i++, fi++) {
			F0 = &flag[fj - 1 + (fi - 1) * row1stepf];
			F1 = &flag[fj - 1 + fi * row1stepf];
			F2 = &flag[fj - 1 + (fi + 1) * row1stepf];
			d_ = &d2_[jj + i * row1stepCb];
			for (j = 0; j < temp_height; j++) {
				if ((!(F1[j + 1] & SIG)) && (F1[j + 1] & NEIGHBOR)) {
					S      = (uchar) ((F1[j + 1] >> 8));
					D      = (char) ((d_[j] & one) >> bittop);
					CX_[P] = (uchar) (SIG_TABLE[S] | (D << 7));
					P++;
					if (D) {
						S      = (uchar) ((F1[j + 1] >> 4) & 0xff);
						SignCX = SIGN[S];
						CX_[P] = (uchar) (SignCX & 0x0f);
						if (d_[j] & NEGATIVE_BIT) {
							negneg = 1;
							CX_[P] |= ((SignCX & 0x80) ^ 0x80);
							P++;
						} else {
							negneg = 0;
							CX_[P] |= ((SignCX & 0x80) ^ 0x00);
							P++;
						}
						F0[j] |= 0x4002;
						F1[j] |= (0x0802 | (negneg * 0x80));
						F2[j] |= 0x8002;
						F0[j + 1] |= (0x0202 | (negneg * 0x20));
						F1[j + 1] |= (SIG | VISIT);
						F2[j + 1] |= (0x0102 | (negneg * 0x10));
						F0[j + 2] |= 0x2002;
						F1[j + 2] |= (0x0402 | (negneg * 0x40));
						F2[j + 2] |= 0x1002;
						d_[j] &= bitmask;
					} else
						F1[j + 1] |= VISIT;
				} else {
					F1[j + 1] &= inVISIT;
				}
			}
		}
	}
	return P;
}
#endif

#if Cplus
byte4 enc_sigpass_Lazzy(uchar *CX_, ubyte2 *flag, struct Image_s *CbData, byte4 width, byte4 height,
                        byte4 row1stepf, byte4 row1stepCb, uchar bittop, byte4 bitmask) {
	ubyte2 *F0, *F1, *F2;
	byte4 *d_, *d2_;
	byte4 i, j, jj, temp_height;
	byte4 fj, fi;
	byte4 one;
	char D;
	char negneg;
	byte4 P;

	d2_ = CbData->data;
	row1stepf /= 2;  // row1stepf1/2;
	row1stepCb /= 4; // row1stepCb1/2;
	one = 1 << bittop;
	P   = 0;
	for (jj = 0, fj = 4; jj < height; jj += 4, fj += 4) {
		temp_height = 4 < (CbData->tby1 - jj) ? 4 : (CbData->tby1 - jj);
		for (i = 0, fi = 1; i < width; i++, fi++) {
			F0 = &flag[fj - 1 + (fi - 1) * row1stepf];
			F1 = &flag[fj - 1 + fi * row1stepf];
			F2 = &flag[fj - 1 + (fi + 1) * row1stepf];
			d_ = &d2_[jj + i * row1stepCb];
			for (j = 0; j < 4; j++) {
				if ((!(F1[j + 1] & SIG)) && (F1[j + 1] & NEIGHBOR)) {
					D      = (char) ((d_[j] & one) >> bittop);
					CX_[P] = (uchar) (D << 7);
					P++;
					if (D) {
						if (d_[j] & NEGATIVE_BIT) {
							negneg = 1;
							CX_[P] = 0x80;
							P++;
						} else {
							negneg = 0;
							CX_[P] = 0;
							P++;
						}
						F0[j] |= 0x4002;
						F1[j] |= (0x0802 | (negneg * 0x80));
						F2[j] |= 0x8002;
						F0[j + 1] |= (0x0202 | (negneg * 0x20));
						F1[j + 1] |= (SIG | VISIT);
						F2[j + 1] |= (0x0102 | (negneg * 0x10));
						F0[j + 2] |= 0x2002;
						F1[j + 2] |= (0x0402 | (negneg * 0x40));
						F2[j + 2] |= 0x1002;
						d_[j] &= bitmask;
					} else
						F1[j + 1] |= VISIT;
				} else {
					F1[j + 1] &= inVISIT;
				}
			}
		}
	}
	return P;
}
#endif

byte4 enc_sigpass_Lazzy_vcausal(uchar *CX_, ubyte2 *flag, struct Image_s *CbData, byte4 width, byte4 height,
                                byte4 row1stepf, byte4 row1stepCb, uchar bittop, byte4 bitmask) {
	ubyte2 *F0, *F1, *F2;
	byte4 *d_, *d2_;
	byte4 i, j, jj, temp_height;
	byte4 fj, fi;
	byte4 one;
	char D;
	char negneg;
	byte4 P;

	d2_ = CbData->data;
	row1stepf /= 2;  // row1stepf1/2;
	row1stepCb /= 4; // row1stepCb1/2;
	one = 1 << bittop;
	P   = 0;
	for (jj = 0, fj = 4; jj < height; jj += 4, fj += 4) {
		temp_height = 4 < (CbData->tby1 - jj) ? 4 : (CbData->tby1 - jj);
		for (i = 0, fi = 1; i < width; i++, fi++) {
			F0 = &flag[fj - 1 + (fi - 1) * row1stepf];
			F1 = &flag[fj - 1 + fi * row1stepf];
			F2 = &flag[fj - 1 + (fi + 1) * row1stepf];
			d_ = &d2_[jj + i * row1stepCb];
			for (j = 0; j < 4; j++) {
				if ((!(F1[j + 1] & SIG)) && (F1[j + 1] & NEIGHBOR)) {
					D      = (char) ((d_[j] & one) >> bittop);
					CX_[P] = (uchar) (D << 7);
					P++;
					if (D) {
						if (d_[j] & NEGATIVE_BIT) {
							negneg = 1;
							CX_[P] = 0x80;
							P++;
						} else {
							negneg = 0;
							CX_[P] = 0;
							P++;
						}
						F0[j] |= 0x4002;
						F1[j] |= (0x0802 | (negneg * 0x80));
						F2[j] |= 0x8002;
						F0[j + 1] |= (0x0202 | (negneg * 0x20));
						F1[j + 1] |= (SIG | VISIT);
						F2[j + 1] |= (0x0102 | (negneg * 0x10));
						F0[j + 2] |= 0x2002;
						F1[j + 2] |= (0x0402 | (negneg * 0x40));
						F2[j + 2] |= 0x1002;
						d_[j] &= bitmask;
					} else
						F1[j + 1] |= VISIT;
				} else {
					F1[j + 1] &= inVISIT;
				}
			}
		}
	}
	return P;
}

#if Cplus
struct StreamChain_s *dec_sigpass(struct Cblk_s *Cblk, struct Image_s *CbData, char bitpos,
                                  uchar *ctx_table, struct mqdec_s *dec, struct StreamChain_s *s,
                                  uchar FlagFlag) {
	byte2 *Flag;
	byte4 *Data;
	byte2 *F0, *F1, *F2;
	byte4 j, ii, jj, temp_height;
	byte4 fj, fi;
	byte4 col1stepf, col1stepCb;
	byte4 row1stepf, row1stepCb;
	uchar SignCX;
	uchar S;
	uchar CX, D;
	byte4 one;
	char negneg;

	Flag       = (byte2 *) &Cblk->flags->data[0];
	Data       = (byte4 *) &CbData->data[0];
	one        = 1 << bitpos;
	col1stepf  = Cblk->flags->col1step;
	row1stepf  = Cblk->flags->row1step;
	col1stepCb = CbData->col1step;
	row1stepCb = CbData->row1step;
	for (jj = CbData->tby0, fj = 4; jj < CbData->tby1; jj += 4, fj += 4) {
		temp_height = 4 < (CbData->tby1 - jj) ? 4 : (CbData->tby1 - jj);
		for (ii = CbData->tbx0, fi = 1; ii < CbData->tbx1; ii++, fi++) {
			F0 = &Flag[(fj - 1) * col1stepf + (fi - 1) * row1stepf];
			F1 = &Flag[(fj - 1) * col1stepf + fi * row1stepf];
			F2 = &Flag[(fj - 1) * col1stepf + (fi + 1) * row1stepf];

			for (j = 0; j < temp_height; j++, ++F0, ++F1, ++F2) {
				if ((!(F1[1] & SIG)) && (F1[1] & NEIGHBOR)) {
					S  = (uchar) (F1[1] >> 8);
					CX = ctx_table[S];
					s  = MQ_Dec(&D, CX, dec, s, QeIndexTable3);
					if (D) {
						S      = (uchar) ((F1[1] >> 4) & 0xff);
						SignCX = SIGN2[S];
						CX     = (uchar) (SignCX & 0xfe);
						s      = MQ_Dec(&D, CX, dec, s, QeIndexTable3);
						if (D ^ (SignCX & 1)) {
							Data[(jj + j) * col1stepCb + ii * row1stepCb] |= (NEGATIVE_BIT | one);
							negneg = 1;
						} else {
							Data[(jj + j) * col1stepCb + ii * row1stepCb] |= one;
							negneg = 0;
						}

						F0[0] |= 0x4002;
						F1[0] |= (0x0802 | (negneg * 0x80));
						F2[0] |= 0x8002;
						F0[1] |= (0x0202 | (negneg * 0x20));
						F1[1] |= (SIG | VISIT);
						F2[1] |= (0x0102 | (negneg * 0x10));
						F0[2] |= 0x2002;
						F1[2] |= (0x0402 | (negneg * 0x40));
						F2[2] |= 0x1002;
					} else {
						F1[1] |= VISIT;
					}
				} else {
					F1[1] &= inVISIT;
				}
			}
		}
	}
	return s;
}

void dec_inVisit(struct Cblk_s *Cblk, struct Image_s *CbData) {
	byte2 *Flag;
	byte2 *F1;
	byte4 j, ii, jj, temp_height;
	byte4 fj, fi;
	byte4 col1stepf;
	byte4 row1stepf;

	Flag      = (byte2 *) &Cblk->flags->data[0];
	col1stepf = Cblk->flags->col1step;
	row1stepf = Cblk->flags->row1step;
	for (jj = CbData->tby0, fj = 4; jj < CbData->tby1; jj += 4, fj += 4) {
		temp_height = 4 < (CbData->tby1 - jj) ? 4 : (CbData->tby1 - jj);
		for (ii = CbData->tbx0, fi = 1; ii < CbData->tbx1; ii++, fi++) {
			F1 = &Flag[(fj - 1) * col1stepf + fi * row1stepf];
			for (j = 0; j < temp_height; j++, ++F1) {
				F1[1] &= inVISIT;
			}
		}
	}
}

struct StreamChain_s *dec_sigpass_vcausal(struct Cblk_s *Cblk, struct Image_s *CbData, char bitpos,
                                          uchar *ctx_table, struct mqdec_s *dec, struct StreamChain_s *str,
                                          uchar FlagFlag) {
	byte2 *Flag;
	byte4 *Data;
	byte2 *F0, *F1, *F2;
	byte4 j, ii, jj, temp_height;
	byte4 fj, fi;
	byte4 col1stepf, col1stepCb;
	byte4 row1stepf, row1stepCb;
	uchar SignCX;
	uchar S;
	uchar CX, D;
	byte4 one;
	char negneg;

	Flag       = (byte2 *) &Cblk->flags->data[0];
	Data       = (byte4 *) &CbData->data[0];
	one        = 1 << bitpos;
	col1stepf  = Cblk->flags->col1step;
	row1stepf  = Cblk->flags->row1step;
	col1stepCb = CbData->col1step;
	row1stepCb = CbData->row1step;
	for (jj = CbData->tby0, fj = 4; jj < CbData->tby1; jj += 4, fj += 4) {
		temp_height = 4 < (CbData->tby1 - jj) ? 4 : (CbData->tby1 - jj);
		if (temp_height == 4) {
			for (ii = CbData->tbx0, fi = 1; ii < CbData->tbx1; ii++, fi++) {
				F0 = &Flag[(fj - 1) * col1stepf + (fi - 1) * row1stepf];
				F1 = &Flag[(fj - 1) * col1stepf + fi * row1stepf];
				F2 = &Flag[(fj - 1) * col1stepf + (fi + 1) * row1stepf];
				for (j = 0; j < temp_height - 1; j++, ++F0, ++F1, ++F2) {
					if (!(F1[1] & SIG) && (F1[1] & NEIGHBOR)) {
						S   = (uchar) (F1[1] >> 8);
						CX  = ctx_table[S];
						str = MQ_Dec(&D, CX, dec, str, QeIndexTable3);
						if (D) {
							S      = (uchar) ((F1[1] >> 4) & 0xff);
							SignCX = SIGN2[S];
							CX     = (uchar) (SignCX & 0xfe);
							str    = MQ_Dec(&D, CX, dec, str, QeIndexTable3);
							if (D ^ (SignCX & 1)) {
								Data[(jj + j) * col1stepCb + ii * row1stepCb] |= (NEGATIVE_BIT | one);
								negneg = 1;
							} else {
								Data[(jj + j) * col1stepCb + ii * row1stepCb] |= one;
								negneg = 0;
							}
							F0[0] |= 0x4002;
							F1[0] |= (0x0802 | (negneg * 0x80));
							F2[0] |= 0x8002;
							F0[1] |= (0x0202 | (negneg * 0x20));
							F1[1] |= (SIG | VISIT);
							F2[1] |= (0x0102 | (negneg * 0x10));
							F0[2] |= 0x2002;
							F1[2] |= (0x0402 | (negneg * 0x40));
							F2[2] |= 0x1002;
						} else {
							F1[1] |= VISIT;
						}
					} else {
						F1[1] &= inVISIT;
					}
				}
				for (; j < temp_height; j++, ++F0, ++F1, ++F2) {
					if (((j == 3) && (!(F1[1] & SIG)) && ((F0[0] | F1[0] | F2[0] | F0[1] | F2[1]) & SIG))) {
						S   = (uchar) ((F1[1] & 0x377f) >> 8);
						CX  = ctx_table[S];
						str = MQ_Dec(&D, CX, dec, str, QeIndexTable3);
						if (D) {
							S      = (uchar) (((F1[1] & 0x377f) >> 4) & 0xff);
							SignCX = SIGN2[S];
							CX     = (uchar) (SignCX & 0xfe);
							str    = MQ_Dec(&D, CX, dec, str, QeIndexTable3);
							if (D ^ (SignCX & 1)) {
								Data[(jj + j) * col1stepCb + ii * row1stepCb] |= (NEGATIVE_BIT | one);
								negneg = 1;
							} else {
								Data[(jj + j) * col1stepCb + ii * row1stepCb] |= one;
								negneg = 0;
							}

							F0[0] |= 0x4002;
							F1[0] |= (0x0802 | (negneg * 0x80));
							F2[0] |= 0x8002;
							F0[1] |= (0x0202 | (negneg * 0x20));
							F1[1] |= (SIG | VISIT);
							F2[1] |= (0x0102 | (negneg * 0x10));
							F0[2] |= 0x2002;
							F1[2] |= (0x0402 | (negneg * 0x40));
							F2[2] |= 0x1002;
						} else {
							F1[1] |= VISIT;
						}
					} else {
						F1[1] &= inVISIT;
					}
				}
			}
		} else {
			for (ii = CbData->tbx0, fi = 1; ii < CbData->tbx1; ii++, fi++) {
				F0 = &Flag[(fj - 1) * col1stepf + (fi - 1) * row1stepf];
				F1 = &Flag[(fj - 1) * col1stepf + fi * row1stepf];
				F2 = &Flag[(fj - 1) * col1stepf + (fi + 1) * row1stepf];
				for (j = 0; j < temp_height; j++, ++F0, ++F1, ++F2) {
					if (!(F1[1] & SIG) && (F1[1] & NEIGHBOR)) {
						S   = (uchar) (F1[1] >> 8);
						CX  = ctx_table[S];
						str = MQ_Dec(&D, CX, dec, str, QeIndexTable3);
						if (D) {
							S      = (uchar) ((F1[1] >> 4) & 0xff);
							SignCX = SIGN2[S];
							CX     = (uchar) (SignCX & 0xfe);
							str    = MQ_Dec(&D, CX, dec, str, QeIndexTable3);
							if (D ^ (SignCX & 1)) {
								Data[(jj + j) * col1stepCb + ii * row1stepCb] |= (NEGATIVE_BIT | one);
								negneg = 1;
							} else {
								Data[(jj + j) * col1stepCb + ii * row1stepCb] |= one;
								negneg = 0;
							}
							F0[0] |= 0x4002;
							F1[0] |= (0x0802 | (negneg * 0x80));
							F2[0] |= 0x8002;
							F0[1] |= (0x0202 | (negneg * 0x20));
							F1[1] |= (SIG | VISIT);
							F2[1] |= (0x0102 | (negneg * 0x10));
							F0[2] |= 0x2002;
							F1[2] |= (0x0402 | (negneg * 0x40));
							F2[2] |= 0x1002;
						} else {
							F1[1] |= VISIT;
						}
					} else {
						F1[1] &= inVISIT;
					}
				}
			}
		}
	}
	return str;
}

struct StreamChain_s *dec_sigpass_Lazy(struct Cblk_s *Cblk, struct Image_s *CbData, char bitpos,
                                       byte4 counter_limit, struct StreamChain_s *str, uchar FlagFlag) {
	byte2 *Flag;
	byte4 *Data;
	byte2 *F0, *F1, *F2;
	byte4 j, ii, jj, temp_height;
	byte4 fj, fi;
	byte4 col1stepf, col1stepCb;
	byte4 row1stepf, row1stepCb;
	uchar CX = 0, D;
	byte4 one;
	char negneg;

	Flag       = (byte2 *) &Cblk->flags->data[0];
	Data       = (byte4 *) &CbData->data[0];
	one        = 1 << bitpos;
	col1stepf  = Cblk->flags->col1step;
	row1stepf  = Cblk->flags->row1step;
	col1stepCb = CbData->col1step;
	row1stepCb = CbData->row1step;
	for (jj = CbData->tby0, fj = 4; jj < CbData->tby1; jj += 4, fj += 4) {
		temp_height = 4 < (CbData->tby1 - jj) ? 4 : (CbData->tby1 - jj);
		for (ii = CbData->tbx0, fi = 1; ii < CbData->tbx1; ii++, fi++) {
			F0 = &Flag[(fj - 1) * col1stepf + (fi - 1) * row1stepf];
			F1 = &Flag[(fj - 1) * col1stepf + fi * row1stepf];
			F2 = &Flag[(fj - 1) * col1stepf + (fi + 1) * row1stepf];

			for (j = 0; j < temp_height; j++, ++F0, ++F1, ++F2) {
				if ((!(F1[1] & SIG)) && (F1[1] & NEIGHBOR)) {
					str = MQ_Dec_Lazy(&D, counter_limit, str);
					if (D) {
						str = MQ_Dec_Lazy(&D, counter_limit, str);
						if (D) {
							Data[(jj + j) * col1stepCb + ii * row1stepCb] |= (NEGATIVE_BIT | one);
							negneg = 1;
						} else {
							Data[(jj + j) * col1stepCb + ii * row1stepCb] |= one;
							negneg = 0;
						}

						F0[0] |= 0x4002;
						F1[0] |= (0x0802 | (negneg * 0x80));
						F2[0] |= 0x8002;
						F0[1] |= (0x0202 | (negneg * 0x20));
						F1[1] |= (SIG | VISIT);
						F2[1] |= (0x0102 | (negneg * 0x10));
						F0[2] |= 0x2002;
						F1[2] |= (0x0402 | (negneg * 0x40));
						F2[2] |= 0x1002;
					} else
						F1[1] |= VISIT;
				} else {
					F1[1] &= inVISIT;
				}
			}
		}
	}
	return str;
}

struct StreamChain_s *dec_sigpass_Lazy_vcausal(struct Cblk_s *Cblk, struct Image_s *CbData, char bitpos,
                                               byte4 counter_limit, struct StreamChain_s *str) {
	byte2 *Flag;
	byte4 *Data;
	byte2 *F0, *F1, *F2;
	byte4 j, ii, jj, temp_height;
	byte4 fj, fi;
	byte4 col1stepf, col1stepCb;
	byte4 row1stepf, row1stepCb;
	uchar D;
	byte4 one;
	char negneg;

	Flag       = (byte2 *) &Cblk->flags->data[0];
	Data       = (byte4 *) &CbData->data[0];
	one        = 1 << bitpos;
	col1stepf  = Cblk->flags->col1step;
	row1stepf  = Cblk->flags->row1step;
	col1stepCb = CbData->col1step;
	row1stepCb = CbData->row1step;
	for (jj = CbData->tby0, fj = 4; jj < CbData->tby1; jj += 4, fj += 4) {
		temp_height = 4 < (CbData->tby1 - jj) ? 4 : (CbData->tby1 - jj);
		for (ii = CbData->tbx0, fi = 1; ii < CbData->tbx1; ii++, fi++) {
			F0 = &Flag[(fj - 1) * col1stepf + (fi - 1) * row1stepf];
			F1 = &Flag[(fj - 1) * col1stepf + fi * row1stepf];
			F2 = &Flag[(fj - 1) * col1stepf + (fi + 1) * row1stepf];

			for (j = 0; j < temp_height; j++, ++F0, ++F1, ++F2) {
				if ((!(F1[1] & SIG)) && (F1[1] & NEIGHBOR)) {
					str = MQ_Dec_Lazy(&D, counter_limit, str);
					if (D) {
						str = MQ_Dec_Lazy(&D, counter_limit, str);
						if (D) {
							Data[(jj + j) * col1stepCb + ii * row1stepCb] |= (NEGATIVE_BIT | one);
							negneg = 1;
						} else {
							Data[(jj + j) * col1stepCb + ii * row1stepCb] |= one;
							negneg = 0;
						}

						F0[0] |= 0x4002;
						F1[0] |= (0x0802 | (negneg * 0x80));
						F2[0] |= 0x8002;
						F0[1] |= (0x0202 | (negneg * 0x20));
						F1[1] |= (SIG | VISIT);
						F2[1] |= (0x0102 | (negneg * 0x10));
						F0[2] |= 0x2002;
						F1[2] |= (0x0402 | (negneg * 0x40));
						F2[2] |= 0x1002;
					} else
						F1[1] |= VISIT;
				} else {
					F1[1] &= inVISIT;
				}
			}
		}
	}
	return str;
}
#endif