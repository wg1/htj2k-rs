/*
Copyright (c) 2000-2021, ICT-Link
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the HTJ2K project.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ImageUtil.h"
#include "j2k2.h"
#include "wavelet_codec.h"
#include "annexE.h"
#include "J2k_Debug.h"

void WaveDec(struct EECod_s *EECod, struct EEQcd_s *EEQcd,
             struct wavelet_s *Wave /*, byte4 T, ubyte2 ccc*/) {
	char NL, lll;
	NL = EECod->DecompLev;

	if (NL) {
		if (EECod->Filter == Filter97) {
			for (lll = 0; lll < NL; lll++) {
				DecWaveIL97(&Wave->ll_d[lll + 1], lll, &Wave->ll_d[lll], &Wave->wave[lll * 3],
				            &Wave->wave[lll * 3 + 1], &Wave->wave[lll * 3 + 2], &Wave->wave[lll * 3 + 3]);
				Dec_Wave97_Q(&Wave->ll_d[lll + 1], lll, EEQcd);
				Dec_H97(&Wave->ll_d[lll + 1]);
				Dec_V97(&Wave->ll_d[lll + 1]);
			}
		} else {
			for (lll = 0; lll < NL; lll++) {
				DecWaveIL53(&Wave->ll_d[lll + 1], lll, &Wave->ll_d[lll], &Wave->wave[lll * 3],
				            &Wave->wave[lll * 3 + 1], &Wave->wave[lll * 3 + 2], &Wave->wave[lll * 3 + 3],
				            EEQcd);
				Dec_H53(&Wave->ll_d[lll + 1]);
				Dec_V53(&Wave->ll_d[lll + 1]);
			}
		}
	} else {
		lll = 0;
		DecWaveIL_NL0(&Wave->ll_d[0], &Wave->wave[0], EEQcd);
	}
}

byte4 WaveEnc(struct J2kParam_s *J2kParam, /*ubyte2 ccc,*/ struct wavelet_s *Wave, struct EECod_s *EECod,
              struct EEQcd_s *EEQcd) {
	uchar bbb;
	char G;
	char rrr;
	char NL;
	NL = Wave->NL;

	if (EECod->Filter == Filter53) {
		for (rrr = NL; rrr > 0; rrr--) {
			if (J2kParam->debugFlag) {
				FILE *fp;
				char debugFname[256];
				byte4 i, j;
				strcpy(debugFname, J2kParam->debugFname);
				//				if( J2kParam->HW )
				//					strcat( debugFname, "_WaveIn_HW.csv" );
				//				else
				strcat(debugFname, "_WaveIn.csv");
				fp = fopen(debugFname, "a+");
				for (j = Wave->ll_d[rrr].tby0; j < Wave->ll_d[rrr].tby1; j++) {
					for (i = Wave->ll_d[rrr].tbx0; i < Wave->ll_d[rrr].tbx1; i++) {
						fprintf(fp, "%d,",
						        Wave->ll_d[rrr]
						            .data[j * Wave->ll_d[rrr].col1step + i * Wave->ll_d[rrr].row1step]);
					}
					fprintf(fp, "\n");
				}
				fclose(fp);
			}
			Enc_V53(&Wave->ll_d[rrr]);
			Enc_H53(&Wave->ll_d[rrr]);
			Enc_WaveIL(&Wave->ll_d[rrr], rrr, &Wave->ll_d[rrr - 1], &Wave->wave[rrr * 3 - 3],
			           &Wave->wave[rrr * 3 - 2], &Wave->wave[rrr * 3 - 1], &Wave->wave[rrr * 3]);

			if (J2kParam->debugFlag) {
				FILE *fp;
				char debugFname[256];
				byte4 i, j;
				char bbb;
				strcpy(debugFname, J2kParam->debugFname);
				//				if( J2kParam->HW )
				//					strcat( debugFname, "_WaveOut_HW.csv" );
				//				else
				strcat(debugFname, "_WaveOut.csv");
				fp = fopen(debugFname, "a+");
				for (bbb = 0; bbb < 4; bbb++) {
					for (j = Wave->wave[bbb].tby0; j < Wave->wave[bbb].tby1; j++) {
						for (i = Wave->wave[bbb].tbx0; i < Wave->wave[bbb].tbx1; i++) {
							fprintf(fp, "%d,",
							        Wave->wave[bbb]
							            .data[j * Wave->wave[bbb].col1step + i * Wave->wave[bbb].row1step]);
						}
						fprintf(fp, "\n");
					}
					fprintf(fp, "\n");
				}
				fprintf(fp, "\n\n");
				fclose(fp);
			}
		}
	} else {
		for (rrr = NL; rrr > 0; rrr--) {
			if (J2kParam->debugFlag) {
				FILE *fp;
				char debugFname[256];
				byte4 i, j;
				strcpy(debugFname, J2kParam->debugFname);
				//				if( J2kParam->HW ){
				//					byte4	*D_TS, *D_;
				//					strcat( debugFname, "_WaveIn_HW.csv" );
				//					fp = fopen( debugFname, "a+");
				//					D_TS = (byte4 *)Wave->ll_d[rrr].data;
				//					for( j=Wave->ll_d[rrr].tby0 ; j<Wave->ll_d[rrr].tby1 ; j++,
				//D_TS+=Wave->ll_d[rrr].col1step ){ 						D_ = D_TS; 						for( i=Wave->ll_d[rrr].tbx0 ;
				//i<Wave->ll_d[rrr].tbx1 ; i++, D_+=Wave->ll_d[rrr].row1step ) 							fprintf( fp, "%d,", (*D_) );
				//						fprintf( fp, "\n" );
				//					}
				//					fclose( fp );
				//				}
				//				else{
				float *f_TS, *f_;
				strcat(debugFname, "_WaveIn.csv");
				fp   = fopen(debugFname, "a+");
				f_TS = (float *) Wave->ll_d[rrr].data;
				for (j = Wave->ll_d[rrr].tby0; j < Wave->ll_d[rrr].tby1;
				     j++, f_TS += Wave->ll_d[rrr].col1step) {
					f_ = f_TS;
					for (i = Wave->ll_d[rrr].tbx0; i < Wave->ll_d[rrr].tbx1; i++, ++f_)
						fprintf(fp, "%f,", (*f_));
					fprintf(fp, "\n");
				}
				fclose(fp);
				//				}
			}

			Enc_V97(&Wave->ll_d[rrr]);
			Enc_H97(&Wave->ll_d[rrr]);
			Enc_WaveIL(&Wave->ll_d[rrr], rrr, &Wave->ll_d[rrr - 1], &Wave->wave[rrr * 3 - 3],
			           &Wave->wave[rrr * 3 - 2], &Wave->wave[rrr * 3 - 1], &Wave->wave[rrr * 3]);

			if (J2kParam->debugFlag) {
				FILE *fp;
				char debugFname[256];
				byte4 i, j;
				char bbb;
				strcpy(debugFname, J2kParam->debugFname);
				//				if( J2kParam->HW ){
				//					byte4	*D_TS, *D_;
				//					strcat( debugFname, "_WaveOut_HW.csv" );
				//					fp = fopen( debugFname, "a+");
				//					for( bbb=0 ; bbb<4 ; bbb++ ){
				//						D_TS = (byte4
				//*)&Wave->wave[bbb].data[Wave->wave[bbb].tby0*Wave->wave[bbb].col1step+Wave->wave[bbb].tbx0*Wave->wave[bbb].row1step];
				//						for( j=Wave->wave[bbb].tby0 ; j<Wave->wave[bbb].tby1 ; j++,
				//D_TS+=Wave->wave[bbb].col1step ){ 							D_ = D_TS; 							for( i=Wave->wave[bbb].tbx0 ;
				//i<Wave->wave[bbb].tbx1 ; i++, D_+=Wave->wave[bbb].row1step ) 								fprintf( fp, "%d,", (*D_) );
				//							fprintf( fp, "\n" );
				//						}
				//						fprintf( fp, "\n" );
				//					}
				//					fprintf( fp, "\n\n" );
				//					fclose( fp );
				//				}
				//				else{
				float *f_TS, *f_;
				strcat(debugFname, "_WaveOut.csv");
				fp = fopen(debugFname, "a+");
				for (bbb = 0; bbb < 4; bbb++) {
					f_TS =
					    (float *) &Wave->wave[bbb].data[Wave->wave[bbb].tby0 * Wave->wave[bbb].col1step
					                                    + Wave->wave[bbb].tbx0 * Wave->wave[bbb].row1step];
					for (j = Wave->wave[bbb].tby0; j < Wave->wave[bbb].tby1;
					     j++, f_TS += Wave->wave[bbb].col1step) {
						f_ = f_TS;
						for (i = Wave->wave[bbb].tbx0; i < Wave->wave[bbb].tbx1; i++, ++f_)
							fprintf(fp, "%f,", (*f_));
						fprintf(fp, "\n");
					}
					fprintf(fp, "\n");
				}
				fprintf(fp, "\n\n");
				fclose(fp);
				//				}
			}
		}
	}

	for (bbb = 0; bbb < Wave->numBands; bbb++) {
		if (EECod->Filter == Filter97) {
			Enc_Quantize(&Wave->wave[bbb], EEQcd->e[bbb], EEQcd->myu[bbb], EEQcd->Rb[bbb]);
		}
		G        = Enc_WaveIL_G(&Wave->wave[bbb], EEQcd->e[bbb], EEQcd->G);
		EEQcd->G = G;
	}
	return EXIT_SUCCESS;
}

#if Cplus
void Enc_WaveIL(struct Image_s *image, byte4 lll, struct Image_s *ll_d, struct Image_s *ll,
                struct Image_s *hl, struct Image_s *lh, struct Image_s *hh) {
	byte4 height, width;
	byte4 i, j, jj;
	byte4 col1step, col1step2, row1step, row2step;
	byte4 xs_L, xs_H, ys_L, ys_H;
	byte4 *D2;
	byte4 *D, *D_TS;

	width    = image->tbx1 - image->tbx0;
	height   = image->tby1 - image->tby0;
	col1step = image->col1step;
	row1step = image->row1step;
	row2step = row1step * 2;

	if (image->tbx0 & 1) {
		xs_L = 1;
		xs_H = 0;
	} // odd
	else {
		xs_L = 0;
		xs_H = 1;
	} // even
	if (image->tby0 & 1) {
		ys_L = 1;
		ys_H = 0;
	} // odd
	else {
		ys_L = 0;
		ys_H = 1;
	} // even

	// ll
	if (lll != 1) {
		D_TS      = ll_d->data;
		col1step2 = ll_d->col1step;
	} else {
		D_TS      = ll->data;
		col1step2 = ll->col1step;
	}
	for (j = ys_L, jj = 0; j < height; j += 2, jj++) {
		D  = &D_TS[jj * col1step2];
		D2 = &image->data[j * col1step + xs_L * row1step];
		for (i = xs_L; i < width; i += 2, ++D, D2 += row2step)
			*D = *D2;
	}

	// hl
	D_TS = hl->data;
	for (j = ys_L, jj = 0; j < height; j += 2, jj++) {
		D  = &D_TS[jj * hl->col1step];
		D2 = &image->data[j * col1step + xs_H * row1step];
		for (i = xs_H; i < width; i += 2, ++D, D2 += row2step)
			*D = *D2;
	}

	// lh
	D_TS = lh->data;
	for (j = ys_H, jj = 0; j < height; j += 2, jj++) {
		D  = &D_TS[jj * lh->col1step];
		D2 = &image->data[j * col1step + xs_L * row1step];
		for (i = xs_L; i < width; i += 2, ++D, D2 += row2step)
			*D = *D2;
	}

	// hh
	D_TS = hh->data;
	for (j = ys_H, jj = 0; j < height; j += 2, jj++) {
		D  = &D_TS[jj * hh->col1step];
		D2 = &image->data[j * col1step + xs_H * row1step];
		for (i = xs_H; i < width; i += 2, ++D, D2 += row2step)
			*D = *D2;
	}
}
#endif

#if Cplus
char Enc_WaveIL_G(struct Image_s *Band, byte2 e, char G) {
	byte4 height, width;
	byte4 i, j;
	byte4 col1step, row1step;
	byte4 *D, *D_TS;
	byte4 maxD, DATA;
	char bittop;

	maxD     = 0;
	row1step = Band->row1step;
	col1step = Band->col1step;
	height   = Band->height;
	width    = Band->width;
	D_TS     = (byte4 *) Band->data;
	for (j = 0; j < height; j++) {
		D = &(D_TS[j * Band->col1step]);
		for (i = 0; i < width; i++, ++D) {
			DATA = abs(*D);
			maxD |= DATA;
			if (*D < 0) *D = (byte4) (DATA | NEGATIVE_BIT);
		}
	}
	for (bittop = 30, j = 0x40000000; j >= 1; j >>= 1, bittop--) {
		if (j & maxD) break;
	}
	G = (char) ((bittop + 2 - e) > G ? (bittop + 2 - e) : G);
	return G;
}
#endif

byte4 Tbxy(byte4 tcxy, byte4 lll, byte4 xo) {
	byte4 lll1;

	if (lll == 0)
		lll1 = 0;
	else
		lll1 = lll - 1;

	if (lll == 0x20) {
		if (tcxy) {
			if (xo)
				return 0;
			else
				return 1;
		} else
			return 0;
	} else
		return ceil2((tcxy - (1 << lll1) * xo), (1 << lll));
}

void Wavelet_Create(struct wavelet_s *wave1, byte4 width, byte4 height, char NL) {
	struct Image_s *wave, *wave_e;
	byte4 lll;
	byte4 x1, y1;
	byte4 /*h=1,*/ l = 0;

	wave1->numBands = (char) (NL * 3 + 1);
	wave1->wave     = new struct Image_s[NL * 3 + 1];
	wave1->ll_d     = new struct Image_s[NL + 1];

	wave1->NL = NL;
	wave_e    = &(wave1->wave[NL * 3]);
	wave      = wave_e;
	x1        = width;
	y1        = height;
	if (NL) {
		for (lll = 1; lll <= NL; lll++) {
			x1 = Tbxy(width, lll, l);
			y1 = Tbxy(height, lll, l);
			if (x1 < 4)
				x1 = 4;
			else if (x1 % 4)
				x1 = (x1 / 4 + 1) * 4;
			if (y1 < 4)
				y1 = 4;
			else if (y1 % 4)
				y1 = (y1 / 4 + 1) * 4;
			// hh
			ImageCreate(wave, x1, y1, 0, x1, 0, y1, BYTE4);
			--wave;

			// lh
			ImageCreate(wave, x1, y1, 0, x1, 0, y1, BYTE4);
			--wave;

			// hl
			ImageCreate(wave, x1, y1, 0, x1, 0, y1, BYTE4);
			--wave;
		}
	}

	// last ll
	ImageCreate(wave, x1, y1, 0, x1, 0, y1, BYTE4);
	ImageCreate(&wave1->ll_d[0], x1, y1, 0, x1, 0, y1, BYTE4);
	if (NL) {
		wave_e = &(wave1->ll_d[NL]);
		wave   = &(wave1->ll_d[1]);
		for (lll = 1; lll < NL; lll++) {
			x1 = Tbxy(width, NL - lll, l);
			y1 = Tbxy(height, NL - lll, l);
			// ll
			if (x1 < 4)
				x1 = 4;
			else if (x1 % 4)
				x1 = (x1 / 4 + 1) * 4;
			if (y1 < 4)
				y1 = 4;
			else if (y1 % 4)
				y1 = (y1 / 4 + 1) * 4;
			ImageCreate(wave, x1, y1, 0, x1, 0, y1, BYTE4);
			++wave;
		}
	}
}

byte4 Wavelet_Destory(struct wavelet_s *wave1) {
	byte4 lll;
	uchar NL;
	NL = wave1->NL;
	if (NL) {
		for (lll = 0; lll < NL; lll++)
			delete[] wave1->ll_d[lll].Pdata;
		for (lll = NL; lll > 0; lll--) {
			delete[] wave1->wave[lll * 3].Pdata;
			delete[] wave1->wave[lll * 3 - 1].Pdata;
			delete[] wave1->wave[lll * 3 - 2].Pdata;
		}
		delete[] wave1->wave[0].Pdata;
	}
	delete[] wave1->ll_d;
	delete[] wave1->wave;
	delete wave1;
	return EXIT_SUCCESS;
}

void WaveAdrCal(struct wavelet_s *Wave, /*struct Tile_s *t,*/ struct Image_s *Image, struct EECod_s *EECod,
                uchar FlagFlag) {
	byte4 h = 1, l = 0;
	byte4 lll;
	struct Image_s *wave, *wave_s;
	struct Image_s *llD, *llD_s;
	uchar NL;

	NL     = EECod->DecompLev;
	wave_s = &Wave->wave[0];
	wave   = &Wave->wave[NL * 3];
	if (NL) {
		for (lll = 1; lll <= NL; lll++) {
			// hh
			wave->tbx0   = Tbxy(Image->tbx0, lll, h);
			wave->tbx1   = Tbxy(Image->tbx1, lll, h);
			wave->tby0   = Tbxy(Image->tby0, lll, h);
			wave->tby1   = Tbxy(Image->tby1, lll, h);
			wave->height = wave->tby1 - wave->tby0;
			wave->width  = wave->tbx1 - wave->tbx0;
			if (FlagFlag)
				printf("HH(%d) tbx0=%d tby0=%d tbx1=%d tby1=%d width=%d height=%d\n", lll, wave->tbx0,
				       wave->tby0, wave->tbx1, wave->tby1, wave->width, wave->height);
			--wave;
			// lh
			wave->tbx0   = Tbxy(Image->tbx0, lll, l);
			wave->tbx1   = Tbxy(Image->tbx1, lll, l);
			wave->tby0   = Tbxy(Image->tby0, lll, h);
			wave->tby1   = Tbxy(Image->tby1, lll, h);
			wave->height = wave->tby1 - wave->tby0;
			wave->width  = wave->tbx1 - wave->tbx0;
			if (FlagFlag)
				printf(/*fp_log,*/ "LH(%d) tbx0=%d tby0=%d tbx1=%d tby1=%d width=%d height=%d\n", lll,
				       wave->tbx0, wave->tby0, wave->tbx1, wave->tby1, wave->width, wave->height);
			--wave;
			// hl
			wave->tbx0   = Tbxy(Image->tbx0, lll, h);
			wave->tbx1   = Tbxy(Image->tbx1, lll, h);
			wave->tby0   = Tbxy(Image->tby0, lll, l);
			wave->tby1   = Tbxy(Image->tby1, lll, l);
			wave->height = wave->tby1 - wave->tby0;
			wave->width  = wave->tbx1 - wave->tbx0;
			if (FlagFlag)
				printf(/*fp_log,*/ "HL(%d) tbx0=%d tby0=%d tbx1=%d tby1=%d width=%d height=%d\n", lll,
				       wave->tbx0, wave->tby0, wave->tbx1, wave->tby1, wave->width, wave->height);
			--wave;
		}
	}
	// last ll
	wave->tbx0   = Tbxy(Image->tbx0, NL, l);
	wave->tbx1   = Tbxy(Image->tbx1, NL, l);
	wave->tby0   = Tbxy(Image->tby0, NL, l);
	wave->tby1   = Tbxy(Image->tby1, NL, l);
	wave->height = wave->tby1 - wave->tby0;
	wave->width  = wave->tbx1 - wave->tbx0;
	if (FlagFlag)
		printf(/*fp_log,*/ "LL(%d) tbx0=%d tby0=%d tbx1=%d tby1=%d width=%d height=%d\n", NL, wave->tbx0,
		       wave->tby0, wave->tbx1, wave->tby1, wave->width, wave->height);

	llD_s = &Wave->ll_d[NL];
	llD   = &Wave->ll_d[0];
	for (lll = NL; lll > 0; lll--) {
		// ll
		llD->tbx0   = Tbxy(Image->tbx0, lll, l);
		llD->tbx1   = Tbxy(Image->tbx1, lll, l);
		llD->tby0   = Tbxy(Image->tby0, lll, l);
		llD->tby1   = Tbxy(Image->tby1, lll, l);
		llD->width  = llD->tbx1 - llD->tbx0;
		llD->height = llD->tby1 - llD->tby0;
		if (FlagFlag)
			printf(/*fp_log,*/ "llD(%d) tbx0=%d tby0=%d tbx1=%d tby1=%d width=%d height=%d\n", lll,
			       llD->tbx0, llD->tby0, llD->tbx1, llD->tby1, llD->width, llD->height);

		++llD;
	}
	llD->tbx0     = Image->tbx0;
	llD->tbx1     = Image->tbx1;
	llD->tby0     = Image->tby0;
	llD->tby1     = Image->tby1;
	llD->col1step = Image->col1step;
	llD->row1step = Image->row1step;
	llD->width    = llD->tbx1 - llD->tbx0;
	llD->height   = llD->tby1 - llD->tby0;
	if (FlagFlag)
		printf("llD(%d) tbx0=%d tby0=%d tbx1=%d tby1=%d width=%d height=%d\n", lll, llD->tbx0, llD->tby0,
		       llD->tbx1, llD->tby1, llD->width, llD->height);
}

void Wavelet_Init(struct wavelet_s *Wave) {
	struct Image_s *wave;
	char NL;

	NL = Wave->NL;
	for (wave = &Wave->wave[0]; wave != &Wave->wave[NL * 3 + 1]; ++wave)
		memset(wave->Pdata, 0, sizeof(byte4) * wave->numData);
}

#if ICT_Link09
void WaveDebug(struct wavelet_s *Wave, char lll) {
	FILE *fp;
	byte4 *D_TS, *D_;
	byte4 j, i;

	fp = fopen("LegacyBandDataWave.csv", "a+");
	if (lll == 0) {
		fprintf(fp, "<<wave(LL)>>,%x, lll=%d, tby0=%d, tby1=%d, tbx0=%d, tbx1=%d, col1=%d, row1=%d\n",
		        Wave->wave[lll * 3 + 0].data, lll, Wave->wave[lll * 3 + 0].tby0,
		        Wave->wave[lll * 3 + 0].tby1, Wave->wave[lll * 3 + 0].tbx0, Wave->wave[lll * 3 + 0].tbx1,
		        Wave->wave[lll * 3 + 0].col1step, Wave->wave[lll * 3 + 0].row1step);
		D_TS = (byte4 *) Wave->wave[lll * 3 + 0].data;
		for (j = Wave->wave[lll * 3 + 0].tby0; j < Wave->wave[lll * 3 + 0].tby1;
		     j++, D_TS += Wave->wave[lll * 3 + 0].col1step) {
			D_ = D_TS;
			for (i = Wave->wave[lll * 3 + 0].tbx0; i < Wave->wave[lll * 3 + 0].tbx1;
			     i++, D_ += Wave->wave[lll * 3 + 0].row1step)
				fprintf(fp, "%d,", (*D_));
			fprintf(fp, "\n");
		}
		fprintf(fp, "\n");
	} else {
		fprintf(fp, "<<ll_d>>,%x, lll=%d, tby0=%d, tby1=%d, tbx0=%d, tbx1=%d, col1=%d, row1=%d\n",
		        Wave->ll_d[lll].data, lll, Wave->ll_d[lll].tby0, Wave->ll_d[lll].tby1, Wave->ll_d[lll].tbx0,
		        Wave->ll_d[lll].tbx1, Wave->ll_d[lll].col1step, Wave->ll_d[lll].row1step);
		D_TS = (byte4 *) Wave->ll_d[lll].data;
		for (j = Wave->ll_d[lll].tby0; j < Wave->ll_d[lll].tby1; j++, D_TS += Wave->ll_d[lll].col1step) {
			D_ = D_TS;
			for (i = Wave->ll_d[lll].tbx0; i < Wave->ll_d[lll].tbx1; i++, D_ += Wave->ll_d[lll].row1step)
				fprintf(fp, "%d,", (*D_));
			fprintf(fp, "\n");
		}
		fprintf(fp, "\n");
	}
	fprintf(fp, "<<wave(HL)>>,%x, lll=%d, tby0=%d, tby1=%d, tbx0=%d, tbx1=%d, col1=%d, row1=%d\n",
	        Wave->wave[lll * 3 + 1].data, lll, Wave->wave[lll * 3 + 1].tby0, Wave->wave[lll * 3 + 1].tby1,
	        Wave->wave[lll * 3 + 1].tbx0, Wave->wave[lll * 3 + 1].tbx1, Wave->wave[lll * 3 + 1].col1step,
	        Wave->wave[lll * 3 + 1].row1step);
	D_TS = (byte4 *) Wave->wave[lll * 3 + 1].data;
	for (j = Wave->wave[lll * 3 + 1].tby0; j < Wave->wave[lll * 3 + 1].tby1;
	     j++, D_TS += Wave->wave[lll * 3 + 1].col1step) {
		D_ = D_TS;
		for (i = Wave->wave[lll * 3 + 1].tbx0; i < Wave->wave[lll * 3 + 1].tbx1;
		     i++, D_ += Wave->wave[lll * 3 + 1].row1step)
			fprintf(fp, "%d,", (*D_));
		fprintf(fp, "\n");
	}
	fprintf(fp, "\n");

	fprintf(fp, "<<wave(LH)>>,%x, lll=%d, tby0=%d, tby1=%d, tbx0=%d, tbx1=%d, col1=%d, row1=%d\n",
	        Wave->wave[lll * 3 + 2].data, lll, Wave->wave[lll * 3 + 2].tby0, Wave->wave[lll * 3 + 2].tby1,
	        Wave->wave[lll * 3 + 2].tbx0, Wave->wave[lll * 3 + 2].tbx1, Wave->wave[lll * 3 + 2].col1step,
	        Wave->wave[lll * 3 + 2].row1step);
	D_TS = (byte4 *) Wave->wave[lll * 3 + 2].data;
	for (j = Wave->wave[lll * 3 + 2].tby0; j < Wave->wave[lll * 3 + 2].tby1;
	     j++, D_TS += Wave->wave[lll * 3 + 2].col1step) {
		D_ = D_TS;
		for (i = Wave->wave[lll * 3 + 2].tbx0; i < Wave->wave[lll * 3 + 2].tbx1;
		     i++, D_ += Wave->wave[lll * 3 + 2].row1step)
			fprintf(fp, "%d,", (*D_));
		fprintf(fp, "\n");
	}
	fprintf(fp, "\n");

	fprintf(fp, "<<wave(HH)>>,%x, lll=%d, tby0=%d, tby1=%d, tbx0=%d, tbx1=%d, col1step=%d, row1step=%d\n",
	        Wave->wave[lll * 3 + 3].data, lll, Wave->wave[lll * 3 + 3].tby0, Wave->wave[lll * 3 + 3].tby1,
	        Wave->wave[lll * 3 + 3].tbx0, Wave->wave[lll * 3 + 3].tbx1, Wave->wave[lll * 3 + 3].col1step,
	        Wave->wave[lll * 3 + 3].row1step);
	D_TS = (byte4 *) Wave->wave[lll * 3 + 3].data;
	for (j = Wave->wave[lll * 3 + 3].tby0; j < Wave->wave[lll * 3 + 3].tby1;
	     j++, D_TS += Wave->wave[lll * 3 + 3].col1step) {
		D_ = D_TS;
		for (i = Wave->wave[lll * 3 + 3].tbx0; i < Wave->wave[lll * 3 + 3].tbx1;
		     i++, D_ += Wave->wave[lll * 3 + 3].row1step)
			fprintf(fp, "%d,", (*D_));
		fprintf(fp, "\n");
	}
	fprintf(fp, "\n");
	fclose(fp);
}
#endif